var Iza = new Artyom();

	  var configArtyom = {
			 lang:'pt-BR',
		     continuous: true,
		     debug: true,
		     listen: true,
		     speed: 1
			};

	  Iza.initialize(configArtyom);

  d = new Date();
    hour = d.getHours();
   /*
    if(hour < 5)
    {
       Iza.say("Boa Noite, Seja bem vindo ao Sistema de Gestão Patrimonial",{
   		onStart: ()=>{
 			Iza.fatality();
   		   },
   		onEnd: ()=> {
   			Iza.initialize(configArtyom);
   		   }
   		});
    }
    else
    if(hour < 8)
    {
    	 Iza.say("Bom dia, Seja bem vindo ao Sistema de Gestão Patrimonial",{
    	   		onStart: ()=>{
   	   			Iza.fatality();
    	   		   },
   	   		onEnd: ()=> {
    	   			Iza.initialize(configArtyom);
   	   		   }
    	 });
    }
    else
    if(hour < 12)
    {
	 Iza.say("Bom dia, Seja bem vindo ao Sistema de Gestão Patrimonial",{
	   		onStart: ()=>{
	   			Iza.fatality();
	   		   },
	   		onEnd: ()=> {
	   			Iza.initialize(configArtyom);
	   		   }
	   });
    }
    else
    if(hour < 18)
    {
	 Iza.say("Boa tarde, Seja bem vindo ao Sistema de Gestão Patrimonial",{
	   		onStart: ()=>{
	   			Iza.fatality();
	   		   },
	   		onEnd: ()=> {
	   			Iza.initialize(configArtyom);
	   		   }
	   	});
    }
    else
    {
	 Iza.say("Boa Noite, Seja bem vindo ao Sistema de Gestão Patrimonial",{
	   		onStart: ()=>{
	   			Iza.fatality();
	   		   },
	   		onEnd: ()=> {
	   			Iza.initialize(configArtyom);
	   		   }
	   	});
    }
    */
     
    
    var commandosSaudacoes = {
        indexes:["Olá","bom dia","boa tarde","boa noite"],
        action: (i)=> { 
          if (i==0){
            Iza.say("Olá tudo bem? em que posso ajudá-lo?",{
				onStart: ()=>{
					Iza.fatality();
				   },
				onEnd: ()=> {
					Iza.initialize(configArtyom);
				   }
				});
           }else if (i==1){
        	   Iza.say("Bom dia! em que posso ajudá-lo?",{
				onStart: ()=>{
					Iza.fatality();
				   },
				onEnd: ()=> {
					Iza.initialize(configArtyom);
				   }
				});
           }else if (i==2){
        	   Iza.say("Boa tarde! em que posso ajudá-lo?",{
            	   
				onStart: ()=>{
					Iza.fatality();
				   },
				onEnd: ()=> {
					Iza.initialize(configArtyom);
				   }
				});
           }else if (i==3){
        	   Iza.say("Boa noite! em que posso ajudá-lo?",{
            	   
   				onStart: ()=>{
   					Iza.fatality();
   				   },
   				onEnd: ()=> {
   					Iza.initialize(configArtyom);
   				   }
   				});
              }
        }
    };


    var commandoHoraAtual = {
            indexes:["Que horas são?","que horas","horas" ], 
            action: ()=> { 
                data  = new Date();
                horas = data.getHours();
                minutos = data.getMinutes();
                
                Iza.say("São " + horas+ " e "+ minutos+ " minutos",{
             	   
    				onStart: ()=>{
    					Iza.fatality();
    				   },
    				onEnd: ()=> {
    					Iza.initialize(configArtyom);
    				   }
    				});
            }
     };


    var commandoDataAtual = {
            indexes:["Que dia é hoje" ], 
            action: ()=> { 
            	var data = new Date();
            	var dias = new Array('domingo','segunda','terça','quarta','quinta','sexta','sábado');
            	var meses = new Array ('janeiro', 'fevereiro', 'março', 'abril', 'Maio', 'junho', 'agosto', 'outubro', 'novembro', 'dezembro');

                // Iza.say("Hoje é " + dias[data.getDay()]);
                Iza.say("Hoje é " + dias[data.getDay()] + ", " + data.getDate () + " de " + meses[data.getMonth()]   +  " de "  +     data.getFullYear(),{
             	   
    				onStart: ()=>{
    					Iza.fatality();
    				   },
    				onEnd: ()=> {
    					Iza.initialize(configArtyom);
    				   }
    				});
            }
     };
    
    
    var commandoPaginas = {
            indexes:["Abrir *"], 
            smart: true,
            action: (i, str)=> { 

                if ((str=="facebook") || (str=="Facebook")){
                	Iza.say("Abrindo  Facebook");
                	window.open("https://www.facebook.com/",'_blank');	
                 }else if ((str=="folha de boa vista") || (str=="Folha de Boa Vista") || (str=="folha boa vista") || (str=="Folha Boa Vista")){
                	Iza.say("Abrindo  Folha de Boa Vista");
                	window.open("http://www.folhabv.com.br/",'_blank');	
            }
        }
     };
    
    var commandoQuemSouEu = {
            indexes:["Quem é","Quem é você","Quem é tu", "Como se chama","Qual o seu nome"], 
            action: ()=> { 
                data  = new Date();
                horas = data.getHours();
                minutos = data.getMinutes();
                
                Iza.sayRandom(["Meu nome é Iza, sou sua assistente patrimonial",
                	"Você pode me chamar de IZA!",
                	"Oi meu nome é Iza, em que posso ajudar?"],{
    				onStart: ()=>{
    					Iza.fatality();
    				   },
    				onEnd: ()=> {
    					Iza.initialize(configArtyom);
    				   }
    				});
            }
     };
    
    var commandoInventario = {
            indexes:["o que é inventário","O que é inventário","o que é Inventário","u que é inventário", "U que é inventário","u que é Inventário"], 
            action: ()=> { 
                Iza.say("Inventário, é o Levantamento e identificação dos bens e locais, visando comprovação de existência física, além da integridade das informações contábeis do usuário responsável.",{
    				onStart: ()=>{
    					Iza.fatality();
    				   },
    				onEnd: ()=> {
    					Iza.initialize(configArtyom);
    				   }
    				});
            }
     };
    
    var commandoAlienacao = {
            indexes:["o que é alienação","O que é alienação","o que é Alienação","u que é alienação", "U que é alienação","u que é alienação"], 
            action: ()=> { 
                Iza.say("Alienação, é o procedimento de transferência da posse e propriedade de um bem, por intermédio de venda, doação ou permuta. NOTA: A alienação de bens da Administração Pública obedece às disposições contidas no artigo 17, Inciso II da Lei Federal 8.666/93.",{
    				onStart: ()=>{
    					Iza.fatality();
    				   },
    				onEnd: ()=> {
    					Iza.initialize(configArtyom);
    				   }
    				});
            }
     };
    
    var commandoBaixaPatrimonial = {
            indexes:["o que é baixa patrimonial","O que é baixa patrimonial","o que é Baixa patrimonial"], 
            action: ()=> { 
                Iza.say("Baixa Patrimonial, é procedimento de exclusão de bens do Ativo Permanente da Unidade Administrativa.",{
    				onStart: ()=>{
    					Iza.fatality();
    				   },
    				onEnd: ()=> {
    					Iza.initialize(configArtyom);
    				   }
    				});
            }
     };
    
    var commandoUnidadeAdministrativa = {
            indexes:["o que é unidade administrativa",
            			"o que é Unidade Administrativa",
            			"o que é Unidade administrariva", 
            			"o que é unidade Administrativa",
            			"O que é unidade administrativa",
            			"O que é Unidade Administrativa",
            			"O que é Unidade administrariva", 
            			"O que é unidade Administrativa",
            			
            			"O que faz uma unidade administrativa",
            			"O que faz uma Unidade Administrativa",
            			"O que faz uma Unidade administrariva", 
            			"O que faz uma unidade Administrativa",
            			
            			"O que faz a unidade administrativa",
            			"O que faz a Unidade Administrativa",
            			"O que faz a Unidade administrariva", 
            			"O que faz a unidade Administrativa"
            			], 
            action: ()=> { 
                Iza.say("Unidade Orçamentária é qualquer unidade, autônoma ou não, que possuam sob sua responsabilidade uma parcela do Orçamento do Governo. A Unidade Administrativa é aquela que possui três características: pessoal, patrimônio e competências próprias. Não é critério necessário ter orçamento para se dizer que uma unidade é administrativa.",{
    				onStart: ()=>{
    					Iza.fatality();
    				   },
    				onEnd: ()=> {
    					Iza.initialize(configArtyom);
    				   }
    				});
            }
     };
    
    
    
    
 function triggerFirstPrompt(){  
    Iza.newPrompt({
        question:"Quantos centimetros devo cortar?",
        // We set the smart property to true to accept wildcards
        smart:true,
        options:["Cortar * cm","Remover * cm"],
        beforePrompt: () => {
            console.log("Before ask");
        },
        onStartPrompt:  () => {
            console.log("The prompt is being executed");
        },
        onEndPrompt: () => {
            console.log("The prompt has been executed succesfully");
        },
        onMatch: (i,wildcard) => {// i returns the index of the given options
            var action;

            var totalCentimeters = parseInt(wildcard);

            action = () => {
                alert(wildcard + " os centimetros serão removidos do seu sanduíche!");
            };

            // A function needs to be returned in onMatch event
            // in order to accomplish what you want to execute
            return action;                       
        }
    });
 }  
 
 
 var commandoOpcoes = {
         indexes:["Cortar","cortar"], 
         action: ()=> { 
        	 triggerFirstPrompt();
         }
  };

 let questaoUm = "Qual o número do item do cardápio?"; 
 let questaoDois = "Qual a carne que prefere?";
 
 function triggerPedido(){  
	    Iza.newPrompt({
	        question: questaoUm,
	    	onStart: ()=>{
	    			Iza.fatality();
	    		   },
	    		onEnd: ()=> {
	    			Iza.initialize(configArtyom);
	    		   },
	        smart:true,
	        options:["pizza número *","Pizza número *","Pizza Número *","número *", "Número *"],
	        onMatch: (i,wildcard) => {
	            var action;

	            action = () => {
	            	//alert(wildcard);
	                if ((wildcard=="1") || (wildcard=="um") || (wildcard=="dois") ){
	                	 Iza.say("Conferindo: Pizza 1 ALEMÃ, com champignon, aliche, catupiry e rebolas",{
		          				onStart: ()=>{
		        					Iza.fatality();
		        				   },
		        				onEnd: ()=> {
		        					Iza.initialize(configArtyom);
		        				   }
		        				});
	                	triggeClassicTipoCarne();
	                }else if ((wildcard=="Dallas") || (wildcard=="dallas") || (wildcard=="DALLAS") || (wildcard=="Dalas") || (wildcard=="dalas") || (wildcard=="DALAS")){
	                	triggeDallasTipoCarne();
	                    }  
	            };

	            return action;                       
	        }
	    });
	 } 
	 
	 
	 function triggeClassicTipoCarne(){  
		    Iza.newPrompt({
		        question: questaoDois,
		        onStart: ()=>{
	    			Iza.fatality();
	    		   },
	    		onEnd: ()=> {
	    			Iza.initialize(configArtyom);
	    		   },
		        smart:true,
		        options:["carne *", "Carne *"],
		        onMatch: (i,str) => {
		            var action;

		            action = () => {
		            	//alert(str);
		            	  if ((str=="Picanha") || (str=="picanha")){
		            		  Iza.say("Conferindo: Hambúguer Classic com carne de Picanha",{
		          				onStart: ()=>{
		        					Iza.fatality();
		        				   },
		        				onEnd: ()=> {
		        					Iza.initialize(configArtyom);
		        				   }
		        				});
		            	  }else  if ((str=="Alcatara") || (str=="alcatara")){
		            		  Iza.say("Conferindo: Hambúguer Classic com carne de Alcatara",{
		          				onStart: ()=>{
		        					Iza.fatality();
		        				   },
		        				onEnd: ()=> {
		        					Iza.initialize(configArtyom);
		        				   }
		        				});
		            	   }else  if ((str=="Maminha") || (str=="maminha")){
		            		   Iza.say("Conferindo: Hambúguer Classic com carne de Maminha",{
		           				onStart: ()=>{
		        					Iza.fatality();
		        				   },
		        				onEnd: ()=> {
		        					Iza.initialize(configArtyom);
		        				   }
		        				});
		              }
		            };
		            return action;                       
		        }
		    });
		 }  
	 
	 function triggeDallasTipoCarne(){  
		    Iza.newPrompt({
		        question: questaoDois,
		        onStart: ()=>{
	    			Iza.fatality();
	    		   },
	    		onEnd: ()=> {
	    			Iza.initialize(configArtyom);
	    		   },
		        smart:true,
		        options:["carne *", "Carne *"],
		        onMatch: (i,str) => {
		            var action;

		            action = () => {
		            	//alert(str);
		            	  if ((str=="Picanha") || (str=="picanha")){
		            		  Iza.say("Conferindo: Hambúguer Dallas com carne de Picanha",{
		          				onStart: ()=>{
		        					Iza.fatality();
		        				   },
		        				onEnd: ()=> {
		        					Iza.initialize(configArtyom);
		        				   }
		        				});
		            	  }else  if ((str=="Alcatara") || (str=="alcatara")){
		            		  Iza.say("Conferindo: Hambúguer Dallas com carne de Alcatara",{
		          				onStart: ()=>{
		        					Iza.fatality();
		        				   },
		        				onEnd: ()=> {
		        					Iza.initialize(configArtyom);
		        				   }
		        				});
		            	   }else  if ((str=="Maminha") || (str=="maminha")){
		            		   Iza.say("Conferindo: Hambúguer Dallas com carne de Maminha",{
		           				onStart: ()=>{
		        					Iza.fatality();
		        				   },
		        				onEnd: ()=> {
		        					Iza.initialize(configArtyom);
		        				   }
		        				});
		              }
		            };
		            return action;                       
		        }
		    });
		 }
 
	 
	 var commandoOpcoesPedidoInicial = {
	         indexes:["Fazer pedido","quero fazer um pedido"], 
	         action: ()=> { 
	        	 triggerPedido();
	         }
	  };
        
    //Iza.addCommands(commandosSaudacoes); 
    Iza.addCommands(commandoHoraAtual); 
    Iza.addCommands(commandoDataAtual); 
    Iza.addCommands(commandoPaginas); 
    Iza.addCommands(commandoQuemSouEu); 
    Iza.addCommands(commandoInventario); 
    Iza.addCommands(commandoAlienacao); 
    Iza.addCommands(commandoBaixaPatrimonial); 
    Iza.addComands(commandoUnidadeAdministrativa);
    
    
    
    
    //Iza.addCommands(commandoOpcoes); 
    //Iza.addCommands(commandoOpcoesPedidoInicial); 
    
