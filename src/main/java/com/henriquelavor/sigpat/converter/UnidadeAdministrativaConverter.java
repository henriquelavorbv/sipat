package com.henriquelavor.sigpat.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.henriquelavor.sigpat.model.UnidadeAdministrativa;
import com.henriquelavor.sigpat.repository.UnidadesAdministrativas;

@FacesConverter(forClass = UnidadeAdministrativa.class)
public class UnidadeAdministrativaConverter implements Converter {
	
	@Inject // funciona graças ao OmniFaces
	private UnidadesAdministrativas unidadesAdministrativas; //repository
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		UnidadeAdministrativa retorno = null;
		
		if (value != null && !"".equals(value)){
				retorno = this.unidadesAdministrativas.porId(new Long(value));
				
			}
			return retorno;
		}

		
	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			UnidadeAdministrativa unidadeAdministrativa = ((UnidadeAdministrativa) value);
			return unidadeAdministrativa.getCodigoUnidadeAdministrativa() == null ? null : unidadeAdministrativa.getCodigoUnidadeAdministrativa().toString();
		}
		return null;
	}	
}