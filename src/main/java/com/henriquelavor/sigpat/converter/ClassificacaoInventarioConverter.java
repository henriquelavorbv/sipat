package com.henriquelavor.sigpat.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.henriquelavor.sigpat.model.ClassificacaoInventario;
import com.henriquelavor.sigpat.repository.ClassificacoesInventarios;

/**
 * @author Henrique Lavor
 * @data 20.08.2016
 * @version 1.0
 */

@FacesConverter(forClass = ClassificacaoInventario.class)
public class ClassificacaoInventarioConverter implements Converter {
	
	@Inject // funciona graças ao OmniFaces
	private ClassificacoesInventarios classificacoesInventarios; //repository
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		ClassificacaoInventario retorno = null;
		
		if (value != null && !"".equals(value)){
				retorno = this.classificacoesInventarios.porId(new Long(value));
			}
			return retorno;
		}

		
	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			ClassificacaoInventario classificacaoInventario = ((ClassificacaoInventario) value);
			return classificacaoInventario.getCodigoClassificacaoInventario() == null ? null 
					: classificacaoInventario.getCodigoClassificacaoInventario().toString();
		}
		return null;
	}	
}