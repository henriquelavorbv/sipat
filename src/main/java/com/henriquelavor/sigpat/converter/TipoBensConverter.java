package com.henriquelavor.sigpat.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.henriquelavor.sigpat.model.TipoBens;
import com.henriquelavor.sigpat.repository.TiposBens;

@FacesConverter(forClass = TipoBens.class)
public class TipoBensConverter implements Converter {
	
	@Inject // funciona graças ao OmniFaces
	private TiposBens tiposBens; //repository
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		TipoBens retorno = null;
		
		if (value != null && !"".equals(value)){
				retorno = this.tiposBens.porId(new Long(value));
				
			}
			return retorno;
		}

		
	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			TipoBens tipoBens = ((TipoBens) value);
			return tipoBens.getCodigoTipoBem() == null ? null 
					: tipoBens.getCodigoTipoBem().toString();
		}
		return null;
	}	
	
}