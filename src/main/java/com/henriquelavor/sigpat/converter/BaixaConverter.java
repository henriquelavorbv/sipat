package com.henriquelavor.sigpat.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.henriquelavor.sigpat.model.Baixa;
import com.henriquelavor.sigpat.repository.Baixas;

/**
 * @author Henrique Lavor
 * @data 26.08.2017
 * @version 1.0
 */

@FacesConverter(forClass = Baixa.class)
public class BaixaConverter implements Converter {
	
	@Inject // funciona graças ao OmniFaces
	private Baixas baixas; //repository
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Baixa retorno = null;
		
		if (value != null && !"".equals(value)){
				retorno = this.baixas.porId(new Long(value));
			}
			return retorno;
		}

		
	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			Baixa baixa = ((Baixa) value);
			return baixa.getCodigoBaixa() == null ? null : baixa.getCodigoBaixa().toString();
		}
		return null;
	}	
}