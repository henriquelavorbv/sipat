package com.henriquelavor.sigpat.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.henriquelavor.sigpat.model.TermoResponsavel;
import com.henriquelavor.sigpat.repository.TermosResponsaveis;

/**
 * @author Henrique Lavor
 * @data 02.12.2016
 * @version 1.0
 */

@FacesConverter(forClass = TermoResponsavel.class)
public class TermoResponsavelConverter implements Converter {
	
	@Inject // funciona graças ao OmniFaces
	private TermosResponsaveis termosResponsaveis; //repository
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		TermoResponsavel retorno = null;
		
		if (value != null && !"".equals(value)){
				retorno = this.termosResponsaveis.porId(new Long(value));
				
			}
			return retorno;
		}

		
	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			TermoResponsavel termoResponsavel = ((TermoResponsavel) value);
			return termoResponsavel.getCodigoTermoResponsavel() == null ? null : termoResponsavel.getCodigoTermoResponsavel().toString();
		}
		return null;
	}	
}