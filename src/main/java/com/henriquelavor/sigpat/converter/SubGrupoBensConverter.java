package com.henriquelavor.sigpat.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.henriquelavor.sigpat.model.SubGrupoBens;
import com.henriquelavor.sigpat.repository.SubGruposBens;

@FacesConverter(forClass = SubGrupoBens.class)
public class SubGrupoBensConverter implements Converter {
	
	@Inject // funciona graças ao OmniFaces
	private SubGruposBens subGruposBens; //repository
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		SubGrupoBens retorno = null;
		
		if (value != null && !"".equals(value)){
				retorno = this.subGruposBens.porId(new Long(value));
				
			}
			return retorno;
		}

		
	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			SubGrupoBens subGrupoBens = ((SubGrupoBens) value);
			return subGrupoBens.getCodigoSubGrupoBem() == null ? null : subGrupoBens.getCodigoSubGrupoBem().toString();
		}
		return null;
	}	
	
	
}