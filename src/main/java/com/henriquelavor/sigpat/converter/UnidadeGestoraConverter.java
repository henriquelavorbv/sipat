package com.henriquelavor.sigpat.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.henriquelavor.sigpat.model.UnidadeGestora;
import com.henriquelavor.sigpat.repository.UnidadesGestoras;

@FacesConverter(forClass = UnidadeGestora.class)
public class UnidadeGestoraConverter implements Converter {
	
	@Inject // funciona graças ao OmniFaces
	private UnidadesGestoras unidadesGestoras; //repository
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		UnidadeGestora retorno = null;
		
		if (value != null && !"".equals(value)){
				retorno = this.unidadesGestoras.porId(new Long(value));
			}
			return retorno;
		}

		
	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			UnidadeGestora unidadeGestora = ((UnidadeGestora) value);
			return unidadeGestora.getCodigoUnidadeGestora() == null ? null 
					: unidadeGestora.getCodigoUnidadeGestora().toString();
		}
		return null;
	}	
}