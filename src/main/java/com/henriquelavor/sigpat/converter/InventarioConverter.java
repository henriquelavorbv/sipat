package com.henriquelavor.sigpat.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.henriquelavor.sigpat.model.Inventario;
import com.henriquelavor.sigpat.repository.Inventarios;

@FacesConverter(forClass = Inventario.class)
public class InventarioConverter implements Converter {
	
	@Inject // funciona graças ao OmniFaces
	private Inventarios inventarios; //repository
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Inventario retorno = null;
		
		if (value != null && !"".equals(value)){
				retorno = this.inventarios.porId(new Long(value));
				
			}
			return retorno;
		}

		
	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			Inventario inventario = ((Inventario) value);
			return inventario.getCodigoInventario() == null ? null : inventario.getCodigoInventario().toString();
		}
		return null;
	}	
}