package com.henriquelavor.sigpat.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.henriquelavor.sigpat.model.DepreciacaoView;
import com.henriquelavor.sigpat.repository.DepreciacoesView;

/**
 * @author Henrique Lavor
 * @data 07.09.2019
 * @version 1.0
 */

@FacesConverter(forClass = DepreciacaoView.class)
public class DepreciacaoViewConverter implements Converter {
	
	@Inject // funciona graças ao OmniFaces
	private DepreciacoesView depreciacoesView; //repository
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		DepreciacaoView retorno = null;
		
		if (value != null && !"".equals(value)){
				retorno = this.depreciacoesView.porId(new Long(value));
			}
			return retorno;
		}

		
	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			DepreciacaoView depreciacaoView = ((DepreciacaoView) value);
			return depreciacaoView.getCodigoBemMovel() == null ? null 
					: depreciacaoView.getCodigoBemMovel().toString();
		}
		return null;
	}	
}