package com.henriquelavor.sigpat.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.henriquelavor.sigpat.model.Reparo;
import com.henriquelavor.sigpat.repository.Reparos;

/**
 * @author Henrique Lavor
 * @data 31.08.2017
 * @version 1.0
 */

@FacesConverter(forClass = Reparo.class)
public class ReparoConverter implements Converter {
	
	@Inject // funciona graças ao OmniFaces
	private Reparos reparos; //repository
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Reparo retorno = null;
		
		if (value != null && !"".equals(value)){
				retorno = this.reparos.porId(new Long(value));
				
			}
			return retorno;
		}

		
	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			Reparo reparo = ((Reparo) value);
			return reparo.getCodigoReparo() == null ? null : reparo.getCodigoReparo().toString();
		}
		return null;
	}	
}