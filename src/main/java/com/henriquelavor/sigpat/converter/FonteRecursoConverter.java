package com.henriquelavor.sigpat.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.henriquelavor.sigpat.model.FonteRecurso;
import com.henriquelavor.sigpat.repository.FontesRecursos;

/**
 * @author Henrique Lavor
 * @data 16.01.2017
 * @version 1.0
 */

@FacesConverter(forClass = FonteRecurso.class)
public class FonteRecursoConverter implements Converter {
	
	@Inject // funciona graças ao OmniFaces
	private FontesRecursos fontesRecursos; //repository
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		FonteRecurso retorno = null;
		
		if (value != null && !"".equals(value)){
				retorno = this.fontesRecursos.porId(new Long(value));
			}
			return retorno;
		}

		
	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			FonteRecurso fonteRecurso = ((FonteRecurso) value);
			return fonteRecurso.getCodigoFonteRecurso() == null ? null 
					: fonteRecurso.getCodigoFonteRecurso().toString();
		}
		return null;
	}	
}