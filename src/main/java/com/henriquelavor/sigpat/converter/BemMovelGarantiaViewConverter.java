package com.henriquelavor.sigpat.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.henriquelavor.sigpat.model.BemMovelGarantiaView;
import com.henriquelavor.sigpat.repository.BensMoveisGarantiasView;

/**
 * @author Henrique Lavor
 * @data 07.09.2019
 * @version 1.0
 */

@FacesConverter(forClass = BemMovelGarantiaView.class)
public class BemMovelGarantiaViewConverter implements Converter {
	
	@Inject // funciona graças ao OmniFaces
	private BensMoveisGarantiasView bensMoveisGarantiasView; //repository
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		BemMovelGarantiaView retorno = null;
		
		if (value != null && !"".equals(value)){
				retorno = this.bensMoveisGarantiasView.porId(new Long(value));
			}
			return retorno;
		}

		
	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			BemMovelGarantiaView bemMovelGarantiaView = ((BemMovelGarantiaView) value);
			return bemMovelGarantiaView.getCodigoBemMovel() == null ? null 
					: bemMovelGarantiaView.getCodigoBemMovel().toString();
		}
		return null;
	}	
}