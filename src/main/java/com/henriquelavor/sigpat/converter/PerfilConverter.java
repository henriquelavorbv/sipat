package com.henriquelavor.sigpat.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.henriquelavor.sigpat.model.Cargo;
import com.henriquelavor.sigpat.model.Perfil;
import com.henriquelavor.sigpat.repository.Perfis;

@FacesConverter(forClass = Perfil.class)
public class PerfilConverter implements Converter {
	
	@Inject // funciona graças ao OmniFaces
	private Perfis perfis; //repository
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Perfil retorno = null;
		
		if (value != null && !"".equals(value)){
				retorno = this.perfis.porId(new Long(value));
			}
			return retorno;
		}

		
	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			Perfil perfil = ((Perfil) value);
			return perfil.getCodigoPerfil() == null ? null 
					: perfil.getCodigoPerfil().toString();
		}
		return null;
	}	
}