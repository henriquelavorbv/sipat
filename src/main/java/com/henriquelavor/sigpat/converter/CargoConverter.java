package com.henriquelavor.sigpat.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.henriquelavor.sigpat.model.Cargo;
import com.henriquelavor.sigpat.repository.Cargos;

@FacesConverter(forClass = Cargo.class)
public class CargoConverter implements Converter {
	
	@Inject // funciona graças ao OmniFaces
	private Cargos cargos; //repository
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Cargo retorno = null;
		
		if (value != null && !"".equals(value)){
				retorno = this.cargos.porId(new Long(value));
			}
			return retorno;
		}

		
	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			Cargo cargo = ((Cargo) value);
			return cargo.getCodigoCargo() == null ? null 
					: cargo.getCodigoCargo().toString();
		}
		return null;
	}	
}