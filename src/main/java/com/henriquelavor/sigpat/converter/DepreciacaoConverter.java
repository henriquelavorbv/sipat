package com.henriquelavor.sigpat.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.henriquelavor.sigpat.model.Baixa;
import com.henriquelavor.sigpat.model.Depreciacao;
import com.henriquelavor.sigpat.repository.Baixas;
import com.henriquelavor.sigpat.repository.Depreciacoes;

/**
 * @author Henrique Lavor
 * @data 19.05.2019
 * @version 1.0
 */

@FacesConverter(forClass = Depreciacao.class)
public class DepreciacaoConverter implements Converter {
	
	@Inject // funciona graças ao OmniFaces
	private Depreciacoes depreciacoes; //repository
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Depreciacao retorno = null;
		
		if (value != null && !"".equals(value)){
				retorno = this.depreciacoes.porId(new Long(value));
			}
			return retorno;
		}

		
	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			Depreciacao depreciacao = ((Depreciacao) value);
			return depreciacao.getCodigoDepreciacao() == null ? null : depreciacao.getCodigoDepreciacao().toString();
		}
		return null;
	}	
}