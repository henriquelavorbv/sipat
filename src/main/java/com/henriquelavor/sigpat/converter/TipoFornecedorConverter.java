package com.henriquelavor.sigpat.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.henriquelavor.sigpat.model.TipoFornecedor;
import com.henriquelavor.sigpat.repository.TiposFornecedores;

@FacesConverter(forClass = TipoFornecedor.class)
public class TipoFornecedorConverter implements Converter {
	
	@Inject // funciona graças ao OmniFaces
	private TiposFornecedores tiposFornecedores; //repository
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		TipoFornecedor retorno = null;
		
		if (value != null && !"".equals(value)){
				retorno = this.tiposFornecedores.porId(new Long(value));
			}
			return retorno;
		}

		
	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			TipoFornecedor tipoFornecedor = ((TipoFornecedor) value);
			return tipoFornecedor.getCodigoTipoFornecedor() == null ? null 
					: tipoFornecedor.getCodigoTipoFornecedor().toString();
		}
		return null;
	}	
}