package com.henriquelavor.sigpat.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.henriquelavor.sigpat.model.Divergencia;
import com.henriquelavor.sigpat.repository.Divergencias;

/**
 * @author Henrique Lavor
 * @data 25.09.2017
 * @version 1.0
 */

@FacesConverter(forClass = Divergencia.class)
public class DivergenciaConverter implements Converter {
	
	@Inject // funciona graças ao OmniFaces
	private Divergencias divergencias; //repository
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Divergencia retorno = null;
		
		if (value != null && !"".equals(value)){
				retorno = this.divergencias.porId(new Long(value));
			}
			return retorno;
		}

		
	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			Divergencia divergencia = ((Divergencia) value);
			return divergencia.getCodigoDivergencia() == null ? null 
					: divergencia.getCodigoDivergencia().toString();
		}
		return null;
	}	
}