package com.henriquelavor.sigpat.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.henriquelavor.sigpat.model.Transferencia;
import com.henriquelavor.sigpat.repository.Transferencias;

/**
 * @author Henrique Lavor
 * @data 15.08.2017
 * @version 1.0
 */

@FacesConverter(forClass = Transferencia.class)
public class TransferenciaConverter implements Converter {
	
	@Inject // funciona graças ao OmniFaces
	private Transferencias transferencias; //repository
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Transferencia retorno = null;
		
		if (value != null && !"".equals(value)){
				retorno = this.transferencias.porId(new Long(value));
				
			}
			return retorno;
		}

		
	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			Transferencia transferencia = ((Transferencia) value);
			return transferencia.getCodigoTransferencia() == null ? null : transferencia.getCodigoTransferencia().toString();
		}
		return null;
	}	
}