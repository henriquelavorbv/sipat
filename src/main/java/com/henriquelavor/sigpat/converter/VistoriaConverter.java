package com.henriquelavor.sigpat.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.henriquelavor.sigpat.model.Vistoria;
import com.henriquelavor.sigpat.repository.Vistorias;

/**
 * @author Henrique Lavor
 * @data 26.09.2017
 * @version 1.0
 */

@FacesConverter(forClass = Vistoria.class)
public class VistoriaConverter implements Converter {
	
	@Inject // funciona graças ao OmniFaces
	private Vistorias vistorias; //repository
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Vistoria retorno = null;
		
		if (value != null && !"".equals(value)){
				retorno = this.vistorias.porId(new Long(value));
			}
			return retorno;
		}

		
	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			Vistoria vistoria = ((Vistoria) value);
			return vistoria.getCodigoVistoria() == null ? null : vistoria.getCodigoVistoria().toString();
		}
		return null;
	}	
}