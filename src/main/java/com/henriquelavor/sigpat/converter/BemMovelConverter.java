package com.henriquelavor.sigpat.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.henriquelavor.sigpat.model.BemMovel;
import com.henriquelavor.sigpat.repository.BensMoveis;

@FacesConverter(forClass = BemMovel.class)
public class BemMovelConverter implements Converter {
	
	@Inject // funciona graças ao OmniFaces
	private BensMoveis bensMoveis; //repository
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		BemMovel retorno = null;
		
		if (value != null && !"".equals(value)){
				retorno = this.bensMoveis.porId(new Long(value));
				
			}
			return retorno;
		}

		
	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			BemMovel bemMovel = ((BemMovel) value);
			return bemMovel.getCodigoBemMovel() == null ? null : bemMovel.getCodigoBemMovel().toString();
		}
		return null;
	}	
}