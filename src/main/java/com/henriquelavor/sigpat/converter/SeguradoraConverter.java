package com.henriquelavor.sigpat.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.henriquelavor.sigpat.model.Seguradora;
import com.henriquelavor.sigpat.repository.Seguradoras;

/**
 * @author Henrique Lavor
 * @data 10.08.2017
 * @version 1.0
 */

@FacesConverter(forClass = Seguradora.class)
public class SeguradoraConverter implements Converter {
	
	@Inject // funciona graças ao OmniFaces
	private Seguradoras seguradoras; //repository
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Seguradora retorno = null;
		
		if (value != null && !"".equals(value)){
				retorno = this.seguradoras.porId(new Long(value));
			}
			return retorno;
		}

		
	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			Seguradora cargo = ((Seguradora) value);
			return cargo.getCodigoSeguradora() == null ? null 
					: cargo.getCodigoSeguradora().toString();
		}
		return null;
	}	
}