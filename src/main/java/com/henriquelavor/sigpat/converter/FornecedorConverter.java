package com.henriquelavor.sigpat.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.henriquelavor.sigpat.model.Fornecedor;
import com.henriquelavor.sigpat.repository.Fornecedores;

@FacesConverter(forClass = Fornecedor.class)
public class FornecedorConverter implements Converter {
	
	@Inject // funciona graças ao OmniFaces
	private Fornecedores fornecedores; //repository
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Fornecedor retorno = null;
		
		if (value != null && !"".equals(value)){
				retorno = this.fornecedores.porId(new Long(value));
				
			}
			return retorno;
		}

		
	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			Fornecedor fornecedor = ((Fornecedor) value);
			return fornecedor.getCodigoFornecedor() == null ? null : fornecedor.getCodigoFornecedor().toString();
		}
		return null;
	}	
}