package com.henriquelavor.sigpat.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.henriquelavor.sigpat.model.Apolice;
import com.henriquelavor.sigpat.repository.Apolices;

/**
 * @author Henrique Lavor
 * @data 10.08.2017
 * @version 1.0
 */

@FacesConverter(forClass = Apolice.class)
public class ApoliceConverter implements Converter {
	
	@Inject // funciona graças ao OmniFaces
	private Apolices apolices; //repository
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Apolice retorno = null;
		
		if (value != null && !"".equals(value)){
				retorno = this.apolices.porId(new Long(value));
				
			}
			return retorno;
		}

		
	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			Apolice apolice = ((Apolice) value);
			return apolice.getCodigoApolice() == null ? null : apolice.getCodigoApolice().toString();
		}
		return null;
	}	
}