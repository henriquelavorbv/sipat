package com.henriquelavor.sigpat.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.henriquelavor.sigpat.model.MarcaBens;
import com.henriquelavor.sigpat.repository.MarcasBens;

@FacesConverter(forClass = MarcaBens.class)
public class MarcaBensConverter implements Converter {
	
	@Inject // funciona graças ao OmniFaces
	private MarcasBens masrcasBens; //repository
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		MarcaBens retorno = null;
		
		if (value != null && !"".equals(value)){
				retorno = this.masrcasBens.porId(new Long(value));
				
			}
			return retorno;
		}

		
	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			MarcaBens marcaBens = ((MarcaBens) value);
			return marcaBens.getCodigoMarcaBem() == null ? null 
					: marcaBens.getCodigoMarcaBem().toString();
		}
		return null;
	}	
	
	
}