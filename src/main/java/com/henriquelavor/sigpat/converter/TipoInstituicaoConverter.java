package com.henriquelavor.sigpat.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.henriquelavor.sigpat.model.TipoInstituicao;
import com.henriquelavor.sigpat.repository.TiposInstituicoes;

@FacesConverter(forClass = TipoInstituicao.class)
public class TipoInstituicaoConverter implements Converter {
	
	@Inject // funciona graças ao OmniFaces
	private TiposInstituicoes tiposInstituicoes; //repository
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		TipoInstituicao retorno = null;
		
		if (value != null && !"".equals(value)){
				retorno = this.tiposInstituicoes.porId(new Long(value));
			}
			return retorno;
		}

		
	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			TipoInstituicao tipoInstituicao = ((TipoInstituicao) value);
			return tipoInstituicao.getCodigoTipoInstituicao() == null ? null 
					: tipoInstituicao.getCodigoTipoInstituicao().toString();
		}
		return null;
	}	
}