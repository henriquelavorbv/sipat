package com.henriquelavor.sigpat.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.henriquelavor.sigpat.model.MotivoBaixa;
import com.henriquelavor.sigpat.repository.MotivosBaixas;

@FacesConverter(forClass = MotivoBaixa.class)
public class MotivoBaixaConverter implements Converter {
	
	@Inject // funciona graças ao OmniFaces
	private MotivosBaixas motivosBaixas; //repository
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		MotivoBaixa retorno = null;
		
		if (value != null && !"".equals(value)){
				retorno = this.motivosBaixas.porId(new Long(value));
			}
			return retorno;
		}

		
	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			MotivoBaixa motivoBaixa = ((MotivoBaixa) value);
			return motivoBaixa.getCodigoMotivoBaixa() == null ? null 
					: motivoBaixa.getCodigoMotivoBaixa().toString();
		}
		return null;
	}	
}