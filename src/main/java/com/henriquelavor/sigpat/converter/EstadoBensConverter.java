package com.henriquelavor.sigpat.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.henriquelavor.sigpat.model.EstadoBens;
import com.henriquelavor.sigpat.repository.EstadosBens;

@FacesConverter(forClass = EstadoBens.class)
public class EstadoBensConverter implements Converter {
	
	@Inject // funciona graças ao OmniFaces
	private EstadosBens estadosBens; //repository
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		EstadoBens retorno = null;
		
		if (value != null && !"".equals(value)){
				retorno = this.estadosBens.porId(new Long(value));
			}
			return retorno;
		}

		
	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			EstadoBens estadoBens = ((EstadoBens) value);
			return estadoBens.getCodigoEstadoBem() == null ? null 
					: estadoBens.getCodigoEstadoBem().toString();
		}
		return null;
	}	
}