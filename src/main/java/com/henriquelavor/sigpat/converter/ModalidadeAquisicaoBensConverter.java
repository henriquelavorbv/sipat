package com.henriquelavor.sigpat.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.henriquelavor.sigpat.model.ModalidadeAquisicaoBens;
import com.henriquelavor.sigpat.repository.ModalidadesAquisicoesBens;

@FacesConverter(forClass = ModalidadeAquisicaoBens.class)
public class ModalidadeAquisicaoBensConverter implements Converter {
	
	@Inject // funciona graças ao OmniFaces
	private ModalidadesAquisicoesBens modalidadesAquisicoesBens; //repository
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		ModalidadeAquisicaoBens retorno = null;
		
		if (value != null && !"".equals(value)){
				retorno = this.modalidadesAquisicoesBens.porId(new Long(value));
			}
			return retorno;
		}

		
	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			ModalidadeAquisicaoBens modalidadeAquisicaoBens = ((ModalidadeAquisicaoBens) value);
			return modalidadeAquisicaoBens.getCodigoModalidadeAquisicaoBem() == null ? null 
					: modalidadeAquisicaoBens.getCodigoModalidadeAquisicaoBem().toString();
		}
		return null;
	}	
}