package com.henriquelavor.sigpat.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.henriquelavor.sigpat.model.TipoTransferencia;
import com.henriquelavor.sigpat.repository.TiposTransferencias;

/**
 * @author Henrique Lavor
 * @data 09.09.2016
 * @version 1.0
 */

@FacesConverter(forClass = TipoTransferencia.class)
public class TipoTransferenciaConverter implements Converter {
	
	@Inject // funciona graças ao OmniFaces
	private TiposTransferencias tiposTransferencias; //repository
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		TipoTransferencia retorno = null;
		
		if (value != null && !"".equals(value)){
				retorno = this.tiposTransferencias.porId(new Long(value));
			}
			return retorno;
		}

		
	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			TipoTransferencia tipoTransferencia = ((TipoTransferencia) value);
			return tipoTransferencia.getCodigoTipoTransferencia() == null ? null 
					: tipoTransferencia.getCodigoTipoTransferencia().toString();
		}
		return null;
	}	
}