package com.henriquelavor.sigpat.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.henriquelavor.sigpat.model.ReferenciaFornecedor;
import com.henriquelavor.sigpat.repository.ReferenciasFornecedores;

@FacesConverter(forClass = ReferenciaFornecedor.class)
public class ReferenciaFornecedorConverter implements Converter {
	
	@Inject // funciona graças ao OmniFaces
	private ReferenciasFornecedores referenciasFornecedores; //repository
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		ReferenciaFornecedor retorno = null;
		
		if (value != null && !"".equals(value)){
				retorno = this.referenciasFornecedores.porId(new Long(value));
			}
			return retorno;
		}

		
	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			ReferenciaFornecedor referenciaFornecedor = ((ReferenciaFornecedor) value);
			return referenciaFornecedor.getCodigoReferenciaFornecedor() == null ? null 
					: referenciaFornecedor.getCodigoReferenciaFornecedor().toString();
		}
		return null;
	}	
}