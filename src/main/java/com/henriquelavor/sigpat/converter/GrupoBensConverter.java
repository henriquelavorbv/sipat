package com.henriquelavor.sigpat.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.henriquelavor.sigpat.model.GrupoBens;
import com.henriquelavor.sigpat.repository.GruposBens;

@FacesConverter(forClass = GrupoBens.class)
public class GrupoBensConverter implements Converter {
	
	@Inject // funciona graças ao OmniFaces
	private GruposBens gruposBens; //repository
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		
		GrupoBens retorno = null;
		
		if (value != null && !"".equals(value)){
				retorno = this.gruposBens.porId(new Long(value));
				
			}
			return retorno;
		}

		
	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			GrupoBens grupoBens = ((GrupoBens) value);
			return grupoBens.getCodigoGrupoBem() == null ? null 
					: grupoBens.getCodigoGrupoBem().toString();
		}
		return null;
	}	
	
	
}