package com.henriquelavor.sigpat.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.henriquelavor.sigpat.model.Usuario;
import com.henriquelavor.sigpat.repository.Usuarios;

@FacesConverter(forClass = Usuario.class)
public class UsuarioConverter implements Converter {
	
	@Inject // funciona graças ao OmniFaces
	private Usuarios usuarios; //repository
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Usuario retorno = null;
		
		if (value != null && !"".equals(value)){
				retorno = this.usuarios.porId(new Long(value));
			}
			return retorno;
		}

		
	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			Usuario usuario = ((Usuario) value);
			return usuario.getCodigoUsuario() == null ? null 
					: usuario.getCodigoUsuario().toString();
		}
		return null;
	}	
}