package com.henriquelavor.sigpat.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.henriquelavor.sigpat.model.Funcionario;
import com.henriquelavor.sigpat.repository.Funcionarios;

@FacesConverter(forClass = Funcionario.class)
public class FuncionarioConverter implements Converter {
	
	@Inject // funciona graças ao OmniFaces
	private Funcionarios funcionarios; //repository
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Funcionario retorno = null;
		
		if (value != null && !"".equals(value)){
				retorno = this.funcionarios.porId(new Long(value));
			}
			return retorno;
		}

		
	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value != null) {
			Funcionario funcionario = ((Funcionario) value);
			return funcionario.getCodigoFuncionario() == null ? null : funcionario.getCodigoFuncionario().toString();
		}
		return null;
	}	
}