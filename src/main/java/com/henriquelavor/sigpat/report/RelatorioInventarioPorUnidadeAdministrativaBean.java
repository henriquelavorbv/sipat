package com.henriquelavor.sigpat.report;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
//import javax.faces.bean.SessionScoped; foi usado porem deixava o valor front-end (view) 
import javax.validation.constraints.NotNull;

import org.primefaces.event.FileUploadEvent;

import com.henriquelavor.sigpat.controller.UploadArquivo;
import com.henriquelavor.sigpat.util.report.AbstractReportBean;

import net.sf.jasperreports.engine.JRParameter;

@ManagedBean(name = "relatorioInventarioPorUnidadeAdministrativaBean")
@RequestScoped
public class RelatorioInventarioPorUnidadeAdministrativaBean extends AbstractReportBean {

	private final String COMPILE_FILE_NAME = "inventario_fisico_ua";

	private Long numeroUnidadeAdministrativa;
	

	@Override
	protected String getCompileFileName() {
		return COMPILE_FILE_NAME;
	}

	@Override
	protected Map<String, Object> getReportParameters() {
		
		Map<String, Object> reportParameters = new HashMap<String, Object>();

		reportParameters.put(JRParameter.REPORT_LOCALE, new Locale("pt", "BR"));
		reportParameters.put("NUMERO_UA", this.numeroUnidadeAdministrativa);
		return reportParameters;
	}

	public String execute() {
		try {

			setExportOption(ExportOption.PDF);
			super.prepareReport(COMPILE_FILE_NAME + numeroUnidadeAdministrativa);

		} catch (Exception e) {
			// make your own exception handling
			e.printStackTrace();
		}

		return null;
	}

	public String executeAuto() {
		try {

			setExportOption(ExportOption.PDF);
			super.prepareReport(COMPILE_FILE_NAME + numeroUnidadeAdministrativa);

		} catch (Exception e) {
			// make your own exception handling
			e.printStackTrace();
		}

		return null;
	}

	@NotNull
	public Long getNumeroUnidadeAdministrativa() {
		return numeroUnidadeAdministrativa;
	}

	public void setNumeroUnidadeAdministrativa(Long numeroUnidadeAdministrativa) {
		this.numeroUnidadeAdministrativa = numeroUnidadeAdministrativa;
	}
}
