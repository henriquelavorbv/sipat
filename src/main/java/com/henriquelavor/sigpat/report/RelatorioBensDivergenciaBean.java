package com.henriquelavor.sigpat.report;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
//import javax.faces.bean.SessionScoped; foi usado porem deixava o valor front-end (view) 
import javax.validation.constraints.NotNull;

import com.henriquelavor.sigpat.util.report.AbstractReportBean;

import net.sf.jasperreports.engine.JRParameter;

@ManagedBean(name = "relatorioBensDivergenciaBean")
@RequestScoped
public class RelatorioBensDivergenciaBean extends AbstractReportBean {

	private final String COMPILE_FILE_NAME = "bens_divergencia";
	
	private Long divergencia;
	
	private String numeroUnidadeGestora;

	

	@Override
	protected String getCompileFileName() {
		return COMPILE_FILE_NAME;
	}

	@Override
	protected Map<String, Object> getReportParameters() {
		Map<String, Object> reportParameters = new HashMap<String, Object>();
		
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
		this.numeroUnidadeGestora = codOrcamentarioSession;

		reportParameters.put(JRParameter.REPORT_LOCALE, new Locale("pt", "BR"));
		reportParameters.put("DIVERGENCIA", this.divergencia);
		reportParameters.put("cod_orgao", this.numeroUnidadeGestora);

		return reportParameters;
	}

	public String execute() {
		try {

			setExportOption(ExportOption.PDF);
			super.prepareReport(COMPILE_FILE_NAME);

		} catch (Exception e) {
			// make your own exception handling
			e.printStackTrace();
		}

		return null;
	}
	
	@NotNull
	public Long getDivergencia() {
		return divergencia;
	}

	public void setDivergencia(Long divergencia) {
		this.divergencia = divergencia;
	}

}
