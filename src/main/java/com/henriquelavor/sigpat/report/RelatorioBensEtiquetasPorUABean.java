package com.henriquelavor.sigpat.report;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
//import javax.faces.bean.SessionScoped; foi usado porem deixava o valor front-end (view) 
import javax.validation.constraints.NotNull;


import com.henriquelavor.sigpat.util.report.AbstractReportBean;

import net.sf.jasperreports.engine.JRParameter;

@ManagedBean(name = "relatorioBensEtiquetasPorUABean")
@RequestScoped
public class RelatorioBensEtiquetasPorUABean extends AbstractReportBean {

	private final String COMPILE_FILE_NAME = "bens_etiquetas_qr_ua";
	

	private Long numeroUnidadeAdministrativa;
	
	

	@Override
	protected String getCompileFileName() {
			return COMPILE_FILE_NAME;
	}

	@Override
	protected Map<String, Object> getReportParameters() {
		Map<String, Object> reportParameters = new HashMap<String, Object>();

		reportParameters.put(JRParameter.REPORT_LOCALE, new Locale("pt", "BR"));
		reportParameters.put("NUMERO_UA", this.numeroUnidadeAdministrativa);
		return reportParameters;
	}

	public String execute() {
		//FacesContext context = FacesContext.getCurrentInstance();
		//se valor da UA for nulo entao gere uma msg de alerta
		//if (this.numeroUnidadeAdministrativa==null) {
		//	context.addMessage(null,
		//			new FacesMessage("Favor selecione a Unidade Administrativa."));
		//	try {
	//			FacesContext.getCurrentInstance().getExternalContext()
//				.redirect("/sigpat/paginas/Relatorios/RelatorioBensEtiquetasPorUA.xhtml");
		//	} catch (IOException e) {
				// TODO Auto-generated catch block
	//			e.printStackTrace();
	//		}
			
	//	}
		
		try {
			setExportOption(ExportOption.PDF);
				super.prepareReport(COMPILE_FILE_NAME + numeroUnidadeAdministrativa);
		} catch (Exception e) {
			// make your own exception handling
			e.printStackTrace();
		}

		return null;
	}

	@NotNull
	public Long getNumeroUnidadeAdministrativa() {
		return numeroUnidadeAdministrativa;
	}

	public void setNumeroUnidadeAdministrativa(Long numeroUnidadeAdministrativa) {
		this.numeroUnidadeAdministrativa = numeroUnidadeAdministrativa;
	}

}
