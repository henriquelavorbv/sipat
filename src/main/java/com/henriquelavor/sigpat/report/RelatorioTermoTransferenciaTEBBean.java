package com.henriquelavor.sigpat.report;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
//import javax.faces.bean.SessionScoped; foi usado porem deixava o valor front-end (view) 
import javax.validation.constraints.NotNull;

import org.primefaces.event.FileUploadEvent;

import com.henriquelavor.sigpat.controller.UploadArquivo;
import com.henriquelavor.sigpat.util.report.AbstractReportBean;

import net.sf.jasperreports.engine.JRParameter;

@ManagedBean(name = "relatorioTermoTransferenciaTEBBean")
@RequestScoped
public class RelatorioTermoTransferenciaTEBBean extends AbstractReportBean {

	private final String COMPILE_FILE_NAME_TEB = "termo_transferencia_teb";
	

	private String numeroTermoTransferencia;
	
	private String numeroTermoTransferenciaAuto;
	
	private String numeroUnidadeGestora;
	
	@Override
	protected String getCompileFileName() {
			 return COMPILE_FILE_NAME_TEB;
	}

	// private TermoResponsavel termoResponsavel = new TermoResponsavel();

	private UploadArquivo arquivo = new UploadArquivo();

	@Override
	protected Map<String, Object> getReportParameters() {
		
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
		
		this.numeroUnidadeGestora = codOrcamentarioSession;
		
		Map<String, Object> reportParameters = new HashMap<String, Object>();

		// Integer codigo = 1989;
		// String numeroTermo = "123321";

		reportParameters.put(JRParameter.REPORT_LOCALE, new Locale("pt", "BR"));
		/*
		if(numeroTermoTransferencia != null && numeroTermoTransferencia.contains("TEB") ){
			reportParameters.put("NUMERO_TERMO", this.numeroTermoTransferencia);
			reportParameters.put("cod_orgao_origem", this.numeroUnidadeGestora);
			reportParameters.put("cod_orgao_destino", this.numeroUnidadeGestora);
		}else if (numeroTermoTransferencia.contains("TEB")){
			reportParameters.put("NUMERO_TERMO", this.numeroTermoTransferenciaAuto);
			reportParameters.put("cod_orgao_origem", this.numeroUnidadeGestora);
			reportParameters.put("cod_orgao_destino", this.numeroUnidadeGestora);
		}
		*/
		if(numeroTermoTransferencia != null){
			reportParameters.put("NUMERO_TERMO", this.numeroTermoTransferencia);
			reportParameters.put("cod_orgao_origem", this.numeroUnidadeGestora);
			reportParameters.put("cod_orgao_destino", this.numeroUnidadeGestora);
		}else {
			reportParameters.put("NUMERO_TERMO", this.numeroTermoTransferenciaAuto);
			reportParameters.put("cod_orgao_origem", this.numeroUnidadeGestora);
			reportParameters.put("cod_orgao_destino", this.numeroUnidadeGestora);
		}
		
		return reportParameters;
	}

	// pdf do relatorio gerado
	public void uploadPdf(FileUploadEvent event) {
		this.arquivo.fileUpload(event, ".pdf", "/resources/enviados/bensmoveis/termosresponsaveis/");
		// this.termoResponsavel.setApolicePdf(this.arquivo.getNome());
		this.arquivo.gravar(); // nova implementacao fileupload
		this.arquivo = new UploadArquivo(); // nova implementacao fileupload
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("Termo enviada com sucesso!"));
	}

	public String execute() {
		try {
			// HttpServletRequest request =
			// (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
			// this.numeroTermo = (String)
			// request.getSession().getAttribute("numeroTermoAtual");
			// System.out.println(this.numeroTermo );

			setExportOption(ExportOption.PDF);
			
				super.prepareReport(COMPILE_FILE_NAME_TEB + numeroTermoTransferencia);
			
			//super.prepareReport(COMPILE_FILE_NAME + numeroTermoTransferencia);
			
			this.arquivo = new UploadArquivo();

			System.out.println("Arquivo relatorio: " + this.arquivo);

			//System.out.println("Arquivo relatorio Compilado: " + COMPILE_FILE_NAME + numeroTermoTransferencia);

		} catch (Exception e) {
			// make your own exception handling
			e.printStackTrace();
		}

		return null;
	}
	
	
	public String executeAuto() {
		try {
			// HttpServletRequest request =
			// (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
			// this.numeroTermo = (String)
			// request.getSession().getAttribute("numeroTermoAtual");
			// System.out.println(this.numeroTermo );

			setExportOption(ExportOption.PDF);
			
				super.prepareReport(COMPILE_FILE_NAME_TEB+ numeroTermoTransferenciaAuto);
			
			//super.prepareReport(COMPILE_FILE_NAME + numeroTermoTransferenciaAuto);
			
			//this.arquivo = new UploadArquivo();

			//System.out.println("Arquivo relatorio: " + this.arquivo);
			//System.out.println("Arquivo relatorio Compilado: " + COMPILE_FILE_NAME + numeroTermoAuto);

		} catch (Exception e) {
			// make your own exception handling
			e.printStackTrace();
		}

		return null;
	}

	@NotNull
	public String getNumeroTermoTransferencia() {
		return numeroTermoTransferencia;
	}

	public void setNumeroTermoTransferencia(String numeroTermoTransferencia) {
		this.numeroTermoTransferencia = numeroTermoTransferencia;
	}

	@NotNull
	public String getNumeroTermoTransferenciaAuto() {
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
				.getRequest();
		return (String) request.getSession().getAttribute("numeroTermoTransferenciaTEBAtual");
	}

	public void setNumeroTermoTransferenciaAuto(String numeroTermoTransferenciaAuto) {
		this.numeroTermoTransferenciaAuto = numeroTermoTransferenciaAuto;
	}
}
