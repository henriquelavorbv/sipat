package com.henriquelavor.sigpat.report;

import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
//import javax.faces.bean.SessionScoped; foi usado porem deixava o valor front-end (view) 

import com.henriquelavor.sigpat.util.report.AbstractReportBean;

import net.sf.jasperreports.engine.JRParameter;

@ManagedBean(name = "relatorioFornecedoresBean")
@RequestScoped
public class RelatorioFornecedoresBean extends AbstractReportBean {

	private final String COMPILE_FILE_NAME = "fornecedores";


	@Override
	protected String getCompileFileName() {
		return COMPILE_FILE_NAME;
	}

	@Override
	protected Map<String, Object> getReportParameters() {
		Map<String, Object> reportParameters = new HashMap<String, Object>();

		reportParameters.put(JRParameter.REPORT_LOCALE, new Locale("pt", "BR"));

		return reportParameters;
	}

	public String execute() {
		try {

			setExportOption(ExportOption.PDF);
			super.prepareReport(COMPILE_FILE_NAME);

		} catch (Exception e) {
			// make your own exception handling
			e.printStackTrace();
		}

		return null;
	}
}
