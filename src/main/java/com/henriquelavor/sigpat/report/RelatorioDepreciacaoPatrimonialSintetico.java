package com.henriquelavor.sigpat.report;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
//import javax.faces.bean.SessionScoped; foi usado porem deixava o valor front-end (view) 
import javax.validation.constraints.NotNull;


import com.henriquelavor.sigpat.util.report.AbstractReportBean;

import net.sf.jasperreports.engine.JRParameter;

@ManagedBean(name = "relatorioDepreciacaoPatrimonialSintetico")
@RequestScoped
public class RelatorioDepreciacaoPatrimonialSintetico extends AbstractReportBean {

	private final String COMPILE_FILE_NAME = "depreciacaoSinteticoPorGrupoBens";
	

	private String numeroUnidadeGestora;
	
	private String periodoInicial;
	private String periodoFim;
	private String ano;
	
	
	@Override
	protected String getCompileFileName() {
			return COMPILE_FILE_NAME;
	}

	@Override
	protected Map<String, Object> getReportParameters() {
		
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
		this.numeroUnidadeGestora = codOrcamentarioSession;
				
		Map<String, Object> reportParameters = new HashMap<String, Object>();

		reportParameters.put(JRParameter.REPORT_LOCALE, new Locale("pt", "BR"));
		reportParameters.put("cod_orgao", this.numeroUnidadeGestora);
		
		reportParameters.put("periodo_ini", this.periodoInicial);
		reportParameters.put("periodo_fim", this.periodoFim);
		reportParameters.put("ano", this.ano);
		
		return reportParameters;
	}

	public String execute() {
		
		try {
			setExportOption(ExportOption.PDF);
				super.prepareReport(COMPILE_FILE_NAME + numeroUnidadeGestora);
		} catch (Exception e) {
			// make your own exception handling
			e.printStackTrace();
		}

		return null;
	}

	@NotNull
	public String getNumeroUnidadeGestora() {
		return numeroUnidadeGestora;
	}

	public void setNumeroUnidadeGestora(String numeroUnidadeGestora) {
		this.numeroUnidadeGestora =  numeroUnidadeGestora;
		
	}

	public String getPeriodoInicial() {
		return periodoInicial;
	}

	public void setPeriodoInicial(String periodoInicial) {
		this.periodoInicial = periodoInicial;
	}

	public String getPeriodoFim() {
		return periodoFim;
	}

	public void setPeriodoFim(String periodoFim) {
		this.periodoFim = periodoFim;
	}

	public String getAno() {
		return ano;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}
	
}
