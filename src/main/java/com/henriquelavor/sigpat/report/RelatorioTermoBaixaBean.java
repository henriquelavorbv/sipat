package com.henriquelavor.sigpat.report;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
//import javax.faces.bean.SessionScoped; foi usado porem deixava o valor front-end (view) 
import javax.validation.constraints.NotNull;


import com.henriquelavor.sigpat.util.report.AbstractReportBean;

import net.sf.jasperreports.engine.JRParameter;

@ManagedBean(name = "relatorioTermoBaixaBean")
@RequestScoped
public class RelatorioTermoBaixaBean extends AbstractReportBean {

	private final String COMPILE_FILE_NAME = "termo_baixa";

	private String numeroTermo;
	
	private String numeroTermoAuto;
	
	private String numeroUnidadeGestora;
	
	@Override
	protected String getCompileFileName() {
		return COMPILE_FILE_NAME;
	}

	// private TermoResponsavel termoResponsavel = new TermoResponsavel();

	//private UploadArquivo arquivo = new UploadArquivo();

	@Override
	protected Map<String, Object> getReportParameters() {
		
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
		this.numeroUnidadeGestora = codOrcamentarioSession;
		
		Map<String, Object> reportParameters = new HashMap<String, Object>();

		// Integer codigo = 1989;
		// String numeroTermo = "123321";

		reportParameters.put(JRParameter.REPORT_LOCALE, new Locale("pt", "BR"));
		
		if(numeroTermo != null){
			reportParameters.put("NUMERO_TERMO", this.numeroTermo);
			reportParameters.put("cod_orgao", this.numeroUnidadeGestora);
		}else{
			reportParameters.put("NUMERO_TERMO", this.numeroTermoAuto);
			reportParameters.put("cod_orgao", this.numeroUnidadeGestora);
		}

		return reportParameters;
	}

/*	// pdf do relatorio gerado
	public void uploadPdf(FileUploadEvent event) {
		//this.arquivo.fileUpload(event, ".pdf", "/resources/enviados/bensmoveis/termosresponsaveis/");
		// this.termoResponsavel.setApolicePdf(this.arquivo.getNome());
		//this.arquivo.gravar(); // nova implementacao fileupload
		//this.arquivo = new UploadArquivo(); // nova implementacao fileupload
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("Termo enviada com sucesso!"));
	}*/

	public String execute() {
		try {
			// HttpServletRequest request =
			// (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
			// this.numeroTermo = (String)
			// request.getSession().getAttribute("numeroTermoAtual");
			// System.out.println(this.numeroTermo );

			setExportOption(ExportOption.PDF);
			super.prepareReport(COMPILE_FILE_NAME + numeroTermo);
			
			//this.arquivo = new UploadArquivo();

			//System.out.println("Arquivo relatorio: " + this.arquivo);

			System.out.println("Arquivo relatorio Compilado: " + COMPILE_FILE_NAME + numeroTermo);

		} catch (Exception e) {
			// make your own exception handling
			e.printStackTrace();
		}

		return null;
	}
	
	
	public String executeAuto() {
		try {
			// HttpServletRequest request =
			// (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
			// this.numeroTermo = (String)
			// request.getSession().getAttribute("numeroTermoAtual");
			// System.out.println(this.numeroTermo );

			setExportOption(ExportOption.PDF);
			super.prepareReport(COMPILE_FILE_NAME + numeroTermoAuto);
			//this.arquivo = new UploadArquivo();

			//System.out.println("Arquivo relatorio: " + this.arquivo);
			//System.out.println("Arquivo relatorio Compilado: " + COMPILE_FILE_NAME + numeroTermoAuto);

		} catch (Exception e) {
			// make your own exception handling
			e.printStackTrace();
		}

		return null;
	}

	@NotNull
	public String getNumeroTermo() {
		return numeroTermo;
	}

	public void setNumeroTermo(String numeroTermo) {
		this.numeroTermo = numeroTermo;
	}

	@NotNull
	public String getNumeroTermoAuto() {
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
				.getRequest();
		return (String) request.getSession().getAttribute("numeroTermoBaixaAtual");
	}

	public void setNumeroTermoAuto(String numeroTermoAuto) {
		this.numeroTermoAuto = numeroTermoAuto;
	}
}
