package com.henriquelavor.sigpat.report;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.henriquelavor.sigpat.util.report.AbstractReportBean;

import net.sf.jasperreports.engine.JRParameter;

@ManagedBean(name = "relatorioInventarioSinteticoTodosGruposBean")
@RequestScoped
public class RelatorioInventarioSinteticoTodosGruposBean extends AbstractReportBean {

	private final String COMPILE_FILE_NAME = "inventario_sintetico_grupos";
	
	private String numeroUnidadeGestora;

	@Override
	protected String getCompileFileName() {
		return COMPILE_FILE_NAME;
	}

	@Override
	protected Map<String, Object> getReportParameters() {
		
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
		this.numeroUnidadeGestora = codOrcamentarioSession;
		
		Map<String, Object> reportParameters = new HashMap<String, Object>();

		// Integer codigo = 1989;
		// String numeroTermo = "123321";

		reportParameters.put(JRParameter.REPORT_LOCALE, new Locale("pt", "BR"));
		reportParameters.put("cod_orgao", this.numeroUnidadeGestora);

		return reportParameters;
	}


	public String execute() {
		try {

			setExportOption(ExportOption.PDF);
			super.prepareReport(COMPILE_FILE_NAME);
			
		} catch (Exception e) {
			// make your own exception handling
			e.printStackTrace();
		}

		return null;
	}
}
