package com.henriquelavor.sigpat.service;

import java.io.Serializable;

import javax.inject.Inject;
import com.henriquelavor.sigpat.util.jpa.Transactional;

import com.henriquelavor.sigpat.model.ReferenciaFornecedor;
import com.henriquelavor.sigpat.repository.ReferenciasFornecedores;

public class CadastroReferenciasFornecedores implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private ReferenciasFornecedores referenciasFornecedores;

	@Transactional
	public void salvar(ReferenciaFornecedor referenciaFornecedor) throws NegocioException {
		ReferenciaFornecedor referenciaFornecedorExistente = referenciasFornecedores.porNome(referenciaFornecedor.getDescricaoReferenciaFornecedor());
		if (referenciaFornecedorExistente != null) {
			throw new NegocioException(
					"Ops! Já existe uma Referência de Fornecedor com o nome informado.");
		}
		this.referenciasFornecedores.guardar(referenciaFornecedor);
	}
	
	@Transactional
	public void editar(ReferenciaFornecedor referenciaFornecedor) throws NegocioException {
		this.referenciasFornecedores.guardar(referenciaFornecedor);
	}
	
	@Transactional
	public void excluir(ReferenciaFornecedor referenciaFornecedor) throws NegocioException {
		referenciaFornecedor = this.referenciasFornecedores.porId(referenciaFornecedor.getCodigoReferenciaFornecedor());
		this.referenciasFornecedores.remover(referenciaFornecedor);
	}
}