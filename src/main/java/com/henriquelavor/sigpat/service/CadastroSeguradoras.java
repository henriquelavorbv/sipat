package com.henriquelavor.sigpat.service;

import java.io.Serializable;

import javax.inject.Inject;
import com.henriquelavor.sigpat.util.jpa.Transactional;

import com.henriquelavor.sigpat.model.Seguradora;
import com.henriquelavor.sigpat.repository.Seguradoras;

/**
 * @author Henrique Lavor
 * @data 10.08.2017
 * @version 1.0
 */

public class CadastroSeguradoras implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private Seguradoras seguradoras;

	@Transactional
	public void salvar(Seguradora seguradora) throws NegocioException {
		Seguradora seguradoraExistente = seguradoras.porNome(seguradora.getDescricaoSeguradora());
		if (seguradoraExistente != null) {
			throw new NegocioException(
					"Ops! Já existe uma Seguradora com o nome informado.");
		}
		this.seguradoras.guardar(seguradora);
	}
	
	@Transactional
	public void editar(Seguradora seguradora) throws NegocioException {
		this.seguradoras.guardar(seguradora);
	}
	
	@Transactional
	public void excluir(Seguradora seguradora) throws NegocioException {
		seguradora = this.seguradoras.porId(seguradora.getCodigoSeguradora());
		this.seguradoras.remover(seguradora);
	}
}