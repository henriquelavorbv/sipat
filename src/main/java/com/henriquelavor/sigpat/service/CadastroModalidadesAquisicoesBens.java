package com.henriquelavor.sigpat.service;

import java.io.Serializable;

import javax.inject.Inject;
import com.henriquelavor.sigpat.util.jpa.Transactional;

import com.henriquelavor.sigpat.model.ModalidadeAquisicaoBens;
import com.henriquelavor.sigpat.repository.ModalidadesAquisicoesBens;

public class CadastroModalidadesAquisicoesBens implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private ModalidadesAquisicoesBens modalidadesAquisicoesBens;

	@Transactional
	public void salvar(ModalidadeAquisicaoBens modalidadeAquisicaoBens) throws NegocioException {
		ModalidadeAquisicaoBens modalidadeAquisicaoBensExistente = modalidadesAquisicoesBens.porNome(modalidadeAquisicaoBens.getDescricaoModalidadeAquisicaoBem());
		if (modalidadeAquisicaoBensExistente != null) {
			throw new NegocioException(
					"Ops! Já existe um Modalidade de Aquisição de Bem com o nome informado.");
		}
		this.modalidadesAquisicoesBens.guardar(modalidadeAquisicaoBens);
	}
	
	@Transactional
	public void editar(ModalidadeAquisicaoBens modalidadeAquisicaoBens) throws NegocioException {
		this.modalidadesAquisicoesBens.guardar(modalidadeAquisicaoBens);
	}
	
	@Transactional
	public void excluir(ModalidadeAquisicaoBens modalidadeAquisicaoBens) throws NegocioException {
		modalidadeAquisicaoBens = this.modalidadesAquisicoesBens.porId(modalidadeAquisicaoBens.getCodigoModalidadeAquisicaoBem());
		this.modalidadesAquisicoesBens.remover(modalidadeAquisicaoBens);
	}
}