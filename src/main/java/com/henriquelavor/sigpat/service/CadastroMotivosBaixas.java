package com.henriquelavor.sigpat.service;

import java.io.Serializable;

import javax.inject.Inject;
import com.henriquelavor.sigpat.util.jpa.Transactional;

import com.henriquelavor.sigpat.model.MotivoBaixa;
import com.henriquelavor.sigpat.repository.MotivosBaixas;

public class CadastroMotivosBaixas implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private MotivosBaixas motivosBaixas;

	@Transactional
	public void salvar(MotivoBaixa motivoBaixa) throws NegocioException {
		MotivoBaixa motivoBaixaExistente = motivosBaixas.porNome(motivoBaixa.getDescricaoMotivoBaixa());
		if (motivoBaixaExistente != null) {
			throw new NegocioException(
					"Ops! Já existe um Motivo de Baixa com o nome informado.");
		}
		this.motivosBaixas.guardar(motivoBaixa);
	}
	
	@Transactional
	public void editar(MotivoBaixa motivoBaixa) throws NegocioException {
		this.motivosBaixas.guardar(motivoBaixa);
	}
	
	@Transactional
	public void excluir(MotivoBaixa motivoBaixa) throws NegocioException {
		motivoBaixa = this.motivosBaixas.porId(motivoBaixa.getCodigoMotivoBaixa());
		this.motivosBaixas.remover(motivoBaixa);
	}
}