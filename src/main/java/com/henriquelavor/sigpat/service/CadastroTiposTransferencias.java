package com.henriquelavor.sigpat.service;

import java.io.Serializable;

import javax.inject.Inject;
import com.henriquelavor.sigpat.util.jpa.Transactional;

import com.henriquelavor.sigpat.model.TipoTransferencia;
import com.henriquelavor.sigpat.repository.TiposTransferencias;

/**
 * @author Henrique Lavor
 * @data 09.09.2016
 * @version 1.0
 */

public class CadastroTiposTransferencias implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private TiposTransferencias tiposTransferencias;

	@Transactional
	public void salvar(TipoTransferencia tipoTransferencia) throws NegocioException {
		TipoTransferencia tipoTransferenciaExistente = tiposTransferencias.porNome(tipoTransferencia.getDescricaoTipoTransferencia());
		if (tipoTransferenciaExistente != null) {
			throw new NegocioException(
					"Ops! Já existe um Tipo de Transferência com o nome informado.");
		}
		this.tiposTransferencias.guardar(tipoTransferencia);
	}
	
	@Transactional
	public void editar(TipoTransferencia tipoTransferencia) throws NegocioException {
		this.tiposTransferencias.guardar(tipoTransferencia);
	}
	
	@Transactional
	public void excluir(TipoTransferencia tipoTransferencia) throws NegocioException {
		tipoTransferencia = this.tiposTransferencias.porId(tipoTransferencia.getCodigoTipoTransferencia());
		this.tiposTransferencias.remover(tipoTransferencia);
	}
}