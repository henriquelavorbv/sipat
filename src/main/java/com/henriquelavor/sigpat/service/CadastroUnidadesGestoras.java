package com.henriquelavor.sigpat.service;

import java.io.Serializable;

import javax.inject.Inject;
import com.henriquelavor.sigpat.util.jpa.Transactional;

import com.henriquelavor.sigpat.model.UnidadeGestora;
import com.henriquelavor.sigpat.repository.UnidadesGestoras;

public class CadastroUnidadesGestoras implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private UnidadesGestoras unidadesGestoras;

	@Transactional
	public void salvar(UnidadeGestora unidadeGestora) throws NegocioException {
		UnidadeGestora unidadeGestoraExistente = unidadesGestoras.porNome(unidadeGestora.getNomeUnidadeGestora());
		if (unidadeGestoraExistente != null) {
			throw new NegocioException(
					"Ops! Já existe uma Unidade Gestora com o nome informado.");
		}
		this.unidadesGestoras.guardar(unidadeGestora);
	}
	
	@Transactional
	public void editar(UnidadeGestora unidadeGestora) throws NegocioException {
		this.unidadesGestoras.guardar(unidadeGestora);
	}
	
	@Transactional
	public void excluir(UnidadeGestora unidadeGestora) throws NegocioException {
		unidadeGestora = this.unidadesGestoras.porId(unidadeGestora.getCodigoUnidadeGestora());
		this.unidadesGestoras.remover(unidadeGestora);
	}
}