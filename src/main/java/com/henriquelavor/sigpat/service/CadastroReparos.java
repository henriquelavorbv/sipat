package com.henriquelavor.sigpat.service;

import java.io.Serializable;

import javax.inject.Inject;
import com.henriquelavor.sigpat.util.jpa.Transactional;

import com.henriquelavor.sigpat.model.Reparo;
import com.henriquelavor.sigpat.repository.Reparos;

/**
 * @author Henrique Lavor
 * @data 31.08.2017
 * @version 1.0
 */

public class CadastroReparos implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private Reparos reparos;

	@Transactional
	public void salvar(Reparo reparo) throws NegocioException {
		//Reparo reparoExistente = reparos.porNumeroBem(reparo.getNumeroApolice(), reparo.getBemMovel());
		
	//	if (reparoExistente != null) {
	//		throw new NegocioException(
	//				"Ops! Já existe uma Apólice para este Bem patrimonial informado.");
	//	}
		this.reparos.guardar(reparo);
	}
	
	@Transactional
	public void editar(Reparo reparo) throws NegocioException {
		this.reparos.guardar(reparo);
	}
	
	@Transactional
	public void excluir(Reparo reparo) throws NegocioException {
		reparo = this.reparos.porId(reparo.getCodigoReparo());
		this.reparos.remover(reparo);
	}
	
}
