package com.henriquelavor.sigpat.service;

import java.io.Serializable;

/**
 * @author Henrique Lavor
 * @data 10.08.2017
 * @version 1.0
 */

import javax.inject.Inject;
import com.henriquelavor.sigpat.util.jpa.Transactional;

import com.henriquelavor.sigpat.model.Apolice;
import com.henriquelavor.sigpat.repository.Apolices;

public class CadastroApolices implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private Apolices apolices;

	@Transactional
	public void salvar(Apolice apolice) throws NegocioException {
		Apolice apoliceExistente = apolices.porNumeroBem(apolice.getNumeroApolice(), apolice.getBemMovel());
		
		if (apoliceExistente != null) {
			throw new NegocioException(
					"Ops! Já existe uma Apólice para este Bem patrimonial informado.");
		}
		this.apolices.guardar(apolice);
	}
	
	@Transactional
	public void editar(Apolice apolice) throws NegocioException {
		this.apolices.guardar(apolice);
	}
	
	@Transactional
	public void excluir(Apolice apolice) throws NegocioException {
		apolice = this.apolices.porId(apolice.getCodigoApolice());
		this.apolices.remover(apolice);
	}
	
	
}
