package com.henriquelavor.sigpat.service;

import java.io.Serializable;

import javax.inject.Inject;
import com.henriquelavor.sigpat.util.jpa.Transactional;

import com.henriquelavor.sigpat.model.UnidadeAdministrativa;
import com.henriquelavor.sigpat.repository.UnidadesAdministrativas;

public class CadastroUnidadesAdministrativas implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private UnidadesAdministrativas unidadesAdministrativas;

	@Transactional
	public void salvar(UnidadeAdministrativa unidadeAdministrativa) throws NegocioException {
		
		UnidadeAdministrativa unidadeAdministrativaBensExistente = unidadesAdministrativas.porNomeUA(unidadeAdministrativa.getNomeUnidadeAdministrativa(),
				unidadeAdministrativa.getNomeResumido(), unidadeAdministrativa.getPredio(), unidadeAdministrativa.getPiso(), unidadeAdministrativa.getCorredor(), unidadeAdministrativa.getSala());
		
		if (unidadeAdministrativaBensExistente != null && !"".equals(unidadeAdministrativaBensExistente)) {
			throw new NegocioException(
					"Ops! Já existe uma Unidade Administrativa com os dados informados. Reveja os campos: Nome da UA, Nome Resumido, Prédio, Piso, Corredor, Sala");
		}
		this.unidadesAdministrativas.guardar(unidadeAdministrativa);
	}
	
	@Transactional
	public void editar(UnidadeAdministrativa unidadeAdministrativa) throws NegocioException {
		this.unidadesAdministrativas.guardar(unidadeAdministrativa);
	}
	
	@Transactional
	public void excluir(UnidadeAdministrativa unidadeAdministrativa) throws NegocioException {
		unidadeAdministrativa = this.unidadesAdministrativas.porId(unidadeAdministrativa.getCodigoUnidadeAdministrativa());
		this.unidadesAdministrativas.remover(unidadeAdministrativa);
	}
}
