package com.henriquelavor.sigpat.service;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import com.henriquelavor.sigpat.util.jpa.Transactional;

import com.henriquelavor.sigpat.model.Baixa;
import com.henriquelavor.sigpat.repository.Baixas;

/**
 * @author Henrique Lavor
 * @data 26.08.2017
 * @version 1.0
 */

public class CadastroBaixas implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private Baixas baixas;

	@Transactional
	public void salvar(Baixa baixa) throws NegocioException {
		List<Baixa> baixaGerada = baixas.todasBaixasGeradasPorNumero(baixa.getNumeroTermoBaixa());
		if (baixaGerada.size() > 0 ) {
			throw new NegocioException("Ops! Já existe um Termo de Baixa GERADO com o número informado.");
			}
		//System.out.println("Numero de termo:"+ baixaGerada.size());
		this.baixas.guardar(baixa);
	}
	
	@Transactional
	public void gerarBaixas() {
		this.baixas.atualizarBaixasGeradas();
	}
	
	
	@Transactional
	public void gerarBaixasManualmente(Baixa baixa) throws NegocioException {
		String numeroTermo;
		numeroTermo = baixa.getNumeroTermoBaixa();
		this.baixas.atualizarBaixasGeradasManualmente(numeroTermo);
	}
	
	@Transactional
	public void excluir(Baixa baixa) throws NegocioException {
		baixa = this.baixas.porId(baixa.getCodigoBaixa());
		this.baixas.remover(baixa);
	}
}
