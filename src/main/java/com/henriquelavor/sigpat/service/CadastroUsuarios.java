package com.henriquelavor.sigpat.service;

import java.io.Serializable;

import javax.inject.Inject;
import com.henriquelavor.sigpat.util.jpa.Transactional;

import com.henriquelavor.sigpat.model.Usuario;
import com.henriquelavor.sigpat.repository.Usuarios;

public class CadastroUsuarios implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private Usuarios usuarios;

	@Transactional
	public void salvar(Usuario usuario) throws NegocioException {
		Usuario usuarioExistente = usuarios.porEmail(usuario.getEmail());
		if (usuarioExistente != null) {
			throw new NegocioException(
					"Ops! Já existe um Usuário com o email informado.");
		}
		this.usuarios.guardar(usuario);
	}
	
	@Transactional
	public void editar(Usuario usuario) throws NegocioException {
		this.usuarios.guardar(usuario);
	}
	
	@Transactional
	public void excluir(Usuario usuario) throws NegocioException {
		usuario = this.usuarios.porId(usuario.getCodigoUsuario());
		this.usuarios.remover(usuario);
	}
}