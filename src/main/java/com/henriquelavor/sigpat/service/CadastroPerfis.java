package com.henriquelavor.sigpat.service;

import java.io.Serializable;

import javax.inject.Inject;
import com.henriquelavor.sigpat.util.jpa.Transactional;

import com.henriquelavor.sigpat.model.Perfil;
import com.henriquelavor.sigpat.repository.Perfis;

public class CadastroPerfis implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private Perfis perfis;

	@Transactional
	public void salvar(Perfil perfil) throws NegocioException {
		Perfil perfilExistente = perfis.porDescricaoPerfil(perfil.getDescricaoPerfil());
		if (perfilExistente != null) {
			throw new NegocioException(
					"Ops! Já existe um Perfil com a descrição informada.");
		}
		this.perfis.guardar(perfil);
	}
	
	@Transactional
	public void editar(Perfil perfil) throws NegocioException {
		this.perfis.guardar(perfil);
	}
	
	@Transactional
	public void excluir(Perfil perfil) throws NegocioException {
		perfil = this.perfis.porId(perfil.getCodigoPerfil());
		this.perfis.remover(perfil);
	}
}