package com.henriquelavor.sigpat.service;

import java.io.Serializable;

import javax.inject.Inject;
import com.henriquelavor.sigpat.util.jpa.Transactional;

import com.henriquelavor.sigpat.model.Funcionario;
import com.henriquelavor.sigpat.repository.Funcionarios;

public class CadastroFuncionarios implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Funcionarios funcionarios;

	@Transactional
	public void salvar(Funcionario funcionario) throws NegocioException {
		Funcionario cpfFuncionarioExistente = funcionarios.porCpf(funcionario.getCpf());
		
		if (cpfFuncionarioExistente != null && !"".equals(cpfFuncionarioExistente))  {
			throw new NegocioException("Ops! Já existe um Funcionário com o cpf informado");
		}
		this.funcionarios.guardar(funcionario);
	}

	@Transactional
	public void editar(Funcionario funcionario) throws NegocioException {
		this.funcionarios.guardar(funcionario);
	}

	@Transactional
	public void excluir(Funcionario funcionario) throws NegocioException {
		funcionario = this.funcionarios.porId(funcionario.getCodigoFuncionario());
		this.funcionarios.remover(funcionario);
	}
}
