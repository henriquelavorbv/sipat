package com.henriquelavor.sigpat.service;

import java.io.Serializable;

import javax.inject.Inject;
import com.henriquelavor.sigpat.util.jpa.Transactional;

import com.henriquelavor.sigpat.model.Divergencia;
import com.henriquelavor.sigpat.repository.Divergencias;

/**
 * @author Henrique Lavor
 * @data 25.09.2017
 * @version 1.0
 */
public class CadastroDivergencias implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private Divergencias divergencias;

	@Transactional
	public void salvar(Divergencia divergencia) throws NegocioException {
		Divergencia divergenciaExistente = divergencias.porNome(divergencia.getDescricaoDivergencia());
		if (divergenciaExistente != null) {
			throw new NegocioException(
					"Ops! Já existe uma Divergência com o nome informado.");
		}
		this.divergencias.guardar(divergencia);
	}
	
	@Transactional
	public void editar(Divergencia divergencia) throws NegocioException {
		this.divergencias.guardar(divergencia);
	}
	
	@Transactional
	public void excluir(Divergencia divergencia) throws NegocioException {
		divergencia = this.divergencias.porId(divergencia.getCodigoDivergencia());
		this.divergencias.remover(divergencia);
	}
}