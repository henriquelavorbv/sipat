package com.henriquelavor.sigpat.service;

import java.io.Serializable;

import javax.inject.Inject;
import com.henriquelavor.sigpat.util.jpa.Transactional;

import com.henriquelavor.sigpat.model.TipoInstituicao;
import com.henriquelavor.sigpat.repository.TiposInstituicoes;

public class CadastroTiposInstituicoes implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private TiposInstituicoes tiposInstituicoes;

	@Transactional
	public void salvar(TipoInstituicao tipoInstituicao) throws NegocioException {
		TipoInstituicao TipoInstituicaoExistente = tiposInstituicoes.porNome(tipoInstituicao.getDescricaoTipoInstituicao());
		if (TipoInstituicaoExistente != null) {
			throw new NegocioException(
					"Ops! Já existe um Tipo de Instituição com o nome informado.");
		}
		this.tiposInstituicoes.guardar(tipoInstituicao);
	}
	
	@Transactional
	public void editar(TipoInstituicao tipoInstituicao) throws NegocioException {
		this.tiposInstituicoes.guardar(tipoInstituicao);
	}
	
	@Transactional
	public void excluir(TipoInstituicao tipoInstituicao) throws NegocioException {
		tipoInstituicao = this.tiposInstituicoes.porId(tipoInstituicao.getCodigoTipoInstituicao());
		this.tiposInstituicoes.remover(tipoInstituicao);
	}
}