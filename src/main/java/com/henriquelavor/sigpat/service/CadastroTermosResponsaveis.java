package com.henriquelavor.sigpat.service;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import com.henriquelavor.sigpat.util.jpa.Transactional;

import com.henriquelavor.sigpat.model.TermoResponsavel;
import com.henriquelavor.sigpat.repository.TermosResponsaveis;

/**
 * @author Henrique Lavor
 * @data 02.12.2016
 * @version 1.0
 */

public class CadastroTermosResponsaveis implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private TermosResponsaveis termosResponsaveis;

	@Transactional
	public void salvar(TermoResponsavel termoResponsavel) throws NegocioException {
		List<TermoResponsavel> termoResponsavelGerado = termosResponsaveis.todosTermosGeradoPorNumero(termoResponsavel.getNumeroTermoResponsabilidade());
		if (termoResponsavelGerado.size() > 0 ) {
			throw new NegocioException("Ops! Já existe um Termo de Responsabilidade GERADO com o número informado.");
			}
		System.out.println("Numero de termo:"+ termoResponsavelGerado.size());
		this.termosResponsaveis.guardar(termoResponsavel);
	}
	
	@Transactional
	public void gerarTermos() {
		this.termosResponsaveis.atualizarTermosGerados();
	}
	
	@Transactional
	public void excluir(TermoResponsavel termoResponsavel) throws NegocioException {
		termoResponsavel = this.termosResponsaveis.porId(termoResponsavel.getCodigoTermoResponsavel());
		this.termosResponsaveis.remover(termoResponsavel);
	}

}
