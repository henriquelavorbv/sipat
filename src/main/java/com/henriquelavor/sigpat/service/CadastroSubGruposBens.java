package com.henriquelavor.sigpat.service;

import java.io.Serializable;

import javax.inject.Inject;
import com.henriquelavor.sigpat.util.jpa.Transactional;

import com.henriquelavor.sigpat.model.SubGrupoBens;
import com.henriquelavor.sigpat.repository.SubGruposBens;

public class CadastroSubGruposBens implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private SubGruposBens subGruposBens;

	@Transactional
	public void salvar(SubGrupoBens subGrupoBens) throws NegocioException {
		SubGrupoBens subGrupoBensExistente = subGruposBens.porNome(subGrupoBens.getDescricaoSubGrupoBem());
		if (subGrupoBensExistente != null) {
			throw new NegocioException(
					"Ops! Já existe um SubGrupo de Bem com o nome informado.");
		}
		this.subGruposBens.guardar(subGrupoBens);
	}
	
	@Transactional
	public void editar(SubGrupoBens subGrupoBens) throws NegocioException {
		this.subGruposBens.guardar(subGrupoBens);
	}
	
	@Transactional
	public void excluir(SubGrupoBens subGrupoBens) throws NegocioException {
		subGrupoBens = this.subGruposBens.porId(subGrupoBens.getCodigoSubGrupoBem());
		this.subGruposBens.remover(subGrupoBens);
	}
	
	
}
