package com.henriquelavor.sigpat.service;

import java.io.Serializable;

import javax.inject.Inject;
import com.henriquelavor.sigpat.util.jpa.Transactional;

import com.henriquelavor.sigpat.model.Inventario;
import com.henriquelavor.sigpat.repository.Inventarios;

public class CadastroInventarios implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private Inventarios inventarios;

	@Transactional
	public void salvar(Inventario inventario) throws NegocioException {
		//Lembrando que esses parametros são preenchidos e requisitados do formumário Inventario
		
	Inventario inventarioUnidadeAdministrativaAbertoExistente = inventarios.porUnidadeAdministrativaInventarioAberto(inventario.getUnidadeAdministrativa());
		
		if (inventarioUnidadeAdministrativaAbertoExistente != null && !"".equals(inventarioUnidadeAdministrativaAbertoExistente)) {
			throw new NegocioException(
					"Ops! Já existe esta Unidade Administrativa está com Inventário ABERTO. Encerre primeiro o Inventário Aberto, e tente novamente.");
			}
		this.inventarios.guardar(inventario);
	}
	
	@Transactional
	public void editar(Inventario inventario) throws NegocioException {
		this.inventarios.guardar(inventario);
	}
	
	@Transactional
	public void excluir(Inventario inventario) throws NegocioException {
		inventario = this.inventarios.porId(inventario.getCodigoInventario());
		this.inventarios.remover(inventario);
	}
}
