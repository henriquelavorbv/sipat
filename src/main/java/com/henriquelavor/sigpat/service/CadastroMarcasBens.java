package com.henriquelavor.sigpat.service;

import java.io.Serializable;

import javax.inject.Inject;
import com.henriquelavor.sigpat.util.jpa.Transactional;

import com.henriquelavor.sigpat.model.MarcaBens;
import com.henriquelavor.sigpat.repository.MarcasBens;

public class CadastroMarcasBens implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private MarcasBens marcasBens;

	@Transactional
	public void salvar(MarcaBens marcaBens) throws NegocioException {
		MarcaBens marcaBensExistente = marcasBens.porNome(marcaBens.getDescricaoMarcaBem());
		if (marcaBensExistente != null) {
			throw new NegocioException(
					"Ops! Já existe uma Marca de Bem com o nome informado.");
		}
		this.marcasBens.guardar(marcaBens);
	}
	
	@Transactional
	public void editar(MarcaBens marcaBens) throws NegocioException {
		this.marcasBens.guardar(marcaBens);
	}
	
	@Transactional
	public void excluir(MarcaBens marcaBens) throws NegocioException {
		marcaBens = this.marcasBens.porId(marcaBens.getCodigoMarcaBem());
		this.marcasBens.remover(marcaBens);
	}
	
	
}
