package com.henriquelavor.sigpat.service;

import java.io.Serializable;

/**
 * @author Henrique Lavor
 * @data 26.09.2017
 * @version 1.0
 */

import javax.inject.Inject;
import com.henriquelavor.sigpat.util.jpa.Transactional;

import com.henriquelavor.sigpat.model.Vistoria;
import com.henriquelavor.sigpat.repository.Vistorias;

public class CadastroVistorias implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private Vistorias vistorias;

	@Transactional
	public void salvar(Vistoria vistoria) throws NegocioException {
		Vistoria vistoriaExistente = vistorias.porVistoriaBem(vistoria.getInventario(), vistoria.getBemMovel());
		
		if (vistoriaExistente != null) {
			throw new NegocioException(
					"Ops! Já existe uma Vistoria com os dados informado.");
		}
		this.vistorias.guardar(vistoria);
	}
	
	@Transactional
	public void editar(Vistoria vistoria) throws NegocioException {
		this.vistorias.guardar(vistoria);
	}
	
	@Transactional
	public void excluir(Vistoria vistoria) throws NegocioException {
		vistoria = this.vistorias.porId(vistoria.getCodigoVistoria());
		this.vistorias.remover(vistoria);
	}
}
