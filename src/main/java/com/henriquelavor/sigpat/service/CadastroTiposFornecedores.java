package com.henriquelavor.sigpat.service;

import java.io.Serializable;

import javax.inject.Inject;
import com.henriquelavor.sigpat.util.jpa.Transactional;

import com.henriquelavor.sigpat.model.TipoFornecedor;
import com.henriquelavor.sigpat.repository.TiposFornecedores;

public class CadastroTiposFornecedores implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private TiposFornecedores tiposFornecedores;

	@Transactional
	public void salvar(TipoFornecedor tipoFornecedor) throws NegocioException {
		TipoFornecedor tipoFornecedorExistente = tiposFornecedores.porNome(tipoFornecedor.getDescricaoTipoFornecedor());
		if (tipoFornecedorExistente != null) {
			throw new NegocioException(
					"Ops! Já existe um Tipo de Fornecedor com o nome informado.");
		}
		this.tiposFornecedores.guardar(tipoFornecedor);
	}
	
	@Transactional
	public void editar(TipoFornecedor tipoFornecedor) throws NegocioException {
		this.tiposFornecedores.guardar(tipoFornecedor);
	}
	
	@Transactional
	public void excluir(TipoFornecedor tipoFornecedor) throws NegocioException {
		tipoFornecedor = this.tiposFornecedores.porId(tipoFornecedor.getCodigoTipoFornecedor());
		this.tiposFornecedores.remover(tipoFornecedor);
	}
}