package com.henriquelavor.sigpat.service;

import java.io.Serializable;

import javax.inject.Inject;
import com.henriquelavor.sigpat.util.jpa.Transactional;

import com.henriquelavor.sigpat.model.GrupoBens;
import com.henriquelavor.sigpat.repository.GruposBens;

public class CadastroGruposBens implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private GruposBens gruposBens;

	@Transactional
	public void salvar(GrupoBens grupoBens) throws NegocioException {
		GrupoBens grupoBensExistente = gruposBens.porNome(grupoBens.getDescricaoGrupoBem());
		if (grupoBensExistente != null) {
			throw new NegocioException(
					"Ops! Já existe um Grupo de Bem com o nome informado.");
		}
		this.gruposBens.guardar(grupoBens);
	}
	
	@Transactional
	public void editar(GrupoBens grupoBens) throws NegocioException {
		this.gruposBens.guardar(grupoBens);
	}
	
	@Transactional
	public void excluir(GrupoBens grupoBens) throws NegocioException {
		grupoBens = this.gruposBens.porId(grupoBens.getCodigoGrupoBem());
		this.gruposBens.remover(grupoBens);
	}
	
	
}
