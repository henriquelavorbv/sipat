package com.henriquelavor.sigpat.service;

import java.io.Serializable;

import javax.inject.Inject;
import com.henriquelavor.sigpat.util.jpa.Transactional;

import com.henriquelavor.sigpat.model.FonteRecurso;
import com.henriquelavor.sigpat.repository.FontesRecursos;
/**
 * @author Henrique Lavor
 * @data 16.01.2017
 * @version 1.0
 */
public class CadastroFontesRecursos implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private FontesRecursos fontesRecursos;

	@Transactional
	public void salvar(FonteRecurso fonteRecurso) throws NegocioException {
		FonteRecurso fonteRecursoExistente = fontesRecursos.porFonte(fonteRecurso.getFonte());
		FonteRecurso nomeFonteRecursoExistente = fontesRecursos.porNome(fonteRecurso.getNomeFonteRecurso());
		
		if (fonteRecursoExistente != null) {
			throw new NegocioException(
					"Ops! Já existe uma Fonte de Recurso, para o dado informado.");
		}
		
		if (nomeFonteRecursoExistente != null) {
			throw new NegocioException(
					"Ops! Já existe um nome de Fonte de Recurso, para o dado informado.");
		}

		this.fontesRecursos.guardar(fonteRecurso);
	}
	
	@Transactional
	public void editar(FonteRecurso fonteRecurso) throws NegocioException {
		this.fontesRecursos.guardar(fonteRecurso);
	}
	
	@Transactional
	public void excluir(FonteRecurso fonteRecurso) throws NegocioException {
		fonteRecurso = this.fontesRecursos.porId(fonteRecurso.getCodigoFonteRecurso());
		this.fontesRecursos.remover(fonteRecurso);
	}
}