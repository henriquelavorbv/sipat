package com.henriquelavor.sigpat.service;

import java.io.Serializable;

import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import com.henriquelavor.sigpat.util.jpa.Transactional;

import com.henriquelavor.sigpat.model.Fornecedor;
import com.henriquelavor.sigpat.repository.Fornecedores;

public class CadastroFornecedores implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Fornecedores fornecedores;

	@Transactional
	public void salvar(Fornecedor fornecedor) throws NegocioException {
		/*
		 * System.out.println("Nome Fantasia" + fornecedor.getNomeFantasia());
		 * if (fornecedor.getNomeFantasia().isEmpty()) { System.out.println(
		 * "Nome Fantazia é vazio"); } else { System.out.println(
		 * "Nome Fantazia é "+fornecedor.getNomeFantasia()); }
		 */
		Fornecedor fornecedorNomeFornecedorExistente = fornecedores.porNomeFantasia(fornecedor.getNomeFornecedor());
		Fornecedor fornecedorCnpjCPFExistente = fornecedores.porCnpjCpf(fornecedor.getCnpjCpf());
		// System.out.println("Nome Fantasia digitado " +
		// fornecedor.getNomeFantasia());
		// System.out.println("Nome Fantasia da pesquisa " +
		// fornecedorNomeFantasiaExistente.getNomeFantasia());

		/*
		 * if (!fornecedor.getNomeFantasia().isEmpty() ||
		 * fornecedorNomeFantasiaExistente != null){
		 */

		if ( (fornecedorNomeFornecedorExistente!= null && !"".equals(fornecedorNomeFornecedorExistente)) ||
				(fornecedorCnpjCPFExistente != null && !"".equals(fornecedorCnpjCPFExistente)))   {
			throw new NegocioException("Ops! Já existe um Fornecedor com os dados informado.");
		}
		this.fornecedores.guardar(fornecedor);
	}

	public void descricaoModificada(ValueChangeEvent event) {
		System.out.println("Valor antigo: " + event.getOldValue());
		System.out.println("Novo valor: " + event.getNewValue());
	}

	@Transactional
	public void editar(Fornecedor fornecedor) throws NegocioException {
		this.fornecedores.guardar(fornecedor);
	}

	@Transactional
	public void excluir(Fornecedor fornecedor) throws NegocioException {
		fornecedor = this.fornecedores.porId(fornecedor.getCodigoFornecedor());
		this.fornecedores.remover(fornecedor);
	}
}
