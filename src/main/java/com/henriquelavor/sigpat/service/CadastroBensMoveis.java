package com.henriquelavor.sigpat.service;

import java.io.Serializable;

import javax.inject.Inject;
import com.henriquelavor.sigpat.util.jpa.Transactional;

import com.henriquelavor.sigpat.model.BemMovel;
import com.henriquelavor.sigpat.repository.BensMoveis;

public class CadastroBensMoveis implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private BensMoveis bensMoveis;

	@Transactional
	public void salvar(BemMovel bemMovel) throws NegocioException {
		
		BemMovel bemMovelExistente = bensMoveis.porNumeroPatrimonial(bemMovel.getNumeroPatrimonio());
		
		if (bemMovelExistente != null && !"".equals(bemMovelExistente)) {
			throw new NegocioException(
					"Ops! Já existe um Bem Móvel com o tombamento informado.");
		}
		this.bensMoveis.guardar(bemMovel);
	}
	
	@Transactional
	public void editar(BemMovel bemMovel) throws NegocioException {
		this.bensMoveis.guardar(bemMovel);
	}
	
	@Transactional
	public void excluir(BemMovel bemMovel) throws NegocioException {
		bemMovel = this.bensMoveis.porId(bemMovel.getCodigoBemMovel());
		this.bensMoveis.remover(bemMovel);
	}
}
