package com.henriquelavor.sigpat.service;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import com.henriquelavor.sigpat.util.jpa.Transactional;

import com.henriquelavor.sigpat.model.Baixa;
import com.henriquelavor.sigpat.model.Transferencia;
import com.henriquelavor.sigpat.repository.Transferencias;

/**
 * @author Henrique Lavor
 * @data 15.08.2017
 * @version 1.0
 */

public class CadastroTransferencias implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private Transferencias transferencias;

	@Transactional
	public void salvar(Transferencia transferencia) throws NegocioException {
		List<Transferencia> termoTransferenciaGerado = transferencias.todosTermosGeradoPorNumeroTransferencia(transferencia.getNumeroTermoTransferencia());
		if (termoTransferenciaGerado.size() > 0 ) {
			throw new NegocioException("Ops! Já existe um Termo de Transferência GERADO com o número informado.");
			}
		System.out.println("Numero de termos de transferencias:"+ termoTransferenciaGerado.size());
		this.transferencias.guardar(transferencia);
	}
	
	@Transactional
	public void gerarTermos() {
		this.transferencias.atualizarTermosGerados();
	}
	
	
	@Transactional
	public void receberTransferencia(Transferencia transferencia) throws NegocioException {
		String numeroTermo;
		
		numeroTermo = transferencia.getNumeroTermoTransferencia();
		
		this.transferencias.atualizarTermoRecebido(numeroTermo);
		
		this.transferencias.atualizarBemRecebido(numeroTermo);
	}
	
	
	@Transactional
	public void estornarTransferencia(Transferencia transferencia) throws NegocioException {
		String numeroTermo;
		
		numeroTermo = transferencia.getNumeroTermoTransferencia();
		
		this.transferencias.atualizarTermEstonar(numeroTermo);
		
		this.transferencias.atualizarBemEstornar(numeroTermo);
	}
	
	
	
	@Transactional
	public void excluir(Transferencia transferencia) throws NegocioException {
		transferencia = this.transferencias.porId(transferencia.getCodigoTransferencia());
		this.transferencias.remover(transferencia);
	}
}
