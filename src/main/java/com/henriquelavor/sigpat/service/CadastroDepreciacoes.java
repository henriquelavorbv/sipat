package com.henriquelavor.sigpat.service;

import java.io.Serializable;

import javax.inject.Inject;
import com.henriquelavor.sigpat.util.jpa.Transactional;

import com.henriquelavor.sigpat.model.Depreciacao;
import com.henriquelavor.sigpat.repository.Depreciacoes;

/**
 * @author Henrique Lavor
 * @data 15.07.2019
 * @version 1.0
 */

public class CadastroDepreciacoes implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private Depreciacoes depreciacoes;

	@Transactional
	public void salvar(Depreciacao depreciacao) throws NegocioException {
		this.depreciacoes.guardar(depreciacao);
	}
	
	@Transactional
	public void editar(Depreciacao depreciacao) throws NegocioException {
		this.depreciacoes.guardar(depreciacao);
	}
	
	@Transactional
	public void excluir(Depreciacao depreciacao) throws NegocioException {
		depreciacao = this.depreciacoes.porId(depreciacao.getCodigoDepreciacao());
		this.depreciacoes.remover(depreciacao);
	}
	
}
