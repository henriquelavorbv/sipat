package com.henriquelavor.sigpat.service;

import java.io.Serializable;

import javax.inject.Inject;
import com.henriquelavor.sigpat.util.jpa.Transactional;

import com.henriquelavor.sigpat.model.ClassificacaoInventario;
import com.henriquelavor.sigpat.repository.ClassificacoesInventarios;

/**
 * @author Henrique Lavor
 * @data 20.08.2016
 * @version 1.0
 */

public class CadastroClassificacoesInventarios implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private ClassificacoesInventarios classificacoesInventarios;

	@Transactional
	public void salvar(ClassificacaoInventario classificacaoInventario) throws NegocioException {
		ClassificacaoInventario classificacaoInventarioExistente = classificacoesInventarios.porNome(classificacaoInventario.getDescricaoClassificacaoInventario());
		if (classificacaoInventarioExistente != null) {
			throw new NegocioException(
					"Ops! Já existe uma Classificação de Inventário com o nome informado.");
		}
		this.classificacoesInventarios.guardar(classificacaoInventario);
	}
	
	@Transactional
	public void editar(ClassificacaoInventario classificacaoInventario) throws NegocioException {
		this.classificacoesInventarios.guardar(classificacaoInventario);
	}
	
	@Transactional
	public void excluir(ClassificacaoInventario classificacaoInventario) throws NegocioException {
		classificacaoInventario = this.classificacoesInventarios.porId(classificacaoInventario.getCodigoClassificacaoInventario());
		this.classificacoesInventarios.remover(classificacaoInventario);
	}
}