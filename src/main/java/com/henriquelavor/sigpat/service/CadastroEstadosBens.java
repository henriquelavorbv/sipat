package com.henriquelavor.sigpat.service;

import java.io.Serializable;

import javax.inject.Inject;
import com.henriquelavor.sigpat.util.jpa.Transactional;

import com.henriquelavor.sigpat.model.EstadoBens;
import com.henriquelavor.sigpat.repository.EstadosBens;

public class CadastroEstadosBens implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private EstadosBens estadosBens;

	@Transactional
	public void salvar(EstadoBens estadoBens) throws NegocioException {
		EstadoBens estadoBensExistente = estadosBens.porNome(estadoBens.getDescricaoEstadoBem());
		if (estadoBensExistente != null) {
			throw new NegocioException(
					"Ops! Já existe um Estado de Bem com o nome informado.");
		}
		this.estadosBens.guardar(estadoBens);
	}
	
	@Transactional
	public void editar(EstadoBens estadoBens) throws NegocioException {
		this.estadosBens.guardar(estadoBens);
	}
	
	@Transactional
	public void excluir(EstadoBens estadoBens) throws NegocioException {
		estadoBens = this.estadosBens.porId(estadoBens.getCodigoEstadoBem());
		this.estadosBens.remover(estadoBens);
	}
}