package com.henriquelavor.sigpat.service;

import java.io.Serializable;

import javax.inject.Inject;
import com.henriquelavor.sigpat.util.jpa.Transactional;

import com.henriquelavor.sigpat.model.Cargo;
import com.henriquelavor.sigpat.repository.Cargos;

public class CadastroCargos implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private Cargos cargos;

	@Transactional
	public void salvar(Cargo cargo) throws NegocioException {
		Cargo cargoExistente = cargos.porNome(cargo.getDescricaoCargo());
		if (cargoExistente != null) {
			throw new NegocioException(
					"Ops! Já existe um Cargo com o nome informado.");
		}
		this.cargos.guardar(cargo);
	}
	
	@Transactional
	public void editar(Cargo cargo) throws NegocioException {
		this.cargos.guardar(cargo);
	}
	
	@Transactional
	public void excluir(Cargo cargo) throws NegocioException {
		cargo = this.cargos.porId(cargo.getCodigoCargo());
		this.cargos.remover(cargo);
	}
}