package com.henriquelavor.sigpat.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.model.FonteRecurso;
import com.henriquelavor.sigpat.repository.FontesRecursos;
import com.henriquelavor.sigpat.service.CadastroFontesRecursos;
import com.henriquelavor.sigpat.service.NegocioException;

/**
 * @author Henrique Lavor
 * @data 16.01.2017
 * @version 1.0
 */

@Named
@ViewScoped
public class CadastroFonteRecursoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CadastroFontesRecursos cadastro;

	@Inject
	private FontesRecursos fontesRecursos;

	private FonteRecurso fonteRecurso = new FonteRecurso();

	private List<FonteRecurso> todasFontesRecursos;

	public void prepararCadastro() {
		this.todasFontesRecursos = fontesRecursos.todas();
		if (this.fonteRecurso == null) {
			this.fonteRecurso = new FonteRecurso();
		}
	}

	public void salvar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.salvar(this.fonteRecurso);
			this.fonteRecurso = new FonteRecurso();
			context.addMessage(null, new FacesMessage("Registro Salvo com sucesso!"));
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public void editar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.editar(this.fonteRecurso);

			this.fonteRecurso = new FonteRecurso();
			context.addMessage(null, new FacesMessage("Registro Atualizado com sucesso!"));
			FacesContext.getCurrentInstance().getExternalContext().redirect("/sigpat/paginas/FonteRecurso/ConsultaFontesRecursos.xhtml");
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}
	
	public List<FonteRecurso> getTodasFontesRecursos() {
		return this.todasFontesRecursos;
	}

	public FonteRecurso getFonteRecurso() {
		return fonteRecurso;
	}

	public void setFonteRecurso(FonteRecurso fonteRecurso) {
		this.fonteRecurso = fonteRecurso;
	}
}