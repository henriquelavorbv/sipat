package com.henriquelavor.sigpat.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.model.GrupoBens;
import com.henriquelavor.sigpat.model.SubGrupoBens;
import com.henriquelavor.sigpat.repository.GruposBens;
import com.henriquelavor.sigpat.service.CadastroSubGruposBens;
import com.henriquelavor.sigpat.service.NegocioException;

@Named
@ViewScoped
public class CadastroSubGrupoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CadastroSubGruposBens cadastro;

	@Inject
	private GruposBens gruposBens;

	private SubGrupoBens subGrupoBens = new SubGrupoBens();

	private List<GrupoBens> todosGruposBens;

	public void prepararCadastro() {

		this.todosGruposBens = this.gruposBens.todos();

		if (this.subGrupoBens == null) {
			this.subGrupoBens = new SubGrupoBens();
		}
	}

	// atualizar lista de grupos 10.09.2016
	public void prepararGrupos() {
		this.todosGruposBens = this.gruposBens.todos();
	}

	public void salvar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.cadastro.salvar(this.subGrupoBens);
			this.subGrupoBens = new SubGrupoBens();
			context.addMessage(null, new FacesMessage("Registro salvo com sucesso!"));
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}

	}

	public void editar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.editar(this.subGrupoBens);
			this.subGrupoBens = new SubGrupoBens();
			context.addMessage(null, new FacesMessage("Registro Atualizado com sucesso!"));
			FacesContext.getCurrentInstance().getExternalContext()
			.redirect("/sigpat/paginas/SubGrupo/ConsultaSubGruposBens.xhtml");
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public List<GrupoBens> getTodosGrupos() {
		return this.todosGruposBens;
	}

	public SubGrupoBens getSubGrupoBens() {
		return subGrupoBens;
	}

	public void setSubGrupoBens(SubGrupoBens subGrupoBens) {
		this.subGrupoBens = subGrupoBens;
	}
}