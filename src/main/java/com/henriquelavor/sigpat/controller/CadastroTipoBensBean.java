package com.henriquelavor.sigpat.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.model.TipoBens;
import com.henriquelavor.sigpat.repository.TiposBens;
import com.henriquelavor.sigpat.service.CadastroTiposBens;
import com.henriquelavor.sigpat.service.NegocioException;

@Named
@ViewScoped
public class CadastroTipoBensBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CadastroTiposBens cadastro;

	@Inject
	private TiposBens tiposBens;

	private TipoBens tipoBens = new TipoBens();
	
	private List<TipoBens> todosTiposBens;

	public void prepararCadastro() {
		this.todosTiposBens = tiposBens.todos();
		if (this.tipoBens == null) {
			this.tipoBens = new TipoBens();
		}
	}

	public void salvar() throws IOException{
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.salvar(this.tipoBens);
			this.tipoBens = new TipoBens();
			context.addMessage(null, new FacesMessage("Registro Salvo com sucesso!"));
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public void editar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.editar(this.tipoBens);

			this.tipoBens = new TipoBens();
			context.addMessage(null, new FacesMessage(
					"Registro Atualizado com sucesso!"));
			FacesContext.getCurrentInstance().getExternalContext()
			.redirect("/sigpat/paginas/TipoBem/ConsultaTiposBens.xhtml");
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}
	
	public List<TipoBens> getTodosTiposBens() {
		return this.todosTiposBens;
	}

	public TipoBens getTipoBens() {
		return tipoBens;
	}

	public void setTipoBens(TipoBens tipoBens) {
		this.tipoBens = tipoBens;
	}
}