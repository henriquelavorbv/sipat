package com.henriquelavor.sigpat.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.primefaces.event.CaptureEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.FlowEvent;

import com.henriquelavor.sigpat.model.Cargo;
import com.henriquelavor.sigpat.model.Funcionario;
import com.henriquelavor.sigpat.model.ResponsavelPatrimonial;
import com.henriquelavor.sigpat.model.TipoSexo;
import com.henriquelavor.sigpat.model.TipoSituacaoFuncionario;
import com.henriquelavor.sigpat.model.UnidadeAdministrativa;
import com.henriquelavor.sigpat.model.UnidadeGestora;
import com.henriquelavor.sigpat.repository.Cargos;
import com.henriquelavor.sigpat.repository.UnidadesAdministrativas;
import com.henriquelavor.sigpat.service.CadastroFuncionarios;
import com.henriquelavor.sigpat.service.NegocioException;

@Named
@ViewScoped
public class CadastroFuncionarioBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CadastroFuncionarios cadastro;

	@Inject
	private UnidadesAdministrativas unidadesAdministrativas;

	@Inject
	private Cargos cargos;

	private Funcionario funcionario = new Funcionario();

	private List<UnidadeGestora> todasUnidadesGestoras;

	private List<UnidadeAdministrativa> todasUnidadesAdministrativas;

	private List<Cargo> todosCargos;

	private UploadArquivo arquivo = new UploadArquivo();
	
	public void uploadAction(FileUploadEvent event) {
		this.arquivo.fileUpload(event, ".jpg", "/resources/enviados/funcionarios/");
		this.funcionario.setFoto(this.arquivo.getNome());
		this.arquivo.gravar(); // nova implementacao fileupload
		this.arquivo = new UploadArquivo(); // nova implementacao fileupload
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("Foto enviada com sucesso!"));
	}
	
	public void photoCamAction(CaptureEvent captureEvent) {
		this.arquivo.oncapture(captureEvent);
		this.funcionario.setFoto(this.arquivo.getFilename()+".jpg");
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("Foto captura e enviada com sucesso!"));
	}
	
	public void prepararCadastro() {
		this.todasUnidadesAdministrativas = this.unidadesAdministrativas.todas();
		this.todosCargos = this.cargos.todos();
		if (this.funcionario == null) {
			this.funcionario = new Funcionario();
		}
	}
	
	//nova implementação 13.08.2016
	public void prepararDados() {
		this.todosCargos = this.cargos.todos();
		this.todasUnidadesAdministrativas = this.unidadesAdministrativas.todas();
	}

	public void salvar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			//this.arquivo.gravar(); // nova implementacao fileupload
			this.cadastro.salvar(this.funcionario);
			this.funcionario = new Funcionario();
			//this.arquivo = new UploadArquivo(); // nova implementacao fileupload
			context.addMessage(null, new FacesMessage("Registro salvo com sucesso!"));
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public void editar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.arquivo.gravar(); // nova implementacao fileupload
			this.cadastro.editar(this.funcionario);
			this.funcionario = new Funcionario();
			this.arquivo = new UploadArquivo(); // nova implementacao fileupload
			context.addMessage(null, new FacesMessage("Registro Atualizado com sucesso!"));
			FacesContext.getCurrentInstance().getExternalContext().redirect("/sigpat/paginas/Funcionario/ConsultaFuncionarios.xhtml");
			
			
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}
	
	public TipoSexo[] getTiposSexo() {
		return TipoSexo.values();
	}

	public ResponsavelPatrimonial[] getResponsaveisPatrimoniais() {
		return ResponsavelPatrimonial.values();
	}
	

	public ResponsavelPatrimonial[] getResponsavelInventarios() {
		return ResponsavelPatrimonial.values();
	}

	public TipoSituacaoFuncionario[] getTiposSituacoesFuncionarios() {
		return TipoSituacaoFuncionario.values();
	}

	public List<UnidadeGestora> getTodasUnidadesGestoras() {
		return this.todasUnidadesGestoras;
	}

	public List<UnidadeAdministrativa> getTodasUnidadesAdministrativas() {
		return this.todasUnidadesAdministrativas;
	}

	public List<Cargo> getTodosCargos() {
		return this.todosCargos;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	// Usado no TAB Wizard
	public String onFlowProcess(FlowEvent event) {
		return event.getNewStep();
	}

	public void executarJS() {
		RequestContext.getCurrentInstance().execute("checarJS();");
	}

}