package com.henriquelavor.sigpat.controller;

import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.FileUploadEvent;

import com.henriquelavor.sigpat.model.Seguradora;
import com.google.common.io.Resources;
import com.henriquelavor.sigpat.model.Apolice;
import com.henriquelavor.sigpat.model.BemMovel;
import com.henriquelavor.sigpat.repository.BensMoveis;
import com.henriquelavor.sigpat.repository.Seguradoras;
import com.henriquelavor.sigpat.service.CadastroApolices;
import com.henriquelavor.sigpat.service.NegocioException;
import com.henriquelavor.sigpat.util.jsf.FacesUtil;
import com.henriquelavor.sigpat.util.mail.Mailer;
import com.outjected.email.api.ContentDisposition;
import com.outjected.email.api.MailMessage;
import com.outjected.email.impl.attachments.BaseAttachment;

/**
 * @author Henrique Lavor
 * @data 10.08.2017
 * @version 1.0
 */

@Named
@ViewScoped
public class CadastroApoliceBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CadastroApolices cadastro;

	@Inject
	private BensMoveis bensMoveis;

	@Inject
	private Seguradoras seguradoras;

	@Inject
	private Mailer mailer;

	private Apolice apolice = new Apolice();

	private List<BemMovel> todosBensMoveis;

	private List<Seguradora> todasSeguradoras;

	private UploadArquivo arquivo = new UploadArquivo();

	// pdf da Apólice de seguro
	public void uploadPdf(FileUploadEvent event) {
		this.arquivo.fileUpload(event, ".pdf", "/resources/enviados/bensmoveis/apolices/");
		this.apolice.setApolicePdf(this.arquivo.getNome());
		this.arquivo.gravar(); // nova implementacao fileupload
		this.arquivo = new UploadArquivo(); // nova implementacao fileupload
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("Apólice de seguro enviada com sucesso!"));
	}

	public void prepararCadastro() {

		this.todosBensMoveis = this.bensMoveis.todoAtivos();
		this.todasSeguradoras = this.seguradoras.todas();

		if (this.apolice == null) {
			this.apolice = new Apolice();
		}
	}
	
	public void prepararBens() {
		this.todosBensMoveis = this.bensMoveis.todoAtivos();
	}

	/*
	 * public void prepararGrupos() { this.todasSeguradoras =
	 * this.seguradoras.todos(); }
	 */
	public void salvar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		
		String apolicePDFExistente = this.getApolice().getApolicePdf();

		try {
			this.cadastro.salvar(this.apolice);
			// se quer notificar por email e existir arquivo para anexar, entao
			// enviar email informando o responsavel pela guarda do bem com
			// anexo
			
			if ((this.apolice.getNotificarEmail()) && (apolicePDFExistente == null)) {
				this.enviarNotificaoEmail();
			} else if ((this.apolice.getNotificarEmail()) && (apolicePDFExistente != null)) {
				this.enviarNotificaoEmailAnexoPDF();
			}
			
			this.apolice = new Apolice();
			this.arquivo = new UploadArquivo();
			context.addMessage(null, new FacesMessage("Registro de Apólice de Seguro de Bem Patrimonial Adicionado com Sucesso!"));
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}

	}

	public void editar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		String apolicePDFExistente = this.getApolice().getApolicePdf();
		try {
			this.cadastro.editar(this.apolice);
			if ((this.apolice.getNotificarEmail()) && (apolicePDFExistente == null)) {
				this.enviarNotificaoEmail();
			} else if ((this.apolice.getNotificarEmail()) && (apolicePDFExistente != null)) {
				this.enviarNotificaoEmailAnexoPDF();
			}
			
			this.apolice = new Apolice();
			this.arquivo = new UploadArquivo(); // nova implementacao fileupload
			context.addMessage(null, new FacesMessage("Registro de Apólice de Seguro de Bem Patrimonial Atualizado com sucesso!"));
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("/sigpat/paginas/Apolice/ConsultaApolices.xhtml");
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public void enviarNotificaoEmail() throws IOException {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String dataContratacao = df.format(this.apolice.getDataContratacao());

		MailMessage message = mailer.prepararNovaMensagem();

		message.to(this.apolice.getBemMovel().getNovoFuncionarioResponsavelPatrimonial().getEmail())
				.subject("SIGPAT: Notificação de Registro de Apólice de Seguro de Bem Patrimonial")
				.bodyHtml("<strong>Unidade Gestora: </strong>"
						+ this.apolice.getBemMovel().getLocalDestino().getUnidadeGestora().getNomeUnidadeGestora() + "("
						+ this.apolice.getBemMovel().getLocalDestino().getUnidadeGestora().getNomeResumido() + ")"
						+ "<br/>" + "<strong>Unidade Administrativa: </strong>"
						+ this.apolice.getBemMovel().getLocalDestino().getNomeUnidadeAdministrativa() + "("
						+ this.apolice.getBemMovel().getLocalDestino().getNomeResumido() + ")" + "<br/>"
						+ "<strong>Bem Patrimonial: </strong>" + this.apolice.getBemMovel().getNumeroPatrimonio()
						+ " - " + this.apolice.getBemMovel().getDescricao() + "<br />" + "<br />"
						+ "<strong>Funcionário Responsável pela Guarda do Bem: </strong>"
						+ this.apolice.getBemMovel().getNovoFuncionarioResponsavelPatrimonial().getNomeFuncionario()
						+ "<br />" + "<strong>Seguradora: </strong>"
						+ this.apolice.getSeguradora().getDescricaoSeguradora() + "<br />"
						+ "<strong>Número da Apólice: </strong>" + this.apolice.getNumeroApolice() + "<br />"
						+ "<strong>Data da Contratação: </strong>" + dataContratacao + "<br />")
				.send();

		FacesUtil.addInfoMessage(
				"Notificação de Registro de Apólice de Seguro de Bem Patrimonial enviado por e-mail com sucesso!");
	}

	public void enviarNotificaoEmailAnexoPDF() throws IOException {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String dataContratacao = df.format(this.apolice.getDataContratacao());

		MailMessage message = mailer.prepararNovaMensagem();

		message.to(this.apolice.getBemMovel().getNovoFuncionarioResponsavelPatrimonial().getEmail())
				.subject("SIGPAT: Notificação de Registro de Apólice de Seguro de Bem Patrimonial")
				.bodyHtml("<strong>Unidade Gestora: </strong>"
						+ this.apolice.getBemMovel().getLocalDestino().getUnidadeGestora().getNomeUnidadeGestora() + "("
						+ this.apolice.getBemMovel().getLocalDestino().getUnidadeGestora().getNomeResumido() + ")"
						+ "<br/>" + "<strong>Unidade Administrativa: </strong>"
						+ this.apolice.getBemMovel().getLocalDestino().getNomeUnidadeAdministrativa() + "("
						+ this.apolice.getBemMovel().getLocalDestino().getNomeResumido() + ")" + "<br/>"
						+ "<strong>Bem Patrimonial: </strong>" + this.apolice.getBemMovel().getNumeroPatrimonio()
						+ " - " + this.apolice.getBemMovel().getDescricao() + "<br />" + "<br />"
						+ "<strong>Funcionário Responsável pela Guarda do Bem: </strong>"
						+ this.apolice.getBemMovel().getNovoFuncionarioResponsavelPatrimonial().getNomeFuncionario()
						+ "<br />" + "<strong>Seguradora: </strong>"
						+ this.apolice.getSeguradora().getDescricaoSeguradora() + "<br />"
						+ "<strong>Número da Apólice: </strong>" + this.apolice.getNumeroApolice() + "<br />"
						+ "<strong>Data da Contratação: </strong>" + dataContratacao + "<br />")
				.addAttachment(new BaseAttachment(this.apolice.getApolicePdf(), "application/x-pdf",
						ContentDisposition.ATTACHMENT,
						Resources.toByteArray(Resources.getResource(
								"../../resources/enviados/bensmoveis/apolices/" + this.apolice.getApolicePdf()))))
				.send();

		FacesUtil.addInfoMessage(
				"Notificação de Registro de Apólice de Seguro de Bem Patrimonial enviado por e-mail com sucesso e anexo PDF!");
	}

	// addAttachment(new BaseAttachment(this.apolice.getApolicePdf(),
	// "application/x-pdf",ContentDisposition.ATTACHMENT,
	// Resources.toByteArray(Resources.getResource("../../resources/enviados/bensmoveis/apolices/"+
	// this.apolice.getApolicePdf()))))

	public List<BemMovel> getTodosBensMoveis() {
		return this.todosBensMoveis;
	}

	public List<Seguradora> getTodasSeguradoras() {
		return this.todasSeguradoras;
	}

	public Apolice getApolice() {
		return apolice;
	}

	public void setApolice(Apolice apolice) {
		this.apolice = apolice;
	}
}