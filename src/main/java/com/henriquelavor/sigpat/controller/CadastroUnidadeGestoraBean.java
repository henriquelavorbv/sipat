package com.henriquelavor.sigpat.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FlowEvent;

import com.henriquelavor.sigpat.model.UnidadeGestora;
import com.henriquelavor.sigpat.model.TipoInstituicao;
import com.henriquelavor.sigpat.model.TipoPoder;
import com.henriquelavor.sigpat.model.TipoSituacaoUnidadeGestora;
import com.henriquelavor.sigpat.repository.TiposInstituicoes;
import com.henriquelavor.sigpat.repository.UnidadesGestoras;
import com.henriquelavor.sigpat.service.CadastroUnidadesGestoras;
import com.henriquelavor.sigpat.service.NegocioException;

@Named
@ViewScoped
public class CadastroUnidadeGestoraBean implements Serializable {
	private static final long serialVersionUID = 1L;

	@Inject
	private CadastroUnidadesGestoras cadastro;

	@Inject
	private UnidadesGestoras unidadesGestoras;
	
	@Inject
	private TiposInstituicoes tiposInstituicoes;
	
	private UnidadeGestora unidadeGestora = new UnidadeGestora();
	
	private List<UnidadeGestora> todasUnidadesGestoras;
	private List<TipoInstituicao> todosTiposInstituicoes;
	
	public void prepararCadastro() {
		this.todasUnidadesGestoras = unidadesGestoras.todas();
		this.todosTiposInstituicoes = tiposInstituicoes.todos();
		
		if (this.unidadeGestora == null) {
			this.unidadeGestora = new UnidadeGestora();
		}
	}
	
	public void prepararUnidadesGestoras() {
		this.todasUnidadesGestoras = unidadesGestoras.todas();
		this.todosTiposInstituicoes = tiposInstituicoes.todos();
	}
	
	public void prepararUnidadesGestorasGeral() {
		this.todasUnidadesGestoras = unidadesGestoras.todasGeral();
	}

	public void salvar() throws IOException{
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.salvar(this.unidadeGestora);
			this.unidadeGestora = new UnidadeGestora();
			context.addMessage(null, new FacesMessage("Registro Salvo com sucesso!"));
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public void editar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.editar(this.unidadeGestora);

			this.unidadeGestora = new UnidadeGestora();
			context.addMessage(null, new FacesMessage(
					"Registro Atualizado com sucesso!"));
			FacesContext.getCurrentInstance().getExternalContext()
			.redirect("/sigpat/paginas/UnidadeGestora/ConsultaUnidadesGestoras.xhtml");
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}
	
	public TipoPoder[] getTiposPoderes() {
		return TipoPoder.values();
	}
	
	public TipoSituacaoUnidadeGestora[] getTiposSituacoesUnidadesGestoras() {
		return TipoSituacaoUnidadeGestora.values();
	}
	
	public List<TipoInstituicao> getTodosTiposInstituicoes() {
		return this.todosTiposInstituicoes;
	}
	
	
	public List<UnidadeGestora> getTodasUnidadesGestoras() {
		return this.todasUnidadesGestoras;
	}

	public UnidadeGestora getUnidadeGestora() {
		return unidadeGestora;
	}

	public void setUnidadeGestora(UnidadeGestora unidadeGestora) {
		this.unidadeGestora = unidadeGestora;
	}
	//usado no TAB Wizard
	public String onFlowProcess(FlowEvent event) {
		return event.getNewStep();
	}
	
	public void executarJS() {
		RequestContext.getCurrentInstance().execute("checarJS();");
	}
}