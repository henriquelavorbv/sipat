package com.henriquelavor.sigpat.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.model.Cargo;
import com.henriquelavor.sigpat.repository.Cargos;
import com.henriquelavor.sigpat.service.CadastroCargos;
import com.henriquelavor.sigpat.service.NegocioException;

@Named
@ViewScoped
public class CadastroCargoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CadastroCargos cadastro;

	@Inject
	private Cargos cargos;

	private Cargo cargo = new Cargo();

	private List<Cargo> todosCargos;

	public void prepararCadastro() {
		this.todosCargos = cargos.todos();
		if (this.cargo == null) {
			this.cargo = new Cargo();
		}
	}

	public void salvar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.salvar(this.cargo);
			this.cargo = new Cargo();
			context.addMessage(null, new FacesMessage("Registro Salvo com sucesso!"));
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public void editar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.editar(this.cargo);

			this.cargo = new Cargo();
			context.addMessage(null, new FacesMessage("Registro Atualizado com sucesso!"));
			FacesContext.getCurrentInstance().getExternalContext().redirect("/sigpat/paginas/Cargo/ConsultaCargos.xhtml");
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}
	
	public List<Cargo> getTodosCargos() {
		return this.todosCargos;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
}