package com.henriquelavor.sigpat.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.model.TipoTransferencia;
import com.henriquelavor.sigpat.repository.TiposTransferencias;
import com.henriquelavor.sigpat.service.CadastroTiposTransferencias;
import com.henriquelavor.sigpat.service.NegocioException;

/**
 * @author Henrique Lavor
 * @data 09.09.2016
 * @version 1.0
 */

@Named
@ViewScoped
public class CadastroTipoTransferenciaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CadastroTiposTransferencias cadastro;

	@Inject
	private TiposTransferencias tiposTransferencias;

	private TipoTransferencia tipoTransferencia = new TipoTransferencia();

	private List<TipoTransferencia> todosTiposTransferencias;

	public void prepararCadastro() {
		this.todosTiposTransferencias = tiposTransferencias.todos();
		if (this.tipoTransferencia == null) {
			this.tipoTransferencia = new TipoTransferencia();
		}
	}

	public void salvar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.salvar(this.tipoTransferencia);
			this.tipoTransferencia = new TipoTransferencia();
			context.addMessage(null, new FacesMessage("Registro Salvo com sucesso!"));
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public void editar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.editar(this.tipoTransferencia);

			this.tipoTransferencia = new TipoTransferencia();
			context.addMessage(null, new FacesMessage("Registro Atualizado com sucesso!"));
			FacesContext.getCurrentInstance().getExternalContext()
			.redirect("/sigpat/paginas/TipoTransferencia/ConsultaTiposTransferencias.xhtml");
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}
	
	public List<TipoTransferencia> getTodosTiposTransferencias() {
		return this.todosTiposTransferencias;
	}

	public TipoTransferencia getTipoTransferencia() {
		return tipoTransferencia;
	}

	public void setTipoTransferencia(TipoTransferencia tipoTransferencia) {
		this.tipoTransferencia = tipoTransferencia;
	}
}