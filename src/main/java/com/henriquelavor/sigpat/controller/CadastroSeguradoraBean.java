package com.henriquelavor.sigpat.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.model.Seguradora;
import com.henriquelavor.sigpat.repository.Seguradoras;
import com.henriquelavor.sigpat.service.CadastroSeguradoras;
import com.henriquelavor.sigpat.service.NegocioException;

/**
 * @author Henrique Lavor
 * @data 10.08.2017
 * @version 1.0
 */

@Named
@ViewScoped
public class CadastroSeguradoraBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CadastroSeguradoras cadastro;

	@Inject
	private Seguradoras seguradoras;

	private Seguradora seguradora = new Seguradora();

	private List<Seguradora> todasSeguradoras;

	public void prepararCadastro() {
		this.todasSeguradoras = seguradoras.todas();
		if (this.seguradora == null) {
			this.seguradora = new Seguradora();
		}
	}
	
	public void salvar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.salvar(this.seguradora);
			this.seguradora = new Seguradora();
			context.addMessage(null, new FacesMessage("Registro Salvo com sucesso!"));
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public void editar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.editar(this.seguradora);

			this.seguradora = new Seguradora();
			context.addMessage(null, new FacesMessage("Registro Atualizado com sucesso!"));
			FacesContext.getCurrentInstance().getExternalContext().redirect("/sigpat/paginas/Seguradora/ConsultaSeguradoras.xhtml");
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}
	
	public List<Seguradora> getTodasSeguradoras() {
		return this.todasSeguradoras;
	}

	public Seguradora getSeguradora() {
		return seguradora;
	}

	public void setSeguradora(Seguradora seguradora) {
		this.seguradora = seguradora;
	}
}