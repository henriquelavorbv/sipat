package com.henriquelavor.sigpat.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.model.ClassificacaoInventario;
import com.henriquelavor.sigpat.repository.ClassificacoesInventarios;
import com.henriquelavor.sigpat.service.CadastroClassificacoesInventarios;
import com.henriquelavor.sigpat.service.NegocioException;

/**
 * @author Henrique Lavor
 * @data 20.08.2016
 * @version 1.0
 */

@Named
@ViewScoped
public class CadastroClassificacaoInventarioBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CadastroClassificacoesInventarios cadastro;

	@Inject
	private ClassificacoesInventarios classificacoesInventarios;

	private ClassificacaoInventario classificacaoInventario = new ClassificacaoInventario();

	private List<ClassificacaoInventario> todasClassificacoesInventarios;

	public void prepararCadastro() {
		this.todasClassificacoesInventarios = classificacoesInventarios.todas();
		if (this.classificacaoInventario == null) {
			this.classificacaoInventario = new ClassificacaoInventario();
		}
	}

	public void salvar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.salvar(this.classificacaoInventario);
			this.classificacaoInventario = new ClassificacaoInventario();
			context.addMessage(null, new FacesMessage("Registro Salvo com sucesso!"));
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public void editar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.editar(this.classificacaoInventario);

			this.classificacaoInventario = new ClassificacaoInventario();
			context.addMessage(null, new FacesMessage("Registro Atualizado com sucesso!"));
			FacesContext.getCurrentInstance().getExternalContext().redirect("/sigpat/paginas/ClassificacaoInventario/ConsultaClassificacoesInventarios.xhtml");
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}
	
	public List<ClassificacaoInventario> getTodasClassificacoesInventarios() {
		return this.todasClassificacoesInventarios;
	}

	public ClassificacaoInventario getClassificacaoInventario() {
		return classificacaoInventario;
	}

	public void setClassificacaoInventario(ClassificacaoInventario classificacaoInventario) {
		this.classificacaoInventario = classificacaoInventario;
	}
}