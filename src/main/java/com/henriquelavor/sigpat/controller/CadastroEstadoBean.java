package com.henriquelavor.sigpat.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.model.EstadoBens;
import com.henriquelavor.sigpat.repository.EstadosBens;
import com.henriquelavor.sigpat.service.CadastroEstadosBens;
import com.henriquelavor.sigpat.service.NegocioException;

@Named
@ViewScoped
public class CadastroEstadoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CadastroEstadosBens cadastro;

	@Inject
	private EstadosBens estadosBens;

	private EstadoBens estadoBens = new EstadoBens();
	
	private List<EstadoBens> todosEstadosBens;

	public void prepararCadastro() {
		this.todosEstadosBens = estadosBens.todos();
		if (this.estadoBens == null) {
			this.estadoBens = new EstadoBens();
		}
	}

	public void salvar() throws IOException{
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.salvar(this.estadoBens);
			this.estadoBens = new EstadoBens();
			context.addMessage(null, new FacesMessage("Registro Salvo com sucesso!"));
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public void editar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.editar(this.estadoBens);

			this.estadoBens = new EstadoBens();
			context.addMessage(null, new FacesMessage(
					"Registro Atualizado com sucesso!"));
			FacesContext.getCurrentInstance().getExternalContext().redirect("/sigpat/paginas/EstadoBem/ConsultaEstadosBens.xhtml");
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}
	
	public List<EstadoBens> getTodosEstadosBens() {
		return this.todosEstadosBens;
	}

	public EstadoBens getEstadoBens() {
		return estadoBens;
	}

	public void setEstadoBens(EstadoBens estadoBens) {
		this.estadoBens = estadoBens;
	}
}