package com.henriquelavor.sigpat.controller;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.model.BemMovelGarantiaView;
import com.henriquelavor.sigpat.repository.BensMoveisGarantiasView;
import com.henriquelavor.sigpat.util.jsf.FacesUtil;
import com.outjected.email.api.MailMessage;

/**
 * @author Henrique Lavor
 * @data 07.09.2019
 * @version 1.0
 */

@Named
@ViewScoped
public class CadastroBemMovelGarantiaViewBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private BensMoveisGarantiasView bensMoveisGarantiasView;

	private BemMovelGarantiaView bemMovelGarantiaView = new BemMovelGarantiaView();

	private List<BemMovelGarantiaView> todosBensMoveisGarantiasView;

	public void prepararCadastro() {
		this.todosBensMoveisGarantiasView = bensMoveisGarantiasView.todos();
	}

	
	public List<BemMovelGarantiaView> getTodosBensMoveisGarantiasView() {
		return this.todosBensMoveisGarantiasView;
	}

	public BemMovelGarantiaView getBemMovelGarantiaView() {
		return bemMovelGarantiaView;
	}

	public void setBemMovelGarantiaView(BemMovelGarantiaView bemMovelGarantiaView) {
		this.bemMovelGarantiaView = bemMovelGarantiaView;
	}
	
}