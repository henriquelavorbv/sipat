package com.henriquelavor.sigpat.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.model.TipoInstituicao;
import com.henriquelavor.sigpat.repository.TiposInstituicoes;
import com.henriquelavor.sigpat.service.CadastroTiposInstituicoes;
import com.henriquelavor.sigpat.service.NegocioException;

@Named
@ViewScoped
public class CadastroTipoInstituicaoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CadastroTiposInstituicoes cadastro;

	@Inject
	private TiposInstituicoes tiposInstituicoes;

	private TipoInstituicao tipoInstituicao = new TipoInstituicao();
	
	private List<TipoInstituicao> todosTiposInstituicoes;

	public void prepararCadastro() {
		this.todosTiposInstituicoes = tiposInstituicoes.todos();
		if (this.tipoInstituicao == null) {
			this.tipoInstituicao = new TipoInstituicao();
		}
	}

	public void salvar() throws IOException{
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.salvar(this.tipoInstituicao);
			this.tipoInstituicao = new TipoInstituicao();
			context.addMessage(null, new FacesMessage("Registro Salvo com sucesso!"));
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public void editar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.editar(this.tipoInstituicao);

			this.tipoInstituicao = new TipoInstituicao();
			context.addMessage(null, new FacesMessage(
					"Registro Atualizado com sucesso!"));
			FacesContext.getCurrentInstance().getExternalContext()
			.redirect("/sigpat/paginas/TipoInstituicao/ConsultaTiposInstituicoes.xhtml");
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}
	
	public List<TipoInstituicao> getTodosTiposInstituicoes() {
		return this.todosTiposInstituicoes;
	}

	public TipoInstituicao getTipoInstituicao() {
		return tipoInstituicao;
	}

	public void setTipoInstituicao(TipoInstituicao tipoInstituicao) {
		this.tipoInstituicao = tipoInstituicao;
	}
}