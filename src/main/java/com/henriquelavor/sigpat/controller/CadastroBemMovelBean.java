package com.henriquelavor.sigpat.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
// import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

// import org.primefaces.context.RequestContext;
import org.primefaces.event.CaptureEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.FlowEvent;

import com.henriquelavor.sigpat.model.BemMovel;
import com.henriquelavor.sigpat.model.EstadoBens;
import com.henriquelavor.sigpat.model.FonteRecurso;
import com.henriquelavor.sigpat.model.Fornecedor;
import com.henriquelavor.sigpat.model.Funcionario;
import com.henriquelavor.sigpat.model.GrupoBens;
import com.henriquelavor.sigpat.model.MarcaBens;
import com.henriquelavor.sigpat.model.ModalidadeAquisicaoBens;
import com.henriquelavor.sigpat.model.SubGrupoBens;
import com.henriquelavor.sigpat.model.TipoBens;
import com.henriquelavor.sigpat.model.Transferencia;
import com.henriquelavor.sigpat.model.UnidadeAdministrativa;
import com.henriquelavor.sigpat.model.UnidadeGestora;
import com.henriquelavor.sigpat.repository.BensMoveis;
import com.henriquelavor.sigpat.repository.EstadosBens;
import com.henriquelavor.sigpat.repository.Fornecedores;
import com.henriquelavor.sigpat.repository.Funcionarios;
import com.henriquelavor.sigpat.repository.GruposBens;
import com.henriquelavor.sigpat.repository.MarcasBens;
import com.henriquelavor.sigpat.repository.ModalidadesAquisicoesBens;
import com.henriquelavor.sigpat.repository.SubGruposBens;
import com.henriquelavor.sigpat.repository.TiposBens;
import com.henriquelavor.sigpat.repository.Transferencias;
import com.henriquelavor.sigpat.repository.UnidadesAdministrativas;
import com.henriquelavor.sigpat.repository.UnidadesGestoras;
import com.henriquelavor.sigpat.repository.FontesRecursos;
import com.henriquelavor.sigpat.service.CadastroBensMoveis;
import com.henriquelavor.sigpat.service.NegocioException;
import com.henriquelavor.sigpat.util.jsf.FacesUtil;

@Named
@ViewScoped
public class CadastroBemMovelBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CadastroBensMoveis cadastro;
	
	@Inject
	private Transferencias transferencias;  //beta

	@Inject
	private TiposBens tipos; // ok

	@Inject
	private MarcasBens marcas; // ok

	@Inject
	private EstadosBens estadosBens; // ok

	@Inject
	private GruposBens gruposBens; // ok

	@Inject
	private SubGruposBens subGruposBens; // ok

	@Inject
	private FontesRecursos fontesRecursos;// ok
	
	@Inject
	private UnidadesGestoras unidadesOrcamentarias;// beta2
	

	@Inject
	private Fornecedores fornecedores;// ok

	@Inject
	private ModalidadesAquisicoesBens modalidades;// ok

	@Inject
	private Funcionarios funcionariosResponsaveisPatrimoniais; // ok

	@Inject
	private UnidadesAdministrativas locaisOrigens;

	@Inject
	private UnidadesAdministrativas locaisDestinos;
	

	// usado para autocompletar sugerindo descricoes ja cadastradas
	@Inject
	private BensMoveis bensMoveis;

	public List<String> pesquisarDescricoes(String descricao) {
		return this.bensMoveis.descricoesQueContem(descricao);
	}

	private BemMovel bemMovel = new BemMovel();
	
	private List<Transferencia> todasTransferencias; //beta 

	private List<TipoBens> todosTipos;
	private List<MarcaBens> todasMarcas;
	private List<EstadoBens> todosEstadosBens;
	private List<GrupoBens> todosGruposBens;
	private List<SubGrupoBens> todosSubGruposBens;
	private List<FonteRecurso> todasFontesRecursos;
	
	private List<UnidadeGestora> todasUnidadesOrcamentarias; //beta2
	
	private List<Fornecedor> todosFornecedores;
	private List<ModalidadeAquisicaoBens> todasModalidades;

	private List<Funcionario> todosFuncionariosResponsaveisPatrimoniais;
	private List<UnidadeAdministrativa> todosLocaisOrigens;
	private List<UnidadeAdministrativa> todosLocaisDestinos;

	private List<SubGrupoBens> subGruposBensSelecionados;
	
	private List<SubGrupoBens> subGruposBensSelecionadosEdit;
	

	private UploadArquivo arquivo = new UploadArquivo();

	//pdf da nota fiscal
	public void uploadPdf(FileUploadEvent event) {
		this.arquivo.fileUpload(event, ".pdf", "/resources/enviados/bensmoveis/notasfiscais/");
		this.bemMovel.setNotaFiscalPdf(this.arquivo.getNome());
		this.arquivo.gravar(); // nova implementacao fileupload
		this.arquivo = new UploadArquivo(); // nova implementacao fileupload
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("Nota Fiscal enviada com sucesso!"));
	}
	
	//foto do bem móvel
	public void uploadBem(FileUploadEvent event) {
		this.arquivo.fileUpload(event, ".jpg", "/resources/enviados/bensmoveis/"); //atualizado 19.01.2017
		this.bemMovel.setFotoBemMovel(this.arquivo.getNome());
		this.arquivo.gravar(); // nova implementacao fileupload
		this.arquivo = new UploadArquivo(); // nova implementacao fileupload
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("Foto do Bem Patrimonial enviado com sucesso!"));
	}
	
	//foto capturada pela webcam do bem móvel
	public void photoCamAction(CaptureEvent captureEvent) {
		this.arquivo.oncaptureBemMovel(captureEvent);
		this.bemMovel.setFotoBemMovel(this.arquivo.getFilename() + ".jpg");
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("Foto captura e enviada com sucesso!"));
	}

	public void prepararCadastro() {
		
		this.todosTipos = this.tipos.todos();
		this.todasMarcas = this.marcas.todas();
		this.todosEstadosBens = this.estadosBens.todos();
		this.todosGruposBens = this.gruposBens.todos();
		this.todosSubGruposBens = this.subGruposBens.todos();
		this.todasFontesRecursos = this.fontesRecursos.todas();
		this.todasUnidadesOrcamentarias = this.unidadesOrcamentarias.todas(); //beta2 atualizado em 17.06.2019
		
		this.todosFornecedores = this.fornecedores.todos();
		this.todasModalidades = this.modalidades.todas();
		this.todosFuncionariosResponsaveisPatrimoniais = this.funcionariosResponsaveisPatrimoniais
				.todosResponsaveisBem();
		this.todosLocaisOrigens = this.locaisOrigens.todas();
		this.todosLocaisDestinos = this.locaisDestinos.todas();

		if (this.bemMovel == null) {
			this.bemMovel = new BemMovel();
		}
	}
	
     public void prepararCadastroEdit() {
		
		//beta
		this.todasTransferencias = this.transferencias.todasTrasferenciasPorNumeroPatrimonial(this.getBemMovel().getNumeroPatrimonio());
		
		this.todosTipos = this.tipos.todos();
		this.todasMarcas = this.marcas.todas();
		this.todosEstadosBens = this.estadosBens.todos();
		this.todosGruposBens = this.gruposBens.todos();
		this.todosSubGruposBens = this.subGruposBens.todos();
		this.todasFontesRecursos = this.fontesRecursos.todas();
		this.todasUnidadesOrcamentarias = this.unidadesOrcamentarias.todas(); //beta2
		this.todosFornecedores = this.fornecedores.todos();
		this.todasModalidades = this.modalidades.todas();
		this.todosFuncionariosResponsaveisPatrimoniais = this.funcionariosResponsaveisPatrimoniais
				.todosResponsaveisBem();
		this.todosLocaisOrigens = this.locaisOrigens.todas();
		this.todosLocaisDestinos = this.locaisDestinos.todas();

		if (this.bemMovel == null) {
			this.bemMovel = new BemMovel();
		}
	}
    
	
	//beta
	//public void prepararTransparencias() {
	//	this.todasTransferencias = this.transferencias.todasTrasferenciasPorNumeroPatrimonial(this.getBemMovel().getNumeroPatrimonio());
	//}
	
	public void prepararTiposBens() {
		this.todosTipos = this.tipos.todos();
	}

	public void prepararMarcas() {
		this.todasMarcas = this.marcas.todas();
	}

	public void prepararEstadosBens() {
		this.todosEstadosBens = this.estadosBens.todos();
	}

	public void prepararGruposBens() {
		this.todosGruposBens = this.gruposBens.todos();
	}

	public void prepararSubGruposBens() {
		this.todosSubGruposBens = this.subGruposBens.todos();
	}

	public void prepararFontesRecusos() {
		this.todasFontesRecursos = this.fontesRecursos.todas();
	}
	
	//beta 2
	public void prepararUnidadesOrcamentarias() {
		this.todasUnidadesOrcamentarias= this.unidadesOrcamentarias.todas();
	}
	

	public void prepararFornecedores() {
		this.todosFornecedores = this.fornecedores.todos();
	}

	public void prepararModalidades() {
		this.todasModalidades = this.modalidades.todas();
	}

	public void prepararFuncionariosResponsaveisPatrimoniais() {
		this.todosFuncionariosResponsaveisPatrimoniais = this.funcionariosResponsaveisPatrimoniais
				.todosResponsaveisBem();

	}

	public void prepararLocaisOrigens() {
		this.todosLocaisOrigens = this.locaisOrigens.todas();
	}

	public void prepararLocaisDestinos() {
		this.todosLocaisDestinos = this.locaisDestinos.todas();
	}

	// implementacao para subgrupo
	public void inicializar() {
		if (FacesUtil.isNotPostback()) {
			todosSubGruposBens = subGruposBens.todos();
		}
	}

	/*
	 * public void carregarSubGruposBens(AjaxBehaviorEvent event) { try {
	 * subGruposBensSelecionados =
	 * subGruposBens.gruposDe(bemMovel.getGrupoBem()); } catch (Exception e) {
	 * e.printStackTrace(); } }
	 */

	public void carregarSubGruposBens() {
		subGruposBensSelecionados = subGruposBens.gruposDe(bemMovel.getGrupoBem());
	}

	public List<SubGrupoBens> getSubGruposBensSelecionados() {
		return subGruposBensSelecionados;
	}
	
	
	public void carregarSubGruposBensEdit() {
		//subGruposBensSelecionadosEdit = subGruposBens.todos();
		subGruposBensSelecionados = subGruposBens.gruposDe(bemMovel.getGrupoBem());
	}

	public List<SubGrupoBens> getSubGruposBensSelecionadosEdit() {
		return subGruposBensSelecionadosEdit;
	}

	// fim
	
	//Salvar apenas um Bem e redirecionar para as Consultas
	public void salvarEsteBem() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
						
		
		try {
			//this.arquivo.gravar(); // nova implementacao fileupload ESTA FUNCAO ESTAVA DANDO MENSAGEM DE ERRO E RESOLVEU APENAS DESATIVANDO
			this.bemMovel.setCodigoOrcamentarioCadastroBem(codOrcamentarioSession); 
			this.cadastro.salvar(this.bemMovel);
			this.bemMovel = new BemMovel();
			this.arquivo = new UploadArquivo(); // nova implementacao fileupload
			context.addMessage(null, new FacesMessage("Registro salvo com sucesso!"));
			FacesContext.getCurrentInstance().getExternalContext().redirect("/sigpat/paginas/BemMovel/ConsultaBensMoveis.xhtml");
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}
	
	//Salvar Bem e deixar todos as informações pre-preenchidas, incrementando apenas o numero do patrimonio
	// e limpar o numero de série
		public void salvarRepetir() throws IOException {
			FacesContext context = FacesContext.getCurrentInstance();
			//Pega a sessao do Codigo UO
			HttpServletRequest req = (HttpServletRequest) 
			FacesContext.getCurrentInstance().getExternalContext().getRequest();
			
			HttpSession session = (HttpSession) req.getSession();
			String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");

			try {
				this.bemMovel.setCodigoOrcamentarioCadastroBem(codOrcamentarioSession);
				
				//this.arquivo.gravar(); // nova implementacao fileupload ESTA FUNCAO ESTAVA DANDO MENSAGEM DE ERRO E RESOLVEU APENAS DESATIVANDO
				this.cadastro.salvar(this.bemMovel);
				//this.bemMovel = new BemMovel();
				this.bemMovel.setNumeroSerie(""); //numero de serie sem valor
				long valorNumeroPatrimonio = Long.parseLong(this.bemMovel.getNumeroPatrimonio())+1; //incrementa + 1
				
				this.bemMovel.setNumeroPatrimonio(Long.toString(valorNumeroPatrimonio)); 
				this.arquivo = new UploadArquivo(); // nova implementacao fileupload
				context.addMessage(null, new FacesMessage("Registro salvo com sucesso!"));
				context.addMessage(null, new FacesMessage("Preenchimento do mesmo Bem disponível, informe apenas o número de série(se houver)!"));
				
				//FacesContext.getCurrentInstance().getExternalContext().redirect("/sigpat/paginas/BemMovel/ConsultaBensMoveis.xhtml");
			} catch (NegocioException e) {
				FacesMessage mensagem = new FacesMessage(e.getMessage());
				mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
				context.addMessage(null, mensagem);
			}

		}
		
	//Salvar apenas um Bem e redirecionar para NOVO cadastro do Bem 
	public void salvarInserirNovo() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");

		try {
			this.bemMovel.setCodigoOrcamentarioCadastroBem(codOrcamentarioSession);
			//this.arquivo.gravar(); // nova implementacao fileupload   ESTA FUNCAO ESTAVA DANDO MENSAGEM DE ERRO E RESOLVEU APENAS DESATIVANDO
			this.cadastro.salvar(this.bemMovel);
			this.bemMovel = new BemMovel();
			this.arquivo = new UploadArquivo(); // nova implementacao fileupload
			context.addMessage(null, new FacesMessage("Registro salvo com sucesso!"));
			FacesContext.getCurrentInstance().getExternalContext().redirect("/sigpat/paginas/BemMovel/CadastroBemMovel.xhtml");
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}

	}

	public void editar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			//this.arquivo.gravar(); // nova implementacao fileupload  ESTA FUNCAO ESTAVA DANDO MENSAGEM DE ERRO E RESOLVEU APENAS DESATIVANDO
			this.cadastro.editar(this.bemMovel);
			this.bemMovel = new BemMovel();
			this.arquivo = new UploadArquivo(); // nova implementacao fileupload
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("/sigpat/paginas/BemMovel/ConsultaBensMoveis.xhtml");
			context.addMessage(null, new FacesMessage("Registro Atualizado com sucesso!"));
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}
	
	//Editar do Administrador
	public void editarGeral() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			//this.arquivo.gravar(); // nova implementacao fileupload  ESTA FUNCAO ESTAVA DANDO MENSAGEM DE ERRO E RESOLVEU APENAS DESATIVANDO
			this.cadastro.editar(this.bemMovel);
			this.bemMovel = new BemMovel();
			this.arquivo = new UploadArquivo(); // nova implementacao fileupload
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("/sigpat/paginas/BemMovel/ConsultaBensMoveisGeral.xhtml");
			context.addMessage(null, new FacesMessage("Registro Atualizado com sucesso!"));
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}
	
	
	//Editar Bem com Garantia somente o Administrador 
		public void editarBemGarantia() throws IOException {
			FacesContext context = FacesContext.getCurrentInstance();
			try {
				//this.arquivo.gravar(); // nova implementacao fileupload  ESTA FUNCAO ESTAVA DANDO MENSAGEM DE ERRO E RESOLVEU APENAS DESATIVANDO
				this.cadastro.editar(this.bemMovel);
				this.bemMovel = new BemMovel();
				this.arquivo = new UploadArquivo(); // nova implementacao fileupload
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("/sigpat/paginas/BemMovel/ConsultaBensMoveisGeralGarantia.xhtml");
				context.addMessage(null, new FacesMessage("Registro Atualizado com sucesso!"));
			} catch (NegocioException e) {
				FacesMessage mensagem = new FacesMessage(e.getMessage());
				mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
				context.addMessage(null, mensagem);
			}
		}
		
	
	public void editarMesAtual() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			//this.arquivo.gravar(); // nova implementacao fileupload  ESTA FUNCAO ESTAVA DANDO MENSAGEM DE ERRO E RESOLVEU APENAS DESATIVANDO
			this.cadastro.editar(this.bemMovel);
			this.bemMovel = new BemMovel();
			this.arquivo = new UploadArquivo(); // nova implementacao fileupload
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("/sigpat/paginas/BemMovel/ConsultaBensMoveisMesAtual.xhtml");
			context.addMessage(null, new FacesMessage("Registro Atualizado com sucesso!"));
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}
	
	public void editarIncorporado() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			//this.arquivo.gravar(); // nova implementacao fileupload  ESTA FUNCAO ESTAVA DANDO MENSAGEM DE ERRO E RESOLVEU APENAS DESATIVANDO
			this.cadastro.editar(this.bemMovel);
			this.bemMovel = new BemMovel();
			this.arquivo = new UploadArquivo(); // nova implementacao fileupload
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("/sigpat/paginas/BemMovel/ConsultaBensMoveisIncorporados.xhtml");
			context.addMessage(null, new FacesMessage("Registro Atualizado com sucesso!"));
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}
	
	public void editarNaoIncorporado() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			//this.arquivo.gravar(); // nova implementacao fileupload  ESTA FUNCAO ESTAVA DANDO MENSAGEM DE ERRO E RESOLVEU APENAS DESATIVANDO
			this.cadastro.editar(this.bemMovel);
			this.bemMovel = new BemMovel();
			this.arquivo = new UploadArquivo(); // nova implementacao fileupload
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("/sigpat/paginas/BemMovel/ConsultaBensMoveisNaoIncorporados.xhtml");
			context.addMessage(null, new FacesMessage("Registro Atualizado com sucesso!"));
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}
	
	
	//beta
	public List<Transferencia> getTodasTransferencias() {
		return this.todasTransferencias= this.transferencias.todasTrasferenciasPorNumeroPatrimonial(this.getBemMovel().getNumeroPatrimonio());
	}

	
	public List<TipoBens> getTodosTipos() {
		return this.todosTipos;
	}

	public List<MarcaBens> getTodasMarcas() {
		return this.todasMarcas;
	}

	public List<EstadoBens> getTodosEstadosBens() {
		return this.todosEstadosBens;
	}

	public List<GrupoBens> getTodosGruposBens() {
		return this.todosGruposBens;
	}

	public List<SubGrupoBens> getTodosSubGruposBens() {
		return this.todosSubGruposBens;
	}

	public List<FonteRecurso> getTodasFontesRecursos() {
		return this.todasFontesRecursos;
	}
	
	//beta2
	public List<UnidadeGestora> getTodasUnidadesOrcamentarias() {
		return this.todasUnidadesOrcamentarias;
	}
	

	public List<Fornecedor> getTodosFornecedores() {
		return this.todosFornecedores;
	}

	public List<ModalidadeAquisicaoBens> getTodasModalidades() {
		return this.todasModalidades;
	}

	public List<Funcionario> getTodosFuncionariosResponsaveisPatrimoniais() {
		return this.todosFuncionariosResponsaveisPatrimoniais;
	}

	public List<UnidadeAdministrativa> getTodosLocaisOrigens() {
		return this.todosLocaisOrigens;
	}

	public List<UnidadeAdministrativa> getTodosLocaisDestinos() {
		return this.todosLocaisDestinos;
	}

	public BemMovel getBemMovel() {
		return bemMovel;
	}

	public void setBemMovel(BemMovel bemMovel) {
		this.bemMovel = bemMovel;
	}

	// Usado no TAB Wizard IDA
	public String onFlowProcess(FlowEvent event) {
		return event.getNewStep();
		
	}
}