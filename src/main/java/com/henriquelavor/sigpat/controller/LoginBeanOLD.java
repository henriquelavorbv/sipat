package com.henriquelavor.sigpat.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.controller.UsuarioNovo;

@Named
@RequestScoped
public class LoginBeanOLD implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	
	@Inject
	private UsuarioNovo usuario;
	private String nomeUsuario;
	private String senha;

	public String login() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		if ("admin".equals(this.nomeUsuario) && "admin".equals(this.senha)) {
			this.usuario.setNome(this.nomeUsuario);
			this.usuario.setDataLogin(new Date());
			//FacesContext.getCurrentInstance().getExternalContext().redirect("/sigpat/dashboard.xhtml");
			return "/dashboard?faces-redirect=true";
			
		} else {
			FacesMessage mensagem = new FacesMessage("Usuário/senha inválidos!");
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
		return null;
	}

	public void logout() throws IOException {
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		FacesContext.getCurrentInstance().getExternalContext().redirect("/sigpat/login.xhtml");
	}

	public String getNomeUsuario() {
		return nomeUsuario;
	}

	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	
}
