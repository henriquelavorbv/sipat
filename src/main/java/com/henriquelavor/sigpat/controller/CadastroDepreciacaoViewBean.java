package com.henriquelavor.sigpat.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.model.DepreciacaoView;
import com.henriquelavor.sigpat.repository.DepreciacoesView;

/**
 * @author Henrique Lavor
 * @data 16.07.2019
 * @version 1.0
 */

@Named
@ViewScoped
public class CadastroDepreciacaoViewBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private DepreciacoesView depreciacoesView;

	private DepreciacaoView depreciacaoView = new DepreciacaoView();
	
	private List<DepreciacaoView> todosBensMoveisDepreciadosView;


	public void prepararCadastro() {
		this.todosBensMoveisDepreciadosView = depreciacoesView.todasDepreciacoes();
	}
	

	public List<DepreciacaoView> getTodosBensMoveisDepreciados() {
		return this.todosBensMoveisDepreciadosView;
	}
	
	
	public DepreciacaoView getDepreciacaView() {
		return depreciacaoView;
	}

	public void setDepreciacaoView(DepreciacaoView depreciacaoView) {
		this.depreciacaoView = depreciacaoView;
	}
}