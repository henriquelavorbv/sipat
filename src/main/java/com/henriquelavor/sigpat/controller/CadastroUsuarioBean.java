package com.henriquelavor.sigpat.controller;

import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.model.Funcionario;
import com.henriquelavor.sigpat.model.Perfil;
import com.henriquelavor.sigpat.model.Usuario;
import com.henriquelavor.sigpat.repository.Funcionarios;
import com.henriquelavor.sigpat.repository.Perfis;
import com.henriquelavor.sigpat.repository.Usuarios;
import com.henriquelavor.sigpat.service.CadastroUsuarios;
import com.henriquelavor.sigpat.service.NegocioException;
import com.henriquelavor.sigpat.util.jsf.FacesUtil;
import com.henriquelavor.sigpat.util.mail.Mailer;
import com.outjected.email.api.MailMessage;

@Named
@ViewScoped
public class CadastroUsuarioBean implements Serializable {
	private static final long serialVersionUID = 1L;

	@Inject
	private CadastroUsuarios cadastro;

	@Inject
	private Usuarios usuarios;
	
	
	@Inject
	private Perfis perfis;
	
	@Inject
	private Funcionarios funcionarios;
	
	
	@Inject
	private Mailer mailer;
	
	private Usuario usuario = new Usuario();
	
	private List<Usuario> todosUsuarios;
	
	private List<Perfil> todosPerfis;
	
	private List<Funcionario> todosFucionarios;
	
	//private List<Funcionario> todosFucnionariosGeral;
	
	
	
	public void prepararCadastro() {
		this.todosUsuarios = usuarios.todos();
		
		this.todosFucionarios = funcionarios.todosDisponiveis();
		
		this.todosPerfis = perfis.todos();
		
		if (this.usuario == null) {
			this.usuario = new Usuario();
		}
	}
	
	
	public void prepararCadastroEdit() {
		this.todosUsuarios = usuarios.todos();
		
		this.todosFucionarios = funcionarios.todosGeral();
		
		this.todosPerfis = perfis.todos();
		
		if (this.usuario == null) {
			this.usuario = new Usuario();
		}
	}
	
	
	public void prepararUsuarios() {
		this.todosUsuarios = usuarios.todos();
		//this.todosFuncionarios = funcionarios.todos();
	}
	
	public void prepararPerfis() {
		this.todosPerfis= this.perfis.todos();
	}

	public void salvar() throws IOException{
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.salvar(this.usuario);
			
			if (this.usuario.getNotificarEmail()){
				this.enviarNotificaoEmail(); //enviar email informando abertura
			}
			
			this.usuario = new Usuario();
			context.addMessage(null, new FacesMessage("Registro Salvo com sucesso!"));
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public void editar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.editar(this.usuario);
			
			if (this.usuario.getNotificarEmail()){
				this.enviarNotificaoEmail(); //enviar email informando abertura
			}

			this.usuario = new Usuario();
			context.addMessage(null, new FacesMessage(
					"Registro Atualizado com sucesso!"));
			FacesContext.getCurrentInstance().getExternalContext()
			.redirect("/sigpat/paginas/Usuario/ConsultaUsuarios.xhtml");
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}
	
	public void enviarNotificaoEmail() throws IOException {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String dataCadastro = df.format(this.usuario.getDataCadastro());

		MailMessage message = mailer.prepararNovaMensagem();

		message.to(this.usuario.getEmail())
				.subject("SIGPAT: Notificação de Conta de Acesso ao Sistema Patrimonial")
				.bodyHtml("<strong>Unidade Gestora: </strong>"
						+ this.usuario.getFuncionario().getUnidadeAdministrativa().getUnidadeGestora().getNomeUnidadeGestora() + "("
						+ this.usuario.getFuncionario().getUnidadeAdministrativa().getUnidadeGestora().getNomeResumido() + ")"
						+ "<br/>" + "<strong>Unidade Administrativa: </strong>"
						+ this.usuario.getFuncionario().getUnidadeAdministrativa().getNomeUnidadeAdministrativa() + "("
						+ this.usuario.getFuncionario().getUnidadeAdministrativa().getNomeResumido() + ")" + "<br/>"
						+ "<br />" + "<br />"
						+ "<strong>Funcionário Cadastro para Acesso ao Sistema: </strong>"
						+ this.usuario.getFuncionario().getNomeFuncionario() + "<br />" + "<br />" 
						+ "<strong>DADOS DE ACESSO : </strong>"  + "<br />"
						+ "<strong>E-mail do usuário: </strong>" + this.usuario.getEmail() + "<br />"
						+ "<strong>Senha: </strong> <strong>" + this.usuario.getPasssword()+ "</strong> <br />"
						+ "<strong>Data da Cadastro: </strong>" + dataCadastro + "<br />"
						)
				.send();

		FacesUtil.addInfoMessage(
				"Notificação de Cadastro de Usuário para Acesso ao Sistema Patrimonial enviado por e-mail com sucesso!");
	}

	
	
	public List<Usuario> getTodosUsuarios() {
		return this.todosUsuarios;
	}
	
	public List<Perfil> getTodosPerfis() {
		return this.todosPerfis;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	
	public List<Funcionario> getTodosFucionarios() {
		return todosFucionarios;
	}
	

	public void setTodosFucionarios(List<Funcionario> todosFucionarios) {
		this.todosFucionarios = todosFucionarios;
	}

}