package com.henriquelavor.sigpat.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.model.MotivoBaixa;
import com.henriquelavor.sigpat.repository.MotivosBaixas;
import com.henriquelavor.sigpat.service.CadastroMotivosBaixas;
import com.henriquelavor.sigpat.service.NegocioException;

@Named
@ViewScoped
public class CadastroMotivoBaixaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CadastroMotivosBaixas cadastro;

	@Inject
	private MotivosBaixas motivosBaixas;

	private MotivoBaixa motivoBaixa = new MotivoBaixa();

	private List<MotivoBaixa> todosMotivosBaixas;

	public void prepararCadastro() {
		this.todosMotivosBaixas = motivosBaixas.todos();
		if (this.motivoBaixa == null) {
			this.motivoBaixa = new MotivoBaixa();
		}
	}

	public void salvar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.salvar(this.motivoBaixa);
			this.motivoBaixa = new MotivoBaixa();
			context.addMessage(null, new FacesMessage("Registro Salvo com sucesso!"));
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public void editar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.editar(this.motivoBaixa);

			this.motivoBaixa = new MotivoBaixa();
			context.addMessage(null, new FacesMessage("Registro Atualizado com sucesso!"));
			FacesContext.getCurrentInstance().getExternalContext()
			.redirect("/sigpat/paginas/MotivoBaixa/ConsultaMotivosBaixas.xhtml");
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}
	
	public List<MotivoBaixa> getTodosMotivosBaixas() {
		return this.todosMotivosBaixas;
	}

	public MotivoBaixa getMotivoBaixa() {
		return motivoBaixa;
	}

	public void setMotivoBaixa(MotivoBaixa motivoBaixa) {
		this.motivoBaixa = motivoBaixa;
	}
}