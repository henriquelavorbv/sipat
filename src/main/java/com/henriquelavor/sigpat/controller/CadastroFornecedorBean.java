package com.henriquelavor.sigpat.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FlowEvent;
import com.henriquelavor.sigpat.model.Fornecedor;
import com.henriquelavor.sigpat.model.ReferenciaFornecedor;
import com.henriquelavor.sigpat.model.TipoFornecedor;
import com.henriquelavor.sigpat.repository.ReferenciasFornecedores;
import com.henriquelavor.sigpat.repository.TiposFornecedores;
import com.henriquelavor.sigpat.service.CadastroFornecedores;
import com.henriquelavor.sigpat.service.NegocioException;

@Named
@ViewScoped
public class CadastroFornecedorBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CadastroFornecedores cadastro;

	@Inject
	private TiposFornecedores tiposFornecedores;

	@Inject
	private ReferenciasFornecedores referenciasFornecedores;


	private Fornecedor fornecedor = new Fornecedor();

	private List<TipoFornecedor> todosTiposFornecedores;

	private List<ReferenciaFornecedor> todasReferenciasFornecedores;


	public void prepararCadastro() {

		this.todosTiposFornecedores = this.tiposFornecedores.todos();
		this.todasReferenciasFornecedores = this.referenciasFornecedores.todas();
		if (this.fornecedor == null) {
			this.fornecedor = new Fornecedor();
		}
	}


	public void salvar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.cadastro.salvar(this.fornecedor);
			this.fornecedor = new Fornecedor();
			context.addMessage(null, new FacesMessage("Registro salvo com sucesso!"));
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}

	}

	public void editar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.editar(this.fornecedor);

			this.fornecedor = new Fornecedor();
			context.addMessage(null, new FacesMessage("Registro Atualizado com sucesso!"));
			FacesContext.getCurrentInstance().getExternalContext().redirect("/sigpat/paginas/Fornecedor/ConsultaFornecedores.xhtml");
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public List<TipoFornecedor> getTodosTiposFornecedores() {
		return this.todosTiposFornecedores;
	}

	public List<ReferenciaFornecedor> getTodasReferenciasFornecedores() {
		return this.todasReferenciasFornecedores;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	// novas implementações 10.06.2016 usado no TAB Wizard
	public String onFlowProcess(FlowEvent event) {
		return event.getNewStep();
	}


	public void executarJS() {
		RequestContext.getCurrentInstance().execute("checarJS();");
	}
}