package com.henriquelavor.sigpat.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.model.Perfil;
import com.henriquelavor.sigpat.repository.Perfis;
import com.henriquelavor.sigpat.service.CadastroPerfis;
import com.henriquelavor.sigpat.service.NegocioException;

@Named
@ViewScoped
public class CadastroPerfilBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CadastroPerfis cadastro;

	@Inject
	private Perfis perfis;

	private Perfil perfil = new Perfil();

	private List<Perfil> todosPerfis;

	public void prepararCadastro() {
		this.todosPerfis = perfis.todos();
		if (this.perfil == null) {
			this.perfil = new Perfil();
		}
	}

	public void salvar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.salvar(this.perfil);
			this.perfil = new Perfil();
			context.addMessage(null, new FacesMessage("Registro Salvo com sucesso!"));
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public void editar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.editar(this.perfil);

			this.perfil = new Perfil();
			context.addMessage(null, new FacesMessage("Registro Atualizado com sucesso!"));
			FacesContext.getCurrentInstance().getExternalContext().redirect("/sigpat/paginas/Perfil/ConsultaPerfis.xhtml");
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}
	
	public List<Perfil> getTodosPerfis() {
		return this.todosPerfis;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}
}