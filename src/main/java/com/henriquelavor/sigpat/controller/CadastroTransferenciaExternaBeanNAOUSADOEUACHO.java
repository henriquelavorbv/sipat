package com.henriquelavor.sigpat.controller;

import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import com.henriquelavor.sigpat.model.BemMovel;
import com.henriquelavor.sigpat.model.EstadoBens;
import com.henriquelavor.sigpat.model.Funcionario;
import com.henriquelavor.sigpat.model.TermoResponsavel;
import com.henriquelavor.sigpat.model.TipoTransferencia;
import com.henriquelavor.sigpat.model.Transferencia;
import com.henriquelavor.sigpat.model.UnidadeAdministrativa;
import com.henriquelavor.sigpat.repository.BensMoveis;
import com.henriquelavor.sigpat.repository.EstadosBens;
import com.henriquelavor.sigpat.repository.Funcionarios;
import com.henriquelavor.sigpat.repository.TiposTransferencias;
import com.henriquelavor.sigpat.repository.Transferencias;
import com.henriquelavor.sigpat.repository.UnidadesAdministrativas;
import com.henriquelavor.sigpat.service.CadastroTransferencias;
import com.henriquelavor.sigpat.service.NegocioException;
import com.henriquelavor.sigpat.util.jsf.FacesUtil;
import com.henriquelavor.sigpat.util.mail.Mailer;
import com.outjected.email.api.MailMessage;

/**
 * @author Henrique Lavor
 * @data 15.08.2017
 * @version 1.0
 */

@Named
@ViewScoped
public class CadastroTransferenciaExternaBeanNAOUSADOEUACHO implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CadastroTransferencias cadastro;

	@Inject
	private TiposTransferencias tiposTransferencias;

	@Inject
	private BensMoveis bensMoveis;

	@Inject
	private UnidadesAdministrativas locaisOrigens;

	@Inject
	private UnidadesAdministrativas locaisDestinos;

	@Inject
	private Funcionarios funcionariosResponsaveis;

	@Inject
	private EstadosBens estadosBens;

	@Inject
	private Transferencias transferencias;
	

	// nova implementacao para autocomplete
	public List<String> pesquisarNumeroTermoTransferencia(String descricao) {
		return this.transferencias.numeroTermoTransferenciaTEBQueContem(descricao);
	}

	private Transferencia transferencia = new Transferencia();

	private String numeroTermoTransferenciaAtual;
	private List<TipoTransferencia> todosTiposTransferencias;

	private List<BemMovel> todosBensMoveis;
	
	private List<UnidadeAdministrativa> todosLocaisOrigens;
	private List<UnidadeAdministrativa> todosLocaisDestinos;
	private List<Funcionario> todosFuncionariosResponsaveis;
	private List<EstadoBens> todosEstadosBens;
	private List<Transferencia> todasTransferencias;

	public void prepararCadastro() {
		this.todasTransferencias = transferencias.todas();
		this.todosTiposTransferencias = this.tiposTransferencias.todos();
		this.todosBensMoveis = this.bensMoveis.todosIncorporados(); // apenas todos os bens ativos e que estao aptos a transferir																// incorporados
		this.todosLocaisOrigens = this.locaisOrigens.todas(); // ver depois possibilidade e necessidade de filtrar excluindo da lista a  unidade adm 999
		this.todosLocaisDestinos = this.locaisDestinos.todas();
		this.todosFuncionariosResponsaveis = this.funcionariosResponsaveis.todosResponsaveisBem();
		this.todosEstadosBens = this.estadosBens.todos();

		if (this.transferencia == null) {
			this.transferencia = new Transferencia();
			  //Checa se Unidade Gestora de Origem é diferente da Unidade Gestora de Destino
			 Date datatual = new Date();
			 this.transferencia.setNumeroTermoTransferencia( "TIB" + Long.toString(datatual.getTime()) + datatual.getYear());
			
			 //this.transferencia.setTipoTransferencia();
		}
		
	}
	
	
	public void prepararCadastroDOCTEB() {
		this.todasTransferencias = transferencias.todas();
		this.todosTiposTransferencias = this.tiposTransferencias.todos();
		this.todosBensMoveis = this.bensMoveis.todosIncorporados(); // apenas todos os bens ativos e que estao aptos a transferir																// incorporados
		this.todosLocaisOrigens = this.locaisOrigens.todas(); // ver depois possibilidade e necessidade de filtrar excluindo da lista a  unidade adm 999
		this.todosLocaisDestinos = this.locaisDestinos.todas();
		this.todosFuncionariosResponsaveis = this.funcionariosResponsaveis.todosResponsaveisBem();
		this.todosEstadosBens = this.estadosBens.todos();

		if (this.transferencia == null) {
			this.transferencia = new Transferencia();
			
		  //Checa se Unidade Gestora de Origem é diferente da Unidade Gestora de Destino
				 Date datatual = new Date();
				 this.transferencia.setNumeroTermoTransferencia( "TEB" + Long.toString(datatual.getTime()) + datatual.getYear());
				 
		}
	}
	
	
	

	public void prepararTiposTransferencias() {
		this.todosTiposTransferencias = this.tiposTransferencias.todos();
	}

	public void prepararBensMoveis() {
		this.todosBensMoveis = this.bensMoveis.todosIncorporados();
	}

	public void prepararBensMoveis(AjaxBehaviorEvent event) throws Exception {
		if (event.getComponent().getAttributes().get("value").toString().trim().equalsIgnoreCase("numeroPatrimonio")) {
			this.todosBensMoveis = this.bensMoveis.todosIncorporados();
		}
	}

	public void prepararLocaisOrigens() {
		this.todosLocaisOrigens = this.locaisOrigens.todas();
	}

	public void prepararLocaisDestinos() {
		this.todosLocaisDestinos = this.locaisDestinos.todas();
	}

	public void prepararFuncionariosResponsaveisPatrimoniais() {
		this.todosFuncionariosResponsaveis = this.funcionariosResponsaveis.todosResponsaveisBem();

	}

	public void prepararEstadosBens() {
		this.todosEstadosBens = this.estadosBens.todos();
	}

	public void prepararTransferencias() {
		this.todasTransferencias = transferencias.todas();
	}

	public void salvarGerarTermoTransferencia() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();

		numeroTermoTransferenciaAtual = this.transferencia.getNumeroTermoTransferencia();

		try {
			this.transferencia.setLocalOrigem(this.transferencia.getBemMovel().getLocalDestino()); //o pulo do gato para setar valores indiretamente!! TOP SHOW DE BOLA
			this.transferencia.setAntigoFuncionarioResponsavelPatrimonial(this.transferencia.getBemMovel().getNovoFuncionarioResponsavelPatrimonial()); 
			
			this.cadastro.salvar(this.transferencia);
			
			this.cadastro.gerarTermos();
			
			if (this.transferencia.getNotificarEmail()){
				this.enviarNotificacaoResponsaveisTransferencia(); //enviar email aos responsaveis(saída e entrada) pela transferencia
			}
			
			this.transferencia = new Transferencia(); //limpa formulario e cria novo objeto vazio, pronto para nova digitacao
			
			context.addMessage(null, new FacesMessage("Transferência Gerada com sucesso!"));

			if (numeroTermoTransferenciaAtual != null) {
				HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
						.getRequest();
				request.getSession().setAttribute("numeroTermoTransferenciaAtual", this.numeroTermoTransferenciaAtual);
			}
			FacesContext.getCurrentInstance().getExternalContext().redirect("/sigpat/paginas/Relatorios/RelatorioTermoTransferenciaAuto.xhtml");
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public void salvarRepetir() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.transferencia.setLocalOrigem(this.transferencia.getBemMovel().getLocalDestino()); //o pulo do gato para setar valores indiretamente!! TOP SHOW DE BOLA
			this.transferencia.setAntigoFuncionarioResponsavelPatrimonial(this.transferencia.getBemMovel().getNovoFuncionarioResponsavelPatrimonial()); //o pulo do gato para setar valores indiretamente!! TOP SHOW DE BOLA
			
			this.cadastro.salvar(this.transferencia);
			this.transferencia.setCodigoTransferencia(null); // código do termo de transferencia sem valor
			this.transferencias.todosTermosPorNumeroTranferencia(this.getTransferencia().getNumeroTermoTransferencia());
			
			if (this.transferencia.getNotificarEmail()){
				this.enviarNotificacaoResponsaveisTransferencia(); //enviar email aos responsaveis(saída e entrada) pela transferencia
			}

			context.addMessage(null, new FacesMessage("Registro salvo com sucesso!"));
			context.addMessage(null, new FacesMessage(
					"Preenchimento do mesmo Termo de Transferência disponível, informe  um outro Bem!"));

		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}

	}
	
	
	@Inject
	private Mailer mailer;
	
	//enviar email ao novo responsavel pela Entrada do bem ao setor
	public void enviarNotificacaoResponsaveisTransferencia(){
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String dataTransferencia = df.format(this.transferencia.getDataTransferencia());
		String dataTransferenciaCadastrada = df.format(this.transferencia.getDataCadastro());
		
		MailMessage message = mailer.prepararNovaMensagem();
		message.to(this.transferencia.getNovoFuncionarioResponsavelPatrimonial().getEmail(),this.transferencia.getAntigoFuncionarioResponsavelPatrimonial().getEmail())
		.subject("SIGPAT: Notificação de Transferência de BEM Patrimonial Nº.: " + this.transferencia.getNumeroTermoTransferencia())
		.bodyHtml("<strong>Data da transferência: </strong>" + dataTransferencia +"<br />" + 
		"<strong>Data de registro no sistema: </strong>" + dataTransferenciaCadastrada +"<br />" +"<br />" +
				
	    "<strong>Tipo de Transferência: </strong>" + this.transferencia.getTipoTransferencia().getDescricaoTipoTransferencia() +"<br />" +"<br />" +
	    
 		"<strong>No. Patrimônio: </strong>" + this.transferencia.getBemMovel().getNumeroPatrimonio() +"<br />" +
 		"<strong>Bem Patrimonial: </strong>" + this.transferencia.getBemMovel().getDescricao() +"<br />" +
 		"<strong>Novo Estado de Conservação do Bem: </strong>" + this.transferencia.getEstadoBem().getDescricaoEstadoBem() +"<br />" +"<br />" + 
 		
		"<strong>Unidade Gestora de Origem: </strong>" + this.transferencia.getLocalOrigem().getUnidadeGestora().getNomeUnidadeGestora() +"("+ this.transferencia.getLocalOrigem().getUnidadeGestora().getNomeResumido()+")"+"<br/>" +
		"<strong>Unidade Administrativa de Origem: </strong>" + this.transferencia.getLocalOrigem().getNomeUnidadeAdministrativa() +"("+ this.transferencia.getLocalOrigem().getNomeResumido()+")" +"<br />" +
		
		"<strong>Funcionário Responsável Saída: </strong>" + this.transferencia.getAntigoFuncionarioResponsavelPatrimonial().getNomeFuncionario() +"<br />" +
		"<strong>Matrícula do Funcionário Receptor/Responsável: </strong>" + this.transferencia.getAntigoFuncionarioResponsavelPatrimonial().getMatricula() +"<br />" +
		"<strong>Cargo: </strong>" + this.transferencia.getAntigoFuncionarioResponsavelPatrimonial().getCargo().getDescricaoCargo() +"<br />" +
		"<strong>Lotação: </strong>" + this.transferencia.getAntigoFuncionarioResponsavelPatrimonial().getUnidadeAdministrativa().getUnidadeGestora().getNomeUnidadeGestora()  +"("+ this.transferencia.getAntigoFuncionarioResponsavelPatrimonial().getUnidadeAdministrativa().getNomeUnidadeAdministrativa() +")" + "<br />" + "<br />" +
		
		"<strong>Unidade Gestora de Destino: </strong>" + this.transferencia.getLocalDestino().getUnidadeGestora().getNomeUnidadeGestora() +"("+ this.transferencia.getLocalDestino().getUnidadeGestora().getNomeResumido()+")"+"<br/>" +
		"<strong>Unidade Administrativa de Destino: </strong>" + this.transferencia.getLocalDestino().getNomeUnidadeAdministrativa() +"("+ this.transferencia.getLocalDestino().getNomeResumido()+")"+"<br/>" +
		
		"<strong>Funcionário Responsável Receptor </strong>" + this.transferencia.getNovoFuncionarioResponsavelPatrimonial().getNomeFuncionario() +"<br />" +
		"<strong>Matrícula do Funcionário Receptor/Responsável: </strong>" + this.transferencia.getNovoFuncionarioResponsavelPatrimonial().getMatricula() +"<br />" +
		"<strong>Cargo: </strong>" + this.transferencia.getNovoFuncionarioResponsavelPatrimonial().getCargo().getDescricaoCargo() +"<br />" +
		"<strong>Lotação: </strong>" + this.transferencia.getNovoFuncionarioResponsavelPatrimonial().getUnidadeAdministrativa().getUnidadeGestora().getNomeUnidadeGestora()  +"("+ this.transferencia.getNovoFuncionarioResponsavelPatrimonial().getUnidadeAdministrativa().getNomeUnidadeAdministrativa() +")" + "<br />" + "<br />" +

		"<strong>Observação: </strong>" + this.transferencia.getObservacao() +"<br />" +"<br />" +
		
		"<strong> Caro Responsável, este email, tem como finalidade, informá-lo que, você tem de posse um documento oficial impresso e "
		+ "assinado do Termo de Transferência No. " + this.transferencia.getNumeroTermoTransferencia()
		+ ", guarde-o em um local seguro, para futuros levantamentos/inventários patrimoniais de sua unidade gestora/administrativa."
		+ "   </strong>" +"<br />")
		.send();
		
		FacesUtil.addInfoMessage("Notificação de Transferência de Bem patrimonial, enviado por e-mail aos responsáveis com sucesso!");
	}

	
	public List<TipoTransferencia> getTodosTiposTransferencias() {
		return this.todosTiposTransferencias;
	}

	public List<BemMovel> getTodosBensMoveis() {
		return this.todosBensMoveis;
	}

	public List<UnidadeAdministrativa> getTodosLocaisOrigens() {
		return this.todosLocaisOrigens;
	}

	public List<UnidadeAdministrativa> getTodosLocaisDestinos() {
		return this.todosLocaisDestinos;
	}

	public List<Funcionario> getTodosFuncionariosResponsaveis() {
		return this.todosFuncionariosResponsaveis;
	}

	public List<Transferencia> getTodasTransferencias() {
		return this.todasTransferencias;
	}

	public List<EstadoBens> getTodosEstadosBens() {
		return this.todosEstadosBens;
	}

	public Transferencia getTransferencia() {
		return transferencia;
	}

	public void setTransferencia(Transferencia transferencia) {
		this.transferencia = transferencia;
	}
}