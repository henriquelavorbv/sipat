package com.henriquelavor.sigpat.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.model.GrupoBens;
import com.henriquelavor.sigpat.repository.GruposBens;
import com.henriquelavor.sigpat.service.CadastroGruposBens;
import com.henriquelavor.sigpat.service.NegocioException;

@Named
@ViewScoped
public class CadastroGrupoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CadastroGruposBens cadastro;

	@Inject
	private GruposBens gruposBens;

	private GrupoBens grupoBens = new GrupoBens();

	private List<GrupoBens> todosGruposBens;

	public void prepararCadastro() {
		this.todosGruposBens = gruposBens.todos();
		if (this.grupoBens == null) {
			this.grupoBens = new GrupoBens();
		}
	}

	public void salvar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.salvar(this.grupoBens);
			this.grupoBens = new GrupoBens();
			context.addMessage(null, new FacesMessage("Registro Salvo com sucesso!"));
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public void editar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.editar(this.grupoBens);

			this.grupoBens = new GrupoBens();
			context.addMessage(null, new FacesMessage("Registro Atualizado com sucesso!"));
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("/sigpat/paginas/GrupoBem/ConsultaGruposBens.xhtml");
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public List<GrupoBens> getTodosGruposBens() {
		return this.todosGruposBens;
	}

	public GrupoBens getGrupoBens() {
		return grupoBens;
	}

	public void setGrupoBens(GrupoBens grupoBens) {
		this.grupoBens = grupoBens;
	}
}