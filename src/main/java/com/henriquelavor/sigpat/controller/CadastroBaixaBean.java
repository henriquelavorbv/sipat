package com.henriquelavor.sigpat.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.FlowEvent;

import com.henriquelavor.sigpat.model.BemMovel;
import com.henriquelavor.sigpat.model.MotivoBaixa;
//import com.henriquelavor.sigpat.model.UnidadeGestora;
import com.henriquelavor.sigpat.model.Baixa;
import com.henriquelavor.sigpat.repository.BensMoveis;
import com.henriquelavor.sigpat.repository.MotivosBaixas;
import com.henriquelavor.sigpat.repository.UnidadesGestoras;
import com.henriquelavor.sigpat.repository.Baixas;
import com.henriquelavor.sigpat.service.CadastroBaixas;
import com.henriquelavor.sigpat.service.NegocioException;

/**
 * @author Henrique Lavor
 * @data 26.08.2017
 * @version 1.0
 */

@Named
@ViewScoped
public class CadastroBaixaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CadastroBaixas cadastro;

	@Inject
	private MotivosBaixas motivosBaixas;
	
	@Inject
	private UnidadesGestoras unidadesGestoras;

	@Inject
	private BensMoveis bensMoveis;

	@Inject
	private Baixas baixas;

	// nova implementacao
	public List<String> pesquisarNumeroTermoBaixa(String descricao) {
		return this.baixas.numeroTermoBaixaQueContem(descricao);
	}

	private Baixa baixa = new Baixa();

	private String numeroTermoBaixaAtual;

	private List<Baixa> todasBaixas;
	private List<MotivoBaixa> todosMotivosBaixa;
	//private List<UnidadeGestora> todasUnidadesGestoras;
	
	private List<BemMovel> todosBensMoveis;

	private UploadArquivo arquivo = new UploadArquivo();

	// pdf da Apólice de seguro
	public void uploadPdf(FileUploadEvent event) {
		this.arquivo.fileUpload(event, ".pdf", "/resources/enviados/bensmoveisbaixados/processos/");
		this.baixa.setProcessoPdf(this.arquivo.getNome());
		this.arquivo.gravar(); // nova implementacao fileupload
		this.arquivo = new UploadArquivo(); // nova implementacao fileupload
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("Processo de Baixa de Bem enviada com sucesso!"));
	}

	// foto(s) do bem móvel
	public void uploadBemFoto(FileUploadEvent event) {
		this.arquivo.fileUpload(event, ".png", "/resources/enviados/bensmoveisbaixados/");
		
		
		if (this.baixa.getFoto1() == "semfotobem.gif") {
			this.baixa.setFoto1(this.arquivo.getNome());
		}else{
			if (this.baixa.getFoto1() != "semfotobem.gif" && this.baixa.getFoto2() == "semfotobem.gif") {
				this.baixa.setFoto2(this.arquivo.getNome());
			}else{
				if (this.baixa.getFoto1() != "semfotobem.gif" && this.baixa.getFoto2() != "semfotobem.gif" && this.baixa.getFoto3() == "semfotobem.gif") {
					this.baixa.setFoto3(this.arquivo.getNome());
				}
			}
		}
		this.arquivo.gravar();
		this.arquivo = new UploadArquivo();
		
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("Foto do Bem Patrimonial enviado com sucesso!"));
	}

	public void prepararCadastro() {
		this.todasBaixas = baixas.todas();
		this.todosMotivosBaixa = this.motivosBaixas.todos();
		
		//this.todasUnidadesGestoras = this.unidadesGestoras.todas();
		
		this.todosBensMoveis = this.bensMoveis.todoAtivos();
		

		if (this.baixa == null) {
			this.baixa = new Baixa();
			 Date datatual = new Date();
			 this.baixa.setNumeroTermoBaixa("BX" + Long.toString(datatual.getTime()) + datatual.getYear());
		}
	}

	public void prepararBaixas() {
		this.todasBaixas = baixas.todas();
	}

	public void prepararMotivosBaixas() {
		this.todosMotivosBaixa = this.motivosBaixas.todos();

	}
	
	//NOVA IMPLEMENTACAO
	//public void prepararUnidadesGestoras() {
	//	this.todasUnidadesGestoras = this.unidadesGestoras.todas();
	//}

	public void prepararBensMoveis() {
		this.todosBensMoveis = this.bensMoveis.todoAtivos();
	}

	public void prepararBensMoveis(AjaxBehaviorEvent event) throws Exception {
		if (event.getComponent().getAttributes().get("value").toString().trim().equalsIgnoreCase("numeroPatrimonio")) {
			this.todosBensMoveis = this.bensMoveis.todos(); 
		}
	}

	public void salvarGerarTermo() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();

		numeroTermoBaixaAtual = this.baixa.getNumeroTermoBaixa();

		try {
			// this.termoResponsavel.setSituacao("Gerado"); //informa o termo
			// foi encerrado e gerado, nao sendo possiveil usa-lo
			this.cadastro.salvar(this.baixa);
			this.cadastro.gerarBaixas();
			this.arquivo = new UploadArquivo();
			context.addMessage(null, new FacesMessage("Termo de Baixa Gerado com sucesso!"));

			if (numeroTermoBaixaAtual != null) {
				HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
						.getRequest();
				request.getSession().setAttribute("numeroTermoBaixaAtual", this.numeroTermoBaixaAtual); //cria uma sessão 
				
			}

			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("/sigpat/paginas/Relatorios/RelatorioTermoBaixaAuto.xhtml");
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}

	}

	public void salvarRepetir() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		
		try {
			this.cadastro.salvar(this.baixa);
			this.baixa.setCodigoBaixa(null); // código do termo de baixa sem
												// valor
			this.baixa.setValorBaixa(null);
			this.baixa.setObservacao(null);
			this.baixa.setFoto1("semfotobem.gif");
			this.baixa.setFoto2("semfotobem.gif");
			this.baixa.setFoto3("semfotobem.gif");
			
			this.baixas.todasBaixasPorNumero(this.getBaixa().getNumeroTermoBaixa());
			this.arquivo = new UploadArquivo();
			context.addMessage(null, new FacesMessage("Registro salvo com sucesso!"));
			context.addMessage(null,
					new FacesMessage("Preenchimento do mesmo Termo de Baixa disponível, informe o próximo Bem."));

		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}

	}

	// usado no TAB Wizard
	public String onFlowProcess(FlowEvent event) {
		return event.getNewStep();
	}

	public List<Baixa> getTodasBaixas() {
		return this.todasBaixas;
	}

	public List<MotivoBaixa> getTodosMotivosBaixa() {
		return this.todosMotivosBaixa;
	}
	
	//nova implementacao
	//public List<UnidadeGestora> getTodasUnidadesGestoras() {
	//	return this.todasUnidadesGestoras;
	//}
	

	public List<BemMovel> getTodosBensMoveis() {
		return this.todosBensMoveis;
	}

	public Baixa getBaixa() {
		return baixa;
	}

	public void setBaixa(Baixa baixa) {
		this.baixa = baixa;
	}

}