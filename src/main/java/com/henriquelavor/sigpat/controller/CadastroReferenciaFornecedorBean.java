package com.henriquelavor.sigpat.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.model.ReferenciaFornecedor;
import com.henriquelavor.sigpat.model.RegraFornecedor;
import com.henriquelavor.sigpat.repository.ReferenciasFornecedores;
import com.henriquelavor.sigpat.service.CadastroReferenciasFornecedores;
import com.henriquelavor.sigpat.service.NegocioException;

@Named
@ViewScoped
public class CadastroReferenciaFornecedorBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CadastroReferenciasFornecedores cadastro;

	@Inject
	private ReferenciasFornecedores referenciasFornecedores;

	private ReferenciaFornecedor referenciaFornecedor = new ReferenciaFornecedor();
	
	private List<ReferenciaFornecedor> todasReferenciasFornecedores;

	public void prepararCadastro() {
		this.todasReferenciasFornecedores = referenciasFornecedores.todas();
		if (this.referenciaFornecedor == null) {
			this.referenciaFornecedor = new ReferenciaFornecedor();
		}
	}

	public void salvar() throws IOException{
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.salvar(this.referenciaFornecedor);
			this.referenciaFornecedor = new ReferenciaFornecedor();
			context.addMessage(null, new FacesMessage("Registro Salvo com sucesso!"));
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public void editar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.editar(this.referenciaFornecedor);

			this.referenciaFornecedor = new ReferenciaFornecedor();
			context.addMessage(null, new FacesMessage(
					"Registro Atualizado com sucesso!"));
			FacesContext.getCurrentInstance().getExternalContext()
			.redirect("/sigpat/paginas/ReferenciaFornecedor/ConsultaReferenciasFornecedores.xhtml");
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}
	
	public RegraFornecedor[] getRegrasForncedores() {
		return RegraFornecedor.values();
	}
	public List<ReferenciaFornecedor> getTodasReferenciasFornecedores() {
		return this.todasReferenciasFornecedores;
	}

	public ReferenciaFornecedor getReferenciaFornecedor() {
		return referenciaFornecedor;
	}

	public void setReferenciaFornecedor(ReferenciaFornecedor referenciaFornecedor) {
		this.referenciaFornecedor = referenciaFornecedor;
	}
}