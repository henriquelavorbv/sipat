package com.henriquelavor.sigpat.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.model.Divergencia;
import com.henriquelavor.sigpat.repository.Divergencias;
import com.henriquelavor.sigpat.service.CadastroDivergencias;
import com.henriquelavor.sigpat.service.NegocioException;

/**
 * @author Henrique Lavor
 * @data 25.09.2017
 * @version 1.0
 */

@Named
@ViewScoped
public class CadastroDivergenciaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CadastroDivergencias cadastro;

	@Inject
	private Divergencias divergencias;

	private Divergencia divergencia = new Divergencia();

	private List<Divergencia> todasDivergencias;

	private List<Divergencia> todasDivergenciasApenas;


	public void prepararCadastro() {
		this.todasDivergencias = divergencias.todas();
		if (this.divergencia == null) {
			this.divergencia = new Divergencia();
		}
	}
	
	public void prepararListaDivergencias() {
		this.todasDivergenciasApenas = divergencias.todasDivergencias();
	}

	public void salvar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.salvar(this.divergencia);
			this.divergencia = new Divergencia();
			context.addMessage(null, new FacesMessage("Registro Salvo com sucesso!"));
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public void editar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.editar(this.divergencia);

			this.divergencia = new Divergencia();
			context.addMessage(null, new FacesMessage("Registro Atualizado com sucesso!"));
			FacesContext.getCurrentInstance().getExternalContext().redirect("/sigpat/paginas/Divergencia/ConsultaDivergencias.xhtml");
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}
	
	public List<Divergencia> getTodasDivergencias() {
		return this.todasDivergencias;
	}
	
	public List<Divergencia> getTodasDivergenciasApenas() {
		return this.todasDivergenciasApenas;
	}

	public Divergencia getDivergencia() {
		return divergencia;
	}

	public void setDivergencia(Divergencia divergencia) {
		this.divergencia = divergencia;
	}
}