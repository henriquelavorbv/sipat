package com.henriquelavor.sigpat.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.henriquelavor.sigpat.model.EstadoBens;
import com.henriquelavor.sigpat.model.Vistoria;
import com.henriquelavor.sigpat.model.BemMovel;
import com.henriquelavor.sigpat.model.Divergencia;
import com.henriquelavor.sigpat.model.Inventario;
import com.henriquelavor.sigpat.repository.BensMoveis;
import com.henriquelavor.sigpat.repository.Divergencias;
import com.henriquelavor.sigpat.repository.EstadosBens;
import com.henriquelavor.sigpat.repository.Inventarios;
import com.henriquelavor.sigpat.service.CadastroVistorias;
import com.henriquelavor.sigpat.service.NegocioException;

/**
 * @author Henrique Lavor
 * @data 26.09.2017
 * @version 1.0
 */

@Named
@ViewScoped
public class CadastroVistoriaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CadastroVistorias cadastro;

	@Inject
	private Inventarios inventarios;

	@Inject
	private BensMoveis bensMoveis;

	@Inject
	private EstadosBens estadosBens;

	@Inject
	private Divergencias divergencias;

	private Vistoria vistoria = new Vistoria();

	private List<Inventario> todosInventarios; //abertos
	
	private List<Inventario> todosInventariosEncerrados; //encerrados
	
	private List<BemMovel> todosBensMoveis; // ok

	private List<EstadoBens> todosEstadosBens;

	private List<Divergencia> todasDivergencias;

	public void prepararCadastro() {
		this.todosInventarios = this.inventarios.todosAberto();
		this.todosInventariosEncerrados = this.inventarios.todosEncerrados();
		
		this.todosBensMoveis = this.bensMoveis.todoAtivos();
		this.todosEstadosBens = this.estadosBens.todos();
		this.todasDivergencias = this.divergencias.todas();
		

		if (this.vistoria == null) {
			this.vistoria = new Vistoria();
		}
	}
	
	public void prepararBens() {
		this.todosInventarios = this.inventarios.todosAberto();
		this.todosInventariosEncerrados = this.inventarios.todosEncerrados();
		this.todosBensMoveis = this.bensMoveis.todoAtivos();
		this.todosEstadosBens = this.estadosBens.todos();
		this.todasDivergencias = this.divergencias.todas();
	}

	public void prepararEstadosBens() {
		this.todosEstadosBens = this.estadosBens.todos();
	}

	public void prepararDivergencias() {
		this.todasDivergencias = this.divergencias.todas();
	}

	public void salvar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.cadastro.salvar(this.vistoria);
			this.vistoria = new Vistoria();
			context.addMessage(null,
					new FacesMessage("Registro de Vistoria de Bem Patrimonial Adicionado com Sucesso!"));
			FacesContext.getCurrentInstance().getExternalContext()
			.redirect("/sigpat/paginas/Inventario/ConsultaInventarios.xhtml");
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public void salvarRepetir() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.cadastro.salvar(this.vistoria);
			this.vistoria.setCodigoVistoria(null); 
													
			this.vistoria.setBemMovel(null);
			this.vistoria.setEstadoBem(null);
			this.vistoria.setDivergencia(null);
			this.vistoria.setObservacao(null);

			context.addMessage(null,
					new FacesMessage("Registro de Vistoria de Bem Patrimonial Adicionado com Sucesso!"));
			context.addMessage(null,
					new FacesMessage("Caso necessário continue vistoriando outros bens desta unidade!"));
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public void editar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.editar(this.vistoria);
			this.vistoria = new Vistoria();
			context.addMessage(null,
					new FacesMessage("Registro de Vistoria de Bem Patrimonial Atualizado com sucesso!"));
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("/sigpat/paginas/Vistoria/ConsultaVistorias.xhtml");
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public List<Inventario> getTodosInventarios() {
		return this.todosInventarios;
	}
	
	public List<Inventario> getTodosInventariosEncerrados() {
		return this.todosInventariosEncerrados;
	}

	public List<BemMovel> getTodosBensMoveis() {
		return this.todosBensMoveis;
	}

	public List<EstadoBens> getTodosEstadosBens() {
		return this.todosEstadosBens;
	}

	public List<Divergencia> getTodasDivergencias() {
		return this.todasDivergencias;
	}

	public Vistoria getVistoria() {
		return vistoria;
	}

	public void setVistoria(Vistoria vistoria) {
		this.vistoria = vistoria;
	}

}