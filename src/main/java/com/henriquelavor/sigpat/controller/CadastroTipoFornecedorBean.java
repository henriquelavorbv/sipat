package com.henriquelavor.sigpat.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.model.TipoFornecedor;
import com.henriquelavor.sigpat.repository.TiposFornecedores;
import com.henriquelavor.sigpat.service.CadastroTiposFornecedores;
import com.henriquelavor.sigpat.service.NegocioException;

@Named
@ViewScoped
public class CadastroTipoFornecedorBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CadastroTiposFornecedores cadastro;

	@Inject
	private TiposFornecedores tiposFornecedores;

	private TipoFornecedor tipoFornecedor = new TipoFornecedor();
	
	private List<TipoFornecedor> todosTiposFornecedores;

	public void prepararCadastro() {
		this.todosTiposFornecedores = tiposFornecedores.todos();
		if (this.tipoFornecedor == null) {
			this.tipoFornecedor = new TipoFornecedor();
		}
	}

	public void salvar() throws IOException{
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.salvar(this.tipoFornecedor);
			this.tipoFornecedor = new TipoFornecedor();
			context.addMessage(null, new FacesMessage("Registro Salvo com sucesso!"));
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public void editar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.editar(this.tipoFornecedor);

			this.tipoFornecedor = new TipoFornecedor();
			context.addMessage(null, new FacesMessage(
					"Registro Atualizado com sucesso!"));
			FacesContext.getCurrentInstance().getExternalContext()
			.redirect("/sigpat/paginas/TipoFornecedor/ConsultaTiposFornecedores.xhtml");
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}
	
	public List<TipoFornecedor> getTodosTiposFornecedores() {
		return this.todosTiposFornecedores;
	}

	public TipoFornecedor getTipoFornecedor() {
		return tipoFornecedor;
	}

	public void setTipoFornecedor(TipoFornecedor tipoFornecedor) {
		this.tipoFornecedor = tipoFornecedor;
	}
}