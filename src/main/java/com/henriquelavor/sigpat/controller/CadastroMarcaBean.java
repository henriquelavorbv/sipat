package com.henriquelavor.sigpat.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.model.MarcaBens;
import com.henriquelavor.sigpat.repository.MarcasBens;
import com.henriquelavor.sigpat.service.CadastroMarcasBens;
import com.henriquelavor.sigpat.service.NegocioException;

@Named
@ViewScoped
public class CadastroMarcaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CadastroMarcasBens cadastro;

	@Inject
	private MarcasBens marcasBens;

	private MarcaBens marcaBens = new MarcaBens();
	
	private List<MarcaBens> todasMarcasBens;

	public void prepararCadastro() {
		this.todasMarcasBens = marcasBens.todas();
		if (this.marcaBens == null) {
			this.marcaBens = new MarcaBens();
		}
	}

	public void salvar() throws IOException{
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.salvar(this.marcaBens);
			this.marcaBens = new MarcaBens();
			context.addMessage(null, new FacesMessage("Registro Salvo com sucesso!"));
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public void editar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.editar(this.marcaBens);

			this.marcaBens = new MarcaBens();
			context.addMessage(null, new FacesMessage(
					"Registro Atualizado com sucesso!"));
			FacesContext.getCurrentInstance().getExternalContext()
			.redirect("/sigpat/paginas/MarcaBem/ConsultaMarcasBens.xhtml");
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}
	
	public List<MarcaBens> getTodasMarcasBens() {
		return this.todasMarcasBens;
	}

	public MarcaBens getMarcaBens() {
		return marcaBens;
	}

	public void setMarcaBens(MarcaBens marcaBens) {
		this.marcaBens = marcaBens;
	}
}