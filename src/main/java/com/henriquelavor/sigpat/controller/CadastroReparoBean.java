package com.henriquelavor.sigpat.controller;

import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.event.FileUploadEvent;

import com.henriquelavor.sigpat.model.Fornecedor;
import com.google.common.io.Resources;
import com.henriquelavor.sigpat.model.Reparo;
import com.henriquelavor.sigpat.model.BemMovel;
import com.henriquelavor.sigpat.repository.BensMoveis;
import com.henriquelavor.sigpat.repository.Fornecedores;
import com.henriquelavor.sigpat.repository.Reparos;
import com.henriquelavor.sigpat.repository.TermosResponsaveis;
import com.henriquelavor.sigpat.service.CadastroReparos;
import com.henriquelavor.sigpat.service.NegocioException;
import com.henriquelavor.sigpat.util.jsf.FacesUtil;
import com.henriquelavor.sigpat.util.mail.Mailer;
import com.outjected.email.api.ContentDisposition;
import com.outjected.email.api.MailMessage;
import com.outjected.email.impl.attachments.BaseAttachment;

/**
 * @author Henrique Lavor
 * @data 31.08.2017
 * @version 1.0
 */

@Named
@ViewScoped
public class CadastroReparoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CadastroReparos cadastro;

	@Inject
	private BensMoveis bensMoveis;

	@Inject
	private Fornecedores fornecedores;
	
	@Inject
	private Reparos reparos;


	@Inject
	private Mailer mailer;
	
	// nova implementacao para autocomplete
	public List<String> pesquisarNumeroPatrimonio(String descricao) {
		return this.reparos.numeroPatrimonioQueContem(descricao);
	}

	private Reparo reparo = new Reparo();
	
	private String numeroTermoSaidaReparoAtual; //novo
	
	private List<BemMovel> todosBensMoveis;
	
	private List<BemMovel> todosBensMoveisRetorno;
	

	private List<Fornecedor> todosFornecedores;

	private UploadArquivo arquivo = new UploadArquivo();

	// pdf da ordem de servico de retorno de reparo pdf
	public void uploadPdf(FileUploadEvent event) {
		this.arquivo.fileUpload(event, ".pdf", "/resources/enviados/bensmoveis/reparos/");
		this.reparo.setOrdemServicoRetornoPdf(this.arquivo.getNome());
		this.arquivo.gravar(); // nova implementacao fileupload
		this.arquivo = new UploadArquivo(); // nova implementacao fileupload
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("Ordem de Serviço de Retorno de Reparo enviada com sucesso!"));
	}

	public void prepararCadastro() {
		this.todosBensMoveis = this.bensMoveis.todoAtivos();
		this.todosFornecedores = this.fornecedores.todosNaoBloqueados(); // beta

		if (this.reparo == null) {
			this.reparo = new Reparo();
		}
	}
	
	public void prepararCadastroRetorno() {
		this.todosBensMoveisRetorno = this.bensMoveis.todos(); 
		this.todosFornecedores = this.fornecedores.todosNaoBloqueados(); // beta

		if (this.reparo == null) {
			this.reparo = new Reparo();
		}
	}

	public void prepararBens() {
		this.todosBensMoveis = this.bensMoveis.todoAtivos();
	}
	
	public void prepararBensRetorno() {
		this.todosBensMoveisRetorno = this.bensMoveis.todos(); 
	}
	

	public void prepararForncedores() {
		this.todosFornecedores = this.fornecedores.todosNaoBloqueados(); // beta
	}

	public void salvar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();

		//String ordemServicoRetornoPDFExistente = this.getReparo().getOrdemServicoRetornoPdf();
		numeroTermoSaidaReparoAtual = this.reparo.getBemMovel().getNumeroPatrimonio(); //novo

		try {
			this.cadastro.salvar(this.reparo);
			// se quer notificar por email e existir arquivo para anexar, entao
			// enviar email informando o responsavel pela guarda do bem com
			// anexo

			if (this.reparo.getNotificarEmail()) {
				this.enviarNotificaoEmailSaida();
			//} else if ((this.reparo.getNotificarEmail()) && (ordemServicoRetornoPDFExistente != null)) {
			//	this.enviarNotificaoEmailAnexoPDFSaida();
			}

			this.reparo = new Reparo();
			this.arquivo = new UploadArquivo();
			context.addMessage(null, new FacesMessage("Registro de Reparo de Bem Patrimonial Adicionado com Sucesso!"));
			
			if (numeroTermoSaidaReparoAtual != null) {
				HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
						.getRequest();
				request.getSession().setAttribute("numeroTermoReparoSaidaAtual", this.numeroTermoSaidaReparoAtual); //cria uma sessão 
				
			}

			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("/sigpat/paginas/Relatorios/RelatorioTermoReparoSaidaAuto.xhtml");
			
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}

	}

	public void editar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		String ordemServicoRetornoPDFExistente = this.getReparo().getOrdemServicoRetornoPdf();
		try {
			this.cadastro.editar(this.reparo);
			/*if ((this.reparo.getNotificarEmail()) && (ordemServicoRetornoPDFExistente == null)) {
				this.enviarNotificaoEmailRetorno();
			} else if ((this.reparo.getNotificarEmail()) && (ordemServicoRetornoPDFExistente != null)) {
				this.enviarNotificaoEmailAnexoPDFRetorno();
			}*/
			if  (ordemServicoRetornoPDFExistente == null) {
				this.enviarNotificaoEmailRetorno();
			} else if (ordemServicoRetornoPDFExistente != null) {
				this.enviarNotificaoEmailAnexoPDFRetorno();
			}

			this.reparo = new Reparo();
			this.arquivo = new UploadArquivo(); // nova implementacao fileupload
			context.addMessage(null, new FacesMessage("Registro de Reparo de Bem Patrimonial Atualizado com sucesso!"));
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("/sigpat/paginas/Reparo/ConsultaReparos.xhtml");
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public void enviarNotificaoEmailSaida() throws IOException {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String dataSaida = df.format(this.reparo.getDataSaida());

		MailMessage message = mailer.prepararNovaMensagem();

		message.to(this.reparo.getBemMovel().getNovoFuncionarioResponsavelPatrimonial().getEmail())
				.subject("SIGPAT: Notificação de Saída Bem Patrimonial para REPARO")
				.bodyHtml("<strong>Unidade Gestora: </strong>"
						+ this.reparo.getBemMovel().getLocalDestino().getUnidadeGestora().getNomeUnidadeGestora() + "("
						+ this.reparo.getBemMovel().getLocalDestino().getUnidadeGestora().getNomeResumido() + ")"
						+ "<br/>" + "<strong>Unidade Administrativa: </strong>"
						+ this.reparo.getBemMovel().getLocalDestino().getNomeUnidadeAdministrativa() + "("
						+ this.reparo.getBemMovel().getLocalDestino().getNomeResumido() + ")" + "<br/>"
						+ "<strong>Bem Patrimonial: </strong>" + this.reparo.getBemMovel().getNumeroPatrimonio() + " - "
						+ this.reparo.getBemMovel().getDescricao() + "<br />" + "<br />"
						+ "<strong>Funcionário Responsável pela Guarda do Bem: </strong>"
						+ this.reparo.getBemMovel().getNovoFuncionarioResponsavelPatrimonial().getNomeFuncionario()
						+ "<br />" + "<strong>Descrição do Defeito: </strong>" + this.reparo.getDescricaoDefeito()
						+ "<br />" + "<strong>Empresa Prestadora do Serviço: </strong>"
						+ this.reparo.getFornecedorReparo().getNomeFornecedor() + "<br />" + "<strong>Data da Saída do Bem: </strong>"
						+ dataSaida + "<br />")
				.send();

		FacesUtil.addInfoMessage(
				"Notificação de Registro de Saída de Bem Patrimonial para REPARO enviado por e-mail com sucesso!");
	}

	
	public void enviarNotificaoEmailRetorno() throws IOException {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String dataRetorno = df.format(this.reparo.getDataRetorno());

		MailMessage message = mailer.prepararNovaMensagem();

		message.to(this.reparo.getBemMovel().getNovoFuncionarioResponsavelPatrimonial().getEmail())
				.subject("SIGPAT: Notificação de Retorno de Reparo de Bem Patrimonial")
				.bodyHtml("<strong>Unidade Gestora: </strong>"
						+ this.reparo.getBemMovel().getLocalDestino().getUnidadeGestora().getNomeUnidadeGestora() + "("
						+ this.reparo.getBemMovel().getLocalDestino().getUnidadeGestora().getNomeResumido() + ")"
						+ "<br/>" + "<strong>Unidade Administrativa: </strong>"
						+ this.reparo.getBemMovel().getLocalDestino().getNomeUnidadeAdministrativa() + "("
						+ this.reparo.getBemMovel().getLocalDestino().getNomeResumido() + ")" + "<br/>"
						+ "<strong>Bem Patrimonial: </strong>" + this.reparo.getBemMovel().getNumeroPatrimonio() + " - "
						+ this.reparo.getBemMovel().getDescricao() + "<br />" + "<br />"
						+ "<strong>Funcionário Responsável pela Guarda do Bem: </strong>"
						+ this.reparo.getBemMovel().getNovoFuncionarioResponsavelPatrimonial().getNomeFuncionario()
						+ "<br />" + "<strong>Descrição do Reparo: </strong>" + this.reparo.getDescricaoReparo()
						+ "<br />" + "<strong>Empresa Prestadora do Serviço: </strong>"
						+ this.reparo.getFornecedorReparo().getNomeFornecedor() + "<br />" 
						+ "<strong>Data do Retorno do Bem: </strong>" + dataRetorno + "<br />" 
						+ "<strong>Valor do Reparo: </strong>" + this.reparo.getValorReparo() + "<br />"
						+ "<strong>Dias de garantia: </strong>" + this.reparo.getDiasDeGarantia() + "<br />" + "<br/>")
				.send();

		FacesUtil.addInfoMessage(
				"Notificação de Registro de Retorno de Reparo de Bem Patrimonial enviado por e-mail com sucesso!");
	}

	public void enviarNotificaoEmailAnexoPDFRetorno() throws IOException {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String dataRetorno = df.format(this.reparo.getDataRetorno());

		MailMessage message = mailer.prepararNovaMensagem();

		message.to(this.reparo.getBemMovel().getNovoFuncionarioResponsavelPatrimonial().getEmail())
				.subject("SIGPAT: Notificação de Retorno de Reparo de Bem Patrimonial")
				.bodyHtml("<strong>Unidade Gestora: </strong>"
						+ this.reparo.getBemMovel().getLocalDestino().getUnidadeGestora().getNomeUnidadeGestora() + "("
						+ this.reparo.getBemMovel().getLocalDestino().getUnidadeGestora().getNomeResumido() + ")"
						+ "<br/>" + "<strong>Unidade Administrativa: </strong>"
						+ this.reparo.getBemMovel().getLocalDestino().getNomeUnidadeAdministrativa() + "("
						+ this.reparo.getBemMovel().getLocalDestino().getNomeResumido() + ")" + "<br/>"
						+ "<strong>Bem Patrimonial: </strong>" + this.reparo.getBemMovel().getNumeroPatrimonio() + " - "
						+ this.reparo.getBemMovel().getDescricao() + "<br />" + "<br />"
						+ "<strong>Funcionário Responsável pela Guarda do Bem: </strong>"
						+ this.reparo.getBemMovel().getNovoFuncionarioResponsavelPatrimonial().getNomeFuncionario()
						+ "<br />" + "<strong>Descrição do Reparo: </strong>" + this.reparo.getDescricaoReparo()
						+ "<br />" + "<strong>Empresa Prestadora do Serviço: </strong>"
						+ this.reparo.getFornecedorReparo().getNomeFornecedor() + "<br />" 
						+ "<strong>Data do Retorno do Bem: </strong>" + dataRetorno + "<br />" 
						+ "<strong>Valor do Reparo: </strong>" + this.reparo.getValorReparo() + "<br />"
						+ "<strong>Dias de garantia: </strong>" + this.reparo.getDiasDeGarantia() + "<br />" + "<br/>")
				.addAttachment(new BaseAttachment(this.reparo.getOrdemServicoRetornoPdf(), "application/x-pdf",
						ContentDisposition.ATTACHMENT,
						Resources.toByteArray(Resources.getResource("../../resources/enviados/bensmoveis/reparos/"
								+ this.reparo.getOrdemServicoRetornoPdf()))))
				.send();

		FacesUtil.addInfoMessage(
				"Notificação de Registro de Retorno de Reparo de Bem Patrimonial, enviado por e-mail com sucesso e anexo PDF!");
	}


	public List<BemMovel> getTodosBensMoveis() {
		return this.todosBensMoveis;
	}
	
	public List<BemMovel> getTodosBensMoveisRetorno() {
		return this.todosBensMoveisRetorno;
	}
	

	public List<Fornecedor> getTodosFornecedores() {
		return this.todosFornecedores;
	}

	public Reparo getReparo() {
		return reparo;
	}

	public void setReparo(Reparo reparo) {
		this.reparo = reparo;
	}
}