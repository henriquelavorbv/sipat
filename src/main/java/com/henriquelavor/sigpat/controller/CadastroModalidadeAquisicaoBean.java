package com.henriquelavor.sigpat.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.model.ModalidadeAquisicaoBens;
import com.henriquelavor.sigpat.repository.ModalidadesAquisicoesBens;
import com.henriquelavor.sigpat.service.CadastroModalidadesAquisicoesBens;
import com.henriquelavor.sigpat.service.NegocioException;

@Named
@ViewScoped
public class CadastroModalidadeAquisicaoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CadastroModalidadesAquisicoesBens cadastro;

	@Inject
	private ModalidadesAquisicoesBens modalidadesAquisicoesBens;

	private ModalidadeAquisicaoBens modalidadeAquisicaoBens = new ModalidadeAquisicaoBens();
	
	private List<ModalidadeAquisicaoBens> todasModalidadesAquisicoesBens;

	public void prepararCadastro() {
		this.todasModalidadesAquisicoesBens = modalidadesAquisicoesBens.todas();
		if (this.modalidadeAquisicaoBens == null) {
			this.modalidadeAquisicaoBens = new ModalidadeAquisicaoBens();
		}
	}

	public void salvar() throws IOException{
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.salvar(this.modalidadeAquisicaoBens);
			this.modalidadeAquisicaoBens = new ModalidadeAquisicaoBens();
			context.addMessage(null, new FacesMessage("Registro Salvo com sucesso!"));
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public void editar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.editar(this.modalidadeAquisicaoBens);

			this.modalidadeAquisicaoBens = new ModalidadeAquisicaoBens();
			context.addMessage(null, new FacesMessage(
					"Registro Atualizado com sucesso!"));
			FacesContext.getCurrentInstance().getExternalContext()
			.redirect("/sigpat/paginas/ModalidadeAquisicaoBem/ConsultaModalidadesAquisicoesBens.xhtml");
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}
	
	public List<ModalidadeAquisicaoBens> getTodasModalidadesAquisicoesBens() {
		return this.todasModalidadesAquisicoesBens;
	}

	public ModalidadeAquisicaoBens getModalidadeAquisicaoBens() {
		return modalidadeAquisicaoBens;
	}

	public void setModalidadeAquisicaoBens(ModalidadeAquisicaoBens modalidadeAquisicaoBens) {
		this.modalidadeAquisicaoBens = modalidadeAquisicaoBens;
	}
}