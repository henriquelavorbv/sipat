package com.henriquelavor.sigpat.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.model.Depreciacao;
import com.henriquelavor.sigpat.repository.Depreciacoes;
import com.henriquelavor.sigpat.service.CadastroDepreciacoes;

/**
 * @author Henrique Lavor
 * @data 15.07.2019
 * @version 1.0
 */

@Named
@ViewScoped
public class CadastroDepreciacaoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CadastroDepreciacoes cadastro;

	@Inject
	private Depreciacoes bensMoveisDepreciados;

	
	@Inject
	private Depreciacoes depreciacoes;

	// nova implementacao para autocomplete
	public List<String> pesquisarNumeroPatrimonio(String descricao) {
		return this.depreciacoes.numeroPatrimonioQueContem(descricao);
	}

	private Depreciacao depreciacao = new Depreciacao();
	
	private List<Depreciacao> todosBensMoveisDepreciados;


	public void prepararCadastro() {
		this.todosBensMoveisDepreciados = bensMoveisDepreciados.todas();
	}
	
	public void prepararBens() {
		this.todosBensMoveisDepreciados = bensMoveisDepreciados.todas();
	}
	

	public List<Depreciacao> getTodosBensMoveisDepreciados() {
		return this.todosBensMoveisDepreciados;
	}
	
	
	public Depreciacao getDepreciacao() {
		return depreciacao;
	}

	public void setDepreciacao(Depreciacao depreciacao) {
		this.depreciacao = depreciacao;
	}
}