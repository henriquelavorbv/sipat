package com.henriquelavor.sigpat.controller;

import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import com.henriquelavor.sigpat.model.BemMovel;
import com.henriquelavor.sigpat.model.EstadoBens;
import com.henriquelavor.sigpat.model.Funcionario;
import com.henriquelavor.sigpat.model.TermoResponsavel;
import com.henriquelavor.sigpat.model.UnidadeAdministrativa;
import com.henriquelavor.sigpat.repository.BensMoveis;
import com.henriquelavor.sigpat.repository.EstadosBens;
import com.henriquelavor.sigpat.repository.Funcionarios;
import com.henriquelavor.sigpat.repository.TermosResponsaveis;
import com.henriquelavor.sigpat.repository.UnidadesAdministrativas;
import com.henriquelavor.sigpat.service.CadastroTermosResponsaveis;
import com.henriquelavor.sigpat.service.NegocioException;
import com.henriquelavor.sigpat.util.jsf.FacesUtil;
import com.henriquelavor.sigpat.util.mail.Mailer;
import com.outjected.email.api.MailMessage;

/**
 * @author Henrique Lavor
 * @data 26.08.2017
 * @version 1.0
 */

@Named
@ViewScoped
public class CadastroTermoResponsavelBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CadastroTermosResponsaveis cadastro;

	@Inject
	private Funcionarios funcionariosResponsaveis;

	@Inject
	private EstadosBens estadosBens;

	@Inject
	private BensMoveis bensMoveis;

	@Inject
	private TermosResponsaveis termosResponsaveis;

	@Inject
	private UnidadesAdministrativas locaisOrigens;

	@Inject
	private UnidadesAdministrativas locaisDestinos;
	
	
	@Inject
	private Mailer mailer;

	// nova implementacao
	public List<String> pesquisarNumeroTermoResponsavel(String descricao) {
		return this.termosResponsaveis.numeroTermoResponsavelQueContem(descricao);
	}

	private TermoResponsavel termoResponsavel = new TermoResponsavel();

	private String numeroTermoAtual;

	private List<TermoResponsavel> todosTermosResponsaveis;
	private List<Funcionario> todosFuncionariosResponsaveis;
	private List<EstadoBens> todosEstadosBens;
	private List<BemMovel> todosBensMoveis;
	private List<UnidadeAdministrativa> todosLocaisOrigens;
	private List<UnidadeAdministrativa> todosLocaisDestinos;
	
	//nova implementacao 10.10.2017
	//private List<TermoResponsavel> termosResponsaveisLista;

	
	//nova implementacao 10.10.2017
	//@PostConstruct
	//public void init() {
	//	termoResponsavel = new TermoResponsavel();
	//	termosResponsaveisLista = new ArrayList<TermoResponsavel>();
	//}

	// private List<TermoResponsavel> termosResponsaveisLista = new
	// ArrayList<>();

	/*
	 * public void prepararListaTermos(String numero){
	 * this.todosTermosResponsaveis =
	 * termosResponsaveis.todosTermosPorNumero("7777"); }
	 */

	public void prepararCadastro() {
		this.todosTermosResponsaveis = termosResponsaveis.todos();
		this.todosFuncionariosResponsaveis = this.funcionariosResponsaveis.todosResponsaveisBem();
		this.todosEstadosBens = this.estadosBens.todos();
		this.todosBensMoveis = this.bensMoveis.todosNaoIncorporados();
		
		/** ver depois
		  * possibilidade e necessidade de filtrar
		  * excluindo da  lista a  unidade adm 999
		  */

		
		this.todosLocaisOrigens = this.locaisOrigens.todas();
		this.todosLocaisDestinos = this.locaisDestinos.todas();
		

		if (this.termoResponsavel == null) {
			this.termoResponsavel = new TermoResponsavel();
			
			//HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			//request.getSession().setAttribute("numeroTermoAtual", this.numeroTermoAtual);
			 Date datatual = new Date();
		  
			this.termoResponsavel.setNumeroTermoResponsabilidade("TER"+Long.toString(datatual.getTime()) + datatual.getYear());

		}
	}

	public void prepararTermosResponsaveis() {
		this.todosTermosResponsaveis = termosResponsaveis.todos();
	}

	public void prepararFuncionariosResponsaveisPatrimoniais() {
		this.todosFuncionariosResponsaveis = this.funcionariosResponsaveis.todosResponsaveisBem();

	}

	public void prepararEstadosBens() {
		this.todosEstadosBens = this.estadosBens.todos();
	}

	public void prepararBensMoveis() {
		this.todosBensMoveis = this.bensMoveis.todosNaoIncorporados();
	}

	public void prepararBensMoveis(AjaxBehaviorEvent event) throws Exception {
		if (event.getComponent().getAttributes().get("value").toString().trim().equalsIgnoreCase("numeroPatrimonio")) {
			this.todosBensMoveis = this.bensMoveis.todosNaoIncorporados();
		}
	}

	public void prepararLocaisOrigens() {
		this.todosLocaisOrigens = this.locaisOrigens.todas();
	}

	public void prepararLocaisDestinos() {
		this.todosLocaisDestinos = this.locaisDestinos.todas();
	}
	
	public void salvarGerarTermo() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();

		numeroTermoAtual = this.termoResponsavel.getNumeroTermoResponsabilidade();

		try {
			this.cadastro.salvar(this.termoResponsavel);
			this.cadastro.gerarTermos();
			
			if (this.termoResponsavel.getNotificarEmail()){
				this.enviarNotificacao(); //enviar email aos responsaveis(saída e entrada) pela Termo de responsabilidade
			}
				
			
			context.addMessage(null, new FacesMessage("Termo de Responsabilidade Gerado com sucesso!"));

			if (numeroTermoAtual != null) {
				HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
						.getRequest();
				request.getSession().setAttribute("numeroTermoAtual", this.numeroTermoAtual);

			}

			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("/sigpat/paginas/Relatorios/RelatorioTermoResponsabilidadeAuto.xhtml");
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}

	}

	public void salvarRepetir() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		
		//termosResponsaveisLista.add(termoResponsavel); //nova implementacao 10.10.2017

		try {
			this.cadastro.salvar(this.termoResponsavel);
			this.termoResponsavel.setCodigoTermoResponsavel(null); //código do termo de responsabilidade sem valor
			
			this.termosResponsaveis.todosTermosPorNumero(this.getTermoResponsavel().getNumeroTermoResponsabilidade());
			
			
			if (this.termoResponsavel.getNotificarEmail()){
				this.enviarNotificacao(); //enviar email aos responsaveis(saída e entrada) pela Termo de responsabilidade
			}
			
			
			context.addMessage(null, new FacesMessage("Registro salvo com sucesso!"));
			context.addMessage(null,
					new FacesMessage("Preenchimento do mesmo Termo disponível, informe o Bem p/ Incorporação!"));

		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}

	}
	
	
	public void enviarNotificacao(){
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String dataTermo = df.format(this.termoResponsavel.getDataTermoResponsabilidade());
		
		MailMessage message = mailer.prepararNovaMensagem();
		message.to(this.termoResponsavel.getFuncionarioResponsavelPatrimonial().getEmail())
		.subject("SIGPAT: Notificação de TERMO DE RESPONSABILIDADE  Nº : " + this.termoResponsavel.getNumeroTermoResponsabilidade())
		.bodyHtml("<strong>Data do Termo: </strong>" + dataTermo +"<br />" + 
		"<strong>Unidade Gestora de Destino: </strong>" + this.termoResponsavel.getLocalDestino().getUnidadeGestora().getNomeUnidadeGestora() +"("+ this.termoResponsavel.getLocalDestino().getUnidadeGestora().getNomeResumido()+")"+"<br/>" +
		"<strong>Unidade Administrativa de Destino: </strong>" + this.termoResponsavel.getLocalDestino().getNomeUnidadeAdministrativa() +"("+ this.termoResponsavel.getLocalDestino().getNomeResumido() +")"+"<br/>" +
		"<strong>Situação: </strong>" + this.termoResponsavel.getSituacao() +"<br />" +
		"<strong>Funcionário Receptor/Responsável: </strong>" + this.termoResponsavel.getFuncionarioResponsavelPatrimonial().getNomeFuncionario() +"<br />" +
		"<strong>Matrícula do Funcionário Receptor/Responsável: </strong>" + this.termoResponsavel.getFuncionarioResponsavelPatrimonial().getMatricula() +"<br />" +"<br />" +
		
		"<strong> Caro Responsável, este email, tem como finalidade, informá-lo(a) que, você tem de posse um documento oficial impresso e "
		+ "assinado do Termo de Responsabilidade No. " + this.termoResponsavel.getNumeroTermoResponsabilidade() 
		+ ", guarde-o em um local seguro, para futuros levantamentos/inventários patrimoniais de sua entidade."
		+ "   </strong>" +"<br />")
		.send();
		
		FacesUtil.addInfoMessage("Notificação de Termo de Responsabilidade enviado por e-mail com sucesso!");
	}

	public List<TermoResponsavel> getTodosTermosResponsaveis() {
		return this.todosTermosResponsaveis;
	}

	public List<Funcionario> getTodosFuncionariosResponsaveis() {
		return this.todosFuncionariosResponsaveis;
	}

	public List<EstadoBens> getTodosEstadosBens() {
		return this.todosEstadosBens;
	}

	public List<BemMovel> getTodosBensMoveis() {
		return this.todosBensMoveis;
	}

	public List<UnidadeAdministrativa> getTodosLocaisOrigens() {
		return this.todosLocaisOrigens;
	}

	public List<UnidadeAdministrativa> getTodosLocaisDestinos() {
		return this.todosLocaisDestinos;
	}
	
	public TermoResponsavel getTermoResponsavel() {
		return termoResponsavel;
	}

	public void setTermoResponsavel(TermoResponsavel termoResponsavel) {
		this.termoResponsavel = termoResponsavel;
	}
	
	
	//nova implementacao 10.10.2017
	//public String reinit() {
	//	termoResponsavel = new TermoResponsavel();
	//	return null;
	//}

	//nova implementacao 10.10.2017
	//public List<TermoResponsavel> getTermosResponsaveisLista() {
	//	return termosResponsaveisLista;
	//}

}