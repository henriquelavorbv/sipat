package com.henriquelavor.sigpat.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FlowEvent;

import com.henriquelavor.sigpat.model.UnidadeGestora;
import com.henriquelavor.sigpat.model.UnidadeAdministrativa;
import com.henriquelavor.sigpat.repository.UnidadesGestoras;
import com.henriquelavor.sigpat.service.CadastroUnidadesAdministrativas;
import com.henriquelavor.sigpat.service.NegocioException;

@Named
@ViewScoped
public class CadastroUnidadeAdministrativaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CadastroUnidadesAdministrativas cadastro;

	@Inject
	private UnidadesGestoras unidadesGestoras;

	private UnidadeAdministrativa unidadeAdministrativa = new UnidadeAdministrativa();

	private List<UnidadeGestora> todasUnidadesGestoras;

	public void prepararCadastro() {

		this.todasUnidadesGestoras = this.unidadesGestoras.todas();

		if (this.unidadeAdministrativa == null) {
			this.unidadeAdministrativa = new UnidadeAdministrativa();
		}
	}
	
	// atualizar lista de grupos 11.09.2016
		public void prepararUnidadesGestoras() {
			this.todasUnidadesGestoras = this.unidadesGestoras.todas();
		}


	public void salvar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.cadastro.salvar(this.unidadeAdministrativa);
			this.unidadeAdministrativa = new UnidadeAdministrativa();
			context.addMessage(null, new FacesMessage("Registro salvo com sucesso!"));
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}

	}

	public void editar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.editar(this.unidadeAdministrativa);

			this.unidadeAdministrativa = new UnidadeAdministrativa();
			context.addMessage(null, new FacesMessage("Registro Atualizado com sucesso!"));
			FacesContext.getCurrentInstance().getExternalContext()
			.redirect("/sigpat/paginas/UnidadeAdministrativa/ConsultaUnidadesAdministrativas.xhtml");
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public List<UnidadeGestora> getTodasUnidadesGestoras() {
		return this.todasUnidadesGestoras;
	}

	public UnidadeAdministrativa getUnidadeAdministrativa() {
		return unidadeAdministrativa;
	}

	public void setUnidadeAdministrativa(UnidadeAdministrativa unidadeAdministrativa) {
		this.unidadeAdministrativa = unidadeAdministrativa;
	}
	
	// Usado no TAB Wizard
	public String onFlowProcess(FlowEvent event) {
		return event.getNewStep();
	}

	/*public void executarJS() {
		RequestContext.getCurrentInstance().execute("checarJS();checarJS1();");
	}*/
}