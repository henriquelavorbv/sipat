package com.henriquelavor.sigpat.controller;

import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.model.ClassificacaoInventario;
import com.henriquelavor.sigpat.model.Funcionario;
import com.henriquelavor.sigpat.model.Inventario;
import com.henriquelavor.sigpat.model.UnidadeAdministrativa;
import com.henriquelavor.sigpat.repository.ClassificacoesInventarios;
import com.henriquelavor.sigpat.repository.Funcionarios;
import com.henriquelavor.sigpat.repository.UnidadesAdministrativas;
import com.henriquelavor.sigpat.service.CadastroInventarios;
import com.henriquelavor.sigpat.service.NegocioException;
import com.henriquelavor.sigpat.util.jsf.FacesUtil;
import com.henriquelavor.sigpat.util.mail.Mailer;
import com.outjected.email.api.MailMessage;

@Named
@ViewScoped
public class CadastroInventarioBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CadastroInventarios cadastro;

	@Inject
	private ClassificacoesInventarios classificacoesInventarios; // novo

	@Inject
	private Funcionarios funcionarios; // novo

	@Inject
	private UnidadesAdministrativas unidadesAdministrativas;

	@Inject
	private Mailer mailer;
	
	private Inventario inventario = new Inventario();

	private List<ClassificacaoInventario> todasClassificacoesInventarios;
	private List<Funcionario> todosFuncionariosResponsavelInventario;
	private List<UnidadeAdministrativa> todasUnidadesAdministrativasDisponiveis;

	public void prepararCadastro() {
		this.todasClassificacoesInventarios = this.classificacoesInventarios.todas();
		this.todosFuncionariosResponsavelInventario = this.funcionarios.todosResponsaveisInventario();
		this.todasUnidadesAdministrativasDisponiveis = this.unidadesAdministrativas.todasDisponiveis();

		if (this.inventario == null) {
			this.inventario = new Inventario();
		}
	}

	public void visualizarUA(AjaxBehaviorEvent event) throws Exception {
		if (event.getComponent().getAttributes().get("value").toString().trim()
				.equalsIgnoreCase("nomeunidadeadministrativa")) {
			this.todasUnidadesAdministrativasDisponiveis = this.unidadesAdministrativas.todasDisponiveis();
		}
	}

	public void salvar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.cadastro.salvar(this.inventario);
			
			if (this.inventario.getNotificarAbertoEmail()){
				this.enviarInventarioAbertura(); //enviar email informando abertura
			}
			this.inventario = new Inventario();
			context.addMessage(null, new FacesMessage("Inventário ABERTO com sucesso!"));
			FacesContext.getCurrentInstance().getExternalContext().redirect("/sigpat/paginas/Inventario/ConsultaInventarios.xhtml");
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}

	}
	

	public void editar() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.editar(this.inventario);
			
			if (this.inventario.getNotificarEncerramentoEmail()){
				this.enviarInventarioEncerramento(); //enviar email informando o encerramento
			}
					
			this.inventario = new Inventario();
			context.addMessage(null, new FacesMessage("Inventário ENCERRADO com sucesso!"));
			FacesContext.getCurrentInstance().getExternalContext()
			.redirect("/sigpat/paginas/Inventario/ConsultaInventarios.xhtml");
			
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}
	
	public void enviarInventarioEncerramento(){
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String dataInicial = df.format(this.inventario.getDataInicial());
		String dataEncerramento = df.format(this.inventario.getDataFinal());
		
		MailMessage message = mailer.prepararNovaMensagem();
		message.to(this.inventario.getFuncionarioResponsavelInventario().getEmail())
		.subject("SIGPAT: Notificação de Encerramento de Inventário Nº : " + this.inventario.getCodigoInventario())
		.bodyHtml("<strong>Data Inicial do Inventário: </strong>" + dataInicial +"<br />" + 
		"<strong>Data de Encerramento do Inventário: </strong>" + dataEncerramento +"<br />" + 
		"<strong>Unidade Gestora: </strong>" + this.inventario.getUnidadeAdministrativa().getUnidadeGestora().getNomeUnidadeGestora() +"("+ this.inventario.getUnidadeAdministrativa().getUnidadeGestora().getNomeResumido()+")"+"<br/>" +
		"<strong>Unidade Administrativa: </strong>" + this.inventario.getUnidadeAdministrativa().getNomeUnidadeAdministrativa() +"("+ this.inventario.getUnidadeAdministrativa().getNomeResumido()+")" + "<br/>" +
		"<strong>Situação: </strong>" + this.inventario.getSituacaoInventario() +"<br />" +
		"<strong>Funcionário Responsável: </strong>" + this.inventario.getFuncionarioResponsavelInventario().getNomeFuncionario() +"<br />" +
		"<strong>Descrição/Objetivo do Inventário: </strong>" + this.inventario.getDescricao() +"<br />" +
		"<strong>Observação: </strong>" + this.inventario.getObservacao() +"<br />")
		.send();
		
		FacesUtil.addInfoMessage("Notificação de Encerramento de Inventário enviado por e-mail com sucesso!");
	}
	
	
	public void enviarInventarioAbertura(){
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String dataInicial = df.format(this.inventario.getDataInicial());
		
		MailMessage message = mailer.prepararNovaMensagem();
		message.to(this.inventario.getFuncionarioResponsavelInventario().getEmail())
		.subject("SIGPAT: Notificação de Abertura de Inventário")
		.bodyHtml("<strong>Data Inicial do Inventário: </strong>" + dataInicial +"<br />" + 
		"<strong>Unidade Gestora: </strong>" + this.inventario.getUnidadeAdministrativa().getUnidadeGestora().getNomeUnidadeGestora() +"("+ this.inventario.getUnidadeAdministrativa().getUnidadeGestora().getNomeResumido()+")"+"<br/>" +
		"<strong>Unidade Administrativa: </strong>" + this.inventario.getUnidadeAdministrativa().getNomeUnidadeAdministrativa() +"("+ this.inventario.getUnidadeAdministrativa().getNomeResumido()+")" + "<br/>" +
		"<strong>Situação: </strong>" + this.inventario.getSituacaoInventario() +"<br />" +
		"<strong>Funcionário Responsável: </strong>" + this.inventario.getFuncionarioResponsavelInventario().getNomeFuncionario() +"<br />" +
		"<strong>Descrição/Objetivo do Inventário: </strong>" + this.inventario.getDescricao() +"<br />")
		.send();
		
		FacesUtil.addInfoMessage("Notificação de Abertura de Inventário enviado por e-mail com sucesso!");
	}

	public List<ClassificacaoInventario> getTodasClassificacoesInventarios() {
		return this.todasClassificacoesInventarios;
	}

	public List<Funcionario> getTodosFuncionariosResponsavelInventario() {
		return this.todosFuncionariosResponsavelInventario;
	}

	public List<UnidadeAdministrativa> getTodasUnidadesAdministrativasDisponiveis() {
		return this.todasUnidadesAdministrativasDisponiveis;
	}

	public Inventario getInventario() {
		return inventario;
	}

	public void setInventario(Inventario inventario) {
		this.inventario = inventario;
	}
}