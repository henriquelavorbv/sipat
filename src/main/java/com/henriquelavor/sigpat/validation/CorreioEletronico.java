package com.henriquelavor.sigpat.validation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.OverridesAttribute;
import javax.validation.Payload;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {})
@NotEmpty
@Email
public @interface CorreioEletronico {
	
	@OverridesAttribute(constraint = Email.class, name="message")
	String message() default " não está certo.";
	
	Class<?>[] groups() default {};
	
	Class<? extends Payload>[] payload() default {}; 
	

}
