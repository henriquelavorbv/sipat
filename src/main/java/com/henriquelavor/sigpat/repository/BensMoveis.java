/*
 * Nome da Classe: BensMoveis
 * Data da Criação: 01.10.2016
 * 
 * ATUALIZAÇÕES: 
 * 
 * No. da Atualização: 01
 * Data atualização: 17.07.2019 as 14:25
 * Objetivo: 
 * A Função public List<BemMovel> todos(String codigoUO) foi atualizada,
 * para permitir filtrar apenas os Bens patrimoniais da  Unidade Gestora logada
 * 
 */

package com.henriquelavor.sigpat.repository;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.henriquelavor.sigpat.model.BemMovel;
import com.henriquelavor.sigpat.model.Transferencia;

/**
 * @author Henrique Lavor
 * @data 01.10.2016
 * @version 1.1
 */

public class BensMoveis implements Serializable {

	private static final long serialVersionUID = 1L;

	private EntityManager manager;

	@Inject
	public BensMoveis(EntityManager manager) {
		this.manager = manager;
	}

	public BemMovel porId(Long codigoBemMovel) {
		return manager.find(BemMovel.class, codigoBemMovel);
	}

	//Lista o bem atraves do numero de patrimonio da UG Logado
	public BemMovel porNumeroPatrimonial(String numeroPatrimonio) {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
		
		TypedQuery<BemMovel> query = manager.createQuery("from BemMovel WHERE localDestino.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession AND numeroPatrimonio=:numeroPatrimonio",
				BemMovel.class);
		query.setParameter("numeroPatrimonio", numeroPatrimonio);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	//Lista de todos os bens patrimoniais de todas as Unidades Gestoras
	public List<BemMovel> todosGeral() {
		 
		TypedQuery<BemMovel> query = manager.createQuery("FROM BemMovel", BemMovel.class);
		return query.getResultList();
	}

	// BemMovel do package: model //Lista de todos bens moveis
	public List<BemMovel> todos() {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
	
		TypedQuery<BemMovel> query = manager.createQuery("from BemMovel  WHERE localDestino.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession", BemMovel.class);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
		return query.getResultList();
	}

	// filtrar somente os bens moveis ATIVOS e da UO logada
	public List<BemMovel> todoAtivos() {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
				
		TypedQuery<BemMovel> query = manager.createQuery("from BemMovel WHERE ativo ='Sim' AND localDestino.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession", BemMovel.class);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
		return query.getResultList();
	}

	// filtrar somente os bens moveis INATIVOS e da UO logada
	public List<BemMovel> todoInativos() {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
		
		TypedQuery<BemMovel> query = manager.createQuery("from BemMovel WHERE ativo ='Não' AND localDestino.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession", BemMovel.class);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
		return query.getResultList();
	}

	//inicio 
	//Implementacao Finalizada em 26.08.2019 na SEFAZ
	//comecei implementar domingo a noite 25.08.2019
	
	
	// filtrar somente os bens moveis NAO INCORPORADOS (QUE NAO TEM TERMO DE
	// RESPONSABILIDADE da Unidade Gestora Logada)
	public List<BemMovel> todosNaoIncorporados() {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
				
		TypedQuery<BemMovel> query = manager.createQuery(
				"from BemMovel WHERE ativo ='Sim' and numeroTermoResponsabilidade ='NAO INCORPORADO' AND localDestino.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession", BemMovel.class);
		
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
		return query.getResultList();
	}

	///fim
	
	
	
	// filtrar somente os bens moveis INCORPORADOS (QUE TEM TERMO DE
	// RESPONSABILIDADE) e da UO logada
	public List<BemMovel> todosIncorporados() {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
		
		TypedQuery<BemMovel> query = manager.createQuery(
				"from BemMovel WHERE ativo ='Sim' and numeroTermoResponsabilidade <> 'NAO INCORPORADO' AND localDestino.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession",
				BemMovel.class);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
		return query.getResultList();
	}
	
	//filtrar somente os bens moveis que saiu para reparo da UO logada
	public List<BemMovel> todosEmReparos() {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
				
		TypedQuery<BemMovel> query = manager.createQuery(
				"from BemMovel WHERE ativo ='Reparo' AND localDestino.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession",
				BemMovel.class);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
		return query.getResultList();
	}
	
	
	//filtrar somente os bens moveis incluidos no mes atual da UO logada
		public List<BemMovel> todosBensIncluidosMesAtual() {
			Calendar calendarioAtual = Calendar.getInstance();
			 int mes = calendarioAtual.get(Calendar.MONTH) + 1;
			
			//Pega a sessao do Codigo UO
			HttpServletRequest req = (HttpServletRequest) 
			FacesContext.getCurrentInstance().getExternalContext().getRequest();
			
			HttpSession session = (HttpSession) req.getSession();
			String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
			 
			TypedQuery<BemMovel> query = manager.createQuery(
					"from BemMovel WHERE month(dataCadastro) = :mes AND localDestino.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession)",
					BemMovel.class);
			query.setParameter("mes", mes);
			query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
			return query.getResultList();
			
		}
	
	// metodo usado para autocomplete para sugestao ao preencher a descricao
	//Nesta função nao foi aplicado o filtro da UO logada, pois trata-se de uma função de uso genérico
	public List<String> descricoesQueContem(String descricao) {
		TypedQuery<String> query = manager.createQuery(
				"select distinct descricao from BemMovel " + "where upper(descricao) like upper(:descricao)",
				String.class);
		query.setParameter("descricao", "%" + descricao + "%");
		return query.getResultList();
	}

	
	
	// pesquisa transferencias por numero patrimonial e da UO logada, ou seja apenas os bens patrimoniais da UO de destino
	public List<Transferencia> todasTrasferenciasPorNumeroPatrimonial(String numero) {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
				
		TypedQuery<Transferencia> query = manager.createQuery(
				"from Transferencia where bemMovel.numeroPatrimonio=:numero and localDestino.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession ORDER BY dataCadastro DESC",
				Transferencia.class);
		query.setParameter("numero", numero);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
		try {
			return query.getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}

	public void adicionar(BemMovel bemMovel) {
		this.manager.persist(bemMovel);
	}

	public BemMovel guardar(BemMovel bemMovel) {
		return this.manager.merge(bemMovel);
	}

	public void remover(BemMovel bemMovel) {
		this.manager.remove(bemMovel);
	}
}