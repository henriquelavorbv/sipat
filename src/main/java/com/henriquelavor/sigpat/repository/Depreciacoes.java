package com.henriquelavor.sigpat.repository;

import java.io.Serializable;

import java.util.List;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.henriquelavor.sigpat.model.Depreciacao;

/**
 * @author Henrique Lavor
 * @data 06.02.2018
 * @version 1.0
 */

public class Depreciacoes implements Serializable {

	private static final long serialVersionUID = 1L;

	private EntityManager manager;

	@Inject
	public Depreciacoes(EntityManager manager) {
		this.manager = manager;
	}

	// pesquisar por ID (código da depreciacao) de uso generico
	public Depreciacao porId(Long codigoDepreciacao) {
		return manager.find(Depreciacao.class, codigoDepreciacao);
	}

	// metodo usado para autocomplete para sugestao ao preencher o numero do
	// patrimonio lista de depreciacoes da UO logada
	public List<String> numeroPatrimonioQueContem(String descricao) {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
								
		TypedQuery<String> query = manager.createQuery("SELECT distinct bemMovel.numeroPatrimonio from Depreciacao "
				+ "WHERE upper(bemMovel.numeroPatrimonio) like upper(:descricao) AND bemMovel.localDestino.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession", String.class);
		query.setParameter("descricao", "%" + descricao + "%");
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
		return query.getResultList();
	}

	// pesquisa por numero do patrimonio e lista as depreciacoes da UO logada
	public List<Depreciacao> todasDepreciacoesPorNumeroPatrimonio(String numeroPatrimonio) {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
		
		TypedQuery<Depreciacao> query = manager.createQuery("from Depreciacao where numeroPatrimonio=:numeroPatrimonio AND bemMovel.localDestino.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession",
				Depreciacao.class);
		try {
			query.setParameter("numeroPatrimonio", numeroPatrimonio);
			query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
			return query.getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}

	// lista todas depreciacoes da UO logada
	public List<Depreciacao> todas() {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");

		TypedQuery<Depreciacao> query = manager.createQuery("from  Depreciacao d WHERE bemMovel.localDestino.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession", Depreciacao.class);

		try {
			query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
			return query.getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}

	// public List<String> todas() {
	// TypedQuery<String> query = manager.createQuery(
	// "SELECT distinct bemMovel.numeroPatrimonio from Depreciacao", String.class);
	// return query.getResultList();
	// }

	// Atualizar Bem Depreciado,
	// Esta função NAO necessita de filtrar UO
	public int atualizarBemDepreciado(Depreciacao depreciacao, int codigoBem) {
		String sql = "UPDATE tb_bem_movel SET situacao_depreciado='Sim' WHERE tb_bem_movel.codigobemmovel=" + codigoBem;
		Query query = manager.createNativeQuery(sql);
		int updateCount1 = query.executeUpdate();
		return updateCount1;
	}

	public void adicionar(Depreciacao depreciacao) {
		this.manager.persist(depreciacao);
	}

	public Depreciacao guardar(Depreciacao depreciacao) {
		return this.manager.merge(depreciacao);
	}

	public void remover(Depreciacao depreciacao) {
		this.manager.remove(depreciacao);
	}

}
