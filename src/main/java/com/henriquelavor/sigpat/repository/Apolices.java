package com.henriquelavor.sigpat.repository;

import java.io.Serializable;

import java.util.List;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.henriquelavor.sigpat.model.Apolice;
import com.henriquelavor.sigpat.model.BemMovel;

/**
 * @author Henrique Lavor
 * @data 10.08.2017
 * @version 1.0
 */

public class Apolices implements Serializable {

	private static final long serialVersionUID = 1L;

	private EntityManager manager;

	@Inject
	public Apolices(EntityManager manager) {
		this.manager = manager;
	}

	public Apolice porId(Long codigoApolice) {
		return manager.find(Apolice.class, codigoApolice);
	}

	//Lista de todas apolices da UO logada
	public List<Apolice> todas() {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
				
		TypedQuery<Apolice> query = manager.createQuery("from Apolice WHERE bemMovel.localDestino.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession", Apolice.class);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
		return query.getResultList();
	}

	public void adicionar(Apolice apolice) {
		this.manager.persist(apolice);
	}

	// pesquisa por número e Listar todas apolices, esta funcão é de uso generico, NAO necessita filtrar UO
	public Apolice porNumeroBem(String numero, BemMovel bemMovel) {
		//Pega a sessao do Codigo UO
		//HttpServletRequest req = (HttpServletRequest) 
		//FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		//HttpSession session = (HttpSession) req.getSession();
		//String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
		
		TypedQuery<Apolice> query = manager.createQuery("from Apolice where numeroApolice=:numero AND bemMovel=:bemMovel", Apolice.class);
		
		//TypedQuery<Apolice> query = manager.createQuery("from Apolice where numeroApolice=:numero AND bemMovel.localDestino.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession", Apolice.class);
		query.setParameter("numero", numero);
		query.setParameter("bemMovel", bemMovel);
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public Apolice guardar(Apolice apolice) {
		return this.manager.merge(apolice);
	}

	public void remover(Apolice apolice) {
		this.manager.remove(apolice);
	}
}
