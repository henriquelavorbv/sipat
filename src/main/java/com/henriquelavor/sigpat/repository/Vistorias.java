package com.henriquelavor.sigpat.repository;

import java.io.Serializable;

import java.util.List;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.henriquelavor.sigpat.model.Vistoria;
import com.henriquelavor.sigpat.model.BemMovel;
import com.henriquelavor.sigpat.model.Inventario;

/**
 * @author Henrique Lavor
 * @data 26.09.2017
 * @version 1.0
 */

public class Vistorias implements Serializable {

	private static final long serialVersionUID = 1L;

	private EntityManager manager;
	
	@Inject
	public Vistorias(EntityManager manager) {
		this.manager = manager;
	}

	public Vistoria porId(Long codigoVistoria) {
		return manager.find(Vistoria.class, codigoVistoria);
	}

	public List<Vistoria> todas() {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
				
		TypedQuery<Vistoria> query = manager.createQuery("from Vistoria WHERE bemMovel.localDestino.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession", Vistoria.class);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
		return query.getResultList();
	}

	public void adicionar(Vistoria vistoria) {
		this.manager.persist(vistoria);
	}

	// pesquisa por bem patrimonial e número da vistoria
	public Vistoria porVistoriaBem(Inventario inventario, BemMovel bemMovel) {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
	
		TypedQuery<Vistoria> query = manager
				.createQuery("from Vistoria where inventario=:inventario AND bemMovel=:bemMovel AND bemMovel.localDestino.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession", Vistoria.class);
		query.setParameter("inventario", inventario);
		query.setParameter("bemMovel", bemMovel);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public Vistoria guardar(Vistoria vistoria) {
		return this.manager.merge(vistoria);
	}

	public void remover(Vistoria vistoria) {
		this.manager.remove(vistoria);
	}
}
