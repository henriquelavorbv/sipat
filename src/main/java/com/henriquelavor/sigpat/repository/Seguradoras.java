package com.henriquelavor.sigpat.repository;

import java.io.Serializable;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.henriquelavor.sigpat.model.Seguradora;

/**
 * @author Henrique Lavor
 * @data 10.08.2017
 * @version 1.0
 */
public class Seguradoras implements Serializable {

	private static final long serialVersionUID = 1L;

	private EntityManager manager;

	@Inject
	public Seguradoras(EntityManager manager) {
		this.manager = manager;
	}

	public Seguradora porId(Long codigoSeguradora) {
		return manager.find(Seguradora.class, codigoSeguradora);
	}

	public List<Seguradora> todas() {
		TypedQuery<Seguradora> query = manager.createQuery("from Seguradora", Seguradora.class);
		return query.getResultList();
	}

	public void adicionar(Seguradora seguradora) {
		this.manager.persist(seguradora);
	}

	// pesquisa por nome
	public Seguradora porNome(String nome) {
		TypedQuery<Seguradora> query = manager.createQuery("from Seguradora where descricaoSeguradora=:nome",
				Seguradora.class);
		query.setParameter("nome", nome);
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public Seguradora guardar(Seguradora seguradora) {
		return this.manager.merge(seguradora);
	}

	public void remover(Seguradora seguradora) {
		this.manager.remove(seguradora);
	}
}