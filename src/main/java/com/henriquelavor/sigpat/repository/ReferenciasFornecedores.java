package com.henriquelavor.sigpat.repository;

import java.io.Serializable;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.henriquelavor.sigpat.model.ReferenciaFornecedor;
/**
 * @author Henrique Lavor
 * @data 27.05.2016
 * @version 1.0
 */

public class ReferenciasFornecedores implements Serializable {

	private static final long serialVersionUID = 1L;

	private EntityManager manager;

	@Inject
	public ReferenciasFornecedores(EntityManager manager) {
		this.manager = manager;
	}

	public ReferenciaFornecedor porId(Long codigoReferenciaFornecedor) {
		return manager.find(ReferenciaFornecedor.class, codigoReferenciaFornecedor);
	}

	//ReferenciaFornecedor package: model
	public List<ReferenciaFornecedor> todas() {
		TypedQuery<ReferenciaFornecedor> query = manager.createQuery("from ReferenciaFornecedor", ReferenciaFornecedor.class);
		return query.getResultList();
	}

	public void adicionar(ReferenciaFornecedor referenciaFornecedor) {
		this.manager.persist(referenciaFornecedor);
	}
	
	// pesquisa por nome
		public ReferenciaFornecedor porNome(String nome) {
			TypedQuery<ReferenciaFornecedor> query = manager.createQuery(
					"from ReferenciaFornecedor where descricaoReferenciaFornecedor=:nome", ReferenciaFornecedor.class);
			query.setParameter("nome", nome);
			try {
				return query.getSingleResult();
			} catch (NoResultException e) {
				return null;
			}
		}
		
		public ReferenciaFornecedor guardar(ReferenciaFornecedor referenciaFornecedor) {
			return this.manager.merge(referenciaFornecedor);
		}
		
		public void remover(ReferenciaFornecedor referenciaFornecedor){
			this.manager.remove(referenciaFornecedor);
		}
}