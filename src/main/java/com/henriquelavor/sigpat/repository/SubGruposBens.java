package com.henriquelavor.sigpat.repository;

import java.io.Serializable;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.henriquelavor.sigpat.model.GrupoBens;
import com.henriquelavor.sigpat.model.SubGrupoBens;

/**
 * @author Henrique Lavor
 * @data 13.04.2016
 * @version 1.0
 */

public class SubGruposBens implements Serializable {

	private static final long serialVersionUID = 1L;

	private EntityManager manager;

	@Inject
	public SubGruposBens(EntityManager manager) {
		this.manager = manager;
	}

	public SubGrupoBens porId(Long codigoSubGrupoBem) {
		return manager.find(SubGrupoBens.class, codigoSubGrupoBem);
	}
	
	// SubGrupoBens do package: model
	public List<SubGrupoBens> todos() {
		TypedQuery<SubGrupoBens> query = manager.createQuery("from SubGrupoBens", SubGrupoBens.class);
		return query.getResultList();
	}

	public void adicionar(SubGrupoBens subGrupoBens) {
		this.manager.persist(subGrupoBens);
	}

	// pesquisa por nome
	public SubGrupoBens porNome(String nome) {
		TypedQuery<SubGrupoBens> query = manager.createQuery("from SubGrupoBens where descricaoSubGrupoBem=:nome",
				SubGrupoBens.class);
		query.setParameter("nome", nome);
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public SubGrupoBens guardar(SubGrupoBens subGrupoBens) {
		return this.manager.merge(subGrupoBens);
	}

	public void remover(SubGrupoBens subGrupoBens) {
		this.manager.remove(subGrupoBens);
	}

	public List<SubGrupoBens> gruposDe(GrupoBens grupoBem) {
		return manager.createQuery("from SubGrupoBens where grupoBens.codigoGrupoBem=:raiz ORDER BY descricaoSubGrupoBem", SubGrupoBens.class).setParameter("raiz",grupoBem.getCodigoGrupoBem()).getResultList();
	}
}
