package com.henriquelavor.sigpat.repository;

import java.io.Serializable;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.henriquelavor.sigpat.model.Fornecedor;

/**
 * @author Henrique Lavor
 * @data 31.05.2016
 * @version 1.0
 */

public class Fornecedores implements Serializable {

	private static final long serialVersionUID = 1L;

	private EntityManager manager;

	@Inject
	public Fornecedores(EntityManager manager) {
		this.manager = manager;
	}

	public Fornecedor porId(Long codigoFornecedor) {
		return manager.find(Fornecedor.class, codigoFornecedor);
	}

	// Fornecedor do package: model
	public List<Fornecedor> todos() {
		TypedQuery<Fornecedor> query = manager.createQuery("from Fornecedor", Fornecedor.class);
		return query.getResultList();
	}
	
	// Fornecedor do package: model
	public List<Fornecedor> todosNaoBloqueados() {
		TypedQuery<Fornecedor> query = manager.createQuery("from Fornecedor WHERE referenciaFornecedor.regraFornecedor !='Bloqueado'", Fornecedor.class);
		return query.getResultList();
	}


	public void adicionar(Fornecedor fornecedor) {
		this.manager.persist(fornecedor);
	}

	// pesquisa por Nome Fornecedor
	public Fornecedor porNomeFantasia(String nome) {
		TypedQuery<Fornecedor> query = manager.createQuery("from Fornecedor where nomeFornecedor=:nome",
				Fornecedor.class);
		query.setParameter("nome", nome);
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	// pesquisa por CNPJ ou CPF
	public Fornecedor porCnpjCpf(String cnpjCpf) {
		TypedQuery<Fornecedor> query = manager.createQuery("from Fornecedor where cnpjCpf=:cnpjCpf",
				Fornecedor.class);
		query.setParameter("cnpjCpf", cnpjCpf);
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public Fornecedor guardar(Fornecedor fornecedor) {
		return this.manager.merge(fornecedor);
	}

	public void remover(Fornecedor fornecedor) {
		this.manager.remove(fornecedor);
	}
}
