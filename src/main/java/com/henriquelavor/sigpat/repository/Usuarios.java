package com.henriquelavor.sigpat.repository;

import java.io.Serializable;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;


import com.henriquelavor.sigpat.model.Usuario;
/**
 * @author Henrique Lavor
 * @data  31.05.2019
 * @version 1.0
 */

public class Usuarios implements Serializable {

	private static final long serialVersionUID = 1L;

	private EntityManager manager;

	
	@Inject
	public Usuarios(EntityManager manager) {
		this.manager = manager;
	}

	public Usuario porId(Long codigoUsuario) {
		return manager.find(Usuario.class, codigoUsuario);
	}

	//Usuario package: model
	public List<Usuario> todos() {
		TypedQuery<Usuario> query = manager.createQuery("from Usuario", Usuario.class);
		return query.getResultList();
	}

	public void adicionar(Usuario usuario) {
		this.manager.persist(usuario);
	}
	
	// pesquisa por email
		public Usuario porEmail(String email) {
		
		Usuario usuario = null;
		
		try {
			usuario =  this.manager.createQuery("from Usuario where lower(email) = :email", Usuario.class)
				.setParameter("email", email.toLowerCase()).getSingleResult();
		} catch (NoResultException e) {
			// nenhum usuário encontrado com o e-mail informado
		}
		return usuario;
			
	}
	
		
		public Usuario guardar(Usuario usuario) {
			return this.manager.merge(usuario);
		}
		
		public void remover(Usuario usuario){
			this.manager.remove(usuario);
		}
}