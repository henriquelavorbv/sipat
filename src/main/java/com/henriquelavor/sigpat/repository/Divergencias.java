package com.henriquelavor.sigpat.repository;

import java.io.Serializable;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.henriquelavor.sigpat.model.Divergencia;
/**
 * @author Henrique Lavor
 * @data 25.09.2017
 * @version 1.0
 */

public class Divergencias implements Serializable {

	private static final long serialVersionUID = 1L;

	private EntityManager manager;

	@Inject
	public Divergencias(EntityManager manager) {
		this.manager = manager;
	}

	public Divergencia porId(Long codigoDivergencia) {
		return manager.find(Divergencia.class, codigoDivergencia);
	}

	public List<Divergencia> todas() {
		TypedQuery<Divergencia> query = manager.createQuery("from Divergencia", Divergencia.class);
		return query.getResultList();
	}
	
	public List<Divergencia> todasDivergencias() {
		TypedQuery<Divergencia> query = manager.createQuery("from Divergencia WHERE descricaoDivergencia != 'Não há divergência'", Divergencia.class);
		return query.getResultList();
	}
	
	

	public void adicionar(Divergencia divergencia) {
		this.manager.persist(divergencia);
	}
	
	// pesquisa por nome
		public Divergencia porNome(String nome) {
			TypedQuery<Divergencia> query = manager.createQuery(
					"from Divergencia where descricaoDivergencia=:nome", Divergencia.class);
			query.setParameter("nome", nome);
			try {
				return query.getSingleResult();
			} catch (NoResultException e) {
				return null;
			}
		}
		
		public Divergencia guardar(Divergencia divergencia) {
			return this.manager.merge(divergencia);
		}
		
		public void remover(Divergencia divergencia){
			this.manager.remove(divergencia);
		}
}