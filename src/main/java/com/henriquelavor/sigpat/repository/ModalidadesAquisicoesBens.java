package com.henriquelavor.sigpat.repository;

import java.io.Serializable;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.henriquelavor.sigpat.model.ModalidadeAquisicaoBens;
/**
 * @author Henrique Lavor
 * @data 18.05.2016
 * @version 1.0
 */

public class ModalidadesAquisicoesBens implements Serializable {

	private static final long serialVersionUID = 1L;

	private EntityManager manager;

	@Inject
	public ModalidadesAquisicoesBens(EntityManager manager) {
		this.manager = manager;
	}

	public ModalidadeAquisicaoBens porId(Long codigoModalidadeAquisicaoBem) {
		return manager.find(ModalidadeAquisicaoBens.class, codigoModalidadeAquisicaoBem);
	}

	// ModalidadeAquisicaoBens do package: model
	public List<ModalidadeAquisicaoBens> todas() {
		TypedQuery<ModalidadeAquisicaoBens> query = manager.createQuery("from ModalidadeAquisicaoBens", ModalidadeAquisicaoBens.class);
		return query.getResultList();
	}

	public void adicionar(ModalidadeAquisicaoBens modalidadeAquisicaoBens) {
		this.manager.persist(modalidadeAquisicaoBens);
	}
	
	// pesquisa por nome
		public ModalidadeAquisicaoBens porNome(String nome) {
			TypedQuery<ModalidadeAquisicaoBens> query = manager.createQuery(
					"from ModalidadeAquisicaoBens where descricaoModalidadeAquisicaoBem=:nome", ModalidadeAquisicaoBens.class);
			query.setParameter("nome", nome);
			try {
				return query.getSingleResult();
			} catch (NoResultException e) {
				return null;
			}
		}
		
		public ModalidadeAquisicaoBens guardar(ModalidadeAquisicaoBens modalidadeAquisicaoBens) {
			return this.manager.merge(modalidadeAquisicaoBens);
		}
		
		public void remover(ModalidadeAquisicaoBens modalidadeAquisicaoBens){
			this.manager.remove(modalidadeAquisicaoBens);
		}
}