package com.henriquelavor.sigpat.repository;

import java.io.Serializable;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.henriquelavor.sigpat.model.EstadoBens;
/**
 * @author Henrique Lavor
 * @data 17.05.2016
 * @version 1.0
 */

public class EstadosBens implements Serializable {

	private static final long serialVersionUID = 1L;

	private EntityManager manager;

	@Inject
	public EstadosBens(EntityManager manager) {
		this.manager = manager;
	}

	public EstadoBens porId(Long codigoEstadoBem) {
		return manager.find(EstadoBens.class, codigoEstadoBem);
	}

	// EstadoBens do package: model
	public List<EstadoBens> todos() {
		TypedQuery<EstadoBens> query = manager.createQuery("from EstadoBens", EstadoBens.class);
		return query.getResultList();
	}

	public void adicionar(EstadoBens estadoBens) {
		this.manager.persist(estadoBens);
	}
	
	// pesquisa por nome
		public EstadoBens porNome(String nome) {
			TypedQuery<EstadoBens> query = manager.createQuery(
					"from EstadoBens where descricaoEstadoBem=:nome", EstadoBens.class);
			query.setParameter("nome", nome);
			try {
				return query.getSingleResult();
			} catch (NoResultException e) {
				return null;
			}
		}
		
		
		public EstadoBens guardar(EstadoBens estadoBens) {
			return this.manager.merge(estadoBens);
		}
		
		public void remover(EstadoBens estadoBens){
			this.manager.remove(estadoBens);
		}
}