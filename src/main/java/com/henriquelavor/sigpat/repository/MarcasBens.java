package com.henriquelavor.sigpat.repository;

import java.io.Serializable;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.henriquelavor.sigpat.model.MarcaBens;

/**
 * @author Henrique Lavor
 * @data 28.04.2016
 * @version 1.0
 */

public class MarcasBens implements Serializable {

	private static final long serialVersionUID = 1L;

	private EntityManager manager;

	@Inject
	public MarcasBens(EntityManager manager) {
		this.manager = manager;
	}

	public MarcaBens porId(Long codigoMarcaBem) {
		return manager.find(MarcaBens.class, codigoMarcaBem);
	}

	// MarcaBens do package: model
	public List<MarcaBens> todas() {
		TypedQuery<MarcaBens> query = manager.createQuery("from MarcaBens", MarcaBens.class);
		return query.getResultList();
	}

	public void adicionar(MarcaBens marcaBens) {
		this.manager.persist(marcaBens);
	}
	
	// pesquisa por nome
		public MarcaBens porNome(String nome) {
			TypedQuery<MarcaBens> query = manager.createQuery(
					"from MarcaBens where descricaoMarcaBem=:nome", MarcaBens.class);
			query.setParameter("nome", nome);
			try {
				return query.getSingleResult();
			} catch (NoResultException e) {
				return null;
			}
		}
		
		public MarcaBens guardar(MarcaBens marcaBens) {
			return this.manager.merge(marcaBens);
		}
		
		public void remover(MarcaBens marcaBens){
			this.manager.remove(marcaBens);
		}
		
}
