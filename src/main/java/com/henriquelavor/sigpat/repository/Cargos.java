package com.henriquelavor.sigpat.repository;

import java.io.Serializable;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.henriquelavor.sigpat.model.Cargo;
/**
 * @author Henrique Lavor
 * @data 19.05.2016
 * @version 1.0
 */

public class Cargos implements Serializable {

	private static final long serialVersionUID = 1L;

	private EntityManager manager;

	@Inject
	public Cargos(EntityManager manager) {
		this.manager = manager;
	}

	public Cargo porId(Long codigoCargo) {
		return manager.find(Cargo.class, codigoCargo);
	}

	//CargoBens package: model
	public List<Cargo> todos() {
		TypedQuery<Cargo> query = manager.createQuery("from Cargo", Cargo.class);
		return query.getResultList();
	}

	public void adicionar(Cargo cargo) {
		this.manager.persist(cargo);
	}
	
	// pesquisa por nome
		public Cargo porNome(String nome) {
			TypedQuery<Cargo> query = manager.createQuery(
					"from Cargo where descricaoCargo=:nome", Cargo.class);
			query.setParameter("nome", nome);
			try {
				return query.getSingleResult();
			} catch (NoResultException e) {
				return null;
			}
		}
		
		public Cargo guardar(Cargo cargo) {
			return this.manager.merge(cargo);
		}
		
		public void remover(Cargo cargo){
			this.manager.remove(cargo);
		}
}