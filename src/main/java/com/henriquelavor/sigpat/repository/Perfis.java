package com.henriquelavor.sigpat.repository;

import java.io.Serializable;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.henriquelavor.sigpat.model.Perfil;
/**
 * @author Henrique Lavor
 * @data 01.06.2019
 * @version 1.0
 */

public class Perfis implements Serializable {

	private static final long serialVersionUID = 1L;

	private EntityManager manager;

	@Inject
	public Perfis(EntityManager manager) {
		this.manager = manager;
	}

	public Perfil porId(Long codigoPerfil) {
		return manager.find(Perfil.class, codigoPerfil);
	}

	public List<Perfil> todos() {
		TypedQuery<Perfil> query = manager.createQuery("from Perfil", Perfil.class);
		return query.getResultList();
	}

	public void adicionar(Perfil perfil) {
		this.manager.persist(perfil);
	}
	
	// pesquisa por nome
		public Perfil porDescricaoPerfil(String descricaoPerfil) {
			TypedQuery<Perfil> query = manager.createQuery(
					"from Perfil where descricaoPerfil=:descricaoPerfil", Perfil.class);
			query.setParameter("descricaoPerfil", descricaoPerfil);
			try {
				return query.getSingleResult();
			} catch (NoResultException e) {
				return null;
			}
		}
		
		public Perfil guardar(Perfil perfil) {
			return this.manager.merge(perfil);
		}
		
		public void remover(Perfil perfil){
			this.manager.remove(perfil);
		}
}