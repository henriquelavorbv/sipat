package com.henriquelavor.sigpat.repository;

import java.io.Serializable;

import java.util.List;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.henriquelavor.sigpat.model.Reparo;

/**
 * @author Henrique Lavor
 * @data 31.08.2017
 * @version 1.0
 */

public class Reparos implements Serializable {

	private static final long serialVersionUID = 1L;

	private EntityManager manager;

	@Inject
	public Reparos(EntityManager manager) {
		this.manager = manager;
	}

	public Reparo porId(Long codigoReparo) {
		return manager.find(Reparo.class, codigoReparo);
	}
	
	// metodo usado para autocomplete para sugestao ao preencher o numero do patrimonio, Lista dos reparos da UO logada	
		public List<String> numeroPatrimonioQueContem(String descricao) {
			//Pega a sessao do Codigo UO
			HttpServletRequest req = (HttpServletRequest) 
			FacesContext.getCurrentInstance().getExternalContext().getRequest();
			
			HttpSession session = (HttpSession) req.getSession();
			String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
			
			TypedQuery<String> query = manager.createQuery(
					"SELECT distinct bemMovel.numeroPatrimonio from Reparo " + "WHERE dataRetorno IS NULL AND upper(bemMovel.numeroPatrimonio) like upper(:descricao) AND bemMovel.localDestino.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession",
					String.class);
			query.setParameter("descricao", "%" + descricao + "%");
			query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
			return query.getResultList();
		}

	//Lista de todos os reparos da UO logada	
	public List<Reparo> todos() {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
		
		
		TypedQuery<Reparo> query = manager.createQuery("from Reparo WHERE bemMovel.localDestino.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession ORDER BY dataSaida DESC", Reparo.class);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
		return query.getResultList();
	}

	public void adicionar(Reparo reparo) {
		this.manager.persist(reparo);
	}

	public Reparo guardar(Reparo reparo) {
		return this.manager.merge(reparo);
	}

	public void remover(Reparo reparo) {
		this.manager.remove(reparo);
	}
}
