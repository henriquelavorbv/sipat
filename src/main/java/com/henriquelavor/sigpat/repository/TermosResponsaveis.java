package com.henriquelavor.sigpat.repository;

import java.io.Serializable;

import java.util.List;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.henriquelavor.sigpat.model.TermoResponsavel;

/**
 * @author Henrique Lavor
 * @data 02.12.2016
 * @version 1.0
 */

public class TermosResponsaveis implements Serializable {

	private static final long serialVersionUID = 1L;

	private EntityManager manager;

	@Inject
	public TermosResponsaveis(EntityManager manager) {
		this.manager = manager;
	}

	public TermoResponsavel porId(Long codigoTermoResponsavel) {
		return manager.find(TermoResponsavel.class, codigoTermoResponsavel);
	}

	// package: model
	//Lista todos os termos de responsabilidade da UO logada
	public List<TermoResponsavel> todos() {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
				
		TypedQuery<TermoResponsavel> query = manager.createQuery("from TermoResponsavel where localDestino.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession", TermoResponsavel.class);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
		return query.getResultList();
	}

	public void adicionar(TermoResponsavel termoResponsavel) {
		this.manager.persist(termoResponsavel);
	}

	// Atualizar Termos Aberto ao solicitar para Gerar Termo da UO logada
	public int atualizarTermosGerados() {
		//Pega a sessao do Codigo UO
		//HttpServletRequest req = (HttpServletRequest) 
		//FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		//HttpSession session = (HttpSession) req.getSession();
		//String IDGestoraSession = (String) session.getAttribute("ID_GESTORA");
		//Long IDGestoraSession = (Long) session.getAttribute("ID_GESTORA");
		
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
				
		//String sql = "UPDATE tb_termo_reponsavel SET situacao='Gerado' WHERE situacao='Aberto' AND cod_local_destino="+IDGestoraSession;
		
		String sql = "UPDATE tb_termo_reponsavel SET situacao='Gerado' FROM (SELECT * FROM tb_termo_reponsavel " +
						"INNER JOIN tb_unidade_administrativa ON " +
						"tb_termo_reponsavel.cod_local_destino = tb_unidade_administrativa.codigounidadeadministrativa "+
						"INNER JOIN tb_unidade_gestora ON "+
						"tb_unidade_administrativa.cod_unidade_gestora = tb_unidade_gestora.codigounidadegestora) subquery " +
						"WHERE tb_termo_reponsavel.situacao='Aberto' AND subquery.codigo_orcamentario='"+ codOrcamentarioSession +"'";
		System.out.println(sql);//teste
		
		Query query = manager.createNativeQuery(sql);
		int updateCount1 = query.executeUpdate();
		return updateCount1;
	}

	// pesquisa por numero do Termo de Responsabilidade da UO logada
	public List<TermoResponsavel> todosTermosPorNumero(String numero) {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
				
		TypedQuery<TermoResponsavel> query = manager
				.createQuery("from TermoResponsavel where numeroTermoResponsabilidade=:numero AND localDestino.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession", TermoResponsavel.class);
		query.setParameter("numero", numero);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
		
		try {
			return query.getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}

	// metodo usado para autocomplete para sugestao ao preencher o numero de termo de responsavel da UO logada
	public List<String> numeroTermoResponsavelQueContem(String descricao) {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
				
		TypedQuery<String> query = manager.createQuery(
				"select distinct numeroTermoResponsabilidade from TermoResponsavel " + " where upper(numeroTermoResponsabilidade) like upper(:descricao) and localDestino.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession",
				String.class);
		query.setParameter("descricao", "%" + descricao + "%");
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
		return query.getResultList();
	}

	// pesquisa por situacao Aberto informando o numero do termo de responsabilidade da UO logada
	public List<TermoResponsavel> todosTermosAbertoPorNumero(String numeroTermo) {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
						
		TypedQuery<TermoResponsavel> query = manager.createQuery(
				"from TermoResponsavel WHERE situacao='Aberto' AND numeroTermoResponsabilidade=:numeroTermo AND localDestino.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession",
				TermoResponsavel.class);
		query.setParameter("numeroTermo", numeroTermo);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
		return query.getResultList();
	}

	// pesquisa por situacao GERADO informando o numero do termo e caso
	// encontre, NAO PERMITIR USAR ESTE TERMO,
	//esta função não necessita filtrar por UO
	public List<TermoResponsavel> todosTermosGeradoPorNumero(String numeroTermo) {
		TypedQuery<TermoResponsavel> query = manager.createQuery(
				"from TermoResponsavel WHERE situacao='Gerado' AND numeroTermoResponsabilidade=:numeroTermo",
				TermoResponsavel.class);
		query.setParameter("numeroTermo", numeroTermo);
		return query.getResultList();
	}

	public TermoResponsavel guardar(TermoResponsavel termoResponsavel) {
		return this.manager.merge(termoResponsavel);
	}

	public void remover(TermoResponsavel termoResponsavel) {
		this.manager.remove(termoResponsavel);
	}
}
