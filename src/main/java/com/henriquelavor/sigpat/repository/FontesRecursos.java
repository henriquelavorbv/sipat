package com.henriquelavor.sigpat.repository;

import java.io.Serializable;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.henriquelavor.sigpat.model.FonteRecurso;

/**
 * @author Henrique Lavor
 * @data 16.01.2017
 * @version 1.0
 */

public class FontesRecursos implements Serializable {

	private static final long serialVersionUID = 1L;

	private EntityManager manager;

	@Inject
	public FontesRecursos(EntityManager manager) {
		this.manager = manager;
	}

	public FonteRecurso porId(Long codigoFonteRecurso) {
		return manager.find(FonteRecurso.class, codigoFonteRecurso);
	}

	// FonteRecurso package: model
	public List<FonteRecurso> todas() {
		TypedQuery<FonteRecurso> query = manager.createQuery("from FonteRecurso", FonteRecurso.class);
		return query.getResultList();
	}

	public void adicionar(FonteRecurso fonteRecurso) {
		this.manager.persist(fonteRecurso);
	}
	
	// pesquisa por fonte
	public FonteRecurso porFonte(Long fonte) {
		TypedQuery<FonteRecurso> query = manager.createQuery("from FonteRecurso where fonte=:fonte", FonteRecurso.class);
		query.setParameter("fonte", fonte);
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}

	}

	// pesquisa por nome
	public FonteRecurso porNome(String nome) {
		TypedQuery<FonteRecurso> query = manager.createQuery("from FonteRecurso where nomeFonteRecurso=:nome", FonteRecurso.class);
		query.setParameter("nome", nome);
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	

	public FonteRecurso guardar(FonteRecurso fonteRecurso) {
		return this.manager.merge(fonteRecurso);
	}

	public void remover(FonteRecurso fonteRecurso) {
		this.manager.remove(fonteRecurso);
	}
}