package com.henriquelavor.sigpat.repository;

import java.io.Serializable;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.henriquelavor.sigpat.model.TipoInstituicao;
/**
 * @author Henrique Lavor
 * @data 23.05.2016
 * @version 1.0
 */

public class TiposInstituicoes implements Serializable {

	private static final long serialVersionUID = 1L;

	private EntityManager manager;

	@Inject
	public TiposInstituicoes(EntityManager manager) {
		this.manager = manager;
	}

	public TipoInstituicao porId(Long codigoTipoInstituicao) {
		return manager.find(TipoInstituicao.class, codigoTipoInstituicao);
	}

	//TipoInstituicao package: model
	public List<TipoInstituicao> todos() {
		TypedQuery<TipoInstituicao> query = manager.createQuery("from TipoInstituicao", TipoInstituicao.class);
		return query.getResultList();
	}

	public void adicionar(TipoInstituicao tipoInstituicao) {
		this.manager.persist(tipoInstituicao);
	}
	
	// pesquisa por nome
		public TipoInstituicao porNome(String nome) {
			TypedQuery<TipoInstituicao> query = manager.createQuery(
					"from TipoInstituicao where descricaoTipoInstituicao=:nome", TipoInstituicao.class);
			query.setParameter("nome", nome);
			try {
				return query.getSingleResult();
			} catch (NoResultException e) {
				return null;
			}
		}
		
		public TipoInstituicao guardar(TipoInstituicao tipoInstituicao) {
			return this.manager.merge(tipoInstituicao);
		}
		
		public void remover(TipoInstituicao tipoInstituicao){
			this.manager.remove(tipoInstituicao);
		}
}