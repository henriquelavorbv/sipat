package com.henriquelavor.sigpat.repository;

import java.io.Serializable;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.henriquelavor.sigpat.model.Funcionario;

/**
 * @author Henrique Lavor
 * @data 29.07.2016
 * @version 1.0
 */

public class Funcionarios implements Serializable {

	private static final long serialVersionUID = 1L;

	private EntityManager manager;

	@Inject
	public Funcionarios(EntityManager manager) {
		this.manager = manager;
	}

	public Funcionario porId(Long codigoFuncionario) {
		return manager.find(Funcionario.class, codigoFuncionario);
	}

	// Lista de todos os funcionarios da UO logada
	public List<Funcionario> todos() {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
				
		TypedQuery<Funcionario> query = manager.createQuery("from Funcionario where unidadeAdministrativa.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession", Funcionario.class);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
		return query.getResultList();
	}
	
	
	// Lista de todos Funcionarios Geral usado na Edição
		public List<Funcionario> todosGeral() {
					
			TypedQuery<Funcionario> query = manager.createQuery("from Funcionario", Funcionario.class);
			return query.getResultList();
		}
	
	
	// Lista de todos os funcionarios da UO logada
		public List<Funcionario> todosDisponiveis() {
			//	TypedQuery<Funcionario> query = manager.createQuery("SELECT f from Usuario u join u.funcionario f where f.codigoFuncionario = u.funcionario.codigoFuncionario", Funcionario.class);
			TypedQuery<Funcionario> query = manager.createQuery("SELECT f from Usuario u right join u.funcionario f where u.funcionario.codigoFuncionario IS NULL", Funcionario.class);
			return query.getResultList();
		}
	
	
	/*// Lista de todos os funcionarios do Sistema
		public List<Funcionario> todosDisponiveis() {
			
			TypedQuery<Funcionario> query = manager.createQuery("SELECT u FROM Usuario u JOIN u.codigoFuncionario.codigoFuncionario f ON u.codigoFuncionario", Funcionario.class);
			return query.getResultList();
		
			//return query.getResultList();
		}
		*/
		
	
	// Lista de todos os funcionarios Responsáveis pela guarda de bens patrimoniais da UO logada
	public List<Funcionario> todosResponsaveisBem() {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
		
		TypedQuery<Funcionario> query = manager.createQuery("from Funcionario WHERE responsavelPatrimonial='Sim' AND tipoSituacaoFuncionario='Ativo' AND unidadeAdministrativa.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession", Funcionario.class);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
		return query.getResultList();
	}
	
	// Lista de todos os funcionarios Responsáveis pelo Inventário Patrimonial da UO logada
	public List<Funcionario> todosResponsaveisInventario() {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
		
		TypedQuery<Funcionario> query = manager.createQuery("from Funcionario WHERE responsavelInventario='Sim' AND tipoSituacaoFuncionario='Ativo' AND unidadeAdministrativa.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession", Funcionario.class);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
		return query.getResultList();
	}

	public void adicionar(Funcionario funcionario) {
		this.manager.persist(funcionario);
	}

	// pesquisa por Nome Funcionario da UO logada
	public Funcionario porNomeFuncionario(String nome) {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
		
		TypedQuery<Funcionario> query = manager.createQuery("from Funcionario where nomeFuncionario=:nome AND unidadeAdministrativa.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession",
				Funcionario.class);
		query.setParameter("nome", nome);
		try {
			query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	// pesquisa por CPF da UO logada
	public Funcionario porCpf(String cpf) {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
				
		TypedQuery<Funcionario> query = manager.createQuery("from Funcionario where cpf=:cpf AND unidadeAdministrativa.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession",
				Funcionario.class);
		query.setParameter("cpf", cpf);
		try {
			query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	public Funcionario guardar(Funcionario funcionario) {
		return this.manager.merge(funcionario);
	}

	public void remover(Funcionario funcionario) {
		this.manager.remove(funcionario);
	}
}
