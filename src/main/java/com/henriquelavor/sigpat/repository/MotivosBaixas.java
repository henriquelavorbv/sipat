package com.henriquelavor.sigpat.repository;

import java.io.Serializable;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.henriquelavor.sigpat.model.MotivoBaixa;
/**
 * @author Henrique Lavor
 * @data 08.08.2016
 * @version 1.0
 */

public class MotivosBaixas implements Serializable {

	private static final long serialVersionUID = 1L;

	private EntityManager manager;

	@Inject
	public MotivosBaixas(EntityManager manager) {
		this.manager = manager;
	}

	public MotivoBaixa porId(Long codigoMotivoBaixa) {
		return manager.find(MotivoBaixa.class, codigoMotivoBaixa);
	}

	//MotivoBaixa package: model
	public List<MotivoBaixa> todos() {
		TypedQuery<MotivoBaixa> query = manager.createQuery("from MotivoBaixa", MotivoBaixa.class);
		return query.getResultList();
	}

	public void adicionar(MotivoBaixa motivoBaixa) {
		this.manager.persist(motivoBaixa);
	}
	
	// pesquisa por nome
		public MotivoBaixa porNome(String nome) {
			TypedQuery<MotivoBaixa> query = manager.createQuery(
					"from MotivoBaixa where descricaoMotivoBaixa=:nome", MotivoBaixa.class);
			query.setParameter("nome", nome);
			try {
				return query.getSingleResult();
			} catch (NoResultException e) {
				return null;
			}
		}
		
		public MotivoBaixa guardar(MotivoBaixa motivoBaixa) {
			return this.manager.merge(motivoBaixa);
		}
		
		public void remover(MotivoBaixa motivoBaixa){
			this.manager.remove(motivoBaixa);
		}
}