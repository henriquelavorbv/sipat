package com.henriquelavor.sigpat.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.henriquelavor.sigpat.model.BemMovelGarantiaView;
/**
 * @author Henrique Lavor
 * @data 07.09.2019
 * @version 1.0
 */

public class BensMoveisGarantiasView implements Serializable {

	private static final long serialVersionUID = 1L;

	private EntityManager manager;

	@Inject
	public BensMoveisGarantiasView(EntityManager manager) {
		this.manager = manager;
	}

	public BemMovelGarantiaView porId(Long codigoBemMovelGarantia) {
		return manager.find(BemMovelGarantiaView.class, codigoBemMovelGarantia);
	}

	public List<BemMovelGarantiaView> todos() {
		TypedQuery<BemMovelGarantiaView> query = manager.createQuery("from BemMovelGarantiaView", BemMovelGarantiaView.class);
		return query.getResultList();
	}
	
}