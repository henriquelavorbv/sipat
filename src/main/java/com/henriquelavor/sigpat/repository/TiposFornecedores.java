package com.henriquelavor.sigpat.repository;

import java.io.Serializable;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.henriquelavor.sigpat.model.TipoFornecedor;
/**
 * @author Henrique Lavor
 * @data 27.05.2016
 * @version 1.0
 */

public class TiposFornecedores implements Serializable {

	private static final long serialVersionUID = 1L;

	private EntityManager manager;

	@Inject
	public TiposFornecedores(EntityManager manager) {
		this.manager = manager;
	}

	public TipoFornecedor porId(Long codigoTipoFornecedor) {
		return manager.find(TipoFornecedor.class, codigoTipoFornecedor);
	}

	//TipoFornecedor package: model
	public List<TipoFornecedor> todos() {
		TypedQuery<TipoFornecedor> query = manager.createQuery("from TipoFornecedor", TipoFornecedor.class);
		return query.getResultList();
	}

	public void adicionar(TipoFornecedor tipoFornecedor) {
		this.manager.persist(tipoFornecedor);
	}
	
	// pesquisa por nome
		public TipoFornecedor porNome(String nome) {
			TypedQuery<TipoFornecedor> query = manager.createQuery(
					"from TipoFornecedor where descricaoTipoFornecedor=:nome", TipoFornecedor.class);
			query.setParameter("nome", nome);
			try {
				return query.getSingleResult();
			} catch (NoResultException e) {
				return null;
			}
		}
		
		public TipoFornecedor guardar(TipoFornecedor tipoFornecedor) {
			return this.manager.merge(tipoFornecedor);
		}
		
		public void remover(TipoFornecedor tipoFornecedor){
			this.manager.remove(tipoFornecedor);
		}
}