package com.henriquelavor.sigpat.repository;

import java.io.Serializable;

import java.util.List;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.henriquelavor.sigpat.model.UnidadeAdministrativa;

/**
 * @author Henrique Lavor
 * @data 25.05.2016
 * @version 1.0
 */

public class UnidadesAdministrativas implements Serializable {

	private static final long serialVersionUID = 1L;

	private EntityManager manager;

	@Inject
	public UnidadesAdministrativas(EntityManager manager) {
		this.manager = manager;
	}

	public UnidadeAdministrativa porId(Long codigoUnidadeAdministrativa) {
		return manager.find(UnidadeAdministrativa.class, codigoUnidadeAdministrativa);
	}
	
	// UnidadeAdministrativa do package: model
	public List<UnidadeAdministrativa> todas() {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
		
		//System.out.println("Func todas - Unidade Administrativa Codigo:" + codOrcamentarioSession);
				
		TypedQuery<UnidadeAdministrativa> query = manager.createQuery("from UnidadeAdministrativa WHERE unidadeGestora.codigoOrcamentario=:codOrcamentarioSession", UnidadeAdministrativa.class);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
		
		
		return query.getResultList();
	}
	
	// Todas Unidade Administrativa NÃO INCORPORADA (UNIP) de todas as Unidades Gestoras
	public List<UnidadeAdministrativa> todasUNIPUnidadeGestoras() {
		
		//Pega a sessao do Codigo UO para fazer consulta de exclusão
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
				
		TypedQuery<UnidadeAdministrativa> query = manager.createQuery("from UnidadeAdministrativa WHERE unidadeGestora.codigoOrcamentario <> :codOrcamentarioSession AND upper(nomeUnidadeAdministrativa) LIKE '%UNIDADE NÃO INCORPORADA%' AND upper(nomeResumido) LIKE '%UNIP%'", UnidadeAdministrativa.class);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
		
		return query.getResultList();
	}
	
	
	//nova implementacao para filtrar somente as UA  que nao estao com inventario Aberto da UO logada
	public List<UnidadeAdministrativa> todasDisponiveis() {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
		
		System.out.println("Func todasDisponiveis - Unidade Administrativa Codigo:" + codOrcamentarioSession);
		
		
		TypedQuery<UnidadeAdministrativa> query = manager.createQuery("from UnidadeAdministrativa WHERE vistoriando ='Não' AND unidadeGestora.codigoOrcamentario=:codOrcamentarioSession", UnidadeAdministrativa.class);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
		return query.getResultList();
	}

	public void adicionar(UnidadeAdministrativa unidadeAdministrativa) {
		this.manager.persist(unidadeAdministrativa);
	}

	// pesquisa por nome da Unidade Administrativa(UA) da UO logada
	public UnidadeAdministrativa porNomeUA(String nome, String nomeResumido, String predio, String piso, String corredor, String sala) {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
		
		TypedQuery<UnidadeAdministrativa> query = manager.createQuery("from UnidadeAdministrativa where nomeUnidadeAdministrativa=:nome AND unidadeGestora.codigoOrcamentario=:codOrcamentarioSession "
				+ "and nomeResumido=:nomeResumido "
				+ "and predio=:predio "
				+ "and piso=:piso "
				+ "and corredor=:corredor "
				+ "and sala=:sala",
				UnidadeAdministrativa.class);
		query.setParameter("nome", nome);
		query.setParameter("nomeResumido", nomeResumido);
		query.setParameter("predio", predio);
		query.setParameter("piso", piso);
		query.setParameter("corredor", corredor);
		query.setParameter("sala", sala);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 

		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
	
	
	

	public UnidadeAdministrativa guardar(UnidadeAdministrativa unidadeAdministrativa) {
		return this.manager.merge(unidadeAdministrativa);
	}

	public void remover(UnidadeAdministrativa unidadeAdministrativa) {
		this.manager.remove(unidadeAdministrativa);
	}
}
