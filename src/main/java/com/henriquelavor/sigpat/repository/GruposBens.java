package com.henriquelavor.sigpat.repository;

import java.io.Serializable;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.henriquelavor.sigpat.model.GrupoBens;
//import com.henriquelavor.sigpat.model.SubGrupoBens;

/**
 * @author Henrique Lavor
 * @data 13.04.2016
 * @version 1.0
 */

public class GruposBens implements Serializable {

	private static final long serialVersionUID = 1L;

	private EntityManager manager;

	@Inject
	public GruposBens(EntityManager manager) {this.manager = manager;}

	public GrupoBens porId(Long codigoGrupoBem) {
		return manager.find(GrupoBens.class, codigoGrupoBem);
	}

	// GrupoBens do package: model
	public List<GrupoBens> todos() {
		TypedQuery<GrupoBens> query = manager.createQuery("from GrupoBens order by contaContabil", GrupoBens.class);
		return query.getResultList();
	}
	
	public void adicionar(GrupoBens grupoBens) {
		this.manager.persist(grupoBens);
	}
	
	// pesquisa por nome
		public GrupoBens porNome(String nome) {
			TypedQuery<GrupoBens> query = manager.createQuery(
					"from GrupoBens where descricaoGrupoBem=:nome", GrupoBens.class);
			query.setParameter("nome", nome);
			try {
				return query.getSingleResult();
			} catch (NoResultException e) {
				return null;
			}
		}
		
		
		public GrupoBens guardar(GrupoBens grupoBens) {
			return this.manager.merge(grupoBens);
		}
		
		public void remover(GrupoBens grupoBens){
			this.manager.remove(grupoBens);
		}
		
}
