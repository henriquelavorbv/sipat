/*
 * Nome da Classe: UnidadesGestoras
 * Data da Criação: 23.05.2016
 * 
 * ATUALIZAÇÕES: 
 * 
 * No. da Atualização: 01
 * Data atualização: 17.07.2019 as 14:25
 * Objetivo: 
 * A Função public List<UnidadeGestora> todas(String codigoUO) foi atualizada,
 * para permitir filtrar apenas a Unidade Gestora logada
 * 
 */


package com.henriquelavor.sigpat.repository;

import java.io.Serializable;

import java.util.List;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.henriquelavor.sigpat.model.UnidadeGestora;
/**
 * @author Henrique Lavor
 * @data 23.05.2016
 * @version 1.0
 * @data 17.07.2019
 * @version 1.1
 */

public class UnidadesGestoras implements Serializable {

	private static final long serialVersionUID = 1L;

	private EntityManager manager;

	@Inject
	public UnidadesGestoras(EntityManager manager) {
		this.manager = manager;
	}

	public UnidadeGestora porId(Long codigoUnidadeGestora) {
		return manager.find(UnidadeGestora.class, codigoUnidadeGestora);
	}

	
	public List<UnidadeGestora> todas() {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
		
		TypedQuery<UnidadeGestora> query = manager.createQuery("from UnidadeGestora WHERE codigoOrcamentario=:codOrcamentarioSession", UnidadeGestora.class);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
		return query.getResultList();
	}
	
	public List<UnidadeGestora> todasGeral() {
		TypedQuery<UnidadeGestora> query = manager.createQuery("from UnidadeGestora", UnidadeGestora.class);
		return query.getResultList();
	}

	public void adicionar(UnidadeGestora unidadeGestora) {
		this.manager.persist(unidadeGestora);
	}
	
	// pesquisa por nome
	public UnidadeGestora porNome(String nome) {
			//Pega a sessao do Codigo UO
			HttpServletRequest req = (HttpServletRequest) 
			FacesContext.getCurrentInstance().getExternalContext().getRequest();
			
			HttpSession session = (HttpSession) req.getSession();
			String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
			
			TypedQuery<UnidadeGestora> query = manager.createQuery(
					"from UnidadeGestora where nomeUnidadeGestora=:nome AND codigoOrcamentario=:codOrcamentarioSession ", UnidadeGestora.class);
			query.setParameter("nome", nome);
			try {
				query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
				return query.getSingleResult();
			} catch (NoResultException e) {
				return null;
			}
		}
		
		public UnidadeGestora guardar(UnidadeGestora unidadeGestora) {
			return this.manager.merge(unidadeGestora);
		}
		
		public void remover(UnidadeGestora unidadeGestora){
			this.manager.remove(unidadeGestora);
		}
}