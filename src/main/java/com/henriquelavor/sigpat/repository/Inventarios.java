package com.henriquelavor.sigpat.repository;

import java.io.Serializable;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.henriquelavor.sigpat.model.Inventario;
import com.henriquelavor.sigpat.model.UnidadeAdministrativa;

/**
 * @author Henrique Lavor
 * @data 02.09.2016
 * @version 1.0
 */

public class Inventarios implements Serializable {

	private static final long serialVersionUID = 1L;

	private EntityManager manager;

	@Inject
	public Inventarios(EntityManager manager) {
		this.manager = manager;
	}

	public Inventario porId(Long codigoInventario) {
		return manager.find(Inventario.class, codigoInventario);
	}

	// Inventario do package: model
	//Lista todos os Inventarios da UO logada
	public List<Inventario> todos() {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
		
		TypedQuery<Inventario> query = manager
				.createQuery("from Inventario WHERE unidadeAdministrativa.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession Order By situacaoInventario ASC, dataInicial ASC", Inventario.class);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
		return query.getResultList();
	}

	// Inventario do package: model
	//Lista de todos Inventarios Aberto da UO logada
	public List<Inventario> todosAberto() {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
				
		TypedQuery<Inventario> query = manager
				.createQuery("from Inventario where situacaoInventario='Aberto' AND unidadeAdministrativa.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession Order By dataInicial ASC", Inventario.class);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
		return query.getResultList();
	}
	
	//Lista de todos inventarios Encerrados da UO loagda
	public List<Inventario> todosEncerrados() {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
				
		TypedQuery<Inventario> query = manager
				.createQuery("from Inventario where situacaoInventario='Encerrado' AND unidadeAdministrativa.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession", Inventario.class);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
		return query.getResultList();
	}
	
	public void adicionar(Inventario inventario) {
		this.manager.persist(inventario);
	}

	// pesquisa por Unidade Administrativa com Inventario Aberto da UO logada
	public Inventario porUnidadeAdministrativaInventarioAberto(UnidadeAdministrativa unidadeAdministrativa) {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
						
		TypedQuery<Inventario> query = manager.createQuery(
				"from Inventario where unidadeAdministrativa=:unidadeAdministrativa AND situacaoInventario='Aberto' AND unidadeAdministrativa.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession",
				Inventario.class);
		// TypedQuery<Inventario> query = manager.createQuery("from Inventario
		// where unidadeAdministrativa=:unidadeAdministrativa "
		// + "and situacaoInventario=:situacaoInventario.", Inventario.class);
		query.setParameter("unidadeAdministrativa", unidadeAdministrativa);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public Inventario guardar(Inventario inventario) {
		return this.manager.merge(inventario);
	}

	public void remover(Inventario inventario) {
		this.manager.remove(inventario);
	}

}
