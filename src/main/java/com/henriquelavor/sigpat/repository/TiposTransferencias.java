package com.henriquelavor.sigpat.repository;

import java.io.Serializable;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.henriquelavor.sigpat.model.TipoTransferencia;
/**
 * @author Henrique Lavor
 * @data 09.08.2016
 * @version 1.0
 */

public class TiposTransferencias implements Serializable {

	private static final long serialVersionUID = 1L;

	private EntityManager manager;

	@Inject
	public TiposTransferencias(EntityManager manager) {
		this.manager = manager;
	}

	public TipoTransferencia porId(Long codigoTipoTransferencia) {
		return manager.find(TipoTransferencia.class, codigoTipoTransferencia);
	}

	//TipoTransferencia package: model
	public List<TipoTransferencia> todos() {
		TypedQuery<TipoTransferencia> query = manager.createQuery("from TipoTransferencia", TipoTransferencia.class);
		return query.getResultList();
	}
	
	//TipoTransferencia Lista apenas o tipo de Transferencia Interna(TIB)
	public List<TipoTransferencia> todosTIB() {
		TypedQuery<TipoTransferencia> query = manager.createQuery("from TipoTransferencia WHERE codigoTipoTransferencia=95", TipoTransferencia.class);
		return query.getResultList();
	}
	
	//TipoTransferencia Lista apenas o tipo de Transferencia Externa (TEB) 
	public List<TipoTransferencia> todosTEB() {
		TypedQuery<TipoTransferencia> query = manager.createQuery("from TipoTransferencia WHERE codigoTipoTransferencia=96", TipoTransferencia.class);
		return query.getResultList();
	}
				

	public void adicionar(TipoTransferencia tipoTransferencia) {
		this.manager.persist(tipoTransferencia);
	}
	
	// pesquisa por nome
		public TipoTransferencia porNome(String nome) {
			TypedQuery<TipoTransferencia> query = manager.createQuery(
					"from TipoTransferencia where descricaoTipoTransferencia=:nome", TipoTransferencia.class);
			query.setParameter("nome", nome);
			try {
				return query.getSingleResult();
			} catch (NoResultException e) {
				return null;
			}
		}
		
		public TipoTransferencia guardar(TipoTransferencia tipoTransferencia) {
			return this.manager.merge(tipoTransferencia);
		}
		
		public void remover(TipoTransferencia tipoTransferencia){
			this.manager.remove(tipoTransferencia);
		}
}