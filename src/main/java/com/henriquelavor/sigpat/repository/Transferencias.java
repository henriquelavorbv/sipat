package com.henriquelavor.sigpat.repository;

import java.io.Serializable;

import java.util.List;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.henriquelavor.sigpat.model.Transferencia;

/**
 * @author Henrique Lavor
 * @data 14.08.2017
 * @version 1.0
 */

public class Transferencias implements Serializable {

	private static final long serialVersionUID = 1L;

	private EntityManager manager;

	@Inject
	public Transferencias(EntityManager manager) {
		this.manager = manager;
	}

	public Transferencia porId(Long codigoTransferencia) {
		return manager.find(Transferencia.class, codigoTransferencia);
	}

	// package: model
	public List<Transferencia> todas() {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
				
		TypedQuery<Transferencia> query = manager.createQuery("from Transferencia "
				+ "WHERE localOrigem.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession or localDestino.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession "
				+ "ORDER BY dataCadastro DESC", Transferencia.class);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
		return query.getResultList();
	}
	
	
	// package: model
		public List<Transferencia> todasPendentes() {
			//Pega a sessao do Codigo UO
			HttpServletRequest req = (HttpServletRequest) 
			FacesContext.getCurrentInstance().getExternalContext().getRequest();
			
			HttpSession session = (HttpSession) req.getSession();
			String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
					
			TypedQuery<Transferencia> query = manager.createQuery("from Transferencia "
					+ "WHERE statusRecebimento='Pendente' AND (localOrigem.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession or localDestino.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession)"
					+ "ORDER BY dataCadastro DESC", Transferencia.class);
			query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
			return query.getResultList();
		}

	// pesquisa por numero patrimonial
	public List<Transferencia> todasTrasferenciasPorNumeroPatrimonial(String numero) {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
		
		TypedQuery<Transferencia> query = manager.createQuery(
				"from Transferencia where bemMovel.numeroPatrimonio=:numero "
				+ "and localDestino.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession "
				+ "ORDER BY dataCadastro DESC",
				Transferencia.class);
		query.setParameter("numero", numero);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
		try {
			return query.getResultList();
			//System.out.println("transferencias: "+ query.getResultList().size());

		} catch (NoResultException e) {
			return null;
		}
		// return null;
	}

	public void adicionar(Transferencia transferencia) {
		this.manager.persist(transferencia);
	}

	// Atualizar Termos em Aberto ao solicitar para Gerar Termo de Transferencia
	public int atualizarTermosGerados() {
		//String sql = "UPDATE tb_transferencia SET situacao='Gerado' WHERE situacao='Aberto'";
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
				
		String sql = "UPDATE tb_transferencia SET situacao='Gerado' FROM (SELECT * FROM tb_transferencia " + 
				"INNER JOIN tb_unidade_administrativa ON " + 
				"tb_transferencia.cod_local_destino = tb_unidade_administrativa.codigounidadeadministrativa " + 
				"INNER JOIN tb_unidade_gestora ON " + 
				"tb_unidade_administrativa.cod_unidade_gestora = tb_unidade_gestora.codigounidadegestora) subquery " + 
				"WHERE tb_transferencia.situacao='Aberto' AND subquery.codigo_orcamentario='"+ codOrcamentarioSession +"'";
		
		Query query = manager.createNativeQuery(sql);
		int updateCount1 = query.executeUpdate();
		return updateCount1;
	}
	
		// FASE 1 do Recebimento de Bens
		// Atualizar Termos de transferencia gerado e bens Pendentes de recebimento
		public int atualizarTermoRecebido(String numeroTermo) {
				
				String sqlTransferencia = "UPDATE tb_transferencia SET status_recebimento='Recebido' WHERE situacao='Gerado' AND status_recebimento='Pendente' "
						+ "AND numero_termo_transferencia ='"+ numeroTermo +"'";
			
				Query query = manager.createNativeQuery(sqlTransferencia);
				int updateCount1 = query.executeUpdate();
				return updateCount1;
				
			}
		// FASE 2 do Recebimento de Bens
		// Atualizar Bens Pendentes para Ativo=Sim 
		public int atualizarBemRecebido(String numeroTermo) {
				String sqlBem = "UPDATE tb_bem_movel SET ativo='Sim', numero_termo_responsabilidade='NAO INCORPORADO' FROM tb_transferencia "
						+ "WHERE situacao='Gerado' AND status_recebimento='Recebido' "
						+ "AND tb_transferencia.cod_bem_movel = tb_bem_movel.codigobemmovel "
						+ "AND tb_transferencia.numero_termo_transferencia='"+ numeroTermo +"'";
			
				Query query2 = manager.createNativeQuery(sqlBem);
				int updateCount2 = query2.executeUpdate();
				return updateCount2;
				
			}
		
		
		// ****** ESTORNO ********
		// FASE 1 do Estorno de Transferencia de bens Externo TEB
		// Estorna os Termos de transferencia gerado e bens Pendentes de recebimento
		public int atualizarTermEstonar(String numeroTermo) {
				
				String sqlTransferenciaEstorno = "UPDATE tb_transferencia SET status_recebimento='Estornado' "
						+ "WHERE status_recebimento='Pendente' "
						+ "AND numero_termo_transferencia ='"+ numeroTermo +"'";
			
				Query query3 = manager.createNativeQuery(sqlTransferenciaEstorno);
				int updateCount3 = query3.executeUpdate();
				return updateCount3;
				
			}
		// FASE 2 do Estono de Bens
		// Atualizar Bens Pendentes para Ativo=Sim 
		public int atualizarBemEstornar(String numeroTermo) {
				
				String sqlBemEstorno =  "UPDATE tb_bem_movel "
						+ "SET cod_local_origem=tb_bem_movel.cod_local_destino, "
						+ "cod_local_destino=tb_bem_movel.cod_local_origem, "
						+ "numero_termo_responsabilidade='NAO INCORPORADO', "
						+ "ativo = 'Sim' "
						+ "FROM tb_transferencia "
						+ "WHERE tb_transferencia.cod_bem_movel = tb_bem_movel.codigobemmovel "
						+ "AND tb_transferencia.numero_termo_transferencia ='"+ numeroTermo +"'";
			
				System.out.println("sqlBemEstorno => "+ sqlBemEstorno);
				
				Query query4 = manager.createNativeQuery(sqlBemEstorno);
				int updateCount4 = query4.executeUpdate();
				return updateCount4;
				
			}
		
		//FIM ESTORNO
		
		
		
		
			
	// pesquisa por numero do Termo de Transferencia
	public List<Transferencia> todosTermosPorNumeroTranferencia(String numero) {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
				
		TypedQuery<Transferencia> query = manager
				.createQuery("from Transferencia where numeroTermoTransferencia=:numero and (localOrigem.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession or localDestino.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession)", Transferencia.class);
		query.setParameter("numero", numero);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
		try {
			return query.getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}

	// TEB - metodo usado para autocomplete para sugestao ao preencher o numero de termo de transferencia
	public List<String> numeroTermoTransferenciaTEBQueContem(String descricao) {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
		
		if (descricao.contains("TEB")) {
		TypedQuery<String> query = manager.createQuery("select distinct numeroTermoTransferencia from Transferencia "
				+ " where upper(numeroTermoTransferencia) like upper(:descricao) "
				+ "and (localOrigem.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession or localDestino.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession)", String.class);
		query.setParameter("descricao", "%" + descricao + "%");
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
		
		return query.getResultList();
		
		}else {
			return null;
		}
		
	}
	
	
	// TIB - metodo usado para autocomplete para sugestao ao preencher o numero de termo de transferencia
		public List<String> numeroTermoTransferenciaTIBQueContem(String descricao) {
			//Pega a sessao do Codigo UO
			HttpServletRequest req = (HttpServletRequest) 
			FacesContext.getCurrentInstance().getExternalContext().getRequest();
			
			HttpSession session = (HttpSession) req.getSession();
			String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
		if (descricao.contains("TIB")) {
			TypedQuery<String> query = manager.createQuery("select distinct numeroTermoTransferencia from Transferencia "
					+ " where upper(numeroTermoTransferencia) like upper(:descricao) "
					+ "and (localOrigem.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession or localDestino.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession)", String.class);
			query.setParameter("descricao", "%" + descricao + "%");
			query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
		
			return query.getResultList();
		}else {
			return null;
		  }
		}

	// pesquisa por situacao Aberto informando o numero do termo de
	// transferencia
	public List<Transferencia> todosTermosAbertoPorNumeroTransferencia(String numeroTermoTransferencia) {
		
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
		
		TypedQuery<Transferencia> query = manager.createQuery(
				"from Transferencia WHERE situacao='Aberto' AND numeroTermoTransferencia=:numeroTermoTransferencia AND localOrigem.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession",
				Transferencia.class);
		query.setParameter("numeroTermoTransferencia", numeroTermoTransferencia);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
		return query.getResultList();
	}

	// pesquisa por situacao GERADO informando o numero do termo de
	// transferencia e caso
	// encontre, NAO PERMITIR USAR ESTE TERMO DE TRANSFERENCIA
	public List<Transferencia> todosTermosGeradoPorNumeroTransferencia(String numeroTermoTransferencia) {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
				
		TypedQuery<Transferencia> query = manager.createQuery(
				"from Transferencia WHERE situacao='Gerado' AND numeroTermoTransferencia=:numeroTermoTransferencia AND (localOrigem.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession or localDestino.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession)",
				Transferencia.class);
		query.setParameter("numeroTermoTransferencia", numeroTermoTransferencia);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
		return query.getResultList();
	}

	public Transferencia guardar(Transferencia transferencia) {
		return this.manager.merge(transferencia);
	}

	public void remover(Transferencia transferencia) {
		this.manager.remove(transferencia);
	}
}