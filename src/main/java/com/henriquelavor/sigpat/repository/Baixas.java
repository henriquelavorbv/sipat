package com.henriquelavor.sigpat.repository;

import java.io.Serializable;

import java.util.List;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.henriquelavor.sigpat.model.Baixa;

/**
 * @author Henrique Lavor
 * @data 26.08.2017
 * @version 1.0
 */

public class Baixas implements Serializable {

	private static final long serialVersionUID = 1L;

	private EntityManager manager;

	@Inject
	public Baixas(EntityManager manager) {
		this.manager = manager;
	}

	public Baixa porId(Long codigoBaixa) {
		return manager.find(Baixa.class, codigoBaixa);
	}

	public List<Baixa> todas() {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
				
		TypedQuery<Baixa> query = manager.createQuery("from Baixa WHERE bemMovelBaixado.localDestino.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession ORDER BY dataCadastro DESC", Baixa.class);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession);
		return query.getResultList();
	}
	
	public List<Baixa> todasGerados() {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
				
		TypedQuery<Baixa> query = manager.createQuery("from Baixa WHERE situacao='Gerado' AND bemMovelBaixado.localDestino.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession ORDER BY dataCadastro DESC", Baixa.class);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession);
		return query.getResultList();
	}

	public void adicionar(Baixa baixa) {
		this.manager.persist(baixa);
	}

	// Atualizar Termos Aberto ao solicitar para Gerar Termo
	public int atualizarBaixasGeradas() {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
		
		String sql = "UPDATE tb_baixa SET situacao='Gerado' FROM tb_bem_movel "
				+ "INNER JOIN tb_unidade_administrativa ON "
				+ "tb_bem_movel.cod_local_destino = tb_unidade_administrativa.codigounidadeadministrativa "
				+ "INNER JOIN tb_unidade_gestora ON "
				+ "tb_unidade_administrativa.cod_unidade_gestora = tb_unidade_gestora.codigounidadegestora "
				+ "WHERE situacao='Aberto' AND cod_bem_movel_baixado = tb_bem_movel.codigobemmovel AND tb_unidade_gestora.codigo_orcamentario ='"+ codOrcamentarioSession +"'";
		
		Query query = manager.createNativeQuery(sql);
		int updateCount1 = query.executeUpdate();
		return updateCount1;
	}
	
	
	
	public int atualizarBaixasGeradasManualmente(String numeroTermo) {
		
		String sql = "UPDATE tb_baixa SET situacao='Gerado' FROM tb_bem_movel "
				+ "INNER JOIN tb_unidade_administrativa ON "
				+ "tb_bem_movel.cod_local_destino = tb_unidade_administrativa.codigounidadeadministrativa "
				+ "INNER JOIN tb_unidade_gestora ON "
				+ "tb_unidade_administrativa.cod_unidade_gestora = tb_unidade_gestora.codigounidadegestora "
				+ "WHERE situacao='Aberto' AND cod_bem_movel_baixado = tb_bem_movel.codigobemmovel AND tb_baixa.numero_termo_baixa ='"+ numeroTermo +"'";
		
		Query query = manager.createNativeQuery(sql);
		int updateCount1 = query.executeUpdate();
		return updateCount1;
	}
	
	
	

	// pesquisa por numero do termo de baixa da UO logada
	public List<Baixa> todasBaixasPorNumero(String numero) {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
						
		TypedQuery<Baixa> query = manager
				.createQuery("from Baixa where numeroTermoBaixa=:numero and bemMovelBaixado.localDestino.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession", Baixa.class);
		query.setParameter("numero", numero);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession);
		try {
			return query.getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}

	// metodo usado para autocomplete para sugestao ao preencher o numero de
	// termo de baixa de bens da UO Logada
	public List<String> numeroTermoBaixaQueContem(String descricao) {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
		
		TypedQuery<String> query = manager
				.createQuery("select distinct numeroTermoBaixa from Baixa "
						+ "where upper(numeroTermoBaixa) like upper(:descricao) and bemMovelBaixado.localDestino.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession", String.class);
		query.setParameter("descricao", "%" + descricao + "%");
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession);
		return query.getResultList();
	}

	// pesquisa por situacao Aberto informando o numero do termo de baixa da UO logada
	public List<Baixa> todasBaixasAbertaPorNumero(String numeroTermo) {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
										
				
		TypedQuery<Baixa> query = manager.createQuery(
				"from Baixa WHERE situacao='Aberto' AND numeroTermoBaixa=:numeroTermo AND bemMovelBaixado.localDestino.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession",
				Baixa.class);
		query.setParameter("numeroTermo", numeroTermo);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession);
		return query.getResultList();
	}

	// pesquisa por situacao GERADO informando o numero do termo e caso 
	// encontre, NAO PERMITIR USAR ESTE TERMO , NAO HOUVE FILTRO DEVIDO A FUNCAO SER DE USO GENERICO para UO Logada
	public List<Baixa> todasBaixasGeradasPorNumero(String numeroTermo) {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
				
		TypedQuery<Baixa> query = manager.createQuery(
				"from Baixa WHERE situacao='Gerado' AND numeroTermoBaixa=:numeroTermo AND bemMovelBaixado.localDestino.unidadeGestora.codigoOrcamentario=:codOrcamentarioSession",
				Baixa.class);
		query.setParameter("numeroTermo", numeroTermo);
		query.setParameter("codOrcamentarioSession", codOrcamentarioSession);
		return query.getResultList();
	}

	public Baixa guardar(Baixa baixa) {
		return this.manager.merge(baixa);
	}

	public void remover(Baixa baixa) {
		this.manager.remove(baixa);
	}
}
