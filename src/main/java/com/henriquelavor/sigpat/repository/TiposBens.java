package com.henriquelavor.sigpat.repository;

import java.io.Serializable;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.henriquelavor.sigpat.model.TipoBens;
/**
 * @author Henrique Lavor
 * @data 13.05.2016
 * @version 1.0
 */

public class TiposBens implements Serializable {

	private static final long serialVersionUID = 1L;

	private EntityManager manager;

	@Inject
	public TiposBens(EntityManager manager) {
		this.manager = manager;
	}

	public TipoBens porId(Long codigoTipoBem) {
		return manager.find(TipoBens.class, codigoTipoBem);
	}

	// TipoBens do package: model
	public List<TipoBens> todos() {
		TypedQuery<TipoBens> query = manager.createQuery("from TipoBens", TipoBens.class);
		return query.getResultList();
	}

	public void adicionar(TipoBens tipoBens) {
		this.manager.persist(tipoBens);
	}
	
	// pesquisa por nome
		public TipoBens porNome(String nome) {
			TypedQuery<TipoBens> query = manager.createQuery(
					"from TipoBens where descricaoTipoBem=:nome", TipoBens.class);
			query.setParameter("nome", nome);
			try {
				return query.getSingleResult();
			} catch (NoResultException e) {
				return null;
			}
		}
		
		public TipoBens guardar(TipoBens tipoBens) {
			return this.manager.merge(tipoBens);
		}
		
		public void remover(TipoBens tipoBens){
			this.manager.remove(tipoBens);
		}
		
}
