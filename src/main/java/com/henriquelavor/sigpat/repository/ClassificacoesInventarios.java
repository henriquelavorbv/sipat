package com.henriquelavor.sigpat.repository;

import java.io.Serializable;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.henriquelavor.sigpat.model.ClassificacaoInventario;
/**
 * @author Henrique Lavor
 * @data 20.08.2016
 * @version 1.0
 */

public class ClassificacoesInventarios implements Serializable {

	private static final long serialVersionUID = 1L;

	private EntityManager manager;

	@Inject
	public ClassificacoesInventarios(EntityManager manager) {
		this.manager = manager;
	}

	public ClassificacaoInventario porId(Long codigoClassificacaoInventario) {
		return manager.find(ClassificacaoInventario.class, codigoClassificacaoInventario);
	}

	//ClassificacaoInventario package: model
	public List<ClassificacaoInventario> todas() {
		TypedQuery<ClassificacaoInventario> query = manager.createQuery("from ClassificacaoInventario", ClassificacaoInventario.class);
		return query.getResultList();
	}

	public void adicionar(ClassificacaoInventario classificacaoInventario) {
		this.manager.persist(classificacaoInventario);
	}
	
	// pesquisa por nome
		public ClassificacaoInventario porNome(String nome) {
			TypedQuery<ClassificacaoInventario> query = manager.createQuery(
					"from ClassificacaoInventario where descricaoClassificacaoInventario=:nome", ClassificacaoInventario.class);
			query.setParameter("nome", nome);
			try {
				return query.getSingleResult();
			} catch (NoResultException e) {
				return null;
			}
		}
		
		public ClassificacaoInventario guardar(ClassificacaoInventario classificacaoInventario) {
			return this.manager.merge(classificacaoInventario);
		}
		
		public void remover(ClassificacaoInventario classificacaoInventario){
			this.manager.remove(classificacaoInventario);
		}
}