package com.henriquelavor.sigpat.repository;

import java.io.Serializable;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.henriquelavor.sigpat.model.DepreciacaoView;

/**
 * @author Henrique Lavor
 * @data 17.07.2019
 * @version 1.0
 */

public class DepreciacoesView implements Serializable {

	private static final long serialVersionUID = 1L;

	private EntityManager manager;

	@Inject
	public DepreciacoesView(EntityManager manager) {
		this.manager = manager;
	}
	
	public DepreciacaoView porId(Long codigoBemMovel) {
		return manager.find(DepreciacaoView.class, codigoBemMovel);
	}


	// todas depreciacoes da UO logada
	public List<DepreciacaoView> todasDepreciacoes() {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
						
			
		TypedQuery<DepreciacaoView> query = manager.createQuery("from DepreciacaoView WHERE codigoOrcamentario=:codOrcamentarioSession", DepreciacaoView.class);
		try {
			query.setParameter("codOrcamentarioSession", codOrcamentarioSession); 
			return query.getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}

}
