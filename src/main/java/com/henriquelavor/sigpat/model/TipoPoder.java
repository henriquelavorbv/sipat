package com.henriquelavor.sigpat.model;

public enum TipoPoder {
	Legislativo, Executivo, Judiciário
}
