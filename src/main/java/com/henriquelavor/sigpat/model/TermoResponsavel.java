package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;

/**
 * @author Henrique Lavor
 * @data 18.03.2017
 * @version 1.5
 */
@Entity
@Table(name = "tb_termo_reponsavel")
public class TermoResponsavel implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long codigoTermoResponsavel;
	private String numeroTermoResponsabilidade;
	private Date dataTermoResponsabilidade;
	private Funcionario funcionarioResponsavelPatrimonial;
	private EstadoBens estadoBem;
	private UnidadeAdministrativa localOrigem; 
	private UnidadeAdministrativa localDestino;
	
	private BemMovel bemMovel;
	
	private String situacao="Aberto";
	
	private Boolean notificarEmail;
	
	
	@Id
	@GeneratedValue
	public Long getCodigoTermoResponsavel() {
		return codigoTermoResponsavel;
	}

	public void setCodigoTermoResponsavel(Long codigoTermoResponsavel) {
		this.codigoTermoResponsavel = codigoTermoResponsavel;
	}

	@NotNull
	@Size(max = 20)
	@Column(name = "numero_termo_responsabilidade", length = 20, nullable = true)
	public String getNumeroTermoResponsabilidade() {
		return numeroTermoResponsabilidade;
	}

	public void setNumeroTermoResponsabilidade(String numeroTermoResponsabilidade) {
		this.numeroTermoResponsabilidade = numeroTermoResponsabilidade;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "data_termo_responsabilidade", nullable = true)
	public Date getDataTermoResponsabilidade() {
		return dataTermoResponsabilidade;
	}

	public void setDataTermoResponsabilidade(Date dataTermoResponsabilidade) {
		this.dataTermoResponsabilidade = dataTermoResponsabilidade;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_local_origem")
	public UnidadeAdministrativa getLocalOrigem() {
		return localOrigem;
	}

	public void setLocalOrigem(UnidadeAdministrativa localOrigem) {
		this.localOrigem = localOrigem;
	}
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_local_destino")
	public UnidadeAdministrativa getLocalDestino() {
		return localDestino;
	}

	public void setLocalDestino(UnidadeAdministrativa localDestino) {
		this.localDestino = localDestino;
	}
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_funcionario_resp_patrimonial")
	public Funcionario getFuncionarioResponsavelPatrimonial() {
		return funcionarioResponsavelPatrimonial;
	}

	public void setFuncionarioResponsavelPatrimonial(Funcionario funcionarioResponsavelPatrimonial) {
		this.funcionarioResponsavelPatrimonial = funcionarioResponsavelPatrimonial;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_estado_bem")
	public EstadoBens getEstadoBem() {
		return estadoBem;
	}

	public void setEstadoBem(EstadoBens estadoBem) {
		this.estadoBem = estadoBem;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_bem_movel")
	public BemMovel getBemMovel() {
		return bemMovel;
	}

	public void setBemMovel(BemMovel bemMovel) {
		this.bemMovel = bemMovel;
	}
	
	@Size(max = 10)
	@Column(name = "situacao", length = 10, nullable = true)
	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	
	@Type(type = "true_false")
	@Column(name = "notificar_email")
	public Boolean getNotificarEmail() {
		return notificarEmail;
	}

	public void setNotificarEmail(Boolean notificarEmail) {
		this.notificarEmail = notificarEmail;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoTermoResponsavel == null) ? 0 : codigoTermoResponsavel.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TermoResponsavel other = (TermoResponsavel) obj;
		if (codigoTermoResponsavel == null) {
			if (other.codigoTermoResponsavel != null)
				return false;
		} else if (!codigoTermoResponsavel.equals(other.codigoTermoResponsavel))
			return false;
		return true;
	}
}
