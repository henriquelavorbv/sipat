package com.henriquelavor.sigpat.model;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import com.henriquelavor.sigpat.controller.UploadArquivo;
import com.henriquelavor.sigpat.repository.Baixas;
import com.henriquelavor.sigpat.service.CadastroBaixas;
import com.henriquelavor.sigpat.service.NegocioException;

/**
 * @author Henrique Lavor
 * @data 26.08.2017
 * @version 1.0
 */

@Named
@ViewScoped
public class ConsultaBaixasBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Baixas baixasRepository;

	@Inject
	private CadastroBaixas cadastro;

	private List<Baixa> baixas;

	private Baixa baixaSelecionada;

	
	public void excluir() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.excluir(this.baixaSelecionada);
			this.consultarTodos();

			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,"Item do Estornado com sucesso!",null));

		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}
	
	public void gerarBaixaManualmente() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.gerarBaixasManualmente(this.baixaSelecionada);
			this.consultarTodos();

			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,"Termo de Baixa Gerado com sucesso!",null));

		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}
	

	public void consultar() {
		String numeroTermo = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("numeroTermo");
		
		this.baixas = baixasRepository.todasBaixasAbertaPorNumero(numeroTermo);
	}
	
	public void consultarTodos() {
		this.baixas =  baixasRepository.todas();
	}
	
	public void consultarTodosGerados() {
		this.baixas =  baixasRepository.todasGerados();
	}
	
	

	public List<Baixa> getBaixas() {
		return baixas;
	}

	public List<Baixa> getConsultar() {
		return baixas;
	}
	
	public Baixa getBaixaSelecionada() {
		return baixaSelecionada;
	}
	
	public void setBaixaSelecionada(Baixa baixaSelecionada) {
		this.baixaSelecionada = baixaSelecionada;
	}
}
