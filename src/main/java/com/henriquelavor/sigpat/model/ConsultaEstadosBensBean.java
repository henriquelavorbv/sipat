
package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.repository.EstadosBens;
import com.henriquelavor.sigpat.service.CadastroEstadosBens;
import com.henriquelavor.sigpat.service.NegocioException;

/**
 * @author Henrique Lavor
 * @data 18.05.2016
 * @version 1.0
 */

@Named
@ViewScoped
public class ConsultaEstadosBensBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EstadosBens estadosBensRepository;
	
	@Inject
	private CadastroEstadosBens cadastro;
	
	private List<EstadoBens> estadosBens;
	
	private EstadoBens estadoBensSelecionado;
	
	public void excluir() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.cadastro.excluir(this.estadoBensSelecionado);
			this.consultar();

			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
					"Estado do Bem Patrimonial excluído com sucesso!",null));

		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	
	public void consultar() {
		this.estadosBens = estadosBensRepository.todos();
	}

	public List<EstadoBens> getEstadosBens() {
		return estadosBens;
	}
	
	public List<EstadoBens> getConsultar() {
		return estadosBens;
	}

	public EstadoBens getEstadoBensSelecionado() {
		return estadoBensSelecionado;
	}
	
	public void setEstadoBensSelecionado(EstadoBens estadoBensSelecionado) {
		this.estadoBensSelecionado = estadoBensSelecionado;
	}
}