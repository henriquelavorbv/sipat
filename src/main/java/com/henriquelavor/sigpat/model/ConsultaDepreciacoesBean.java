
package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.repository.Depreciacoes;

/**
 * @author Henrique Lavor
 * @data 21.05.2019
 * @version 1.0
 */

@Named
@ViewScoped
public class ConsultaDepreciacoesBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Depreciacoes depreciacoesRepository;
	
	
	private List<Depreciacao> depreciacoes;
	
	private Depreciacao depreciacaoSelecionada;
	
	public void consultar() {
		this.depreciacoes = depreciacoesRepository.todas();
	}
	
	
	public List<Depreciacao> getDepreciacoes() {
		return depreciacoes;
	}
	
	public List<Depreciacao> getConsultar() {
		return depreciacoes;
	}
	

	public Depreciacao getDepreciacaoSelecionada() {
		return depreciacaoSelecionada;
	}
	
	public void setDepreciacaoSelecionada(Depreciacao depreciacaoSelecionada) {
		this.depreciacaoSelecionada = depreciacaoSelecionada;
	}
}