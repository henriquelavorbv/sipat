
package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.repository.Funcionarios;
import com.henriquelavor.sigpat.service.CadastroFuncionarios;
import com.henriquelavor.sigpat.service.NegocioException;

/**
 * @author Henrique Lavor
 * @data 11.11.2018
 * @version 1.0
 */

@Named
@ViewScoped
public class ConsultaFuncionariosBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Funcionarios funcionariosRepository;

	@Inject
	private CadastroFuncionarios cadastro;

	private List<Funcionario> funcionarios;

	private Funcionario funcionarioSelecionado;

	public void excluir() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.cadastro.excluir(this.funcionarioSelecionado);
			this.consultar();
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,"Funcionário excluído com sucesso!",null));
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public void consultar() {
		this.funcionarios = funcionariosRepository.todos();
	}

	public List<Funcionario> getFuncionarios() {
		return funcionarios;
	}

	public List<Funcionario> getConsultar() {
		return funcionarios;
	}
	
	public Funcionario getFuncionarioSelecionado() {
		return funcionarioSelecionado;
	}
	
	public void setFuncionarioSelecionado(Funcionario funcionarioSelecionado) {
		this.funcionarioSelecionado = funcionarioSelecionado;
	}
}
