
package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.repository.ReferenciasFornecedores;
import com.henriquelavor.sigpat.service.CadastroReferenciasFornecedores;
import com.henriquelavor.sigpat.service.NegocioException;

/**
 * @author Henrique Lavor
 * @data 27.05.2016
 * @version 1.0
 */

@Named
@ViewScoped
public class ConsultaReferenciasFornecedoresBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private ReferenciasFornecedores referenciasFornecedoresRepository;
	
	@Inject
	private CadastroReferenciasFornecedores cadastro;
	
	private List<ReferenciaFornecedor> referenciasFornecedores;
	
	private ReferenciaFornecedor referenciaFornecedorSelecionada;
	
	public void excluir() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.cadastro.excluir(this.referenciaFornecedorSelecionada);
			this.consultar();

			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
					"Referência de Fornecedor excluída com sucesso!",null));
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	
	public void consultar() {
		this.referenciasFornecedores = referenciasFornecedoresRepository.todas();
	}

	public List<ReferenciaFornecedor> getReferenciasFornecedores() {
		return referenciasFornecedores;
	}
	
	public List<ReferenciaFornecedor> getConsultar() {
		return referenciasFornecedores;
	}

	public ReferenciaFornecedor getReferenciaFornecedorSelecionada() {
		return referenciaFornecedorSelecionada;
	}
	
	public void setReferenciaFornecedorSelecionada(ReferenciaFornecedor referenciaFornecedorSelecionada) {
		this.referenciaFornecedorSelecionada = referenciaFornecedorSelecionada;
	}
}