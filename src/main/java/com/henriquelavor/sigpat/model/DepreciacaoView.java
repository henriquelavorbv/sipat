package com.henriquelavor.sigpat.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

/**
 * @author Henrique Lavor
 * @data 16.07.2019
 * @version 1.0
 */

@Entity
@Immutable
@Table(name = "vw_bem_depreciado")
public class DepreciacaoView implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long codigoBemMovel;
	private String numeroPatrimonio;
	private String descricaoBem;
	private String codigoOrcamentario;
	

	@Id
	@Column(name = "cod_bens")	
    public Long getCodigoBemMovel() {
		return codigoBemMovel;
	}

	public void setCodigoBemMovel(Long codigoBemMovel) {
		this.codigoBemMovel = codigoBemMovel;
	}

	@Column(name = "numero_patrimonio", length = 20)
	public String getNumeroPatrimonio() {
		return numeroPatrimonio;
	}

	public void setNumeroPatrimonio(String numeroPatrimonio) {
		this.numeroPatrimonio = numeroPatrimonio;
	}

	@Column(name = "descricao_bem", length = 600)
	public String getDescricaoBem() {
		return descricaoBem;
	}

	public void setDescricaoBem(String descricaoBem) {
		this.descricaoBem = descricaoBem;
	}

	@Column(name = "codigo_orcamentario", length = 5)
	public String getCodigoOrcamentario() {
		return codigoOrcamentario;
	}

	public void setCodigoOrcamentario(String codigoOrcamentario) {
		this.codigoOrcamentario = codigoOrcamentario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoBemMovel == null) ? 0 : codigoBemMovel.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DepreciacaoView other = (DepreciacaoView) obj;
		if (codigoBemMovel == null) {
			if (other.codigoBemMovel != null)
				return false;
		} else if (!codigoBemMovel.equals(other.codigoBemMovel))
			return false;
		return true;
	}

	
}
