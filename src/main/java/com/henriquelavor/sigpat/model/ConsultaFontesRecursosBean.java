
package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.repository.FontesRecursos;
import com.henriquelavor.sigpat.service.CadastroFontesRecursos;
import com.henriquelavor.sigpat.service.NegocioException;

/**
 * @author Henrique Lavor
 * @data 16.01.2017
 * @version 1.0
 */

@Named
@ViewScoped
public class ConsultaFontesRecursosBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private FontesRecursos fontesRecursosRepository;
	
	@Inject
	private CadastroFontesRecursos cadastro;
	
	private List<FonteRecurso> fontesRecursos;
	
	private FonteRecurso fonteRecursoSelecionada;
	
	public void excluir() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.cadastro.excluir(this.fonteRecursoSelecionada);
			this.consultar();

			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
					"Fonte de Recurso excluída com sucesso!",null));

		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	
	public void consultar() {
		this.fontesRecursos = fontesRecursosRepository.todas();
	}

	public List<FonteRecurso> getFontesRecursos() {
		return fontesRecursos;
	}
	
	public List<FonteRecurso> getConsultar() {
		return fontesRecursos;
	}

	public FonteRecurso getFonteRecursoSelecionada() {
		return fonteRecursoSelecionada;
	}
	
	public void setFonteRecursoSelecionada(FonteRecurso fonteRecursoSelecionada) {
		this.fonteRecursoSelecionada = fonteRecursoSelecionada;
	}
}