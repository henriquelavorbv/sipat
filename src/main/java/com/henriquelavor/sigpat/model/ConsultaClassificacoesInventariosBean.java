
package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.repository.ClassificacoesInventarios;
import com.henriquelavor.sigpat.service.CadastroClassificacoesInventarios;
import com.henriquelavor.sigpat.service.NegocioException;

/**
 * @author Henrique Lavor
 * @data 20.08.2016
 * @version 1.0
 */

@Named
@ViewScoped
public class ConsultaClassificacoesInventariosBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private ClassificacoesInventarios classificacoesInvenatariosRepository;
	
	@Inject
	private CadastroClassificacoesInventarios cadastro;
	
	private List<ClassificacaoInventario> classificacoesInventarios;
	
	private ClassificacaoInventario classificacaoInventarioSelecionado;
	
	public void excluir() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.cadastro.excluir(this.classificacaoInventarioSelecionado);
			this.consultar();

			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
					"Classificação de Inventário excluído com sucesso!",null));

		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	
	public void consultar() {
		this.classificacoesInventarios = classificacoesInvenatariosRepository.todas();
	}

	public List<ClassificacaoInventario> getClassificacoesInventarios() {
		return classificacoesInventarios;
	}
	
	public List<ClassificacaoInventario> getConsultar() {
		return classificacoesInventarios;
	}

	public ClassificacaoInventario getClassificacaoInventarioSelecionado() {
		return classificacaoInventarioSelecionado;
	}
	
	public void setClassificacaoInventarioSelecionado(ClassificacaoInventario classificacaoInventarioSelecionado) {
		this.classificacaoInventarioSelecionado = classificacaoInventarioSelecionado;
	}
}