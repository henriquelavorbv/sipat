
package com.henriquelavor.sigpat.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Henrique Lavor
 * @data 20.08.2016
 * @version 1.0
 */
@Entity
@Table(name = "tb_classificacao_inventario")
public class ClassificacaoInventario implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long codigoClassificacaoInventario;
	private String descricaoClassificacaoInventario;

	@Id
	@GeneratedValue
	public Long getCodigoClassificacaoInventario() {
		return codigoClassificacaoInventario;
	}

	public void setCodigoClassificacaoInventario(Long codigoClassificacaoInventario) {
		this.codigoClassificacaoInventario = codigoClassificacaoInventario;
	}

	@NotNull
	@Size(max = 100)
	@Column(name = "descricao", length = 100, nullable = false)
	public String getDescricaoClassificacaoInventario() {
		return descricaoClassificacaoInventario;
	}

	public void setDescricaoClassificacaoInventario(String descricaoClassificacaoInventario) {
		this.descricaoClassificacaoInventario = descricaoClassificacaoInventario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((codigoClassificacaoInventario == null) ? 0 : codigoClassificacaoInventario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClassificacaoInventario other = (ClassificacaoInventario) obj;
		if (codigoClassificacaoInventario == null) {
			if (other.codigoClassificacaoInventario != null)
				return false;
		} else if (!codigoClassificacaoInventario.equals(other.codigoClassificacaoInventario))
			return false;
		return true;
	}
}
