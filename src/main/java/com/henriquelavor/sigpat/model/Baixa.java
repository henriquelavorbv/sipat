package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.faces.context.FacesContext;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Henrique Lavor
 * @data 26.08.2017
 * @version 1.0
 */
@Entity
@Table(name = "tb_baixa")
public class Baixa implements Serializable {

	private static final long serialVersionUID = 1L;
	
	//informacaoes para baixa do bem
	private Long codigoBaixa;
	private String numeroTermoBaixa; //ok
	private Date dataBaixa; 
	private Date dataCadastro = new Date();
	private MotivoBaixa motivoBaixa;
	private BemMovel bemMovelBaixado;
	
			
	//private UnidadeGestora unidadeGestora; // = idUnidadeOrcamentarioSession;
	
	private BigDecimal valorBaixa;
	private String numeroProcesso;
	
	private String processoPdf;
	private String foto1 = "semfotobem.gif"; 
	private String foto2 = "semfotobem.gif"; 
	private String foto3 = "semfotobem.gif"; 
	private String observacao;
	private String estornado="Não";     //por padrao ao dar baixa deve-se deixar estornado=Não     
	private Date dataEstorno; //informacoes para estorno de baixa do bem
	private String situacao="Aberto";
	
	@Id
	@GeneratedValue
	public Long getCodigoBaixa() {
		return codigoBaixa;
	}

	public void setCodigoBaixa(Long codigoBaixa) {
		this.codigoBaixa = codigoBaixa;
	}

	@NotNull
	@Size(max = 20)
	@Column(name = "numero_termo_baixa", length = 20, nullable = false)
	public String getNumeroTermoBaixa() {
		return numeroTermoBaixa;
	}

	public void setNumeroTermoBaixa(String numeroTermoBaixa) {
		this.numeroTermoBaixa = numeroTermoBaixa;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "data_baixa", nullable = false)
	public Date getDataBaixa() {
		return dataBaixa;
	}

	public void setDataBaixa(Date dataBaixa) {
		this.dataBaixa = dataBaixa;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name= "data_cadastro", nullable=true)
	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	
	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_motivo_baixa")
	public MotivoBaixa getMotivoBaixa() {
		return motivoBaixa;
	}

	public void setMotivoBaixa(MotivoBaixa motivoBaixa) {
		this.motivoBaixa = motivoBaixa;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_bem_movel_baixado")
	public BemMovel getBemMovelBaixado() {
		return bemMovelBaixado;
	}

	public void setBemMovelBaixado(BemMovel bemMovelBaixado) {
		this.bemMovelBaixado = bemMovelBaixado;
	}
	
	/*
	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_unidade_gestora")
	public UnidadeGestora getUnidadeGestora() {
		return unidadeGestora;
	}
	*/

	
	//public void setUnidadeGestora(UnidadeGestora unidadeGestora) {
		/*
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		UnidadeGestora idUnidadeOrcamentarioSession = (UnidadeGestora) session.getAttribute("ID_GESTORA");
		*/
	//	this.unidadeGestora = unidadeGestora;
		//this.unidadeGestora = idUnidadeOrcamentarioSession;
	//}


	@Column(name= "valor_baixa", precision = 12, scale = 2, nullable = false)
	public BigDecimal getValorBaixa() {
		return valorBaixa;
	}

	public void setValorBaixa(BigDecimal valorBaixa) {
		this.valorBaixa = valorBaixa;
	}

	@NotNull
	@Size(max = 30)
	@Column(name = "numero_processo", length = 30, nullable = false)
	public String getNumeroProcesso() {
		return numeroProcesso;
	}

	public void setNumeroProcesso(String numeroProcesso) {
		this.numeroProcesso = numeroProcesso;
	}


	@Size(max = 200)
	@Column(name = "processo_pdf", length = 200, nullable = true)
	public String getProcessoPdf() {
		return processoPdf;
	}

	public void setProcessoPdf(String processoPdf) {
		this.processoPdf = processoPdf;
	}

	@Size(max = 200)
	@Column(name = "foto1_bem_movel", length = 200, nullable = true)
	public String getFoto1() {
		return foto1;
	}

	public void setFoto1(String foto1) {
		this.foto1 = foto1;
	}

	@Size(max = 200)
	@Column(name = "foto2_bem_movel", length = 200, nullable = true)
	public String getFoto2() {
		return foto2;
	}

	public void setFoto2(String foto2) {
		this.foto2 = foto2;
	}

	@Size(max = 200)
	@Column(name = "foto3_bem_movel", length = 200, nullable = true)
	public String getFoto3() {
		return foto3;
	}

	public void setFoto3(String foto3) {
		this.foto3 = foto3;
	}

	@Size(max = 700)
	@Column(name = "observacao", length = 700, nullable = true)
	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	@NotNull
	@Size(max = 5)
	@Column(name = "estornado" , length = 5, nullable = false)
	public String getEstornado() {
		return estornado;
	}

	public void setEstornado(String estornado) {
		this.estornado = estornado;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "data_estorno", nullable = true)
	public Date getDataEstorno() {
		return dataEstorno;
	}

	public void setDataEstorno(Date dataEstorno) {
		this.dataEstorno = dataEstorno;
	}
	
	@Size(max = 10)
	@Column(name = "situacao", length = 10, nullable = true)
	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoBaixa == null) ? 0 : codigoBaixa.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Baixa other = (Baixa) obj;
		if (codigoBaixa == null) {
			if (other.codigoBaixa != null)
				return false;
		} else if (!codigoBaixa.equals(other.codigoBaixa))
			return false;
		return true;
	}
}
