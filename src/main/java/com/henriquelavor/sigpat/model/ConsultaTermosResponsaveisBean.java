package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.repository.TermosResponsaveis;
import com.henriquelavor.sigpat.service.CadastroTermosResponsaveis;
import com.henriquelavor.sigpat.service.NegocioException;
import com.henriquelavor.sigpat.util.jsf.FacesUtil;
import com.henriquelavor.sigpat.util.mail.Mailer;
import com.outjected.email.api.MailMessage;

/**
 * @author Henrique Lavor
 * @data 02.12.2016
 * @version 1.0
 */

@Named
@ViewScoped
public class ConsultaTermosResponsaveisBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private TermosResponsaveis termosResponsaveisRepository;

	@Inject
	private CadastroTermosResponsaveis cadastro;

	private List<TermoResponsavel> termosResponsaveis;

	private TermoResponsavel termoResponsavelSelecionado;

	
	@Inject
	private Mailer mailer;
	
	public void enviarNotificacao(){
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String dataTermo = df.format(this.termoResponsavelSelecionado.getDataTermoResponsabilidade());
		
		MailMessage message = mailer.prepararNovaMensagem();
		message.to(this.termoResponsavelSelecionado.getFuncionarioResponsavelPatrimonial().getEmail())
		.subject("SIGPAT: Notificação de TERMO DE RESPONSABILIDADE  Nº : " + this.termoResponsavelSelecionado.getNumeroTermoResponsabilidade())
		.bodyHtml("<strong>Data do Termo: </strong>" + dataTermo +"<br />" + 
		"<strong>Unidade Gestora de Destino: </strong>" + this.termoResponsavelSelecionado.getLocalDestino().getUnidadeGestora().getNomeUnidadeGestora() +"("+ this.termoResponsavelSelecionado.getLocalDestino().getUnidadeGestora().getNomeResumido()+")"+"<br/>" +
		"<strong>Unidade Administrativa de Destino: </strong>" + this.termoResponsavelSelecionado.getLocalDestino().getNomeUnidadeAdministrativa() +"("+ this.termoResponsavelSelecionado.getLocalDestino().getNomeResumido() +")"+"<br/>" +
		"<strong>Situação: </strong>" + this.termoResponsavelSelecionado.getSituacao() +"<br />" +
		"<strong>Funcionário Receptor/Responsável: </strong>" + this.termoResponsavelSelecionado.getFuncionarioResponsavelPatrimonial().getNomeFuncionario() +"<br />" +
		"<strong>Matrícula do Funcionário Receptor/Responsável: </strong>" + this.termoResponsavelSelecionado.getFuncionarioResponsavelPatrimonial().getMatricula() +"<br />" +"<br />" +
		
		"<strong> Caro Responsável, este email, tem como finalidade, informá-lo(a) que, você tem de posse um documento oficial impresso e "
		+ "assinado do Termo de Responsabilidade No. " + this.termoResponsavelSelecionado.getNumeroTermoResponsabilidade() 
		+ ", guarde-o em um local seguro, para futuros levantamentos/inventários patrimoniais de sua entidade."
		+ "   </strong>" +"<br />")
		.send();
		
		FacesUtil.addInfoMessage("Notificação de Termo de Responsabilidade enviado por e-mail com sucesso!");
	}
	
	public void excluir(TermoResponsavel termoResponsavelSelecionado) {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.excluir(this.termoResponsavelSelecionado);
			
			this.consultar();

			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,"Item do Termo Responsabilidade de Bem Patrimonial excluído com sucesso!",null));

		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public void consultar() {
		String numeroTermo = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("numeroTermo");
		
		//String numero = this.termoResponsavelSelecionado.getNumeroTermoResponsabilidade();
		this.termosResponsaveis = termosResponsaveisRepository.todosTermosAbertoPorNumero(numeroTermo);
	}
	
	public void consultarTodos() {
		this.termosResponsaveis =  termosResponsaveisRepository.todos();
	}

	public List<TermoResponsavel> getTermosResponsaveis() {
		return termosResponsaveis;
	}

	public List<TermoResponsavel> getConsultar() {
		return termosResponsaveis;
	}
	
	public TermoResponsavel getTermoResponsavelSelecionado() {
		return termoResponsavelSelecionado;
	}
	
	public void setTermoResponsavelSelecionado(TermoResponsavel termoResponsavelSelecionado) {
		this.termoResponsavelSelecionado = termoResponsavelSelecionado;
	}
}
