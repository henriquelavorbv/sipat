
package com.henriquelavor.sigpat.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Henrique Lavor
 * @data 16.01.2017
 * @version 1.0
 */
@Entity
@Table(name = "tb_fonte_recurso")
public class FonteRecurso implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long codigoFonteRecurso;
	private Long fonte;
	private String nomeFonteRecurso;

	@Id
	@GeneratedValue
	public Long getCodigoFonteRecurso() {
		return codigoFonteRecurso;
	}

	
	public void setCodigoFonteRecurso(Long codigoFonteRecurso) {
		this.codigoFonteRecurso = codigoFonteRecurso;
	}

	@Column(name = "fonte")
	public Long getFonte() {
		return fonte;
	}

	public void setFonte(Long fonte) {
		this.fonte = fonte;
	}

	@NotNull 
	@Size(max=300)
	@Column(name="nome_fonte_recurso",length=300,nullable=false)
	public String getNomeFonteRecurso() {
		return nomeFonteRecurso;
	}


	public void setNomeFonteRecurso(String nomeFonteRecurso) {
		this.nomeFonteRecurso = nomeFonteRecurso;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoFonteRecurso == null) ? 0 : codigoFonteRecurso.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FonteRecurso other = (FonteRecurso) obj;
		if (codigoFonteRecurso == null) {
			if (other.codigoFonteRecurso != null)
				return false;
		} else if (!codigoFonteRecurso.equals(other.codigoFonteRecurso))
			return false;
		return true;
	}
}