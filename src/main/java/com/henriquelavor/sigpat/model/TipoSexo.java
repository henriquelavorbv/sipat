package com.henriquelavor.sigpat.model;

public enum TipoSexo {
	Masculino, Feminino, Outros
}
