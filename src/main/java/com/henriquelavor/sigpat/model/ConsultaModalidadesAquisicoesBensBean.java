
package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.repository.ModalidadesAquisicoesBens;
import com.henriquelavor.sigpat.service.CadastroModalidadesAquisicoesBens;
import com.henriquelavor.sigpat.service.NegocioException;

/**
 * @author Henrique Lavor
 * @data 18.05.2016
 * @version 1.0
 */

@Named
@ViewScoped
public class ConsultaModalidadesAquisicoesBensBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private ModalidadesAquisicoesBens modalidadesAquisicoesBensRepository;
	
	@Inject
	private CadastroModalidadesAquisicoesBens cadastro;
	
	private List<ModalidadeAquisicaoBens> modalidadesAquisicoesBens;
	
	private ModalidadeAquisicaoBens modalidadeAquisicaoBensSelecionado;
	
	public void excluir() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.cadastro.excluir(this.modalidadeAquisicaoBensSelecionado);
			this.consultar();

			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
					"Modalidade de Aquisição do Bem Patrimonial excluído com sucesso!",null));

		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	
	public void consultar() {
		this.modalidadesAquisicoesBens = modalidadesAquisicoesBensRepository.todas();
	}

	public List<ModalidadeAquisicaoBens> getModalidadesAquisicoesBens() {
		return modalidadesAquisicoesBens;
	}
	
	public List<ModalidadeAquisicaoBens> getConsultar() {
		return modalidadesAquisicoesBens;
	}

	public ModalidadeAquisicaoBens getModalidadeAquisicaoBensSelecionado() {
		return modalidadeAquisicaoBensSelecionado;
	}
	
	public void setModalidadeAquisicaoBensSelecionado(ModalidadeAquisicaoBens modalidadeAquisicaoBensSelecionado) {
		this.modalidadeAquisicaoBensSelecionado = modalidadeAquisicaoBensSelecionado;
	}
}