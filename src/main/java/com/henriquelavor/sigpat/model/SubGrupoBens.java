package com.henriquelavor.sigpat.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
/**
 * @author Henrique Lavor
 * @data 06.04.2016
 * @version 1.0
 */
@Entity
@Table (name="tb_sub_grupo_bem")
public class SubGrupoBens implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Long codigoSubGrupoBem;
	private GrupoBens grupoBens;
	private String descricaoSubGrupoBem;
	
	@Id
	@GeneratedValue
	public Long getCodigoSubGrupoBem() {
		return codigoSubGrupoBem;
	}
	
	public void setCodigoSubGrupoBem(Long codigoSubGrupoBem) {
		this.codigoSubGrupoBem = codigoSubGrupoBem;
	}
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_grupo_bem")
	public GrupoBens getGrupoBens() {
		return grupoBens;
	}
	
	public void setGrupoBens(GrupoBens grupoBens) {
		this.grupoBens = grupoBens;
	}
	
	@NotEmpty
	@Size(max = 100)
	@Column(name = "descricao", length = 100, nullable = false)
	public String getDescricaoSubGrupoBem() {
		return descricaoSubGrupoBem;
	}
	
	public void setDescricaoSubGrupoBem(String descricaoSubGrupoBem) {
		this.descricaoSubGrupoBem = descricaoSubGrupoBem;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoSubGrupoBem == null) ? 0 : codigoSubGrupoBem.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubGrupoBens other = (SubGrupoBens) obj;
		if (codigoSubGrupoBem == null) {
			if (other.codigoSubGrupoBem != null)
				return false;
		} else if (!codigoSubGrupoBem.equals(other.codigoSubGrupoBem))
			return false;
		return true;
	}
}
