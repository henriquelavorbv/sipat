
package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.repository.MotivosBaixas;
import com.henriquelavor.sigpat.service.CadastroMotivosBaixas;
import com.henriquelavor.sigpat.service.NegocioException;

/**
 * @author Henrique Lavor
 * @data 08.08.2016
 * @version 1.0
 */

@Named
@ViewScoped
public class ConsultaMotivosBaixasBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private MotivosBaixas motivosBaixasRepository;
	
	@Inject
	private CadastroMotivosBaixas cadastro;
	
	private List<MotivoBaixa> motivosBaixas;
	
	private MotivoBaixa motivoBaixaSelecionado;
	
	public void excluir() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.cadastro.excluir(this.motivoBaixaSelecionado);
			this.consultar();

			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
					"Motivo de Baixa de Bem excluído com sucesso!",null));

		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	
	public void consultar() {
		this.motivosBaixas = motivosBaixasRepository.todos();
	}

	public List<MotivoBaixa> getMotivosBaixas() {
		return motivosBaixas;
	}
	
	public List<MotivoBaixa> getConsultar() {
		return motivosBaixas;
	}

	public MotivoBaixa getMotivoBaixaSelecionado() {
		return motivoBaixaSelecionado;
	}
	
	public void setMotivoBaixaSelecionado(MotivoBaixa motivoBaixaSelecionado) {
		this.motivoBaixaSelecionado = motivoBaixaSelecionado;
	}
}