
package com.henriquelavor.sigpat.model;

import java.io.Serializable;
//import java.util.ArrayList;
//import java.util.List;
import java.util.Date;

//import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
//import javax.persistence.JoinTable;
//import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;


/**
 * @author Henrique Lavor
 * @data 31.05.2019
 * @version 1.0
 */

@Entity
@Table (name="tb_usuarios")
public class Usuario implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long codigoUsuario; //ok
	private Funcionario funcionario; //ok
	private String email; //ok
	private String passsword; //ok
	private Date dataCadastro = new Date();
   // private List<Perfil> perfis = new ArrayList<>();
    private Perfil perfil;
	 
	 private Boolean notificarEmail;
		

	@Id
	@GeneratedValue
	@Column(name = "codigo_usuario")
	public Long getCodigoUsuario() {
		return codigoUsuario;
	}

	public void setCodigoUsuario(Long codigoUsuario) {
		this.codigoUsuario = codigoUsuario;
	}
	
	
	@OneToOne(optional = false)
	@JoinColumn(name = "cod_funcionario")
	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}
	
	
	@NotNull
	@Size(max = 200)
	@Column(name = "email", length = 200, nullable = false, unique = true)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email.trim().toLowerCase();
	}

	@NotNull
	@Size(max = 20)
	@Column(name = "password", length = 20, nullable = false)
	public String getPasssword() {
		return passsword;
	}
	
	public void setPasssword(String passsword) {
		this.passsword = passsword;
	}


	@Column(name = "data_registro_criado")
    @Temporal(TemporalType.DATE)
	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
	
	
	
	/*
	 * @ManyToMany(cascade = CascadeType.ALL)
	 * 
	 * @JoinTable(name = "tb_usuario_perfil", joinColumns
	 * = @JoinColumn(name="codigo_usuario"), inverseJoinColumns = @JoinColumn(name =
	 * "codigo_perfil")) public List<Perfil> getPerfis() { return perfis; } public
	 * void setPerfis(List<Perfil> perfis) { this.perfis = perfis; }
	 */
	
	
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "codigo_perfil")
	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	@Type(type = "true_false")
	@Column(name = "notificar_email")
	public Boolean getNotificarEmail() {
		return notificarEmail;
	}

	public void setNotificarEmail(Boolean notificarEmail) {
		this.notificarEmail = notificarEmail;
	}

	
	/*
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "tb_usuario_perfil", joinColumns = @JoinColumn(name="codigo_usuario"),
			inverseJoinColumns = @JoinColumn(name = "codigo_perfil"))
	public List<Perfil> getPerfis() {
		return perfis;
	}


	public void setPerfis(List<Perfil> perfis) {
		this.perfis = perfis;
	}
	 */
	
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoUsuario == null) ? 0 : codigoUsuario.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (codigoUsuario == null) {
			if (other.codigoUsuario != null)
				return false;
		} else if (!codigoUsuario.equals(other.codigoUsuario))
			return false;
		return true;
	}

	
	
}
