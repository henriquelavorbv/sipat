
package com.henriquelavor.sigpat.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * @author Henrique Lavor
 * @data 17.05.2016
 * @version 1.0
 */
@Entity
@Table (name="tb_cargo")
public class Cargo implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long codigoCargo;
	private String descricaoCargo;
	
	@Id
	@GeneratedValue
	public Long getCodigoCargo() {
		return codigoCargo;
	}
	public void setCodigoCargo(Long codigoCargo) {
		this.codigoCargo = codigoCargo;
	}
	
	@NotNull
	@Size(max = 100)
	@Column(name = "descricao", length = 100, nullable = false)
	public String getDescricaoCargo() {
		return descricaoCargo;
	}
	public void setDescricaoCargo(String descricaoCargo) {
		this.descricaoCargo = descricaoCargo;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoCargo == null) ? 0 : codigoCargo.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cargo other = (Cargo) obj;
		if (codigoCargo == null) {
			if (other.codigoCargo != null)
				return false;
		} else if (!codigoCargo.equals(other.codigoCargo))
			return false;
		return true;
	}
}
