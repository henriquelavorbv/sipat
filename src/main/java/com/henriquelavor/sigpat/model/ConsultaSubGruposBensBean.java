
package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.repository.SubGruposBens;
import com.henriquelavor.sigpat.service.CadastroSubGruposBens;
import com.henriquelavor.sigpat.service.NegocioException;

/**
 * @author Henrique Lavor
 * @data 13.04.2016
 * @version 1.0
 */

@Named
@ViewScoped
public class ConsultaSubGruposBensBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private SubGruposBens subGruposBensRepository;

	@Inject
	private CadastroSubGruposBens cadastro;

	private List<SubGrupoBens> subGruposBens;

	private SubGrupoBens subGrupoBensSelecionado;

	public void excluir() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.cadastro.excluir(this.subGrupoBensSelecionado);
			this.consultar();

			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,"SubGrupo de Bem Patrimonial excluído com sucesso!",null));

		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public void consultar() {
		this.subGruposBens = subGruposBensRepository.todos();
	}

	public List<SubGrupoBens> getSubGruposBens() {
		return subGruposBens;
	}

	public List<SubGrupoBens> getConsultar() {
		return subGruposBens;
	}
	
	public SubGrupoBens getSubGrupoBensSelecionado() {
		return subGrupoBensSelecionado;
	}
	
	public void setSubGrupoBensSelecionado(SubGrupoBens subGrupoBensSelecionado) {
		this.subGrupoBensSelecionado = subGrupoBensSelecionado;
	}
}
