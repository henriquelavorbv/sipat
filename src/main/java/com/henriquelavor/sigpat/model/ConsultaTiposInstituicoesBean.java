
package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.repository.TiposInstituicoes;
import com.henriquelavor.sigpat.service.CadastroTiposInstituicoes;
import com.henriquelavor.sigpat.service.NegocioException;

/**
 * @author Henrique Lavor
 * @data 23.05.2016
 * @version 1.0
 */

@Named
@ViewScoped
public class ConsultaTiposInstituicoesBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private TiposInstituicoes tiposInstituicoesRepository;
	
	@Inject
	private CadastroTiposInstituicoes cadastro;
	
	private List<TipoInstituicao> tiposInstituicoes;
	
	private TipoInstituicao tipoInstituicaoSelecionado;
	
	public void excluir() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.cadastro.excluir(this.tipoInstituicaoSelecionado);
			this.consultar();

			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
					"Tipo de Instituição excluído com sucesso!",null));

		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	
	public void consultar() {
		this.tiposInstituicoes = tiposInstituicoesRepository.todos();
	}

	public List<TipoInstituicao> getTiposIntituicoes() {
		return tiposInstituicoes;
	}
	
	public List<TipoInstituicao> getConsultar() {
		return tiposInstituicoes;
	}

	public TipoInstituicao getTipoInstituicaoSelecionado() {
		return tipoInstituicaoSelecionado;
	}
	
	public void setTipoInstituicaoSelecionado(TipoInstituicao tipoInstituicaoSelecionado) {
		this.tipoInstituicaoSelecionado = tipoInstituicaoSelecionado;
	}
}