package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;

/**
 * @author Henrique Lavor
 * @data 26.09.2017
 * @version 1.0
 */
@Entity
@Table(name = "tb_vistoria")
public class Vistoria implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long codigoVistoria; // auto  ok
	private Inventario inventario; // auto ok
	private BemMovel bemMovel; // informar ok
	private EstadoBens estadoBem; // informar
	private Date dataVistoria = new Date(); // auto
	private Divergencia divergencia; // informar (opcional)
	private String observacao; // opcional

	@Id
	@GeneratedValue
	public Long getCodigoVistoria() {
		return codigoVistoria;
	}

	public void setCodigoVistoria(Long codigoVistoria) {
		this.codigoVistoria = codigoVistoria;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_inventario")
	public Inventario getInventario() {
		return inventario;
	}

	public void setInventario(Inventario inventario) {
		this.inventario = inventario;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_bem")
	public BemMovel getBemMovel() {
		return bemMovel;
	}

	public void setBemMovel(BemMovel bemMovel) {
		this.bemMovel = bemMovel;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_estado_bem")
	public EstadoBens getEstadoBem() {
		return estadoBem;
	}

	public void setEstadoBem(EstadoBens estadoBem) {
		this.estadoBem = estadoBem;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "data_vistoria", nullable = false)
	public Date getDataVistoria() {
		return dataVistoria;
	}

	public void setDataVistoria(Date dataVistoria) {
		this.dataVistoria = dataVistoria;
	}

	@ManyToOne(optional = true)
	@JoinColumn(name = "cod_divergencia")
	public Divergencia getDivergencia() {
		return divergencia;
	}

	public void setDivergencia(Divergencia divergencia) {
		this.divergencia = divergencia;
	}

	@Size(max = 700)
	@Column(name = "observacao", length = 700, nullable = true)
	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoVistoria == null) ? 0 : codigoVistoria.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vistoria other = (Vistoria) obj;
		if (codigoVistoria == null) {
			if (other.codigoVistoria != null)
				return false;
		} else if (!codigoVistoria.equals(other.codigoVistoria))
			return false;
		return true;
	}
}
