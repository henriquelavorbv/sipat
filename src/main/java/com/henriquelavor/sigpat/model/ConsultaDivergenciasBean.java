
package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.repository.Divergencias;
import com.henriquelavor.sigpat.service.CadastroDivergencias;
import com.henriquelavor.sigpat.service.NegocioException;

/**
 * @author Henrique Lavor
 * @data 25.09.2017
 * @version 1.0
 */

@Named
@ViewScoped
public class ConsultaDivergenciasBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Divergencias divergenciasRepository;
	
	@Inject
	private CadastroDivergencias cadastro;
	
	private List<Divergencia> divergencias;
	
	private Divergencia divergenciaSelecionada;
	
	public void excluir() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.cadastro.excluir(this.divergenciaSelecionada);
			this.consultar();

			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
					"Divergência excluída com sucesso!",null));

		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	
	public void consultar() {
		this.divergencias = divergenciasRepository.todas();
	}

	public List<Divergencia> getDivergencias() {
		return divergencias;
	}
	
	public List<Divergencia> getConsultar() {
		return divergencias;
	}

	public Divergencia getDivergenciaSelecionada() {
		return divergenciaSelecionada;
	}
	
	public void setDivergenciaSelecionada(Divergencia divergenciaSelecionada) {
		this.divergenciaSelecionada = divergenciaSelecionada;
	}
}