package com.henriquelavor.sigpat.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.henriquelavor.sigpat.validation.CorreioEletronico;

/**
 * @author Henrique Lavor
 * @data 29.07.2016
 * @version 1.0
 */
@Entity
@Table(name = "tb_funcionario")
public class Funcionario implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long codigoFuncionario;
	private String nomeFuncionario;
	private TipoSexo sexo;
	private String cpf;
	private String telefone;
	private String celular;
	private String email;

	private String cep;
	private String uf;
	private String municipio;
	private String distrito;
	private String logradouro;
	private String numero;
	private String complemento;
	private String latitude;
	private String longitude;
	
	//private UnidadeGestora unidadeGestora;
	private UnidadeAdministrativa unidadeAdministrativa;

	private Cargo cargo;
	private String matricula="Não Informado";
	
	private TipoSituacaoFuncionario tipoSituacaoFuncionario;
	
	private String foto = "semfoto.png";
	
	
	private ResponsavelPatrimonial responsavelPatrimonial;
	
	private ResponsavelPatrimonial responsavelInventario;
	
	@Id
	@GeneratedValue
	public Long getCodigoFuncionario() {
		return codigoFuncionario;
	}

	public void setCodigoFuncionario(Long codigoFuncionario) {
		this.codigoFuncionario = codigoFuncionario;
	}

	@NotNull
	@Size(max = 150)
	@Column(name = "nome_funcionario", length = 150, nullable = true)
	public String getNomeFuncionario() {
		return nomeFuncionario;
	}

	public void setNomeFuncionario(String nomeFuncionario) {
		this.nomeFuncionario = nomeFuncionario;
	}
	
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "sexo", nullable = false, length=20)
	public TipoSexo getSexo() {
		return sexo;
	}

	public void setSexo(TipoSexo sexo) {
		this.sexo = sexo;
	}

	@NotNull
	@Size(max = 14)
	@Column(name = "cpf", length = 14, nullable = true)
	public String getCpf() {
		return cpf;
	}

	
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	@NotNull
	@Size(max = 20)
	@Column(name = "telefone", length = 20, nullable = true)
	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	@NotNull
	@Size(max = 20)
	@Column(name = "celular", length = 20, nullable = true)
	public String getCelular() {
		return celular;
	}
	
	@NotNull
	@Size(max = 150)
	@CorreioEletronico
	@Column(name = "email", length = 150, nullable = true)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	public void setCelular(String celular) {
		this.celular = celular;
	}

	@NotNull
	@Size(max = 9)
	@Column(name = "cep", length = 9, nullable = true)
	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	@NotNull
	@Size(max = 2)
	@Column(name = "uf", length = 2, nullable = true)
	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	@NotNull
	@Size(max = 100)
	@Column(name = "municipio", length = 100, nullable = true)
	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	@NotNull
	@Size(max = 150)
	@Column(name = "distrito", length = 150, nullable = true)
	public String getDistrito() {
		return distrito;
	}

	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}

	@NotNull
	@Size(max = 100)
	@Column(name = "logradouro", length = 100, nullable = true)
	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	@NotNull
	@Size(max = 10)
	@Column(name = "numero", length = 10, nullable = true)
	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	@NotNull
	@Size(max = 150)
	@Column(name = "complemento", length = 150, nullable = true)
	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	@NotNull
	@Size(max = 20)
	@Column(name = "latitude", length = 20, nullable = true)
	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	@NotNull
	@Size(max = 20)
	@Column(name = "longitude", length = 20, nullable = true)
	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_unidade_administrativa")
	public UnidadeAdministrativa getUnidadeAdministrativa() {
		return unidadeAdministrativa;
	}

	public void setUnidadeAdministrativa(UnidadeAdministrativa unidadeAdministrativa) {
		this.unidadeAdministrativa = unidadeAdministrativa;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_cargo")
	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	@NotNull
	@Size(max = 20)
	@Column(name = "matricula", length = 20, nullable = true)
	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	
	@NotNull
	@Size(max = 100)
	@Column(name = "foto", length = 100, nullable = true)
	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "responsavel_patrimonial", nullable = false, length=4)
	public ResponsavelPatrimonial getResponsavelPatrimonial() {
		return responsavelPatrimonial;
	}

	public void setResponsavelPatrimonial(ResponsavelPatrimonial responsavelPatrimonial) {
		this.responsavelPatrimonial = responsavelPatrimonial;
	}
	
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "responsavel_inventario", nullable = false, length=4)
	public ResponsavelPatrimonial getResponsavelInventario() {
		return responsavelInventario;
	}

	public void setResponsavelInventario(ResponsavelPatrimonial responsavelInventario) {
		this.responsavelInventario = responsavelInventario;
	}
	
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	public TipoSituacaoFuncionario getTipoSituacaoFuncionario() {
		return tipoSituacaoFuncionario;
	}

	public void setTipoSituacaoFuncionario(TipoSituacaoFuncionario tipoSituacaoFuncionario) {
		this.tipoSituacaoFuncionario = tipoSituacaoFuncionario;
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoFuncionario == null) ? 0 : codigoFuncionario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Funcionario other = (Funcionario) obj;
		if (codigoFuncionario == null) {
			if (other.codigoFuncionario != null)
				return false;
		} else if (!codigoFuncionario.equals(other.codigoFuncionario))
			return false;
		return true;
	}
}
