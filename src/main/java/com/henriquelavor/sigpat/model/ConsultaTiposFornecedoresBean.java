
package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.repository.TiposFornecedores;
import com.henriquelavor.sigpat.service.CadastroTiposFornecedores;
import com.henriquelavor.sigpat.service.NegocioException;

/**
 * @author Henrique Lavor
 * @data 27.05.2016
 * @version 1.0
 */

@Named
@ViewScoped
public class ConsultaTiposFornecedoresBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private TiposFornecedores tiposFornecedoresRepository;
	
	@Inject
	private CadastroTiposFornecedores cadastro;
	
	private List<TipoFornecedor> tiposFornecedores;
	
	private TipoFornecedor tipoFornecedorSelecionado;
	
	public void excluir() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.cadastro.excluir(this.tipoFornecedorSelecionado);
			this.consultar();
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
					"Tipo de Fornecedor excluído com sucesso!",null));
		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	
	public void consultar() {
		this.tiposFornecedores = tiposFornecedoresRepository.todos();
	}

	public List<TipoFornecedor> getTiposFornecedores() {
		return tiposFornecedores;
	}
	
	public List<TipoFornecedor> getConsultar() {
		return tiposFornecedores;
	}

	public TipoFornecedor getTipoFornecedorSelecionado() {
		return tipoFornecedorSelecionado;
	}
	
	public void setTipoFornecedorSelecionado(TipoFornecedor tipoFornecedorSelecionado) {
		this.tipoFornecedorSelecionado = tipoFornecedorSelecionado;
	}
}