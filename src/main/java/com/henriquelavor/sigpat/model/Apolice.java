package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author Henrique Lavor
 * @data 10.08.2017
 * @version 1.0
 */
@Entity
@Table(name = "tb_apolice")
public class Apolice implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long codigoApolice;
	private BemMovel bemMovel;
	private Seguradora seguradora;
	private String numeroApolice;
	private Date dataContratacao;
	private String apolicePdf;
	private Boolean notificarEmail;

	@Id
	@GeneratedValue
	public Long getCodigoApolice() {
		return codigoApolice;
	}

	public void setCodigoApolice(Long codigoApolice) {
		this.codigoApolice = codigoApolice;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_bem")
	public BemMovel getBemMovel() {
		return bemMovel;
	}

	public void setBemMovel(BemMovel bemMovel) {
		this.bemMovel = bemMovel;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_seguradora")
	public Seguradora getSeguradora() {
		return seguradora;
	}

	public void setSeguradora(Seguradora seguradora) {
		this.seguradora = seguradora;
	}

	@NotEmpty
	@Size(max = 30)
	@Column(name = "numero_apolice", length = 30, nullable = false)
	public String getNumeroApolice() {
		return numeroApolice;
	}

	public void setNumeroApolice(String numeroApolice) {
		this.numeroApolice = numeroApolice;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "data_contratacao", nullable = false)
	public Date getDataContratacao() {
		return dataContratacao;
	}

	public void setDataContratacao(Date dataContratacao) {
		this.dataContratacao = dataContratacao;
	}

	@Size(max = 200)
	@Column(name = "apolice_pdf", length = 200, nullable = true)
	public String getApolicePdf() {
		return apolicePdf;
	}

	public void setApolicePdf(String apolicePdf) {
		this.apolicePdf = apolicePdf;
	}
	

	@Type(type = "true_false")
	@Column(name = "notificar_email")
	public Boolean getNotificarEmail() {
		return notificarEmail;
	}

	public void setNotificarEmail(Boolean notificarEmail) {
		this.notificarEmail = notificarEmail;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoApolice == null) ? 0 : codigoApolice.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Apolice other = (Apolice) obj;
		if (codigoApolice == null) {
			if (other.codigoApolice != null)
				return false;
		} else if (!codigoApolice.equals(other.codigoApolice))
			return false;
		return true;
	}
}
