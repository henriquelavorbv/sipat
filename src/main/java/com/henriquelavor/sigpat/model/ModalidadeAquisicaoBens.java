
package com.henriquelavor.sigpat.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * @author Henrique Lavor
 * @data 18.05.2016
 * @version 1.0
 */
@Entity
@Table (name="tb_modalidade_aquisicao_bem")
public class ModalidadeAquisicaoBens implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long codigoModalidadeAquisicaoBem;
	private String descricaoModalidadeAquisicaoBem;
	
	@Id
	@GeneratedValue
	public Long getCodigoModalidadeAquisicaoBem() {
		return codigoModalidadeAquisicaoBem;
	}
	
	public void setCodigoModalidadeAquisicaoBem(Long codigoModalidadeAquisicaoBem) {
		this.codigoModalidadeAquisicaoBem = codigoModalidadeAquisicaoBem;
	}
	
	@NotNull
	@Size(max = 30)
	@Column(name = "descricao", length = 30, nullable = false)
	public String getDescricaoModalidadeAquisicaoBem() {
		return descricaoModalidadeAquisicaoBem;
	}
	
	public void setDescricaoModalidadeAquisicaoBem(String descricaoModalidadeAquisicaoBem) {
		this.descricaoModalidadeAquisicaoBem = descricaoModalidadeAquisicaoBem;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((codigoModalidadeAquisicaoBem == null) ? 0 : codigoModalidadeAquisicaoBem.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ModalidadeAquisicaoBens other = (ModalidadeAquisicaoBens) obj;
		if (codigoModalidadeAquisicaoBem == null) {
			if (other.codigoModalidadeAquisicaoBem != null)
				return false;
		} else if (!codigoModalidadeAquisicaoBem.equals(other.codigoModalidadeAquisicaoBem))
			return false;
		return true;
	}
}
