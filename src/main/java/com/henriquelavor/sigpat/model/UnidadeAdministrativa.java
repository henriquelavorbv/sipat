package com.henriquelavor.sigpat.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Henrique Lavor
 * @data 25.05.2016
 * @version 1.0
 */
@Entity
@Table(name = "tb_unidade_administrativa")
public class UnidadeAdministrativa implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long codigoUnidadeAdministrativa;
	private UnidadeGestora unidadeGestora;
	private String nomeUnidadeAdministrativa;
	private String nomeResumido;

	private String cep;
	private String logradouro;
	private String distrito;
	private String municipio;
	private String uf;
	private String numero;
	private String complemento;

	private String latitude;
	private String longitude;

	// novas implementações 27.08.2016
	private String predio;
	private String piso;
	private String corredor;
	private String sala;
	
	//nova implementação 04.09.2016
	//nova atualização 19.01.2017 valor padrao= Não
	private String vistoriando="Não";  //Unidade administrativa com Inventario Aberto, caso vistoriando=Sim


	@Id
	@GeneratedValue
	public Long getCodigoUnidadeAdministrativa() {
		return codigoUnidadeAdministrativa;
	}

	public void setCodigoUnidadeAdministrativa(Long codigoUnidadeAdministrativa) {
		this.codigoUnidadeAdministrativa = codigoUnidadeAdministrativa;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_unidade_gestora")
	public UnidadeGestora getUnidadeGestora() {
		return unidadeGestora;
	}

	public void setUnidadeGestora(UnidadeGestora unidadeGestora) {
		this.unidadeGestora = unidadeGestora;
	}

	@NotNull
	@Size(max = 150)
	@Column(name = "nome_unid_administrativa", length = 150, nullable = false)
	public String getNomeUnidadeAdministrativa() {
		return nomeUnidadeAdministrativa;
	}

	public void setNomeUnidadeAdministrativa(String nomeUnidadeAdministrativa) {
		this.nomeUnidadeAdministrativa = nomeUnidadeAdministrativa;
	}

	@NotNull
	@Size(max = 30)
	@Column(name = "nome_resumido", length = 30, nullable = false)
	public String getNomeResumido() {
		return nomeResumido;
	}

	public void setNomeResumido(String nomeResumido) {
		this.nomeResumido = nomeResumido;
	}

	@NotNull
	@Size(max = 9)
	@Column(name = "cep", length = 9, nullable = true)
	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	@NotNull
	@Size(max = 2)
	@Column(name = "uf", length = 2, nullable = true)
	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	@NotNull
	@Size(max = 100)
	@Column(name = "municipio", length = 100, nullable = true)
	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	@NotNull
	@Size(max = 150)
	@Column(name = "distrito", length = 150, nullable = true)
	public String getDistrito() {
		return distrito;
	}

	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}

	@NotNull
	@Size(max = 100)
	@Column(name = "logradouro", length = 100, nullable = true)
	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	@NotNull
	@Size(max = 10)
	@Column(name = "numero", length = 10, nullable = true)
	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	@NotNull
	@Size(max = 150)
	@Column(name = "complemento", length = 150, nullable = true)
	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	@NotNull
	@Size(max = 20)
	@Column(name = "latitude", length = 20, nullable = true)
	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	@NotNull
	@Size(max = 20)
	@Column(name = "longitude", length = 20, nullable = true)
	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	@NotNull
	@Size(max = 100)
	@Column(name = "predio", length = 100, nullable = false)
	public String getPredio() {
		return predio;
	}

	public void setPredio(String predio) {
		this.predio = predio;
	}

	@NotNull
	@Size(max = 8)
	@Column(name = "piso", length = 8, nullable = false)
	public String getPiso() {
		return piso;
	}

	public void setPiso(String piso) {
		this.piso = piso;
	}

	@NotNull
	@Size(max = 10)
	@Column(name = "corredor", length = 10, nullable = false)
	public String getCorredor() {
		return corredor;
	}

	public void setCorredor(String corredor) {
		this.corredor = corredor;
	}

	@NotNull
	@Size(max = 50)
	@Column(name = "sala", length = 50, nullable = false)
	public String getSala() {
		return sala;
	}

	
	public void setSala(String sala) {
		this.sala = sala;
	}
	
	@Size(max = 5)
	@Column(name = "vistoriando", length = 5, nullable = true)
	public String getVistoriando() {
		return vistoriando;
	}

	public void setVistoriando(String vistoriando) {
		this.vistoriando = vistoriando;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoUnidadeAdministrativa == null) ? 0 : codigoUnidadeAdministrativa.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UnidadeAdministrativa other = (UnidadeAdministrativa) obj;
		if (codigoUnidadeAdministrativa == null) {
			if (other.codigoUnidadeAdministrativa != null)
				return false;
		} else if (!codigoUnidadeAdministrativa.equals(other.codigoUnidadeAdministrativa))
			return false;
		return true;
	}
}
