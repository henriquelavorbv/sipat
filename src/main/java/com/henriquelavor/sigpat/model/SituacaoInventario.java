package com.henriquelavor.sigpat.model;
/**
 * @author Henrique Lavor
 * @data 21.08.2016
 * @version 1.0
 */

public enum SituacaoInventario {
	Aberto, Cancelado, Encerrado
}
