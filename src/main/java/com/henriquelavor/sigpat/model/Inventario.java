package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;

/**
 * @author Henrique Lavor
 * @data 02.09.2016
 * @version 1.0
 */

@Entity
@Table(name = "tb_inventario")
public class Inventario implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long codigoInventario;
	private ClassificacaoInventario classificacaoInventario;
	private UnidadeAdministrativa unidadeAdministrativa;
	private Funcionario funcionarioResponsavelInventario;
	private Date dataInicial;
	private Date dataFinal; // somente usado quando for encerrar o inventario
	private String descricao;
	private String observacao;
	private SituacaoInventario situacaoInventario;
	private Boolean notificarEncerramentoEmail;
	private Boolean notificarAbertoEmail;

	@Id
	@GeneratedValue
	public Long getCodigoInventario() {
		return codigoInventario;
	}

	public void setCodigoInventario(Long codigoInventario) {
		this.codigoInventario = codigoInventario;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_classificacao_inventario")
	public ClassificacaoInventario getClassificacaoInventario() {
		return classificacaoInventario;
	}

	public void setClassificacaoInventario(ClassificacaoInventario classificacaoInventario) {
		this.classificacaoInventario = classificacaoInventario;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_unidade_administrativa")
	public UnidadeAdministrativa getUnidadeAdministrativa() {
		return unidadeAdministrativa;
	}

	public void setUnidadeAdministrativa(UnidadeAdministrativa unidadeAdministrativa) {
		this.unidadeAdministrativa = unidadeAdministrativa;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_funcionario_responsavel_inventario")
	public Funcionario getFuncionarioResponsavelInventario() {
		return funcionarioResponsavelInventario;
	}

	public void setFuncionarioResponsavelInventario(Funcionario funcionarioResponsavelInventario) {
		this.funcionarioResponsavelInventario = funcionarioResponsavelInventario;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "data_inicial", nullable = false)
	public Date getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "data_final", nullable = true)
	public Date getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}

	@NotNull
	@Size(max = 600)
	@Column(name = "descricao", length = 600, nullable = false)
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Size(max = 600)
	@Column(name = "observacao", length = 600, nullable = true)
	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "situacao_inventario", nullable = true, length = 15)
	public SituacaoInventario getSituacaoInventario() {
		return situacaoInventario;
	}

	public void setSituacaoInventario(SituacaoInventario situacaoInventario) {
		this.situacaoInventario = situacaoInventario;
	}

	
	@Type(type = "true_false")
	@Column(name = "notificar_email_aberto")
	public Boolean getNotificarAbertoEmail() {
		return notificarAbertoEmail;
	}

	public void setNotificarAbertoEmail(Boolean notificarAbertoEmail) {
		this.notificarAbertoEmail = notificarAbertoEmail;
	}

	
	@Type(type = "true_false")
	@Column(name = "notificar_email_encerramento")
	public Boolean getNotificarEncerramentoEmail() {
		return notificarEncerramentoEmail;
	}

	public void setNotificarEncerramentoEmail(Boolean notificarEncerramentoEmail) {
		this.notificarEncerramentoEmail = notificarEncerramentoEmail;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoInventario == null) ? 0 : codigoInventario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Inventario other = (Inventario) obj;
		if (codigoInventario == null) {
			if (other.codigoInventario != null)
				return false;
		} else if (!codigoInventario.equals(other.codigoInventario))
			return false;
		return true;
	}
}
