
package com.henriquelavor.sigpat.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * @author Henrique Lavor
 * @data 27.05.2016
 * @version 1.0
 */
@Entity
@Table (name="tb_referencia_fornecedor")
public class ReferenciaFornecedor implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long codigoReferenciaFornecedor;
	private String descricaoReferenciaFornecedor;
	private RegraFornecedor regraFornecedor;
	
	@Id
	@GeneratedValue
	public Long getCodigoReferenciaFornecedor() {
		return codigoReferenciaFornecedor;
	}
	
	public void setCodigoReferenciaFornecedor(Long codigoReferenciaFornecedor) {
		this.codigoReferenciaFornecedor = codigoReferenciaFornecedor;
	}
	
	@NotNull
	@Size(max = 50)
	@Column(name = "descricao", length = 50, nullable = false)
	public String getDescricaoReferenciaFornecedor() {
		return descricaoReferenciaFornecedor;
	}
	public void setDescricaoReferenciaFornecedor(String descricaoReferenciaFornecedor) {
		this.descricaoReferenciaFornecedor = descricaoReferenciaFornecedor;
	}
	
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	public RegraFornecedor getRegraFornecedor() {
		return regraFornecedor;
	}

	public void setRegraFornecedor(RegraFornecedor regraFornecedor) {
		this.regraFornecedor = regraFornecedor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoReferenciaFornecedor == null) ? 0 : codigoReferenciaFornecedor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReferenciaFornecedor other = (ReferenciaFornecedor) obj;
		if (codigoReferenciaFornecedor == null) {
			if (other.codigoReferenciaFornecedor != null)
				return false;
		} else if (!codigoReferenciaFornecedor.equals(other.codigoReferenciaFornecedor))
			return false;
		return true;
	}
	
}
