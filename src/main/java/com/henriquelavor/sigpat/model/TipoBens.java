
package com.henriquelavor.sigpat.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Henrique Lavor
 * @data 13.05.2016
 * @version 1.0
 */
@Entity
@Table(name = "tb_tipo_bem")
public class TipoBens implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long codigoTipoBem;
	private String descricaoTipoBem;

	@Id
	@GeneratedValue
	public Long getCodigoTipoBem() {
		return codigoTipoBem;
	}

	public void setCodigoTipoBem(Long codigoTipoBem) {
		this.codigoTipoBem = codigoTipoBem;
	}

	@NotNull
	@Size(max = 30)
	@Column(name = "descricao", length = 30, nullable = false)
	public String getDescricaoTipoBem() {
		return descricaoTipoBem;
	}

	public void setDescricaoTipoBem(String descricaoTipoBem) {
		this.descricaoTipoBem = descricaoTipoBem;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoTipoBem == null) ? 0 : codigoTipoBem.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoBens other = (TipoBens) obj;
		if (codigoTipoBem == null) {
			if (other.codigoTipoBem != null)
				return false;
		} else if (!codigoTipoBem.equals(other.codigoTipoBem))
			return false;
		return true;
	}
}
