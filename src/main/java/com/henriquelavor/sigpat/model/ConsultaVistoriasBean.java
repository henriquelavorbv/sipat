
package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.repository.Vistorias;
import com.henriquelavor.sigpat.service.CadastroVistorias;
import com.henriquelavor.sigpat.service.NegocioException;

/**
 * @author Henrique Lavor
 * @data 26.09.2017
 * @version 1.0
 */

@Named
@ViewScoped
public class ConsultaVistoriasBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Vistorias vistoriasRepository;

	@Inject
	private CadastroVistorias cadastro;

	private List<Vistoria> vistorias;

	private Vistoria vistoriaSelecionada;

	public void excluir() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.cadastro.excluir(this.vistoriaSelecionada);
			this.consultar();

			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
					"Vistoria de Bem Patrimonial excluída com sucesso!", null));

		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public void consultar() {
		this.vistorias = vistoriasRepository.todas();
	}

	public List<Vistoria> getVistorias() {
		return vistorias;
	}

	public List<Vistoria> getConsultar() {
		return vistorias;
	}

	public Vistoria getVistoriaSelecionada() {
		return vistoriaSelecionada;
	}

	public void setVistoriaSelecionada(Vistoria vistoriaSelecionada) {
		this.vistoriaSelecionada = vistoriaSelecionada;
	}
}
