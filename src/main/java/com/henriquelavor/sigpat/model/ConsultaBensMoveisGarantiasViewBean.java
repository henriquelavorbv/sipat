
package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.repository.BensMoveisGarantiasView;
import com.henriquelavor.sigpat.service.CadastroClassificacoesInventarios;
import com.henriquelavor.sigpat.service.NegocioException;
import com.henriquelavor.sigpat.util.jsf.FacesUtil;
import com.henriquelavor.sigpat.util.mail.Mailer;
import com.outjected.email.api.MailMessage;

/**
 * @author Henrique Lavor
 * @data 07.09.2019
 * @version 1.0
 */

@Named
@ViewScoped
public class ConsultaBensMoveisGarantiasViewBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private BensMoveisGarantiasView bensMoveisGarantiasViewRepository;
	
	@Inject
	private Mailer mailer;
	
	private List<BemMovelGarantiaView> bensMoveisGarantiasView;
	
	private BemMovelGarantiaView bemMovelGarantiaViewSelecionado;
	
	
	public void consultar() {
		this.bensMoveisGarantiasView = bensMoveisGarantiasViewRepository.todos();
	}

	public List<BemMovelGarantiaView> getBensMoveisGarantiasView() {
		return bensMoveisGarantiasView;
	}
	
	public List<BemMovelGarantiaView> getConsultar() {
		return bensMoveisGarantiasView;
	}

	public BemMovelGarantiaView getBemMovelGarantiaViewSelecionado() {
		return bemMovelGarantiaViewSelecionado;
	}
	
	public void setBemMovelGarantiaViewSelecionado(BemMovelGarantiaView bemMovelGarantiaViewSelecionado) {
		this.bemMovelGarantiaViewSelecionado = bemMovelGarantiaViewSelecionado;
	}
	
	
	public void enviarNotificacao(){
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String dataAquisicao = df.format(this.bemMovelGarantiaViewSelecionado. getDataAquisicao());
		String dataLimiteGarantia = df.format(this.bemMovelGarantiaViewSelecionado.getDataLimiteGarantia());
		
		MailMessage message = mailer.prepararNovaMensagem();
		message.to(this.bemMovelGarantiaViewSelecionado.getEmailFuncionario())
		.subject("SIGPAT: Notificação de Prazo de Garantia de Bem Patrimonial Nº : " + this.bemMovelGarantiaViewSelecionado.getNumeroPatrimonio())
		.bodyHtml("<strong>Data de Aquisição do Bem: </strong>" + dataAquisicao +"<br />" + 
		"<strong>Data de Término da Garantia do bem: </strong>" + dataLimiteGarantia +"<br />" + 
		"<strong>Dias Restantes da Garantia: </strong>" + this.bemMovelGarantiaViewSelecionado.getDiasGarantia() +"<br /><br />" + 
		"<strong>Unidade Gestora/Administrativa: </strong>" + this.bemMovelGarantiaViewSelecionado.getUaDestino()+"<br/>" +
		"<strong>Situação do Bem Ativo: </strong>" + this.bemMovelGarantiaViewSelecionado.getAtivo() +"<br />" +
		"<strong>Funcionário Responsável: </strong>" + this.bemMovelGarantiaViewSelecionado.getNomeFuncionario() +"<br />" +
		"<strong>Descrição do Bem: </strong>" + this.bemMovelGarantiaViewSelecionado.getDescricaoBem() +"<br /><br />" +
		"<strong>Em caso dúvida, favor entrar em contato com o setor responsável. </strong>")
		.send();
		
		FacesUtil.addInfoMessage("Notificação de Prazo de Garantia do Bem enviado por e-mail com sucesso ao responsável pelo Bem!");
	}
}