
package com.henriquelavor.sigpat.model;

import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.google.common.io.Resources;
import com.henriquelavor.sigpat.repository.Reparos;
import com.henriquelavor.sigpat.service.CadastroReparos;
import com.henriquelavor.sigpat.service.NegocioException;
import com.henriquelavor.sigpat.util.jsf.FacesUtil;
import com.henriquelavor.sigpat.util.mail.Mailer;
import com.outjected.email.api.ContentDisposition;
import com.outjected.email.api.MailMessage;
import com.outjected.email.impl.attachments.BaseAttachment;

/**
 * @author Henrique Lavor
 * @data 01.09.2017
 * @version 1.0
 */

@Named
@ViewScoped
public class ConsultaReparosBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Reparos reparosRepository;

	@Inject
	private CadastroReparos cadastro;

	private List<Reparo> reparos;

	private Reparo reparoSelecionado;

	@Inject
	private Mailer mailer;

	public void enviarNotificacaoEmailSaida() throws IOException {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String dataSaida = df.format(this.reparoSelecionado.getDataSaida());

		MailMessage message = mailer.prepararNovaMensagem();

		message.to(this.reparoSelecionado.getBemMovel().getNovoFuncionarioResponsavelPatrimonial().getEmail())
				.subject("SIGPAT: Notificação de Saída Bem Patrimonial para REPARO")
				.bodyHtml("<strong>Unidade Gestora: </strong>"
						+ this.reparoSelecionado.getBemMovel().getLocalDestino().getUnidadeGestora().getNomeUnidadeGestora() + "("
						+ this.reparoSelecionado.getBemMovel().getLocalDestino().getUnidadeGestora().getNomeResumido() + ")"
						+ "<br/>" + "<strong>Unidade Administrativa: </strong>"
						+ this.reparoSelecionado.getBemMovel().getLocalDestino().getNomeUnidadeAdministrativa() + "("
						+ this.reparoSelecionado.getBemMovel().getLocalDestino().getNomeResumido() + ")" + "<br/>"
						+ "<strong>Bem Patrimonial: </strong>" + this.reparoSelecionado.getBemMovel().getNumeroPatrimonio() + " - "
						+ this.reparoSelecionado.getBemMovel().getDescricao() + "<br />" + "<br />"
						+ "<strong>Funcionário Responsável pela Guarda do Bem: </strong>"
						+ this.reparoSelecionado.getBemMovel().getNovoFuncionarioResponsavelPatrimonial().getNomeFuncionario()
						+ "<br />" + "<strong>Descrição do Defeito: </strong>" + this.reparoSelecionado.getDescricaoDefeito()
						+ "<br />" + "<strong>Empresa Prestadora do Serviço: </strong>"
						+ this.reparoSelecionado.getFornecedorReparo().getNomeFornecedor() + "<br />" + "<strong>Data da Saída do Bem: </strong>"
						+ dataSaida + "<br />")
				.send();

		FacesUtil.addInfoMessage(
				"Notificação de Registro de Saída de Bem Patrimonial para REPARO enviado por e-mail com sucesso!");
	}


	public void excluir() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.cadastro.excluir(this.reparoSelecionado);
			this.consultar();

			context.addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_WARN, "Reparo de Bem Patrimonial excluído com sucesso!", null));

		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public void consultar() {
		this.reparos = reparosRepository.todos();
	}

	public List<Reparo> getReparos() {
		return reparos;
	}

	public List<Reparo> getConsultar() {
		return reparos;
	}

	public Reparo getReparoSelecionado() {
		return reparoSelecionado;
	}

	public void setReparoSelecionado(Reparo reparoSelecionado) {
		this.reparoSelecionado = reparoSelecionado;
	}
}
