
package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.repository.MarcasBens;
import com.henriquelavor.sigpat.service.CadastroMarcasBens;
import com.henriquelavor.sigpat.service.NegocioException;

/**
 * @author Henrique Lavor
 * @data 28.04.2016
 * @version 1.0
 */

@Named
@ViewScoped
public class ConsultaMarcasBensBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private MarcasBens marcasBensRepository;
	
	@Inject
	private CadastroMarcasBens cadastro;
	
	private List<MarcaBens> marcasBens;
	
	private MarcaBens marcaBensSelecionado;
	
	public void excluir() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.cadastro.excluir(this.marcaBensSelecionado);
			this.consultar();

			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
					"Marca de Bem Patrimonial excluído com sucesso!",null));

		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	
	public void consultar() {
		this.marcasBens = marcasBensRepository.todas();
	}

	public List<MarcaBens> getMarcasBens() {
		return marcasBens;
	}
	
	public List<MarcaBens> getConsultar() {
		return marcasBens;
	}

	public MarcaBens getMarcaBensSelecionado() {
		return marcaBensSelecionado;
	}
	
	public void setMarcaBensSelecionado(MarcaBens marcaBensSelecionado) {
		this.marcaBensSelecionado = marcaBensSelecionado;
	}
}
