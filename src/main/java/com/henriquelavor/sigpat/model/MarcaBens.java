
package com.henriquelavor.sigpat.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * @author Henrique Lavor
 * @data 28.04.2016
 * @version 1.0
 */
@Entity
@Table (name="tb_marca_bem")
public class MarcaBens implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long codigoMarcaBem;
	private String descricaoMarcaBem;

	
	@Id
	@GeneratedValue
	public Long getCodigoMarcaBem() {
		return codigoMarcaBem;
	}

	public void setCodigoMarcaBem(Long codigoMarcaBem) {
		this.codigoMarcaBem = codigoMarcaBem;
	}

	@NotNull
	@Size(max = 80)
	@Column(name = "descricao", length = 100, nullable = false)
	public String getDescricaoMarcaBem() {
		return descricaoMarcaBem;
	}

	public void setDescricaoMarcaBem(String descricaoMarcaBem) {
		this.descricaoMarcaBem = descricaoMarcaBem;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoMarcaBem == null) ? 0 : codigoMarcaBem.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MarcaBens other = (MarcaBens) obj;
		if (codigoMarcaBem == null) {
			if (other.codigoMarcaBem != null)
				return false;
		} else if (!codigoMarcaBem.equals(other.codigoMarcaBem))
			return false;
		return true;
	}
}
