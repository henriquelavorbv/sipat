package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.joda.time.DateTime;
import org.joda.time.Days;

/**
 * @author Henrique Lavor
 * @data 01.10.2016
 * @data_atualizacao 16.01.2017
 * @version 1.1
 */
@Entity
@Table(name = "tb_bem_movel")
public class BemMovel implements Serializable {

	private static final long serialVersionUID = 1L;
	
	//aba dados patrimoniais
	private String fotoBemMovel = "semfotobem.gif";
	private Long codigoBemMovel;
	private String numeroPatrimonio = "";
	private Date dataTombamento;
	private String descricao;
	private TipoBens tipo;
	private MarcaBens marca;
	private String modelo;
	private String numeroSerie;
	private EstadoBens estadoBem;
	private GrupoBens grupoBem;
	private SubGrupoBens subGrupoBem;
	//aba aquisicao
	private String numeroProcesso;
	private String numeroEmpenho;
	private Date dataEmissaoEmpenho;
	private String numeroNotaFiscal;
	private String notaFiscalPdf;
	private Date dataLimiteGarantia;
	private FonteRecurso fonteRecurso; //nova atualizacao 16.01.2017 nova classe FonteRecurso
	
	private UnidadeGestora unidadeOrcamentariaFonteRecurso; //beta
	private String numeroConvenio=""; //beta
	
	private String elementoDespesa; // Valor padrao 4.4.50.52.00 Equipamentos e Material Permanente
	private Fornecedor fornecedor;
	private Date dataAquisicao;
	private ModalidadeAquisicaoBens modalidadeAquisicao;
	//private int quantidade=1; //valor minimo inicial

	//financeiro R$
	private BigDecimal valorAquisicao;
	private BigDecimal valorReavaliado; //criar historico em tabela de reavaliacao do bem 
	private Date dataReavaliacao;       //criar historico em tabela de reavaliacao do bem 
	
	//baixa do bem
	private BigDecimal valorBaixa; 
	private Date dataBaixa; 
	//private String numeroTermoBaixa="";
	
	//responsabilidade  
	private Funcionario antigoFuncionarioResponsavelPatrimonial; //opcional
	private Funcionario novoFuncionarioResponsavelPatrimonial; //opcional
	
	private String numeroTermoResponsabilidade="NAO INCORPORADO";
	private Date dataTermoResponsabilidade;
	
	
	//dados de origem e destino
	private UnidadeAdministrativa localOrigem; 
	private UnidadeAdministrativa localDestino;
	
	//nao tem aba apenas operacoes internas
	private String numeroTransferencia;
	private String situacaoDepreciado = "Não"; //Valor padrao ao criar um bem este ainda nao foi depreciado
	
	private String ativo = "Sim"; //Valor padrao ao criar o bem deve estar ativo
	
    private Date dataCadastro = new Date();
    
    private String codigoOrcamentarioCadastroBem;
    
	@Id
	@GeneratedValue
	public Long getCodigoBemMovel() {
		return codigoBemMovel;
	}

	public void setCodigoBemMovel(Long codigoBemMovel) {
		this.codigoBemMovel = codigoBemMovel;
	}

	@NotNull
	@Size(max = 20)
	@Column(name = "numero_patrimonio", length = 20, nullable = false)
	public String getNumeroPatrimonio() {
		return numeroPatrimonio;
	}

	public void setNumeroPatrimonio(String numeroPatrimonio) {
		this.numeroPatrimonio = numeroPatrimonio;
	}

	@Temporal(TemporalType.DATE)
	@Column(name= "data_tombamento", nullable=true)
	public Date getDataTombamento() {
		return dataTombamento;
	}

	public void setDataTombamento(Date dataTombamento) {
		this.dataTombamento = dataTombamento;
	}

	@NotNull
	@Size(max = 600)
	@Column(name = "descricao", length = 600, nullable = false)
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_tipo")
	public TipoBens getTipo() {
		return tipo;
	}

	public void setTipo(TipoBens tipo) {
		this.tipo = tipo;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_marca")
	public MarcaBens getMarca() {
		return marca;
	}

	public void setMarca(MarcaBens marca) {
		this.marca = marca;
	}

	@NotNull
	@Size(max = 50)
	@Column(name = "modelo", length = 50, nullable = true)
	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	@NotNull
	@Size(max = 30)
	@Column(name = "numero_serie", length = 30, nullable = true)
	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}


	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_estado_bem")
	public EstadoBens getEstadoBem() {
		return estadoBem;
	}

	public void setEstadoBem(EstadoBens estadoBem) {
		this.estadoBem = estadoBem;
	}


	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_grupo_bem")
	public GrupoBens getGrupoBem() {
		return grupoBem;
	}

	public void setGrupoBem(GrupoBens grupoBem) {
		this.grupoBem = grupoBem;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_sub_grupo_bem")
	public SubGrupoBens getSubGrupoBem() {
		return subGrupoBem;
	}

	public void setSubGrupoBem(SubGrupoBens subGrupoBem) {
		this.subGrupoBem = subGrupoBem;
	}

	@NotNull
	@Size(max = 30)
	@Column(name = "numero_processo", length = 30, nullable = true)
	public String getNumeroProcesso() {
		return numeroProcesso;
	}

	public void setNumeroProcesso(String numeroProcesso) {
		this.numeroProcesso = numeroProcesso;
	}

	@NotNull
	@Size(max = 30)
	@Column(name = "numero_empenho", length = 30, nullable = true)
	public String getNumeroEmpenho() {
		return numeroEmpenho;
	}

	public void setNumeroEmpenho(String numeroEmpenho) {
		this.numeroEmpenho = numeroEmpenho;
	}

	@Temporal(TemporalType.DATE)
	@Column(name= "data_emissao_empenho", nullable=true)
	public Date getDataEmissaoEmpenho() {
		return dataEmissaoEmpenho;
	}

	public void setDataEmissaoEmpenho(Date dataEmissaoEmpenho) {
		this.dataEmissaoEmpenho = dataEmissaoEmpenho;
	}

	@NotNull
	@Size(max = 30)
	@Column(name = "numero_nota_fiscal", length = 30, nullable = true)
	public String getNumeroNotaFiscal() {
		return numeroNotaFiscal;
	}

	public void setNumeroNotaFiscal(String numeroNotaFiscal) {
		this.numeroNotaFiscal = numeroNotaFiscal;
	}

	@Size(max = 200)
	@Column(name = "nota_fiscal_pdf", length = 200, nullable = true)
	public String getNotaFiscalPdf() {
		return notaFiscalPdf;
	}

	public void setNotaFiscalPdf(String notaFiscalPdf) {
		this.notaFiscalPdf = notaFiscalPdf;
	}

	@Temporal(TemporalType.DATE)
	@Column(name= "data_limite_garantia", nullable=true)
	public Date getDataLimiteGarantia() {
		return dataLimiteGarantia;
	}

	public void setDataLimiteGarantia(Date dataLimiteGarantia) {
		this.dataLimiteGarantia = dataLimiteGarantia;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_fonte_recurso")
	public FonteRecurso getFonteRecurso() {
		return fonteRecurso;
	}

	public void setFonteRecurso(FonteRecurso fonteRecurso) {
		this.fonteRecurso = fonteRecurso;
	}
	
	//beta
	@ManyToOne(optional = true)
	@JoinColumn(name = "cod_unidade_orcamentaria")
	public UnidadeGestora getUnidadeOrcamentariaFonteRecurso() {
		return unidadeOrcamentariaFonteRecurso;
	}

	public void setUnidadeOrcamentariaFonteRecurso(UnidadeGestora unidadeOrcamentariaFonteRecurso) {
		this.unidadeOrcamentariaFonteRecurso = unidadeOrcamentariaFonteRecurso;
	}

	
	@NotNull
	@Size(max = 20)
	@Column(name = "numero_convenio", length = 20, nullable = true)
	public String getNumeroConvenio() {
		return numeroConvenio;
	}
	
   
	public void setNumeroConvenio(String numeroConvenio) {
		this.numeroConvenio = numeroConvenio;
	}
	
	//fim beta
	
	@NotNull
	@Size(max = 20)
	@Column(name = "elemento_despesa", length = 20, nullable = true)
	public String getElementoDespesa() {
		return elementoDespesa;
	}

	public void setElementoDespesa(String elementoDespesa) {
		this.elementoDespesa = elementoDespesa;
	}

	@Temporal(TemporalType.DATE)
	@Column(name= "data_aquisicao", nullable=false)
	public Date getDataAquisicao() {
		return dataAquisicao;
	}

	public void setDataAquisicao(Date dataAquisicao) {
		this.dataAquisicao = dataAquisicao;
	}

	@Column(name= "valor_aquisicao", precision = 12, scale = 2, nullable = false)
	public BigDecimal getValorAquisicao() {
		return valorAquisicao;
	}

	public void setValorAquisicao(BigDecimal valorAquisicao) {
		this.valorAquisicao = valorAquisicao;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_modalidade_aquisicao")
	public ModalidadeAquisicaoBens getModalidadeAquisicao() {
		return modalidadeAquisicao;
	}

	public void setModalidadeAquisicao(ModalidadeAquisicaoBens modalidadeAquisicao) {
		this.modalidadeAquisicao = modalidadeAquisicao;
	}

	@Column(name= "valor_reavaliado", precision = 12, scale = 4, nullable = true)
	public BigDecimal getValorReavaliado() {
		return valorReavaliado;
	}

	public void setValorReavaliado(BigDecimal valorReavaliado) {
		this.valorReavaliado = valorReavaliado;
	}

	@Temporal(TemporalType.DATE)
	@Column(name= "data_reavaliacao", nullable=true)
	public Date getDataReavaliacao() {
		return dataReavaliacao;
	}

	public void setDataReavaliacao(Date dataReavaliacao) {
		this.dataReavaliacao = dataReavaliacao;
	}
	
	@Column(name= "valor_baixa", precision = 12, scale = 4, nullable = true)
	public BigDecimal getValorBaixa() {
		return valorBaixa;
	}

	public void setValorBaixa(BigDecimal valorBaixa) {
		this.valorBaixa = valorBaixa;
	}

	@Temporal(TemporalType.DATE)
	@Column(name= "data_baixa", nullable=true)
	public Date getDataBaixa() {
		return dataBaixa;
	}

	public void setDataBaixa(Date dataBaixa) {
		this.dataBaixa = dataBaixa;
	}
		
	@NotNull
	@Size(max = 5)
	@Column(name = "situacao_depreciado", length = 5, nullable = false)
	public String getSituacaoDepreciado() {
		return situacaoDepreciado;
	}

	public void setSituacaoDepreciado(String situacaoDepreciado) {
		this.situacaoDepreciado = situacaoDepreciado;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_forncedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	@ManyToOne(optional = true)
	@JoinColumn(name = "cod_antigo_funcionario_resp_patrimonial")
	public Funcionario getAntigoFuncionarioResponsavelPatrimonial() {
		return antigoFuncionarioResponsavelPatrimonial;
	}

	public void setAntigoFuncionarioResponsavelPatrimonial(Funcionario antigoFuncionarioResponsavelPatrimonial) {
		this.antigoFuncionarioResponsavelPatrimonial = antigoFuncionarioResponsavelPatrimonial;
	}
	
	@ManyToOne(optional = true)
	@JoinColumn(name = "cod_novo_funcionario_resp_patrimonial")
	public Funcionario getNovoFuncionarioResponsavelPatrimonial() {
		return novoFuncionarioResponsavelPatrimonial;
	}

	public void setNovoFuncionarioResponsavelPatrimonial(Funcionario novoFuncionarioResponsavelPatrimonial) {
		this.novoFuncionarioResponsavelPatrimonial = novoFuncionarioResponsavelPatrimonial;
	}
	
	

	@NotNull
	@Size(max = 20)
	@Column(name = "numero_termo_responsabilidade", length = 20, nullable = true)
	public String getNumeroTermoResponsabilidade() {
		return numeroTermoResponsabilidade;
	}

	public void setNumeroTermoResponsabilidade(String numeroTermoResponsabilidade) {
		this.numeroTermoResponsabilidade = numeroTermoResponsabilidade;
	}

	@Temporal(TemporalType.DATE)
	@Column(name= "data_termo_responsabilidade", nullable=true)
	public Date getDataTermoResponsabilidade() {
		return dataTermoResponsabilidade;
	}

	public void setDataTermoResponsabilidade(Date dataTermoResponsabilidade) {
		this.dataTermoResponsabilidade = dataTermoResponsabilidade;
	}

	@Size(max = 20)
	@Column(name = "numero_transferencia", length = 20, nullable = true)
	public String getNumeroTransferencia() {
		return numeroTransferencia;
	}

	public void setNumeroTransferencia(String numeroTransferencia) {
		this.numeroTransferencia = numeroTransferencia;
	}

	@ManyToOne(optional = true)
	@JoinColumn(name = "cod_local_origem")
	public UnidadeAdministrativa getLocalOrigem() {
		return localOrigem;
	}

	public void setLocalOrigem(UnidadeAdministrativa localOrigem) {
		this.localOrigem = localOrigem;
	}


	@ManyToOne(optional = true)
	@JoinColumn(name = "cod_local_destino")
	public UnidadeAdministrativa getLocalDestino() {
		return localDestino;
	}

	public void setLocalDestino(UnidadeAdministrativa localDestino) {
		this.localDestino = localDestino;
	}

	@Size(max = 200)
	@Column(name = "foto_bem_movel", length = 200, nullable = true)
	public String getFotoBemMovel() {
		return fotoBemMovel;
	}

	public void setFotoBemMovel(String fotoBemMovel) {
		this.fotoBemMovel = fotoBemMovel;
	}

	@NotNull
	@Size(max = 5)
	@Column(name = "ativo", length = 10, nullable = false)
	public String getAtivo() {
		return ativo;
	}

	public void setAtivo(String ativo) {
		this.ativo = ativo;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name= "data_cadastro", nullable=true)
	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
	
	
	@NotNull
	@Size(max = 5)
	@Column(name = "codigo_orcamentario_cadastro_bem", length = 5, nullable = true)
    public String getCodigoOrcamentarioCadastroBem() {
		return codigoOrcamentarioCadastroBem;
	}

	public void setCodigoOrcamentarioCadastroBem(String codigoOrcamentarioCadastroBem) {
		this.codigoOrcamentarioCadastroBem = codigoOrcamentarioCadastroBem;
	}


	/*@Column(name = "quantidade", nullable = true)
	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}*/
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoBemMovel == null) ? 0 : codigoBemMovel.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BemMovel other = (BemMovel) obj;
		if (codigoBemMovel == null) {
			if (other.codigoBemMovel != null)
				return false;
		} else if (!codigoBemMovel.equals(other.codigoBemMovel))
			return false;
		return true;
	}
}
