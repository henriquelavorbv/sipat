
package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.repository.Perfis;
import com.henriquelavor.sigpat.service.CadastroPerfis;
import com.henriquelavor.sigpat.service.NegocioException;

/**
 * @author Henrique Lavor
 * @data 01.06.2019
 * @version 1.0
 */

@Named
@ViewScoped
public class ConsultaPerfisBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Perfis perfisRepository;
	
	@Inject
	private CadastroPerfis cadastro;
	
	private List<Perfil> perfis;
	
	private Perfil perfilSelecionado;
	
	public void excluir() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.cadastro.excluir(this.perfilSelecionado);
			this.consultar();

			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
					"Perfil de Usuário excluído com sucesso!",null));

		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	
	public void consultar() {
		this.perfis = perfisRepository.todos();
	}

	public List<Perfil> getPerfis() {
		return perfis;
	}
	
	public List<Perfil> getConsultar() {
		return perfis;
	}

	public Perfil getPerfilSelecionado() {
		return perfilSelecionado;
	}
	
	public void setPerfilSelecionado(Perfil perfilSelecionado) {
		this.perfilSelecionado = perfilSelecionado;
	}
}