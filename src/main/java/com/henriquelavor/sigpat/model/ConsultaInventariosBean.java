
package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.repository.Inventarios;
import com.henriquelavor.sigpat.service.CadastroInventarios;
import com.henriquelavor.sigpat.service.NegocioException;
import com.henriquelavor.sigpat.util.jsf.FacesUtil;
import com.henriquelavor.sigpat.util.mail.Mailer;
import com.outjected.email.api.MailMessage;

/**
 * @author Henrique Lavor
 * @data 03.09.2016
 * @version 1.0
 */

@Named
@ViewScoped
public class ConsultaInventariosBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Inventarios inventariosRepository;

	@Inject
	private CadastroInventarios cadastro;

	private List<Inventario> inventarios;

	private Inventario inventarioSelecionado;
	
	
	@Inject
	private Mailer mailer;
	
	public void enviarInventario(){
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String dataInicial = df.format(this.inventarioSelecionado.getDataInicial());
		
		MailMessage message = mailer.prepararNovaMensagem();
		message.to(this.inventarioSelecionado.getFuncionarioResponsavelInventario().getEmail())
		.subject("SIGPAT: Notificação de Inventário Nº : " + this.inventarioSelecionado.getCodigoInventario())
		.bodyHtml("<strong>Data Inicial do Inventário: </strong>" + dataInicial +"<br />" + 
		"<strong>Unidade Gestora: </strong>" + this.inventarioSelecionado.getUnidadeAdministrativa().getUnidadeGestora().getNomeUnidadeGestora() +"("+ this.inventarioSelecionado.getUnidadeAdministrativa().getUnidadeGestora().getNomeResumido()+")"+"<br/>" +
		"<strong>Unidade Administrativa: </strong>" + this.inventarioSelecionado.getUnidadeAdministrativa().getNomeUnidadeAdministrativa() +"("+ this.inventarioSelecionado.getUnidadeAdministrativa().getNomeResumido()+")" + "<br/>" +
		"<strong>Situação: </strong>" + this.inventarioSelecionado.getSituacaoInventario() +"<br />" +
		"<strong>Funcionário Responsável: </strong>" + this.inventarioSelecionado.getFuncionarioResponsavelInventario().getNomeFuncionario() +"<br />" +
		"<strong>Descrição/Objetivo do Inventário: </strong>" + this.inventarioSelecionado.getDescricao() +"<br />")
		.send();
		
		FacesUtil.addInfoMessage("Notificação de Inventário enviado por e-mail com sucesso!");
	}

	public void excluir() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.cadastro.excluir(this.inventarioSelecionado);
			this.consultar();

			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,"Inventário patrimonial excluído com sucesso!",null));

		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public void consultar() {
		this.inventarios = inventariosRepository.todos();
	}

	public List<Inventario> getInventarios() {
		return inventarios;
	}

	public List<Inventario> getConsultar() {
		return inventarios;
	}
	
	public Inventario getInventarioSelecionado() {
		return inventarioSelecionado;
	}
	
	public void setInventarioSelecionado(Inventario inventarioSelecionado) {
		this.inventarioSelecionado = inventarioSelecionado;
	}
}
