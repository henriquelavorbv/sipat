package com.henriquelavor.sigpat.model;

public enum TipoSituacaoUnidadeGestora {
	Ativo, Inativo
}
