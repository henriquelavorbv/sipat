/*
 * Nome da Classe: ConsultaUnidadesGestorasBean
 * Data da Criação: 24.05.2016
 * 
 * ATUALIZAÇÕES: 
 * 
 * No. da Atualização: 01
 * Data atualização: 17.07.2019 as 14:25
 * Objetivo: 
 * A Função public void consultar() foi atualizada,
 * para permitir filtrar apenas a Unidade Gestora logada
 * 
 */

package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.henriquelavor.sigpat.repository.UnidadesGestoras;
import com.henriquelavor.sigpat.service.CadastroUnidadesGestoras;
import com.henriquelavor.sigpat.service.NegocioException;

/**
 * @author Henrique Lavor
 * @data 24.05.2016
 * @version 1.1
 */

@Named
@ViewScoped
public class ConsultaUnidadesGestorasBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private UnidadesGestoras unidadesGestorasRepository;
	
	@Inject
	private CadastroUnidadesGestoras cadastro;
	
	private List<UnidadeGestora> unidadesGestoras;
	
	private UnidadeGestora unidadeGestoraSelecionada;
	
	public void excluir() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.cadastro.excluir(this.unidadeGestoraSelecionada);
			this.consultar();

			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
					"Unidade Gestora excluída com sucesso!",null));

		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	
	public void consultar() {
		this.unidadesGestoras = unidadesGestorasRepository.todas();
	}
	
	public void consultarGeral() {
		this.unidadesGestoras = unidadesGestorasRepository.todasGeral();
	}

	public List<UnidadeGestora> getUnidadesGestoras() {
		return unidadesGestoras;
	}
	
	public List<UnidadeGestora> getConsultar() {
		return unidadesGestoras;
	}
	
	public List<UnidadeGestora> getConsultarGeral() {
		return unidadesGestoras;
	}
	

	public UnidadeGestora getUnidadeGestoraSelecionada() {
		return unidadeGestoraSelecionada;
	}
	
	public void setUnidadeGestoraSelecionada(UnidadeGestora unidadeGestoraSelecionada) {
		this.unidadeGestoraSelecionada = unidadeGestoraSelecionada;
	}
}