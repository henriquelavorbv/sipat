package com.henriquelavor.sigpat.model;

import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.henriquelavor.sigpat.repository.Transferencias;
import com.henriquelavor.sigpat.service.CadastroTransferencias;
import com.henriquelavor.sigpat.service.NegocioException;
import com.henriquelavor.sigpat.util.jsf.FacesUtil;
import com.henriquelavor.sigpat.util.mail.Mailer;
import com.outjected.email.api.MailMessage;

/**
 * @author Henrique Lavor
 * @data 15.08.2017
 * @version 1.0
 */

@Named
@ViewScoped
public class ConsultaTransferenciasBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Transferencias transferenciasRepository;

	@Inject
	private CadastroTransferencias cadastro;

	private List<Transferencia> transferencias;

	private Transferencia transferenciaSelecionada;
	
	private String codigoOrcamentarioLogado;
	
	
	@Inject
	private Mailer mailer;
	
	public void enviarNotificacao() {
		if (this.transferenciaSelecionada.getNumeroTermoTransferencia().contains("TIB")) {
			this.enviarNotificacaoResponsaveisTransferencia();
		}else if (this.transferenciaSelecionada.getNumeroTermoTransferencia().contains("TEB")) {
			this.enviarNotificacaoResponsaveisTransferenciaTEB();
		}	
	}
	
	

	
	//enviar email ao novo responsavel pela Entrada do bem ao setor
	public void enviarNotificacaoResponsaveisTransferencia(){
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String dataTransferencia = df.format(this.transferenciaSelecionada.getDataTransferencia());
		String dataTransferenciaCadastrada = df.format(this.transferenciaSelecionada.getDataCadastro());
		
		MailMessage message = mailer.prepararNovaMensagem();
		message.to(this.transferenciaSelecionada.getNovoFuncionarioResponsavelPatrimonial().getEmail(),this.transferenciaSelecionada.getAntigoFuncionarioResponsavelPatrimonial().getEmail())
		.subject("SIGPAT: Notificação de Transferência Interna de BEM Patrimonial TIB Nº.: " + this.transferenciaSelecionada.getNumeroTermoTransferencia())
		.bodyHtml("<strong>Data da transferência: </strong>" + dataTransferencia +"<br />" + 
		"<strong>Data de registro no sistema: </strong>" + dataTransferenciaCadastrada +"<br />" +"<br />" +
				
	    "<strong>Tipo de Transferência: </strong>" + this.transferenciaSelecionada.getTipoTransferencia().getDescricaoTipoTransferencia() +"<br />" +"<br />" +
	    
 		"<strong>No. Patrimônio: </strong>" + this.transferenciaSelecionada.getBemMovel().getNumeroPatrimonio() +"<br />" +
 		"<strong>Bem Patrimonial: </strong>" + this.transferenciaSelecionada.getBemMovel().getDescricao() +"<br />" +
 		"<strong>Novo Estado de Conservação do Bem: </strong>" + this.transferenciaSelecionada.getEstadoBem().getDescricaoEstadoBem() +"<br />" +"<br />" + 
 		
		"<strong>Unidade Gestora de Origem: </strong>" + this.transferenciaSelecionada.getLocalOrigem().getUnidadeGestora().getNomeUnidadeGestora() +"("+ this.transferenciaSelecionada.getLocalOrigem().getUnidadeGestora().getNomeResumido()+")"+"<br/>" +
		"<strong>Unidade Administrativa de Origem: </strong>" + this.transferenciaSelecionada.getLocalOrigem().getNomeUnidadeAdministrativa() +"("+ this.transferenciaSelecionada.getLocalOrigem().getNomeResumido()+")" +"<br />" +
		
		"<strong>Funcionário Responsável Saída: </strong>" + this.transferenciaSelecionada.getAntigoFuncionarioResponsavelPatrimonial().getNomeFuncionario() +"<br />" +
		"<strong>Matrícula do Funcionário Receptor/Responsável: </strong>" + this.transferenciaSelecionada.getAntigoFuncionarioResponsavelPatrimonial().getMatricula() +"<br />" +
		"<strong>Cargo: </strong>" + this.transferenciaSelecionada.getAntigoFuncionarioResponsavelPatrimonial().getCargo().getDescricaoCargo() +"<br />" +
		"<strong>Lotação: </strong>" + this.transferenciaSelecionada.getAntigoFuncionarioResponsavelPatrimonial().getUnidadeAdministrativa().getUnidadeGestora().getNomeUnidadeGestora()  +"("+ this.transferenciaSelecionada.getAntigoFuncionarioResponsavelPatrimonial().getUnidadeAdministrativa().getNomeUnidadeAdministrativa() +")" + "<br />" + "<br />" +
		
		"<strong>Unidade Gestora de Destino: </strong>" + this.transferenciaSelecionada.getLocalDestino().getUnidadeGestora().getNomeUnidadeGestora() +"("+ this.transferenciaSelecionada.getLocalDestino().getUnidadeGestora().getNomeResumido()+")"+"<br/>" +
		"<strong>Unidade Administrativa de Destino: </strong>" + this.transferenciaSelecionada.getLocalDestino().getNomeUnidadeAdministrativa() +"("+ this.transferenciaSelecionada.getLocalDestino().getNomeResumido()+")"+"<br/>" +
		
		"<strong>Funcionário Responsável Receptor </strong>" + this.transferenciaSelecionada.getNovoFuncionarioResponsavelPatrimonial().getNomeFuncionario() +"<br />" +
		"<strong>Matrícula do Funcionário Receptor/Responsável: </strong>" + this.transferenciaSelecionada.getNovoFuncionarioResponsavelPatrimonial().getMatricula() +"<br />" +
		"<strong>Cargo: </strong>" + this.transferenciaSelecionada.getNovoFuncionarioResponsavelPatrimonial().getCargo().getDescricaoCargo() +"<br />" +
		"<strong>Lotação: </strong>" + this.transferenciaSelecionada.getNovoFuncionarioResponsavelPatrimonial().getUnidadeAdministrativa().getUnidadeGestora().getNomeUnidadeGestora()  +"("+ this.transferenciaSelecionada.getNovoFuncionarioResponsavelPatrimonial().getUnidadeAdministrativa().getNomeUnidadeAdministrativa() +")" + "<br />" + "<br />" +

		"<strong>Observação: </strong>" + this.transferenciaSelecionada.getObservacao() +"<br />" +"<br />" +
		
		"<strong> Caro Responsável, este email, tem como finalidade, informá-lo que, você tem de posse um documento oficial impresso e "
		+ "assinado do Termo de Transferência No. " + this.transferenciaSelecionada.getNumeroTermoTransferencia()
		+ ", guarde-o em um local seguro, para futuros levantamentos/inventários patrimoniais de sua unidade gestora/administrativa."
		+ "   </strong>" +"<br />")
		.send();
		
		FacesUtil.addInfoMessage("Notificação de Termo de Responsabilidade enviado por e-mail aos responsáveis com sucesso!");
	}
	
	
	public void enviarNotificacaoResponsaveisTransferenciaTEB(){
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String dataTransferencia = df.format(this.transferenciaSelecionada.getDataTransferencia());
		String dataTransferenciaCadastrada = df.format(this.transferenciaSelecionada.getDataCadastro());
		
		MailMessage message = mailer.prepararNovaMensagem();
		message.to(this.transferenciaSelecionada.getAntigoFuncionarioResponsavelPatrimonial().getEmail())
		.subject("SIGPAT: Notificação de Transferência Externa de BEM Patrimonial TEB Nº.: " + this.transferenciaSelecionada.getNumeroTermoTransferencia())
		.bodyHtml("<strong>Data da transferência: </strong>" + dataTransferencia +"<br />" + 
		"<strong>Data de registro no sistema: </strong>" + dataTransferenciaCadastrada +"<br />" +"<br />" +
				
	    "<strong>Tipo de Transferência: </strong>" + this.transferenciaSelecionada.getTipoTransferencia().getDescricaoTipoTransferencia() +"<br />" +"<br />" +
	    
 		"<strong>No. Patrimônio: </strong>" + this.transferenciaSelecionada.getBemMovel().getNumeroPatrimonio() +"<br />" +
 		"<strong>Bem Patrimonial: </strong>" + this.transferenciaSelecionada.getBemMovel().getDescricao() +"<br />" +
 		"<strong>Novo Estado de Conservação do Bem: </strong>" + this.transferenciaSelecionada.getEstadoBem().getDescricaoEstadoBem() +"<br />" +"<br />" + 
 		
		"<strong>Unidade Gestora de Origem: </strong>" + this.transferenciaSelecionada.getLocalOrigem().getUnidadeGestora().getNomeUnidadeGestora() +"("+ this.transferenciaSelecionada.getLocalOrigem().getUnidadeGestora().getNomeResumido()+")"+"<br/>" +
		"<strong>Unidade Administrativa de Origem: </strong>" + this.transferenciaSelecionada.getLocalOrigem().getNomeUnidadeAdministrativa() +"("+ this.transferenciaSelecionada.getLocalOrigem().getNomeResumido()+")" +"<br />" +
		
		"<strong>Funcionário Responsável Saída: </strong>" + this.transferenciaSelecionada.getAntigoFuncionarioResponsavelPatrimonial().getNomeFuncionario() +"<br />" +
		"<strong>Matrícula do Funcionário: </strong>" + this.transferenciaSelecionada.getAntigoFuncionarioResponsavelPatrimonial().getMatricula() +"<br />" +
		"<strong>Cargo: </strong>" + this.transferenciaSelecionada.getAntigoFuncionarioResponsavelPatrimonial().getCargo().getDescricaoCargo() +"<br />" +
		"<strong>Lotação: </strong>" + this.transferenciaSelecionada.getAntigoFuncionarioResponsavelPatrimonial().getUnidadeAdministrativa().getUnidadeGestora().getNomeUnidadeGestora()  +"("+ this.transferenciaSelecionada.getAntigoFuncionarioResponsavelPatrimonial().getUnidadeAdministrativa().getNomeUnidadeAdministrativa() +")" + "<br />" + "<br />" +
		
		"<strong>Unidade Gestora de Destino: </strong>" + this.transferenciaSelecionada.getLocalDestino().getUnidadeGestora().getNomeUnidadeGestora() +"("+ this.transferenciaSelecionada.getLocalDestino().getUnidadeGestora().getNomeResumido()+")"+"<br/>" +
		"<strong>Unidade Administrativa de Destino: </strong>" + this.transferenciaSelecionada.getLocalDestino().getNomeUnidadeAdministrativa() +"("+ this.transferenciaSelecionada.getLocalDestino().getNomeResumido()+")"+"<br/>" +
		
		"<strong>Observação: </strong>" + this.transferenciaSelecionada.getObservacao() +"<br />" +"<br />" +
		
		"<strong> Caro Responsável, este email, tem como finalidade, informá-lo que, a partir desta data, você não tem a posse do bem patrimonial,conforme documento oficial impresso e "
		+ "assinado do Termo de Transferência TEB No. " + this.transferenciaSelecionada.getNumeroTermoTransferencia()
		+ ", guarde-o em um local seguro, para futuros levantamentos/inventários patrimoniais de sua unidade gestora/administrativa."
		+ "   </strong>" +"<br />")
		.send();
		
		FacesUtil.addInfoMessage("Notificação de Transferência Externa de Bem patrimonial - TEB, enviado por e-mail aos responsáveis com sucesso!");
	}

	

	/* NAO USADO
	public void excluir(Transferencia transferenciaSelecionada) {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.excluir(this.transferenciaSelecionada);
			
			this.consultar();

			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,"Item do Termo Transferência de Bem Patrimonial excluído com sucesso!",null));

		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}
	*/
	
	
	
	public void gerarRecebimento() throws IOException{
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.receberTransferencia(this.transferenciaSelecionada);
			//this.cadastro.receberTransferencia(this.transferenciaSelecionada);
			
			//this.consultarTodasPendentes();

			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,"Termo de Transferência Externa RECEBIDO com sucesso!",null));
			

			FacesContext.getCurrentInstance().getExternalContext().redirect("/sigpat/paginas/Transferencia/ConsultaTransferenciasTEB.xhtml");

		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}
	
	
	public void gerarEstorno() throws IOException{
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.cadastro.estornarTransferencia(this.transferenciaSelecionada);
			//this.cadastro.receberTransferencia(this.transferenciaSelecionada);
			
			//this.consultarTodasPendentes();

			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,"Termo de Transferência Externa ESTORNAR com sucesso!",null));
			

			FacesContext.getCurrentInstance().getExternalContext().redirect("/sigpat/paginas/Transferencia/ConsultaTransferenciasTEB.xhtml");

		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}
	
	
	public void consultarTodas() {
		this.transferencias =  transferenciasRepository.todas();
	}
	
	public void consultarTodasPendentes() {
	
		this.transferencias =  transferenciasRepository.todasPendentes();
		
	}
	
	
	public void consultarTodasPorBens(String numero) {
		this.transferencias =  transferenciasRepository.todasTrasferenciasPorNumeroPatrimonial(numero);
	}

	public List<Transferencia> getTransferencias() {
		return transferencias;
	}

	public List<Transferencia> getConsultar() {
		return transferencias;
	}
	
	public Transferencia getTransferenciaSelecionada() {
		return transferenciaSelecionada;
	}
	
	public void setTransferenciaSelecionada(Transferencia transferenciaSelecionada) {
		this.transferenciaSelecionada = transferenciaSelecionada;
	}




	public String getCodigoOrcamentarioLogado() {
		//Pega a sessao do Codigo UO
		HttpServletRequest req = (HttpServletRequest) 
		FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		HttpSession session = (HttpSession) req.getSession();
		String codOrcamentarioSession = (String) session.getAttribute("COD_ORCAMENTARIA");
		
		this.codigoOrcamentarioLogado = codOrcamentarioSession;
				
		return codigoOrcamentarioLogado;
	}




	
	
	
	
}
