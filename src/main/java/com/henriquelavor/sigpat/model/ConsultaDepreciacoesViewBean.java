
package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.repository.DepreciacoesView;

/**
 * @author Henrique Lavor
 * @data 16.07.2019
 * @version 1.0
 */

@Named
@ViewScoped
public class ConsultaDepreciacoesViewBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private DepreciacoesView depreciacoesRepository;
	
	
	private List<DepreciacaoView> depreciacoes;
	
	private DepreciacaoView depreciacaoSelecionada;
	
	public void consultar() {
		this.depreciacoes = depreciacoesRepository.todasDepreciacoes();
	}
	
	
	public List<DepreciacaoView> getDepreciacoes() {
		return depreciacoes;
	}
	
	public List<DepreciacaoView> getConsultar() {
		return depreciacoes;
	}
	

	public DepreciacaoView getDepreciacaoSelecionada() {
		return depreciacaoSelecionada;
	}
	
	public void setDepreciacaoSelecionada(DepreciacaoView depreciacaoSelecionada) {
		this.depreciacaoSelecionada = depreciacaoSelecionada;
	}
}