
package com.henriquelavor.sigpat.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Henrique Lavor
 * @data 10.08.2017
 * @version 1.0
 */
@Entity
@Table(name = "tb_seguradora")
public class Seguradora implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long codigoSeguradora;
	private String descricaoSeguradora;

	@Id
	@GeneratedValue
	public Long getCodigoSeguradora() {
		return codigoSeguradora;
	}

	public void setCodigoSeguradora(Long codigoSeguradora) {
		this.codigoSeguradora = codigoSeguradora;
	}

	@NotNull
	@Size(max = 100)
	@Column(name = "descricao", length = 100, nullable = false)
	public String getDescricaoSeguradora() {
		return descricaoSeguradora;
	}

	public void setDescricaoSeguradora(String descricaoSeguradora) {
		this.descricaoSeguradora = descricaoSeguradora;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoSeguradora == null) ? 0 : codigoSeguradora.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Seguradora other = (Seguradora) obj;
		if (codigoSeguradora == null) {
			if (other.codigoSeguradora != null)
				return false;
		} else if (!codigoSeguradora.equals(other.codigoSeguradora))
			return false;
		return true;
	}
}
