package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;

/**
 * @author Henrique Lavor
 * @data 14.08.2017
 * @version 1.0
 */
@Entity
@Table(name = "tb_transferencia")
public class Transferencia implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long codigoTransferencia; //OK
	private String numeroTermoTransferencia; //OK
	private Date dataTransferencia; //OK
	private TipoTransferencia tipoTransferencia; //OK
	private BemMovel bemMovel; //OK
	
	private UnidadeAdministrativa localOrigem; // ok!! pulo do gato!
	
	private UnidadeAdministrativa localDestino; // nova localização   OK
	
	private Funcionario antigoFuncionarioResponsavelPatrimonial; //OK
	private Funcionario novoFuncionarioResponsavelPatrimonial; //OK
	
	private EstadoBens estadoBem; //OK
	private String observacao; //OK
	private String situacao="Aberto"; //OK
	private Boolean notificarEmail;
	
	private Date dataCadastro = new Date(); //OK
	
	private String statusRecebimento;
	
	
	@Id
	@GeneratedValue
	public Long getCodigoTransferencia() {
		return codigoTransferencia;
	}

	public void setCodigoTransferencia(Long codigoTransferencia) {
		this.codigoTransferencia = codigoTransferencia;
	}
	
	@NotNull
	@Size(max = 20)
	@Column(name = "numero_termo_transferencia", length = 20, nullable = true)
	public String getNumeroTermoTransferencia() {
		return numeroTermoTransferencia;
	}
	
	public void setNumeroTermoTransferencia(String numeroTermoTransferencia) {
		this.numeroTermoTransferencia = numeroTermoTransferencia;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_transferencia", nullable = true)
	public Date getDataTransferencia() {
		return dataTransferencia;
	}

	public void setDataTransferencia(Date dataTransferencia) {
		this.dataTransferencia = dataTransferencia;
	}
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_tipo_transferencia")
	public TipoTransferencia getTipoTransferencia() {
		return tipoTransferencia;
	}
	
	public void setTipoTransferencia(TipoTransferencia tipoTransferencia) {
		this.tipoTransferencia = tipoTransferencia;
	}
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_bem_movel")
	public BemMovel getBemMovel() {
		return bemMovel;
	}
	
	public void setBemMovel(BemMovel bemMovel) {
		this.bemMovel = bemMovel;
	}
	
	@ManyToOne(optional = true)
	@JoinColumn(name = "cod_local_origem")
	public UnidadeAdministrativa getLocalOrigem() {
		return localOrigem;
	}
	
	public void setLocalOrigem(UnidadeAdministrativa localOrigem) {
		this.localOrigem = localOrigem;
	}
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_local_destino")
	public UnidadeAdministrativa getLocalDestino() {
		return localDestino;
	}
	
	public void setLocalDestino(UnidadeAdministrativa localDestino) {
		this.localDestino = localDestino;
	}
	

	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_antigo_funcionario_resp_patrimonial")
	public Funcionario getAntigoFuncionarioResponsavelPatrimonial() {
		return antigoFuncionarioResponsavelPatrimonial;
	}
	
	public void setAntigoFuncionarioResponsavelPatrimonial(Funcionario antigoFuncionarioResponsavelPatrimonial) {
		this.antigoFuncionarioResponsavelPatrimonial = antigoFuncionarioResponsavelPatrimonial;
	}
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_novo_funcionario_resp_patrimonial")
	public Funcionario getNovoFuncionarioResponsavelPatrimonial() {
		return novoFuncionarioResponsavelPatrimonial;
	}

	public void setNovoFuncionarioResponsavelPatrimonial(Funcionario novoFuncionarioResponsavelPatrimonial) {
		this.novoFuncionarioResponsavelPatrimonial = novoFuncionarioResponsavelPatrimonial;
	}

	
	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_estado_bem")
	public EstadoBens getEstadoBem() {
		return estadoBem;
	}
	
	public void setEstadoBem(EstadoBens estadoBem) {
		this.estadoBem = estadoBem;
	}
	
	@Size(max = 700)
	@Column(name = "observacao", length = 700, nullable = true)
	public String getObservacao() {
		return observacao;
	}
	
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	@Size(max = 10)
	@Column(name = "situacao", length = 10, nullable = true)
	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	@Type(type = "true_false")
	@Column(name = "notificar_email")
	public Boolean getNotificarEmail() {
		return notificarEmail;
	}

	public void setNotificarEmail(Boolean notificarEmail) {
		this.notificarEmail = notificarEmail;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name= "data_cadastro", nullable=true)
	public Date getDataCadastro() {
		return dataCadastro;
	}
	
	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
	
	@Size(max = 15)
	@Column(name = "status_recebimento", length = 15, nullable = true)
	public String getStatusRecebimento() {
		return statusRecebimento;
	}

	public void setStatusRecebimento(String statusRecebimento) {
		this.statusRecebimento = statusRecebimento;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoTransferencia == null) ? 0 : codigoTransferencia.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Transferencia other = (Transferencia) obj;
		if (codigoTransferencia == null) {
			if (other.codigoTransferencia != null)
				return false;
		} else if (!codigoTransferencia.equals(other.codigoTransferencia))
			return false;
		return true;
	}
}
