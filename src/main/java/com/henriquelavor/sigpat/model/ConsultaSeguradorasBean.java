
package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.repository.Seguradoras;
import com.henriquelavor.sigpat.service.CadastroSeguradoras;
import com.henriquelavor.sigpat.service.NegocioException;

/**
 * @author Henrique Lavor
 * @data 10.08.2017
 * @version 1.0
 */

@Named
@ViewScoped
public class ConsultaSeguradorasBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Seguradoras seguradorasRepository;
	
	@Inject
	private CadastroSeguradoras cadastro;
	
	private List<Seguradora> seguradoras;
	
	private Seguradora seguradoraSelecionada;
	
	public void excluir() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.cadastro.excluir(this.seguradoraSelecionada);
			this.consultar();

			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
					"Seguradora excluído com sucesso!",null));

		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	
	public void consultar() {
		this.seguradoras = seguradorasRepository.todas();
	}

	public List<Seguradora> getSeguradoras() {
		return seguradoras;
	}
	
	public List<Seguradora> getConsultar() {
		return seguradoras;
	}

	public Seguradora getSeguradoraSelecionada() {
		return seguradoraSelecionada;
	}
	
	public void setSeguradoraSelecionada(Seguradora seguradoraSelecionada) {
		this.seguradoraSelecionada = seguradoraSelecionada;
	}
}