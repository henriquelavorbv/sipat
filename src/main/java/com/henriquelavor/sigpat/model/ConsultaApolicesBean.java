
package com.henriquelavor.sigpat.model;

import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.google.common.io.Resources;
import com.henriquelavor.sigpat.repository.Apolices;
import com.henriquelavor.sigpat.service.CadastroApolices;
import com.henriquelavor.sigpat.service.NegocioException;
import com.henriquelavor.sigpat.util.jsf.FacesUtil;
import com.henriquelavor.sigpat.util.mail.Mailer;
import com.outjected.email.api.ContentDisposition;
import com.outjected.email.api.MailMessage;
import com.outjected.email.impl.attachments.BaseAttachment;

/**
 * @author Henrique Lavor
 * @data 10.08.2017
 * @version 1.0
 */

@Named
@ViewScoped
public class ConsultaApolicesBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Apolices apolicesRepository;

	@Inject
	private CadastroApolices cadastro;

	private List<Apolice> apolices;

	private Apolice apoliceSelecionada;

	@Inject
	private Mailer mailer;

	public void enviarNotificacaoEmail() throws IOException {
		//FacesContext context = FacesContext.getCurrentInstance();
		String apolicePDFExistente = this.apoliceSelecionada.getApolicePdf();
		if ((this.apoliceSelecionada.getNotificarEmail()) && (apolicePDFExistente == null)) {
			this.enviarNotificaoEmail();
		} else if ((this.apoliceSelecionada.getNotificarEmail()) && (apolicePDFExistente != null)) {
			this.enviarNotificaoEmailAnexoPDF();
		}
		//context.addMessage(null, new FacesMessage(
		//		"Notificação enviada sobre o Registro de Apólice de Seguro de Bem Patrimonial com sucesso!"));
	}

	public void enviarNotificaoEmail() throws IOException {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String dataContratacao = df.format(this.apoliceSelecionada.getDataContratacao());

		MailMessage message = mailer.prepararNovaMensagem();

		message.to(this.apoliceSelecionada.getBemMovel().getNovoFuncionarioResponsavelPatrimonial().getEmail())
				.subject("SIGPAT: Notificação de Registro de Apólice de Seguro de Bem Patrimonial")
				.bodyHtml("<strong>Unidade Gestora: </strong>"
						+ this.apoliceSelecionada.getBemMovel().getLocalDestino().getUnidadeGestora().getNomeUnidadeGestora() + "("
						+ this.apoliceSelecionada.getBemMovel().getLocalDestino().getUnidadeGestora().getNomeResumido() + ")"
						+ "<br/>" + "<strong>Unidade Administrativa: </strong>"
						+ this.apoliceSelecionada.getBemMovel().getLocalDestino().getNomeUnidadeAdministrativa() + "("
						+ this.apoliceSelecionada.getBemMovel().getLocalDestino().getNomeResumido() + ")" + "<br/>"
						+ "<strong>Bem Patrimonial: </strong>" + this.apoliceSelecionada.getBemMovel().getNumeroPatrimonio()
						+ " - " + this.apoliceSelecionada.getBemMovel().getDescricao() + "<br />" + "<br />"
						+ "<strong>Funcionário Responsável pela Guarda do Bem: </strong>"
						+ this.apoliceSelecionada.getBemMovel().getNovoFuncionarioResponsavelPatrimonial().getNomeFuncionario()
						+ "<br />" + "<strong>Seguradora: </strong>"
						+ this.apoliceSelecionada.getSeguradora().getDescricaoSeguradora() + "<br />"
						+ "<strong>Número da Apólice: </strong>" + this.apoliceSelecionada.getNumeroApolice() + "<br />"
						+ "<strong>Data da Contratação: </strong>" + dataContratacao + "<br />")
				.send();

		FacesUtil.addInfoMessage(
				"Notificação de Registro de Apólice de Seguro de Bem Patrimonial enviado por e-mail com sucesso!");
	}

	public void enviarNotificaoEmailAnexoPDF() throws IOException {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String dataContratacao = df.format(this.apoliceSelecionada.getDataContratacao());

		MailMessage message = mailer.prepararNovaMensagem();

		message.to(this.apoliceSelecionada.getBemMovel().getNovoFuncionarioResponsavelPatrimonial().getEmail())
				.subject("SIGPAT: Notificação de Registro de Apólice de Seguro de Bem Patrimonial")
				.bodyHtml("<strong>Unidade Gestora: </strong>"
						+ this.apoliceSelecionada.getBemMovel().getLocalDestino().getUnidadeGestora().getNomeUnidadeGestora() + "("
						+ this.apoliceSelecionada.getBemMovel().getLocalDestino().getUnidadeGestora().getNomeResumido() + ")"
						+ "<br/>" + "<strong>Unidade Administrativa: </strong>"
						+ this.apoliceSelecionada.getBemMovel().getLocalDestino().getNomeUnidadeAdministrativa() + "("
						+ this.apoliceSelecionada.getBemMovel().getLocalDestino().getNomeResumido() + ")" + "<br/>"
						+ "<strong>Bem Patrimonial: </strong>" + this.apoliceSelecionada.getBemMovel().getNumeroPatrimonio()
						+ " - " + this.apoliceSelecionada.getBemMovel().getDescricao() + "<br />" + "<br />"
						+ "<strong>Funcionário Responsável pela Guarda do Bem: </strong>"
						+ this.apoliceSelecionada.getBemMovel().getNovoFuncionarioResponsavelPatrimonial().getNomeFuncionario()
						+ "<br />" + "<strong>Seguradora: </strong>"
						+ this.apoliceSelecionada.getSeguradora().getDescricaoSeguradora() + "<br />"
						+ "<strong>Número da Apólice: </strong>" + this.apoliceSelecionada.getNumeroApolice() + "<br />"
						+ "<strong>Data da Contratação: </strong>" + dataContratacao + "<br />")
				.addAttachment(new BaseAttachment(this.apoliceSelecionada.getApolicePdf(), "application/x-pdf",
						ContentDisposition.ATTACHMENT,
						Resources.toByteArray(Resources.getResource(
								"../../resources/enviados/bensmoveis/apolices/" + this.apoliceSelecionada.getApolicePdf()))))
				.send();

		FacesUtil.addInfoMessage(
				"Notificação de Registro de Apólice de Seguro de Bem Patrimonial enviado por e-mail com sucesso e anexo PDF!");
	}

	public void excluir() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.cadastro.excluir(this.apoliceSelecionada);
			this.consultar();

			context.addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_WARN, "Apólice de Seguro excluída com sucesso!", null));

		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public void consultar() {
		this.apolices = apolicesRepository.todas();
	}

	public List<Apolice> getApolices() {
		return apolices;
	}

	public List<Apolice> getConsultar() {
		return apolices;
	}

	public Apolice getApoliceSelecionada() {
		return apoliceSelecionada;
	}

	public void setApoliceSelecionada(Apolice apoliceSelecionada) {
		this.apoliceSelecionada = apoliceSelecionada;
	}
}
