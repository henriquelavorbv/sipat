
package com.henriquelavor.sigpat.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * @author Henrique Lavor
 * @data 25.09.2017
 * @version 1.0
 */
@Entity
@Table (name="tb_divergencia")
public class Divergencia implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long codigoDivergencia;
	private String descricaoDivergencia;
	
	@Id
	@GeneratedValue
	public Long getCodigoDivergencia() {
		return codigoDivergencia;
	}
	
	public void setCodigoDivergencia(Long codigoDivergencia) {
		this.codigoDivergencia = codigoDivergencia;
	}
	
	@NotNull
	@Size(max = 100)
	@Column(name = "descricao", length = 100, nullable = false)
	public String getDescricaoDivergencia() {
		return descricaoDivergencia;
	}
	
	public void setDescricaoDivergencia(String descricaoDivergencia) {
		this.descricaoDivergencia = descricaoDivergencia;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoDivergencia == null) ? 0 : codigoDivergencia.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Divergencia other = (Divergencia) obj;
		if (codigoDivergencia == null) {
			if (other.codigoDivergencia != null)
				return false;
		} else if (!codigoDivergencia.equals(other.codigoDivergencia))
			return false;
		return true;
	}
}
