
package com.henriquelavor.sigpat.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * @author Henrique Lavor
 * @data 17.05.2016
 * @version 1.0
 */
@Entity
@Table (name="tb_estado_bem")
public class EstadoBens implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long codigoEstadoBem;
	private String descricaoEstadoBem;
	
	@Id
	@GeneratedValue
	public Long getCodigoEstadoBem() {
		return codigoEstadoBem;
	}
	public void setCodigoEstadoBem(Long codigoEstadoBem) {
		this.codigoEstadoBem = codigoEstadoBem;
	}
	
	@NotNull
	@Size(max = 30)
	@Column(name = "descricao", length = 30, nullable = false)
	public String getDescricaoEstadoBem() {
		return descricaoEstadoBem;
	}
	public void setDescricaoEstadoBem(String descricaoEstadoBem) {
		this.descricaoEstadoBem = descricaoEstadoBem;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoEstadoBem == null) ? 0 : codigoEstadoBem.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EstadoBens other = (EstadoBens) obj;
		if (codigoEstadoBem == null) {
			if (other.codigoEstadoBem != null)
				return false;
		} else if (!codigoEstadoBem.equals(other.codigoEstadoBem))
			return false;
		return true;
	}
}
