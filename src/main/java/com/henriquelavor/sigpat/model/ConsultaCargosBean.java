
package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.repository.Cargos;
import com.henriquelavor.sigpat.service.CadastroCargos;
import com.henriquelavor.sigpat.service.NegocioException;

/**
 * @author Henrique Lavor
 * @data 19.05.2016
 * @version 1.0
 */

@Named
@ViewScoped
public class ConsultaCargosBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Cargos cargosRepository;
	
	@Inject
	private CadastroCargos cadastro;
	
	private List<Cargo> cargos;
	
	private Cargo cargoSelecionado;
	
	public void excluir() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.cadastro.excluir(this.cargoSelecionado);
			this.consultar();

			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
					"Cargo Funcional excluído com sucesso!",null));

		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	
	public void consultar() {
		this.cargos = cargosRepository.todos();
	}

	public List<Cargo> getCargos() {
		return cargos;
	}
	
	public List<Cargo> getConsultar() {
		return cargos;
	}

	public Cargo getCargoSelecionado() {
		return cargoSelecionado;
	}
	
	public void setCargoSelecionado(Cargo cargoSelecionado) {
		this.cargoSelecionado = cargoSelecionado;
	}
}