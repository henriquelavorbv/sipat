package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Immutable;

/**
 * @author Henrique Lavor
 * @data 16.07.2019
 * @version 1.0
 */
@Entity
@Immutable
@Table(name = "vw_bens_garantia")
public class BemMovelGarantiaView implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long codigoBemMovel;
	private String numeroPatrimonio;
	private String numeroTermoResponsabilidade;
	private String descricaoBem;
	private String fotoBemMovel;
	
	private String nomeFuncionario;
	private String emailFuncionario;
	
	private Date dataAquisicao;
	private Date dataLimiteGarantia;
	private Long diasGarantia;
	private String uaOrigem;
	private String uaDestino;
	private String ativo;

	@Id
	@Column(name = "codigobemmovel")
	public Long getCodigoBemMovel() {
		return codigoBemMovel;
	}

	public void setCodigoBemMovel(Long codigoBemMovel) {
		this.codigoBemMovel = codigoBemMovel;
	}

	@Column(name = "numero_patrimonio", length = 20)
	public String getNumeroPatrimonio() {
		return numeroPatrimonio;
	}

	public void setNumeroPatrimonio(String numeroPatrimonio) {
		this.numeroPatrimonio = numeroPatrimonio;
	}

	@Column(name = "numero_termo_responsabilidade", length = 20)
	public String getNumeroTermoResponsabilidade() {
		return numeroTermoResponsabilidade;
	}

	public void setNumeroTermoResponsabilidade(String numeroTermoResponsabilidade) {
		this.numeroTermoResponsabilidade = numeroTermoResponsabilidade;
	}

	@Column(name = "descricao_bem", length = 600)
	public String getDescricaoBem() {
		return descricaoBem;
	}

	public void setDescricaoBem(String descricaoBem) {
		this.descricaoBem = descricaoBem;
	}

	@Column(name = "foto_bem_movel", length = 200)
	public String getFotoBemMovel() {
		return fotoBemMovel;
	}

	public void setFotoBemMovel(String fotoBemMovel) {
		this.fotoBemMovel = fotoBemMovel;
	}

	@Temporal(TemporalType.DATE)
	@Column(name= "data_aquisicao")
	public Date getDataAquisicao() {
		return dataAquisicao;
	}

	public void setDataAquisicao(Date dataAquisicao) {
		this.dataAquisicao = dataAquisicao;
	}

	@Temporal(TemporalType.DATE)
	@Column(name= "data_limite_garantia")
	public Date getDataLimiteGarantia() {
		return dataLimiteGarantia;
	}

	public void setDataLimiteGarantia(Date dataLimiteGarantia) {
		this.dataLimiteGarantia = dataLimiteGarantia;
	}

	
	@Column(name= "dias_garantia")
	public Long getDiasGarantia() {
		return diasGarantia;
	}

	public void setDiasGarantia(Long diasGarantia) {
		this.diasGarantia = diasGarantia;
	}

	@Column(name= "ua_origem")
	public String getUaOrigem() {
		return uaOrigem;
	}

	public void setUaOrigem(String uaOrigem) {
		this.uaOrigem = uaOrigem;
	}

	@Column(name= "ua_destino")
	public String getUaDestino() {
		return uaDestino;
	}

	public void setUaDestino(String uaDestino) {
		this.uaDestino = uaDestino;
	}

	@Column(name= "ativo")
	public String getAtivo() {
		return ativo;
	}

	public void setAtivo(String ativo) {
		this.ativo = ativo;
	}

	@Column(name = "nome_funcionario", length = 150)
	public String getNomeFuncionario() {
		return nomeFuncionario;
	}

	public void setNomeFuncionario(String nomeFuncionario) {
		this.nomeFuncionario = nomeFuncionario;
	}

	@Column(name = "email", length = 150)
	public String getEmailFuncionario() {
		return emailFuncionario;
	}

	public void setEmailFuncionario(String emailFuncionario) {
		this.emailFuncionario = emailFuncionario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoBemMovel == null) ? 0 : codigoBemMovel.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BemMovelGarantiaView other = (BemMovelGarantiaView) obj;
		if (codigoBemMovel == null) {
			if (other.codigoBemMovel != null)
				return false;
		} else if (!codigoBemMovel.equals(other.codigoBemMovel))
			return false;
		return true;
	}

}
