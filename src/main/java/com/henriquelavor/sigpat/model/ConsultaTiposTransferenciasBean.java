
package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.repository.TiposTransferencias;
import com.henriquelavor.sigpat.service.CadastroTiposTransferencias;
import com.henriquelavor.sigpat.service.NegocioException;

/**
 * @author Henrique Lavor
 * @data 09.09.2016
 * @version 1.0
 */

@Named
@ViewScoped
public class ConsultaTiposTransferenciasBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private TiposTransferencias tiposTransferenciasRepository;

	@Inject
	private CadastroTiposTransferencias cadastro;

	private List<TipoTransferencia> tiposTransferencias;

	private TipoTransferencia tipoTransferenciaSelecionada;

	public void excluir() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.cadastro.excluir(this.tipoTransferenciaSelecionada);
			this.consultar();

			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
					"Tipo de Transferência de Bem excluído com sucesso!", null));

		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public void consultar() {
		this.tiposTransferencias = tiposTransferenciasRepository.todos();
	}

	public List<TipoTransferencia> getTiposTransferencias() {
		return tiposTransferencias;
	}

	public List<TipoTransferencia> getConsultar() {
		return tiposTransferencias;
	}

	public TipoTransferencia getTipoTransferenciaSelecionada() {
		return tipoTransferenciaSelecionada;
	}

	public void setTipoTransferenciaSelecionada(TipoTransferencia tipoTransferenciaSelecionada) {
		this.tipoTransferenciaSelecionada = tipoTransferenciaSelecionada;
	}
}