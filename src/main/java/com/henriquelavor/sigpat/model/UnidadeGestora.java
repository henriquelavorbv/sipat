package com.henriquelavor.sigpat.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Henrique Lavor
 * @data 23.05.2016
 * @data_Manutecao 02.06.2016
 * @version 1.0
 */
@Entity
@Table(name = "tb_unidade_gestora")
public class UnidadeGestora implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long codigoUnidadeGestora;
	private String codigoOrcamentario;
	private String nomeUnidadeGestora;
	private String nomeResumido;
	private TipoPoder tipoPoder;
	private TipoInstituicao tipoInstituicao;
	private String cnpj;

	private String cep;
	private String logradouro;
	private String distrito;
	private String municipio;
	private String uf;
	private String numero;
	private String complemento;

	private String latitude;
	private String longitude;
	private TipoSituacaoUnidadeGestora tipoSituacaoUnidadeGestora;

	@Id
	@GeneratedValue
	public Long getCodigoUnidadeGestora() {
		return codigoUnidadeGestora;
	}

	public void setCodigoUnidadeGestora(Long codigoUnidadeGestora) {
		this.codigoUnidadeGestora = codigoUnidadeGestora;
	}

	@NotNull
	@Size(max = 5)
	@Column(name = "codigo_orcamentario", length = 5, nullable = false)
	public String getCodigoOrcamentario() {
		return codigoOrcamentario;
	}

	public void setCodigoOrcamentario(String codigoOrcamentario) {
		this.codigoOrcamentario = codigoOrcamentario;
	}

	@NotNull
	@Size(max = 150)
	@Column(name = "nome_unidade_gestora", length = 150, nullable = false)
	public String getNomeUnidadeGestora() {
		return nomeUnidadeGestora;
	}

	public void setNomeUnidadeGestora(String nomeUnidadeGestora) {
		this.nomeUnidadeGestora = nomeUnidadeGestora;
	}

	@NotNull
	@Size(max = 50)
	@Column(name = "nome_resumido", length = 50, nullable = false)
	public String getNomeResumido() {
		return nomeResumido;
	}

	public void setNomeResumido(String nomeResumido) {
		this.nomeResumido = nomeResumido;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	public TipoPoder getTipoPoder() {
		return tipoPoder;
	}

	public void setTipoPoder(TipoPoder tipoPoder) {
		this.tipoPoder = tipoPoder;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_tipo_instituicao")
	public TipoInstituicao getTipoInstituicao() {
		return tipoInstituicao;
	}

	public void setTipoInstituicao(TipoInstituicao tipoInstituicao) {
		this.tipoInstituicao = tipoInstituicao;
	}

	@NotNull
	@Size(max = 18)
	@Column(name = "cnpj", length = 18, nullable = true)
	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	@NotNull
	@Size(max = 9)
	@Column(name = "cep", length = 9, nullable = true)
	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	@NotNull
	@Size(max = 100)
	@Column(name = "logradouro", length = 100, nullable = true)
	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	@NotNull
	@Size(max = 150)
	@Column(name = "distrito", length = 150, nullable = true)
	public String getDistrito() {
		return distrito;
	}

	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}

	@NotNull
	@Size(max = 10)
	@Column(name = "numero", length = 10, nullable = true)
	public String getNumero() {
		return numero;
	}

	@NotNull
	@Size(max = 100)
	@Column(name = "municipio", length = 100, nullable = true)
	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	@NotNull
	@Size(max = 2)
	@Column(name = "uf", length = 2, nullable = true)
	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	@NotNull
	@Size(max = 150)
	@Column(name = "complemento", length = 150, nullable = true)
	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	@NotNull
	@Size(max = 20)
	@Column(name = "latitude", length = 20, nullable = true)
	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	@NotNull
	@Size(max = 20)
	@Column(name = "longitude", length = 20, nullable = true)
	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	public TipoSituacaoUnidadeGestora getTipoSituacaoUnidadeGestora() {
		return tipoSituacaoUnidadeGestora;
	}

	public void setTipoSituacaoUnidadeGestora(TipoSituacaoUnidadeGestora tipoSituacaoUnidadeGestora) {
		this.tipoSituacaoUnidadeGestora = tipoSituacaoUnidadeGestora;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoUnidadeGestora == null) ? 0 : codigoUnidadeGestora.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UnidadeGestora other = (UnidadeGestora) obj;
		if (codigoUnidadeGestora == null) {
			if (other.codigoUnidadeGestora != null)
				return false;
		} else if (!codigoUnidadeGestora.equals(other.codigoUnidadeGestora))
			return false;
		return true;
	}
}
