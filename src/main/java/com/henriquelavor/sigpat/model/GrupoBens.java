
package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Henrique Lavor
 * @data 06.04.2016
 * @version 1.0
 */
@Entity
@Table (name="tb_grupo_bem")
public class GrupoBens implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long codigoGrupoBem;
	private String descricaoGrupoBem;
	private BigDecimal taxaDepreciacao;
	private String contaContabil;
	private Long vidaUtil;
	private BigDecimal valorResidual;

	@Id
	@GeneratedValue
	public Long getCodigoGrupoBem() {
		return codigoGrupoBem;
	}

	public void setCodigoGrupoBem(Long codigoGrupoBem) {
		this.codigoGrupoBem = codigoGrupoBem;
	}
	
	@NotNull
	@Size(max = 100)
	@Column(name = "descricao", length = 100, nullable = false)
	public String getDescricaoGrupoBem() {
		return descricaoGrupoBem;
	}

	public void setDescricaoGrupoBem(String descricaoGrupoBem) {
		this.descricaoGrupoBem = descricaoGrupoBem;
	}

	@Column(name = "taxa_depreciacao", precision = 12, scale = 4, nullable = false)
	public BigDecimal getTaxaDepreciacao() {
		return taxaDepreciacao;
	}

	
	public void setTaxaDepreciacao(BigDecimal taxaDepreciacao) {
		this.taxaDepreciacao = taxaDepreciacao;
	}

	@NotNull
	@Size(max = 12)
	@Column(name = "conta", length = 12, nullable = false)
	public String getContaContabil() {
		return contaContabil;
	}

	public void setContaContabil(String contaContabil) {
		this.contaContabil = contaContabil;
	}

	@NotNull
	@Column(name = "vida_util", nullable = false)
	public Long getVidaUtil() {
		return vidaUtil;
	}

	public void setVidaUtil(Long vidaUtil) {
		this.vidaUtil = vidaUtil;
	}

	@Column(name = "valor_residual", precision = 12, scale = 4, nullable = false)
	public BigDecimal getValorResidual() {
		return valorResidual;
	}

	public void setValorResidual(BigDecimal valorResidual) {
		this.valorResidual = valorResidual;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoGrupoBem == null) ? 0 : codigoGrupoBem.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GrupoBens other = (GrupoBens) obj;
		if (codigoGrupoBem == null) {
			if (other.codigoGrupoBem != null)
				return false;
		} else if (!codigoGrupoBem.equals(other.codigoGrupoBem))
			return false;
		return true;
	}
}
