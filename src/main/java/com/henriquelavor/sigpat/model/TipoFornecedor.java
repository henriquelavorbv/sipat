
package com.henriquelavor.sigpat.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * @author Henrique Lavor
 * @data 27.05.2016
 * @version 1.0
 */
@Entity
@Table (name="tb_tipo_fornecedor")
public class TipoFornecedor implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long codigoTipoFornecedor;
	private String descricaoTipoFornecedor;
	
	@Id
	@GeneratedValue
	public Long getCodigoTipoFornecedor() {
		return codigoTipoFornecedor;
	}
	
	public void setCodigoTipoFornecedor(Long codigoTipoFornecedor) {
		this.codigoTipoFornecedor = codigoTipoFornecedor;
	}
	
	@NotNull
	@Size(max = 50)
	@Column(name = "descricao", length = 50, nullable = false)
	public String getDescricaoTipoFornecedor() {
		return descricaoTipoFornecedor;
	}
	
	public void setDescricaoTipoFornecedor(String descricaoTipoFornecedor) {
		this.descricaoTipoFornecedor = descricaoTipoFornecedor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoTipoFornecedor == null) ? 0 : codigoTipoFornecedor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoFornecedor other = (TipoFornecedor) obj;
		if (codigoTipoFornecedor == null) {
			if (other.codigoTipoFornecedor != null)
				return false;
		} else if (!codigoTipoFornecedor.equals(other.codigoTipoFornecedor))
			return false;
		return true;
	}
}
