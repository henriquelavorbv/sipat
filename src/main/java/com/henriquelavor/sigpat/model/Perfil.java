
package com.henriquelavor.sigpat.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * @author Henrique Lavor
 * @data 01.06.2019
 * @version 1.0
 */
@Entity
@Table (name="tb_perfil")
public class Perfil implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long codigoPerfil;
	private String descricaoPerfil;
	private String nomePerfil;
	
	@Id
	@GeneratedValue
	@Column(name = "codigo_perfil")
	public Long getCodigoPerfil() {
		return codigoPerfil;
	}
	
	public void setCodigoPerfil(Long codigoPerfil) {
		this.codigoPerfil = codigoPerfil;
	}
	
	@NotNull
	@Size(max = 100)
	@Column(name = "descricao_perfil", length = 100, nullable = false)
	public String getDescricaoPerfil() {
		return descricaoPerfil;
	}
	public void setDescricaoPerfil(String descricaoPerfil) {
		this.descricaoPerfil = descricaoPerfil;
	}
	
	
	@NotNull
	@Size(max = 100)
	@Column(name = "nome_perfil", length = 100, nullable = false)
	public String getNomePerfil() {
		return nomePerfil;
	}

	public void setNomePerfil(String nomePerfil) {
		this.nomePerfil = nomePerfil;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoPerfil == null) ? 0 : codigoPerfil.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Perfil other = (Perfil) obj;
		if (codigoPerfil == null) {
			if (other.codigoPerfil != null)
				return false;
		} else if (!codigoPerfil.equals(other.codigoPerfil))
			return false;
		return true;
	}
	
}
