
package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.repository.TiposBens;
import com.henriquelavor.sigpat.service.CadastroTiposBens;
import com.henriquelavor.sigpat.service.NegocioException;

/**
 * @author Henrique Lavor
 * @data 28.04.2016
 * @version 1.0
 */

@Named
@ViewScoped
public class ConsultaTiposBensBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private TiposBens tiposBensRepository;
	
	@Inject
	private CadastroTiposBens cadastro;
	
	private List<TipoBens> tiposBens;
	
	private TipoBens tipoBensSelecionado;
	
	public void excluir() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.cadastro.excluir(this.tipoBensSelecionado);
			this.consultar();

			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
					"Tipo de Bem Patrimonial excluído com sucesso!",null));

		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	
	public void consultar() {
		this.tiposBens = tiposBensRepository.todos();
	}

	public List<TipoBens> getTiposBens() {
		return tiposBens;
	}
	
	public List<TipoBens> getConsultar() {
		return tiposBens;
	}

	public TipoBens getTipoBensSelecionado() {
		return tipoBensSelecionado;
	}
	
	public void setTipoBensSelecionado(TipoBens tipoBensSelecionado) {
		this.tipoBensSelecionado = tipoBensSelecionado;
	}
}
