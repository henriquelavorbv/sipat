package com.henriquelavor.sigpat.model;

//Enumerador usado na Vistoria do bem
public enum SituacaoUsoBem {
	Alocado, Cedido, Ocioso
}
