package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author Henrique Lavor
 * @data 06.02.2018
 * @version 1.0
 */
@Entity
@Table(name = "tb_depreciacao")
public class Depreciacao implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long codigoDepreciacao;
    private Date dataDepreciacao;
    private BemMovel bemMovel; //codigo do bem depreciado
   // private String numeroPatrimonio = "";
    private BigDecimal valorDepreciado; //valor corrente da depreciacao
    private int qtdeDepreciacoesExecutada;
    private int qtdeDepreciacoesPrevista;
    private BigDecimal valorResidual;
    private BigDecimal valorDepreciacaoAcumulada = BigDecimal.ZERO;//= new BigDecimal(0.0000); //soma de todas as depreciacoes do bem
    private BigDecimal saldoBem;
	
    @Id
	@GeneratedValue
	@Column(name = "cod_depreciacao")
	public Long getCodigoDepreciacao() {
		return codigoDepreciacao;
	}

	public void setCodigoDepreciacao(Long codigoDepreciacao) {
		this.codigoDepreciacao = codigoDepreciacao;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "data_depreciacao", nullable = false)
	public Date getDataDepreciacao() {
		return dataDepreciacao;
	}

	public void setDataDepreciacao(Date dataDepreciacao) {
		this.dataDepreciacao = dataDepreciacao;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_bens")
	public BemMovel getBemMovel() {
		return bemMovel;
	}

	public void setBemMovel(BemMovel bemMovel) {
		this.bemMovel = bemMovel;
	}
	
	/*
	@NotNull
	@Size(max = 20)
	@Column(name = "numero_patrimonio", length = 20, nullable = false)
	public String getNumeroPatrimonio() {
		return numeroPatrimonio;
	}
	
	
	public void setNumeroPatrimonio(String numeroPatrimonio) {
		this.numeroPatrimonio = numeroPatrimonio;
	}
	 */
	
	@Column(name= "valor_depreciado", precision = 12, scale = 4, nullable = false)
	public BigDecimal getValorDepreciado() {
		return valorDepreciado;
	}

	public void setValorDepreciado(BigDecimal valorDepreciado) {
		this.valorDepreciado = valorDepreciado;
	}
	
	@Column(name = "qtde_depreciacoes_executada", nullable = false)
	public int getQtdeDepreciacoesExecutada() {
		return qtdeDepreciacoesExecutada;
	}

	public void setQtdeDepreciacoesExecutada(int qtdeDepreciacoesExecutada) {
		this.qtdeDepreciacoesExecutada = qtdeDepreciacoesExecutada;
	}

	@Column(name = "qtde_depreciacoes_prevista", nullable = false)
	public int getQtdeDepreciacoesPrevista() {
		return qtdeDepreciacoesPrevista;
	}

	public void setQtdeDepreciacoesPrevista(int qtdeDepreciacoesPrevista) {
		this.qtdeDepreciacoesPrevista = qtdeDepreciacoesPrevista;
	}

	@Column(name= "valor_residual", precision = 12, scale = 4, nullable = false)
	public BigDecimal getValorResidual() {
		return valorResidual;
	}

	public void setValorResidual(BigDecimal valorResidual) {
		this.valorResidual = valorResidual;
	}

	@Column(name= "valor_depreciacao_acumulada", precision = 12, scale = 4, nullable = false)
	public BigDecimal getValorDepreciacaoAcumulada() {
		return valorDepreciacaoAcumulada;
	}

	public void setValorDepreciacaoAcumulada(BigDecimal valorDepreciacaoAcumulada) {
		this.valorDepreciacaoAcumulada = valorDepreciacaoAcumulada;
	}

	@Column(name= "saldo_bem", precision = 12, scale = 4, nullable = false)
	public BigDecimal getSaldoBem() {
		return saldoBem;
	}

	public void setSaldoBem(BigDecimal saldoBem) {
		this.saldoBem = saldoBem;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoDepreciacao == null) ? 0 : codigoDepreciacao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Depreciacao other = (Depreciacao) obj;
		if (codigoDepreciacao == null) {
			if (other.codigoDepreciacao != null)
				return false;
		} else if (!codigoDepreciacao.equals(other.codigoDepreciacao))
			return false;
		return true;
	}
}
