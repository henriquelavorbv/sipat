package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;


/**
 * @author Henrique Lavor
 * @data 31.08.2017
 * @version 1.0
 */
@Entity
@Table(name = "tb_reparo")
public class Reparo implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long codigoReparo; //ok
	private BemMovel bemMovel; //ok
	private Fornecedor fornecedorReparo; //ok
	private Date dataSaida; //ok
	private String descricaoDefeito; //ok
	private Date dataRetorno; //ok
	private String descricaoReparo; //ok
	private String ordemServicoRetornoPdf; //ok
	private BigDecimal valorReparo; //ok
	private Long diasDeGarantia;  //ok
	private Boolean notificarEmail; //ok
	
	@Id
	@GeneratedValue
	public Long getCodigoReparo() {
		return codigoReparo;
	}

	public void setCodigoReparo(Long codigoReparo) {
		this.codigoReparo = codigoReparo;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_bem")
	public BemMovel getBemMovel() {
		return bemMovel;
	}

	public void setBemMovel(BemMovel bemMovel) {
		this.bemMovel = bemMovel;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_fornecedor_reparo")
	public Fornecedor getFornecedorReparo() {
		return fornecedorReparo;
	}

	public void setFornecedorReparo(Fornecedor fornecedorReparo) {
		this.fornecedorReparo = fornecedorReparo;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "data_saida", nullable = true)
	public Date getDataSaida() {
		return dataSaida;
	}

	public void setDataSaida(Date dataSaida) {
		this.dataSaida = dataSaida;
	}

	@NotNull
	@Size(max = 700)
	@Column(name = "descricao_defeito", length = 700, nullable = true)
	public String getDescricaoDefeito() {
		return descricaoDefeito;
	}

	public void setDescricaoDefeito(String descricaoDefeito) {
		this.descricaoDefeito = descricaoDefeito;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "data_retorno", nullable = true)
	public Date getDataRetorno() {
		return dataRetorno;
	}

	public void setDataRetorno(Date dataRetorno) {
		this.dataRetorno = dataRetorno;
	}

	@Size(max = 700)
	@Column(name = "descricao_reparo", length = 700, nullable = true)
	public String getDescricaoReparo() {
		return descricaoReparo;
	}
	
	
	@Size(max = 200)
	@Column(name = "ordem_servico_retorno_pdf", length = 200, nullable = true)
	public String getOrdemServicoRetornoPdf() {
		return ordemServicoRetornoPdf;
	}

	public void setOrdemServicoRetornoPdf(String ordemServicoRetornoPdf) {
		this.ordemServicoRetornoPdf = ordemServicoRetornoPdf;
	}

	public void setDescricaoReparo(String descricaoReparo) {
		this.descricaoReparo = descricaoReparo;
	}

	@Column(name = "valor_reparo", precision = 12, scale = 2, nullable = true)
	public BigDecimal getValorReparo() {
		return valorReparo;
	}

	public void setValorReparo(BigDecimal valorReparo) {
		this.valorReparo = valorReparo;
	}

	@Column(name = "dias_garantia_reparo", nullable = true)
	public Long getDiasDeGarantia() {
		return diasDeGarantia;
	}

	public void setDiasDeGarantia(Long diasDeGarantia) {
		this.diasDeGarantia = diasDeGarantia;
	}
	
	@Type(type = "true_false")
	@Column(name = "notificar_email")
	public Boolean getNotificarEmail() {
		return notificarEmail;
	}

	public void setNotificarEmail(Boolean notificarEmail) {
		this.notificarEmail = notificarEmail;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoReparo == null) ? 0 : codigoReparo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Reparo other = (Reparo) obj;
		if (codigoReparo == null) {
			if (other.codigoReparo != null)
				return false;
		} else if (!codigoReparo.equals(other.codigoReparo))
			return false;
		return true;
	}
}
