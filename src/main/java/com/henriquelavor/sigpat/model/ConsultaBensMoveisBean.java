
package com.henriquelavor.sigpat.model;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.henriquelavor.sigpat.repository.BensMoveis;
import com.henriquelavor.sigpat.security.Seguranca;
import com.henriquelavor.sigpat.service.CadastroBensMoveis;
import com.henriquelavor.sigpat.service.NegocioException;

/**
 * @author Henrique Lavor
 * @data 01.10.2016
 * @version 1.0
 */

@Named
@ViewScoped
public class ConsultaBensMoveisBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private BensMoveis bensMoveisRepository;

	@Inject
	private CadastroBensMoveis cadastro;

	// Todos os bens móveis cadastrados
	private List<BemMovel> bensMoveis;

	//// Todos os bens móveis não incorporados
	private List<BemMovel> bensMoveisNaoIncorporados;

	// Todos os bens móveis que saiu da entidade para reparo
	private List<BemMovel> bensMoveisEmReparos; // 0.0

	// Todos os bens móveis incorporados
	private List<BemMovel> bensMoveisIncorporados; // 0

	// Todos os bens móveis incorporados
	private List<BemMovel> bensMoveisMesAtual; // 0

	private BemMovel bemMovelSelecionado;

	public void excluir() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.cadastro.excluir(this.bemMovelSelecionado);
			this.getConsultar();
			//this.consultar();
			context.addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_WARN, "Bem Móvel excluído com sucesso!", null));

		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	// todos os bens cadastrados
	public void consultar() throws IOException {
		this.bensMoveis = bensMoveisRepository.todos();
	}
	
	// todos os bens cadastrados de todas as Unidades Gestoras
	public void consultarGeral() throws IOException {
		this.bensMoveis = bensMoveisRepository.todosGeral();
	}


	// todos os bens cadastrados e todas as transferencias de bens
	// public void consultarBensETrasnsferencias() {
	// this.bensMoveis = bensMoveisRepository.todos();
	// }

	// todos os bens incluidos no mes atual
	public void consultarBensMesAtual() {
		this.bensMoveisMesAtual = bensMoveisRepository.todosBensIncluidosMesAtual(); // 1
	}

	// todos os bens incorporados
	public void consultarIncorporados() {
		this.bensMoveisIncorporados = bensMoveisRepository.todosIncorporados(); // 1
	}

	// todos os bens que nao estao na entidade e estão em reparo
	public void consultarNaoIncorporados() {
		this.bensMoveisNaoIncorporados = bensMoveisRepository.todosNaoIncorporados();
	}

	// todos os bens que nao estao na entidade e estão em reparo
	public void consultarTodosEmReparos() {
		this.bensMoveisEmReparos = bensMoveisRepository.todosEmReparos(); // 0.1
	}

	// Todos os bens móveis cadastrados
	public List<BemMovel> getBensMoveis() {
		return bensMoveis;
	}

	// Todos os bens móveis incorporados
	public List<BemMovel> getBensMoveisMesAtual() {
		return bensMoveisMesAtual;
	}
	
	public List<BemMovel> getConsultarBensMesAtual() {
		return bensMoveisMesAtual;
	}

	// Todos os bens móveis Não incorporados
	public List<BemMovel> getBensMoveisNaoIncorporados() {
		return bensMoveisNaoIncorporados;
	}

	// Todos os bens móveis incorporados
	public List<BemMovel> getBensMoveisIncorporados() { // 2
		return bensMoveisIncorporados;
	}

	// Todos os bens móveis saiu da entidade para reparo
	public List<BemMovel> getBensMoveisEmReparos() {
		return bensMoveisEmReparos;
	}

	public List<BemMovel> getConsultar() {
		return bensMoveis;
	}
	
	public List<BemMovel> getConsultarGeral() {
		return bensMoveis;
	}

	public List<BemMovel> getConsultarNaoIncorporados() {
		return bensMoveisNaoIncorporados;
	}

	public List<BemMovel> getConsultarIncorporados() { // 3
		return bensMoveisIncorporados;
	}

	public List<BemMovel> getConsultarTodosEmReparos() {
		return bensMoveisEmReparos;
	}

	public BemMovel getBemMovelSelecionado() {
		return bemMovelSelecionado;
	}

	public void setBemMovelSelecionado(BemMovel bemMovelSelecionado) {
		this.bemMovelSelecionado = bemMovelSelecionado;
	}
	
}
