
package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.repository.UnidadesAdministrativas;
import com.henriquelavor.sigpat.service.CadastroUnidadesAdministrativas;
import com.henriquelavor.sigpat.service.NegocioException;

/**
 * @author Henrique Lavor
 * @data 25.04.2016
 * @version 1.0
 */

@Named
@ViewScoped
public class ConsultaUnidadesAdministrativasBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private UnidadesAdministrativas unidadesAdministrativasRepository;

	@Inject
	private CadastroUnidadesAdministrativas cadastro;

	private List<UnidadeAdministrativa> unidadesAdministrativas;

	private UnidadeAdministrativa unidadeAdministrativaSelecionada;

	public void excluir() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.cadastro.excluir(this.unidadeAdministrativaSelecionada);
			this.consultar();

			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,"Unidade Administrativa excluída com sucesso!",null));

		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	public void consultar() {
		this.unidadesAdministrativas = unidadesAdministrativasRepository.todas();
	}

	public List<UnidadeAdministrativa> getUnidadesAdministrativas() {
		return unidadesAdministrativas;
	}

	public List<UnidadeAdministrativa> getConsultar() {
		return unidadesAdministrativas;
	}
	
	public UnidadeAdministrativa getUnidadeAdministrativaSelecionada() {
		return unidadeAdministrativaSelecionada;
	}
	
	public void setUnidadeAdministrativaSelecionada(UnidadeAdministrativa unidadeAdministrativaSelecionada) {
		this.unidadeAdministrativaSelecionada = unidadeAdministrativaSelecionada;
	}
}
