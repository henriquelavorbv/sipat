
package com.henriquelavor.sigpat.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * @author Henrique Lavor
 * @data 23.05.2016
 * @version 1.0
 */
@Entity
@Table (name="tb_tipo_instituicao")
public class TipoInstituicao implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long codigoTipoInstituicao;
	private String descricaoTipoInstituicao;
	
	@Id
	@GeneratedValue
	public Long getCodigoTipoInstituicao() {
		return codigoTipoInstituicao;
	}
	public void setCodigoTipoInstituicao(Long codigoTipoInstituicao) {
		this.codigoTipoInstituicao = codigoTipoInstituicao;
	}
	
	@NotNull
	@Size(max = 100)
	@Column(name = "descricao", length = 100, nullable = false)
	public String getDescricaoTipoInstituicao() {
		return descricaoTipoInstituicao;
	}
	public void setDescricaoTipoInstituicao(String descricaoTipoInstituicao) {
		this.descricaoTipoInstituicao = descricaoTipoInstituicao;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoTipoInstituicao == null) ? 0 : codigoTipoInstituicao.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoInstituicao other = (TipoInstituicao) obj;
		if (codigoTipoInstituicao == null) {
			if (other.codigoTipoInstituicao != null)
				return false;
		} else if (!codigoTipoInstituicao.equals(other.codigoTipoInstituicao))
			return false;
		return true;
	}
}
