package com.henriquelavor.sigpat.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.henriquelavor.sigpat.validation.CorreioEletronico;

/**
 * @author Henrique Lavor
 * @data 08.06.2016
 * @data_manutenção 28.06.2016
 * @version 1.0
 */
@Entity
@Table(name = "tb_fornecedor")
public class Fornecedor implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long codigoFornecedor;
	private TipoFornecedor tipoForncedor;
	private String nomeFornecedor;
	private String cnpjCpf;
	private String descricaoFornecedor;
	private ReferenciaFornecedor referenciaFornecedor;

	private String cep;
	private String logradouro;
	private String distrito;
	private String municipio;
	private String uf;
	private String numero;
	private String complemento;
	private String latitude;
	private String longitude;

	private String nomeContato;
	private String telefone;
	private String fax;
	private String email;
	private String site;

	@Id
	@GeneratedValue
	public Long getCodigoFornecedor() {
		return codigoFornecedor;
	}

	public void setCodigoFornecedor(Long codigoFornecedor) {
		this.codigoFornecedor = codigoFornecedor;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_tipo_fornecedor")
	public TipoFornecedor getTipoForncedor() {
		return tipoForncedor;
	}

	public void setTipoForncedor(TipoFornecedor tipoForncedor) {
		this.tipoForncedor = tipoForncedor;
	}

	@NotNull
	@Size(max = 150)
	@Column(name = "nome_fornecedor", length = 150, nullable = true)
	public String getNomeFornecedor() {
		return nomeFornecedor;
	}

	public void setNomeFornecedor(String nomeFornecedor) {
		this.nomeFornecedor = nomeFornecedor;
	}

	@NotNull
	@Size(max = 20)
	@Column(name = "cnpj_cpf", length = 20, nullable = true)
	public String getCnpjCpf() {
		return cnpjCpf;
	}

	public void setCnpjCpf(String cnpjCpf) {
		this.cnpjCpf = cnpjCpf;
	}

	@NotNull
	@Size(max = 500)
	@Column(name = "descricao_fornecedor", length = 500, nullable = true)
	public String getDescricaoFornecedor() {
		return descricaoFornecedor;
	}

	public void setDescricaoFornecedor(String descricaoFornecedor) {
		this.descricaoFornecedor = descricaoFornecedor;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "cod_referencia_fornecedor")
	public ReferenciaFornecedor getReferenciaFornecedor() {
		return referenciaFornecedor;
	}

	public void setReferenciaFornecedor(ReferenciaFornecedor referenciaFornecedor) {
		this.referenciaFornecedor = referenciaFornecedor;
	}

	@NotNull
	@Size(max = 9)
	@Column(name = "cep", length = 9, nullable = true)
	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	@NotNull
	@Size(max = 100)
	@Column(name = "logradouro", length = 100, nullable = true)
	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	@NotNull
	@Size(max = 150)
	@Column(name = "distrito", length = 150, nullable = true)
	public String getDistrito() {
		return distrito;
	}

	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}

	@NotNull
	@Size(max = 10)
	@Column(name = "numero", length = 10, nullable = true)
	public String getNumero() {
		return numero;
	}


	@NotNull
	@Size(max = 100)
	@Column(name = "municipio", length = 100, nullable = true)
	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	@NotNull
	@Size(max = 2)
	@Column(name = "uf", length = 2, nullable = true)
	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	@NotNull
	@Size(max = 150)
	@Column(name = "complemento", length = 150, nullable = true)
	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	@NotNull
	@Size(max = 20)
	@Column(name = "latitude", length = 20, nullable = true)
	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	@NotNull
	@Size(max = 20)
	@Column(name = "longitude", length = 20, nullable = true)
	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	@NotNull
	@Size(max = 100)
	@Column(name = "nome_contato", length = 100, nullable = true)
	public String getNomeContato() {
		return nomeContato;
	}

	public void setNomeContato(String nomeContato) {
		this.nomeContato = nomeContato;
	}

	@NotNull
	@Size(max = 20)
	@Column(name = "telefone", length = 20, nullable = true)
	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	@NotNull
	@Size(max = 20)
	@Column(name = "fax", length = 20, nullable = true)
	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	@NotNull
	@Size(max = 150)
	@CorreioEletronico
	@Column(name = "email", length = 150, nullable = true)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@NotNull
	@Size(max = 150)
	@Column(name = "site", length = 150, nullable = true)
	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoFornecedor == null) ? 0 : codigoFornecedor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fornecedor other = (Fornecedor) obj;
		if (codigoFornecedor == null) {
			if (other.codigoFornecedor != null)
				return false;
		} else if (!codigoFornecedor.equals(other.codigoFornecedor))
			return false;
		return true;
	}
}
