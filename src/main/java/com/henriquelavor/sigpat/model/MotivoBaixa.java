
package com.henriquelavor.sigpat.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * @author Henrique Lavor
 * @data 08.08.2016
 * @version 1.0
 */
@Entity
@Table (name="tb_motivo_baixa")
public class MotivoBaixa implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long codigoMotivoBaixa;
	private String descricaoMotivoBaixa;
	
	@Id
	@GeneratedValue
	public Long getCodigoMotivoBaixa() {
		return codigoMotivoBaixa;
	}
	public void setCodigoMotivoBaixa(Long codigoMotivoBaixa) {
		this.codigoMotivoBaixa = codigoMotivoBaixa;
	}
	
	@NotNull
	@Size(max = 30)
	@Column(name = "descricao", length = 30, nullable = false)
	public String getDescricaoMotivoBaixa() {
		return descricaoMotivoBaixa;
	}
	public void setDescricaoMotivoBaixa(String descricaoMotivoBaixa) {
		this.descricaoMotivoBaixa = descricaoMotivoBaixa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoMotivoBaixa == null) ? 0 : codigoMotivoBaixa.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MotivoBaixa other = (MotivoBaixa) obj;
		if (codigoMotivoBaixa == null) {
			if (other.codigoMotivoBaixa != null)
				return false;
		} else if (!codigoMotivoBaixa.equals(other.codigoMotivoBaixa))
			return false;
		return true;
	}
}
