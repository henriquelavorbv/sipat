
package com.henriquelavor.sigpat.model;

import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.repository.Usuarios;
import com.henriquelavor.sigpat.service.CadastroUsuarios;
import com.henriquelavor.sigpat.service.NegocioException;
import com.henriquelavor.sigpat.util.jsf.FacesUtil;
import com.henriquelavor.sigpat.util.mail.Mailer;
import com.outjected.email.api.MailMessage;

/**
 * @author Henrique Lavor
 * @data 31.05.2019
 * @version 1.0
 */

@Named
@ViewScoped
public class ConsultaUsuariosBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Usuarios usuariosRepository;
	
	@Inject
	private CadastroUsuarios cadastro;
	
	private List<Usuario> usuarios;
	
	private Usuario usuarioSelecionado;
	
	@Inject
	private Mailer mailer;
	
	public void enviarNotificacaoEmail() throws IOException {
		//if (this.usuarioSelecionado.getNotificarEmail()) {
			this.enviarNotificaoEmail();
		//} 
	}
	
	
	public void enviarNotificaoEmail() throws IOException {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String dataCadastro = df.format(this.usuarioSelecionado.getDataCadastro());

		MailMessage message = mailer.prepararNovaMensagem();

		message.to(this.usuarioSelecionado.getEmail())
				.subject("SIGPAT: Notificação de Conta de Acesso ao Sistema Patrimonial")
				.bodyHtml("<strong>Unidade Gestora: </strong>"
						+ this.usuarioSelecionado.getFuncionario().getUnidadeAdministrativa().getUnidadeGestora().getNomeUnidadeGestora() + "("
						+ this.usuarioSelecionado.getFuncionario().getUnidadeAdministrativa().getUnidadeGestora().getNomeResumido() + ")"
						+ "<br/>" + "<strong>Unidade Administrativa: </strong>"
						+ this.usuarioSelecionado.getFuncionario().getUnidadeAdministrativa().getNomeUnidadeAdministrativa() + "("
						+ this.usuarioSelecionado.getFuncionario().getUnidadeAdministrativa().getNomeResumido() + ")" + "<br/>"
						+ "<br />" + "<br />"
						+ "<strong>Funcionário Cadastro para Acesso ao Sistema: </strong>"
						+ this.usuarioSelecionado.getFuncionario().getNomeFuncionario() + "<br />" + "<br />" 
						+ "<strong>DADOS DE ACESSO : </strong>"  + "<br />"
						+ "<strong>E-mail do usuário: </strong>" + this.usuarioSelecionado.getEmail() + "<br />"
						+ "<strong>Senha: </strong> <strong>" + this.usuarioSelecionado.getPasssword()+ "</strong> <br />"
						+ "<strong>Data da Cadastro: </strong>" + dataCadastro + "<br />"
						)
				.send();

		FacesUtil.addInfoMessage(
				"Notificação de Cadastro de Usuário para Acesso ao Sistema Patrimonial enviado por e-mail com sucesso!");
	}


	
	public void excluir() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.cadastro.excluir(this.usuarioSelecionado);
			this.consultar();

			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
					"Usuário excluído com sucesso!",null));

		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	
	public void consultar() {
		this.usuarios = usuariosRepository.todos();
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}
	
	public List<Usuario> getConsultar() {
		return usuarios;
	}

	public Usuario getUsuarioSelecionado() {
		return usuarioSelecionado;
	}
	
	public void setUsuarioSelecionado(Usuario usuarioSelecionado) {
		this.usuarioSelecionado = usuarioSelecionado;
	}
}