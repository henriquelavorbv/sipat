package com.henriquelavor.sigpat.model;

public enum TipoSituacaoFuncionario {
	Ativo, Inativo
}
