
package com.henriquelavor.sigpat.model;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.henriquelavor.sigpat.repository.GruposBens;
import com.henriquelavor.sigpat.service.CadastroGruposBens;
import com.henriquelavor.sigpat.service.NegocioException;

/**
 * @author Henrique Lavor
 * @data 13.04.2016
 * @version 1.0
 */

@Named
@ViewScoped
public class ConsultaGruposBensBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private GruposBens gruposBensRepository;
	
	@Inject
	private CadastroGruposBens cadastro;
	
	private List<GrupoBens> gruposBens;
	
	private GrupoBens grupoBensSelecionado;
	
	public void excluir() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.cadastro.excluir(this.grupoBensSelecionado);
			this.consultar();

			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
					"Grupo de Bem Patrimonial excluído com sucesso!",null));

		} catch (NegocioException e) {
			FacesMessage mensagem = new FacesMessage(e.getMessage());
			mensagem.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage(null, mensagem);
		}
	}

	
	public void consultar() {
		this.gruposBens = gruposBensRepository.todos();
	}

	public List<GrupoBens> getGruposBens() {
		return gruposBens;
	}
	
	public List<GrupoBens> getConsultar() {
		return gruposBens;
	}

	public GrupoBens getGrupoBensSelecionado() {
		return grupoBensSelecionado;
	}
	
	public void setGrupoBensSelecionado(GrupoBens grupoBensSelecionado) {
		this.grupoBensSelecionado = grupoBensSelecionado;
	}
	
	
	
}
