package com.henriquelavor.sigpat.util.report;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
    public static Connection getConnection() throws SQLException{
        try {
            Class.forName("org.postgresql.Driver");
            //Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/sigpat", "postgres", "postgres");
            Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/sigpat", "postgres", "@@lavor@@2019");
            return con;
        } catch (Exception ex) {
            System.out.println("Database.getConnection() Error -->" + ex.getMessage());
            return null;
        }
    }
 
    public static void close(Connection con) {
        try {
            con.close();
        } catch (Exception ex) {
        }
    }
}

