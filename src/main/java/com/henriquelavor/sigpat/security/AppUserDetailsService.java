package com.henriquelavor.sigpat.security;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.henriquelavor.sigpat.model.Usuario;
import com.henriquelavor.sigpat.repository.Usuarios;
import com.henriquelavor.sigpat.util.cdi.CDIServiceLocator;

public class AppUserDetailsService implements UserDetailsService {

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Usuarios usuarios = CDIServiceLocator.getBean(Usuarios.class);
		Usuario usuario = usuarios.porEmail(email);
		
		UsuarioSistema user = null;
		
		if (usuario != null) {
			user = new UsuarioSistema(usuario, getPerfis(usuario));
		} else {
			throw new UsernameNotFoundException("Usuário não encontrado.");
		}
		
		return user;
	}
	
	
	private Collection<? extends GrantedAuthority> getPerfis(Usuario usuario) {
		List<SimpleGrantedAuthority> authorities = new ArrayList<	>();
		
		authorities.add(new SimpleGrantedAuthority("ROLE_" + usuario.getPerfil().getNomePerfil().toUpperCase()));
		
		return authorities;
	}

	/*
	private Collection<? extends GrantedAuthority> getPerfis(Usuario usuario) {
		List<SimpleGrantedAuthority> authorities = new ArrayList<>();
		
		for (Perfil perfil : usuario.getPerfis()) {
			authorities.add(new SimpleGrantedAuthority("ROLE_" + perfil.getNomePerfil().toUpperCase()));
		}
		
		return authorities;
	}
	*/

}