package com.henriquelavor.sigpat.security;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

@Named
@RequestScoped
public class Seguranca {
	
	//private String codigoUO;

	public String getMatriculaFuncionario() {
		String nome = null;
		
		UsuarioSistema usuarioLogado = getUsuarioLogado();
		
		if (usuarioLogado != null) {
			nome = usuarioLogado.getUsuario().getFuncionario().getMatricula();
		}
		
		return nome;
	}

	public String getNomeUsuario() {
		String nome = null;
		
		UsuarioSistema usuarioLogado = getUsuarioLogado();
		
		if (usuarioLogado != null) {
			nome = usuarioLogado.getUsuario().getFuncionario().getNomeFuncionario();
		}
		
		return nome;
	}
	
	
	public String getFotoUsuario() {
		String foto = null;
		
		UsuarioSistema usuarioLogado = getUsuarioLogado();
		
		if (usuarioLogado != null) {
			foto = usuarioLogado.getUsuario().getFuncionario().getFoto();
		}
		
		return foto;
	}
	
	public String getCodigoUO() {
		FacesContext fc = FacesContext.getCurrentInstance(); //setando SESSION
		HttpSession session = (HttpSession) fc.getExternalContext().getSession(false); //setando SESSION
		
		
		String uo = null;
		
		UsuarioSistema usuarioLogado = getUsuarioLogado();
		
		if (usuarioLogado != null) {
			uo = usuarioLogado.getUsuario().getFuncionario().getUnidadeAdministrativa().getUnidadeGestora().getCodigoOrcamentario();
			session.setAttribute("COD_ORCAMENTARIA", uo); //setando SESSION
		}
		return uo;
	}
	
	
	public Long getCodigoIDUnidadeGestora() {
		FacesContext fc = FacesContext.getCurrentInstance(); //setando SESSION
		HttpSession session = (HttpSession) fc.getExternalContext().getSession(false); //setando SESSION
		
		Long idUnidadeGestora = null;
		
		UsuarioSistema usuarioLogado = getUsuarioLogado();
		
		if (usuarioLogado != null) {
			idUnidadeGestora = usuarioLogado.getUsuario().getFuncionario().getUnidadeAdministrativa().getUnidadeGestora().getCodigoUnidadeGestora();
			session.setAttribute("ID_GESTORA", idUnidadeGestora); //setando SESSION da Chave Primaria da tabela tb_unidade_gestora
		}
		return idUnidadeGestora;
	}
	
	public Long getCodigoUnidadeAdministrativa() {
		FacesContext fc = FacesContext.getCurrentInstance(); //setando SESSION
		HttpSession session = (HttpSession) fc.getExternalContext().getSession(false); //setando SESSION
		
		Long idUnidadeAdministrativa = null;
		
		UsuarioSistema usuarioLogado = getUsuarioLogado();
		
		if (usuarioLogado != null) {
			idUnidadeAdministrativa = usuarioLogado.getUsuario().getFuncionario().getUnidadeAdministrativa().getCodigoUnidadeAdministrativa();
			session.setAttribute("ID_UA", idUnidadeAdministrativa); //setando SESSION da Chave Primaria da tabela tb_unidade_administrativa
		}
		return idUnidadeAdministrativa;
	}
	
	
	//public void setCodigoUO(String codigoUO) {
	//	this.codigoUO= codigoUO;
	//}
	
	
	public String getUnidadeGestora() {
		String nomeUnidadeGestora = null;
		
		UsuarioSistema usuarioLogado = getUsuarioLogado();
		
		if (usuarioLogado != null) {
			nomeUnidadeGestora = usuarioLogado.getUsuario().getFuncionario().getUnidadeAdministrativa().getUnidadeGestora().getNomeUnidadeGestora();
		}
		return nomeUnidadeGestora;
	}
	
	
	
	
	public String getUAFuncionario() {
		String ua = null;
		
		UsuarioSistema usuarioLogado = getUsuarioLogado();
		
		if (usuarioLogado != null) {
			ua = usuarioLogado.getUsuario().getFuncionario().getUnidadeAdministrativa().getNomeUnidadeAdministrativa() + " ("+ 
	usuarioLogado.getUsuario().getFuncionario().getUnidadeAdministrativa().getNomeResumido() + "/" + 
					usuarioLogado.getUsuario().getFuncionario().getUnidadeAdministrativa().getUnidadeGestora().getNomeResumido() +")";
		}
		return ua;
	}
	
	public String getCargoFuncionario() {
		String cargo = null;
		
		UsuarioSistema usuarioLogado = getUsuarioLogado();
		
		if (usuarioLogado != null) {
			cargo = usuarioLogado.getUsuario().getFuncionario().getCargo().getDescricaoCargo();
		}
		return cargo;
	}
	
	public String getResponsavelPatrimonial() {
		String responsavelPatrimonial = null;
		
		UsuarioSistema usuarioLogado = getUsuarioLogado();
		
		if (usuarioLogado != null) {
			responsavelPatrimonial = usuarioLogado.getUsuario().getFuncionario().getResponsavelPatrimonial().toString();
		}
		return responsavelPatrimonial;
	}
	
	public String getResponsavelInventario() {
		String responsavelInventario = null;
		
		UsuarioSistema usuarioLogado = getUsuarioLogado();
		
		if (usuarioLogado != null) {
			responsavelInventario = usuarioLogado.getUsuario().getFuncionario().getResponsavelInventario().toString();
		}
		return responsavelInventario;
	}
	
	public String getAutoridades() {
		String autoridades = null;
		
		UsuarioSistema usuarioLogado = getUsuarioLogado();
		
		if (usuarioLogado != null) {
			autoridades = usuarioLogado.getAuthorities().toString();
		}
		return autoridades;
	}
	
	@Produces
	@UsuarioLogado
	private UsuarioSistema getUsuarioLogado() {
		
		UsuarioSistema usuario = null;
		
		UsernamePasswordAuthenticationToken auth = (UsernamePasswordAuthenticationToken) 
		FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal(); //usuario que esta logado
		
		if (auth != null && auth.getPrincipal() != null) {
			usuario = (UsuarioSistema) auth.getPrincipal();
		}
		
		return usuario;
	}
	
}
