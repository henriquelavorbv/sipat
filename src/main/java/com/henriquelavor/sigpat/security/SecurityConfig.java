package com.henriquelavor.sigpat.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Bean
	public AppUserDetailsService userDetailsService() {
		return new AppUserDetailsService();
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		JsfLoginUrlAuthenticationEntryPoint jsfLoginEntry = new JsfLoginUrlAuthenticationEntryPoint();
		jsfLoginEntry.setLoginFormUrl("/login.xhtml");
		jsfLoginEntry.setRedirectStrategy(new JsfRedirectStrategy());
		
		JsfAccessDeniedHandler jsfDeniedEntry = new JsfAccessDeniedHandler();
		jsfDeniedEntry.setLoginPath("/access.xhtml");
		jsfDeniedEntry.setContextRelative(true);
		
		http
			.csrf().disable()
			.headers().frameOptions().sameOrigin()
			.and()
			
		.authorizeRequests()
			.antMatchers("/login.xhtml", "/error.xhtml", "/javax.faces.resource/**").permitAll()
			.antMatchers("/dashboard.xhtml", "/access.xhtml").authenticated()
			.antMatchers("/paginas/Apolice/Cadastro*").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES")
			.antMatchers("/paginas/Apolice/Consulta*").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES", "AUDITORES")

			.antMatchers("/paginas/Baixa/CadastroBaixa.xhtml").hasAnyRole("ADMINISTRADORES", "GESTORES")
			.antMatchers("/paginas/Baixa/CadastroBaixaEdit.xhtml").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES","AUDITORES")
			.antMatchers("/paginas/Baixa/Consulta*").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES","AUDITORES")
			
			.antMatchers("/paginas/BemMovel/CadastroBemMovel.xhtml").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES")
			.antMatchers("/paginas/BemMovel/CadastroBemMovelEdit*").hasAnyRole("ADMINISTRADORES", "GESTORES")
			.antMatchers("/paginas/BemMovel/CadastroBemMovelTransferencias*").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES","AUDITORES")
			.antMatchers("/paginas/BemMovel/Consulta*").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES","AUDITORES")
			
			.antMatchers("/paginas/Cargo/Cadastro*").hasAnyRole("ADMINISTRADORES","GESTORES")
			.antMatchers("/paginas/Cargo/Consulta*").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES","AUDITORES")
		
			.antMatchers("/paginas/ClassificacaoInventario/Cadastro*").hasAnyRole("ADMINISTRADORES")
			.antMatchers("/paginas/ClassificacaoInventario/Consulta*").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES","AUDITORES")
		
			.antMatchers("/paginas/Divergencia/Cadastro*").hasAnyRole("ADMINISTRADORES")
			.antMatchers("/paginas/Divergencia/Consulta*").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES","AUDITORES")
		
			.antMatchers("/paginas/EstadoBem/Cadastro*").hasAnyRole("ADMINISTRADORES")
			.antMatchers("/paginas/EstadoBem/Consulta*").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES","AUDITORES")
		
			.antMatchers("/paginas/FonteRecurso/Cadastro*").hasAnyRole("ADMINISTRADORES")
			.antMatchers("/paginas/FonteRecurso/Consulta*").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES","AUDITORES")
		
			.antMatchers("/paginas/Fornecedor/Cadastro*").hasAnyRole("ADMINISTRADORES", "GESTORES")
			.antMatchers("/paginas/Fornecedor/Consulta*").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES","AUDITORES")
		
			.antMatchers("/paginas/Funcionario/Cadastro*").hasAnyRole("ADMINISTRADORES", "GESTORES")
			.antMatchers("/paginas/Funcionario/Consulta*").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES","AUDITORES")
		
			.antMatchers("/paginas/GrupoBem/Cadastro*").hasAnyRole("ADMINISTRADORES")
			.antMatchers("/paginas/GrupoBem/Consulta*").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES","AUDITORES")
			
			.antMatchers("/paginas/Inventario/Cadastro*").hasAnyRole("ADMINISTRADORES","GESTORES")
			.antMatchers("/paginas/Inventario/Consulta*").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES","AUDITORES")
		
			.antMatchers("/paginas/MarcaBem/Cadastro*").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES")
			.antMatchers("/paginas/MarcaBem/Consulta*").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES","AUDITORES")
			
			.antMatchers("/paginas/ModalidadeAquisicaoBem/Cadastro*").hasAnyRole("ADMINISTRADORES")
			.antMatchers("/paginas/ModalidadeAquisicaoBem/Consulta*").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES","AUDITORES")
			
			.antMatchers("/paginas/MotivoBaixa/Cadastro*").hasAnyRole("ADMINISTRADORES")
			.antMatchers("/paginas/MotivoBaixa/Consulta*").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES","AUDITORES")
			
			.antMatchers("/paginas/ReferenciaFornecedor/Cadastro*").hasAnyRole("ADMINISTRADORES")
			.antMatchers("/paginas/ReferenciaFornecedor/Consulta*").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES","AUDITORES")
			
			.antMatchers("/paginas/Reparo/CadastroReparo.xhtml").hasAnyRole("ADMINISTRADORES","GESTORES")
			.antMatchers("/paginas/Reparo/CadastroReparoEdit.xhtml").hasAnyRole("ADMINISTRADORES","GESTORES")
			.antMatchers("/paginas/Reparo/CadastroReparoEditVER*").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES","AUDITORES")
			.antMatchers("/paginas/Reparo/Consulta*").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES","AUDITORES")
			
			.antMatchers("/paginas/Seguradora/Cadastro*").hasAnyRole("ADMINISTRADORES","GESTORES")
			.antMatchers("/paginas/Seguradora/Consulta*").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES","AUDITORES")
			
			.antMatchers("/paginas/SubGrupo/Cadastro*").hasAnyRole("ADMINISTRADORES")
			.antMatchers("/paginas/SubGrupo/Consulta*").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES","AUDITORES")
			
			.antMatchers("/paginas/TermoResponsavel/Cadastro*").hasAnyRole("ADMINISTRADORES", "GESTORES")
			.antMatchers("/paginas/TermoResponsavel/Consulta*").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES","AUDITORES")
		
			.antMatchers("/paginas/TipoBem/Cadastro*").hasAnyRole("ADMINISTRADORES")
			.antMatchers("/paginas/TipoBem/Consulta*").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES","AUDITORES")
			
			.antMatchers("/paginas/TipoFornecedor/Cadastro*").hasAnyRole("ADMINISTRADORES")
			.antMatchers("/paginas/TipoFornecedor/Consulta*").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES","AUDITORES")
			
			.antMatchers("/paginas/TipoInstituicao/Cadastro*").hasAnyRole("ADMINISTRADORES")
			.antMatchers("/paginas/TipoInstituicao/Consulta*").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES","AUDITORES")
			
			.antMatchers("/paginas/TipoTransferencia/Cadastro*").hasAnyRole("ADMINISTRADORES")
			.antMatchers("/paginas/TipoTransferencia/Consulta*").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES","AUDITORES")
			
			.antMatchers("/paginas/Transferencia/CadastroTransferenciaTEB.xhtml").hasAnyRole("ADMINISTRADORES","GESTORES")
			.antMatchers("/paginas/Transferencia/CadastroTransferenciaTIB.xhtml").hasAnyRole("ADMINISTRADORES","GESTORES")
			.antMatchers("/paginas/Transferencia/Consulta*").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES","AUDITORES")
			

			.antMatchers("/paginas/UnidadeAdministrativa/Cadastro*").hasAnyRole("ADMINISTRADORES","GESTORES")
			.antMatchers("/paginas/UnidadeAdministrativa/Consulta*").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES","AUDITORES")
			
			.antMatchers("/paginas/UnidadeGestora/Cadastro*").hasAnyRole("ADMINISTRADORES")
			.antMatchers("/paginas/UnidadeGestora/Consulta*").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES","AUDITORES")
			
			.antMatchers("/paginas/Usuario/**").hasAnyRole("ADMINISTRADORES")
			
			.antMatchers("/paginas/Vistoria/Cadastro*").hasAnyRole("ADMINISTRADORES","GESTORES")
			.antMatchers("/paginas/Vistoria/Consulta*").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES","AUDITORES")
			
			.antMatchers("/paginas/Relatorios/**").hasAnyRole("ADMINISTRADORES","GESTORES")
			
			.antMatchers("/paginas/**").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES")
			
			.antMatchers("/**").hasAnyRole("ADMINISTRADORES", "AUXILIARES", "GESTORES")
			
			.and()
		
		.formLogin()
			.loginPage("/login.xhtml")
			.failureUrl("/login.xhtml?invalid=true")
			.and()
		
		.logout()
			.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
			.and()
		
		.exceptionHandling()
			.accessDeniedPage("/access.xhtml")
			.authenticationEntryPoint(jsfLoginEntry)
			.accessDeniedHandler(jsfDeniedEntry);
	}
	
}
